//
//  SplashScreenVC.h
//  AquaSmart
//
//  Created by Neotech on 31/03/17.
//  Copyright © 2017 Mac User. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MainPresenter.h"
#import "Device.h"
extern NSString *const IS_BACK_HIDDEN;
extern NSString *const IS_CLOSE_HIDDEN;
extern NSString *const BACK_SELECTOR;
extern NSString *const NOTIFY_SELECTOR;
extern NSString *const NOTIFY_SELECTOR_HIDDEN;
extern NSString *const BACK_TEXT;
extern NSString *const TITLE_TEXT;
extern NSString *const BACK_IMAGE;
static const NSString *ItemStatusContext;

@interface SplashScreenVC : UIViewController

@property (weak, nonatomic) IBOutlet UIImageView *splashImageView;
@property (weak, nonatomic) IBOutlet UIImageView *backImageView;
@property (weak, nonatomic) IBOutlet UIButton *notifyButton;
@property (weak, nonatomic) IBOutlet UIImageView *neotechLogo;
@property (weak, nonatomic) IBOutlet UIImageView *networkImageView;
@property (weak, nonatomic) IBOutlet UILabel *titlelabel;
@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (strong, nonatomic) MainPresenter *presenter;
@property(nonatomic)BOOL timeSyncFirst;
//@property (weak, nonatomic)NSTimer *timer;
@property(nonatomic)BOOL isSplashFirstLoad;
-(void)showAlertViewForWifi;
-(void)setNotificationIconStatus;
-(void)setWifiStatusIcon;
-(void)invalidateTimer;
-(void)showAlertForIpFail;
-(void)getAquaSmartMacID;
-(void)pushViewController:(UIViewController*)viewController;
@property (weak, nonatomic) IBOutlet UIButton *backButton;
-(void)setHeaderViewData:(NSDictionary*)dict;
-(void)updateHeadrLabel:(NSString*)screenName;
- (void)checkNetworkStatus:(NSNotification *)notice;
-(void)getFirmwareVersionCallApi;
-(void)showAlertView:(NSTimer *)myTimer;
+(instancetype)sharedInstance;

@property (strong,atomic) UIViewController *controller;
@end
