 //
//  SplashScreenVC.m
//  AquaSmart
//
//  Created by Neotech on 31/03/17.
//  Copyright © 2017 Mac User. All rights reserved.
//

#import "SplashScreenVC.h"
#import "AppDelegate.h"
//#import "MBProgressHUD.h"
#import "SVProgressHUD.h"
//#import "Communicator.h"
#import <SystemConfiguration/CaptiveNetwork.h>
#import "FirmwareUpgradeVC.h"
NSString *const IS_BACK_HIDDEN = @"IS_BACK_HIDDEN";
NSString *const IS_CLOSE_HIDDEN = @"IS_CLOSE_HIDDEN";
NSString *const BACK_SELECTOR = @"BACK_SELECTOR";
NSString *const CLOSE_SELECTOR = @"CLOSE_SELECTOR";
NSString *const BACK_TEXT = @"BACK_TEXT";
NSString *const TITLE_TEXT = @"TITLE_TEXT";
NSString *const BACK_IMAGE = @"BACK_IMAGE";
NSString *const NOTIFY_SELECTOR = @"NOTIFY_SELECTOR";
NSString *const NOTIFY_SELECTOR_HIDDEN = @"NOTIFY_SELECTOR_HIDDEN";

@interface SplashScreenVC ()<PacketReciveProtocol,MainPresenterDelegate>
{
    GCDAsyncSocket *asyncSocket;
         NSString *HexFileVersion;
    NSString *firmwareVersion;
    //int value = [string intValue];
    int firmwareVersionInt ;

    NSTimer *timer;
    NSString *hexFileVersionWithDot;
    NSString *savedValue;
    int timeinterval;
    NSArray *listItems;
    NSString *str;
    NSString *str1;
    NSString *finalString;
    NSArray *mutArray;
    NSString *finStr;
    NSString *upperMacID;
}
@end

@implementation SplashScreenVC
+(instancetype)sharedInstance {
    
    static SplashScreenVC *splash;
    static dispatch_once_t once;
    dispatch_once(&once, ^{
        splash = [[self alloc] init];
    });
    
    return splash;
}
- (void)viewDidLoad {
    
    
    self.presenter = [[MainPresenter alloc]initWithDelegate:self];
    
    //Add observers to monitor specific values on presenter. On change of those values MainVC UI will be updated
    [self addObserversForKVO];
    
    
    [self setAutomaticallyAdjustsScrollViewInsets:YES];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.view.frame = CGRectZero;
    self.view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
    [[WMSSettingsController sharedInstance]SplashFirstTime:YES];
    [[WMSSettingsController sharedInstance]SyncFirstTime:YES];
     [[WMSSettingsController sharedInstance]WaterLavelFirstTime:YES];
    [[WMSSettingsController sharedInstance]GetTankSettings:YES];
    [super viewDidLoad];
    
    
    hexFileVersionWithDot = @"11.00.24";
    
    self.headerView.hidden = YES;
    [AppDelegate appDelegate].superVC = self;
    
        [self.navigationController setNavigationBarHidden:YES];
    
    
    
    // Do any additional setup after loading the view.
}


#pragma mark - KVO Observers
-(void)addObserversForKVO {
    
    [self.presenter addObserver:self forKeyPath:@"connectedDevices" options:NSKeyValueObservingOptionNew context:nil];
    [self.presenter addObserver:self forKeyPath:@"progressValue" options:NSKeyValueObservingOptionNew context:nil];
    [self.presenter addObserver:self forKeyPath:@"isScanRunning" options:NSKeyValueObservingOptionNew context:nil];
}

-(void)removeObserversForKVO {
    
    [self.presenter removeObserver:self forKeyPath:@"connectedDevices"];
    [self.presenter removeObserver:self forKeyPath:@"progressValue"];
    [self.presenter removeObserver:self forKeyPath:@"isScanRunning"];
}

-(void)viewDidAppear:(BOOL)animated{
   [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onNetWorkChange:) name:NET_WORK_CHANGE object:nil];
 //   [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onWifiChange:) name:WIFI_CHANGED object:nil];
    
//    if([[AppDelegate appDelegate] netStatus]==ReachableViaWiFi){
//        self.networkImageView.image = [UIImage imageNamed:@"BLUE_WIFI_CONNECTED"];
//    }else{
//        self.networkImageView.image = [UIImage imageNamed:@"BLUE_WIFI_DISCONNECTED"];
//    }
}

- (void)onNetWorkChange:(NSNotification *)notification{
    
    CFArrayRef myArray = CNCopySupportedInterfaces();
    CFDictionaryRef myDict = CNCopyCurrentNetworkInfo(CFArrayGetValueAtIndex(myArray, 0));
    NSLog(@"Connected at:%@",myDict);
    NSDictionary *myDictionary = (__bridge_transfer NSDictionary*)myDict;
    NSString * BSSID = [myDictionary objectForKey:@"BSSID"];
    NSString *ssid = [myDictionary objectForKey:@"SSID"];
    NSLog(@"bssid is %@",BSSID);
    NSLog(@"ssid is %@",ssid);
    NSDictionary *parameter = [notification userInfo];
    int status = [[parameter valueForKey:@"status"] intValue];
            if(status==ReachableViaWiFi && [ssid isEqualToString:@"AquaSmart-005"]){
                
               [AppDelegate appDelegate].isWifiConnected = YES;
                bool vIn = [AppDelegate appDelegate].isWifiConnected;
                [AppDelegate appDelegate].iswificonnectedint = vIn?1:0;
                [[TcpPacketClass sharedInstance]start];

                if ([[WMSSettingsController sharedInstance]isSplashFirstTime]==YES) {
                    
                //[[SplashScreenVC sharedInstance]getFirmwareVersionCallApi];
                    [[SplashScreenVC sharedInstance]setWifiStatusIcon];
                    [[WMSSettingsController sharedInstance]SplashFirstTime:NO];
                }
                
                
                else
                {
                [[SplashScreenVC sharedInstance]setWifiStatusIcon];
                }
        self.networkImageView.image = [UIImage imageNamed:@"BLUE_WIFI_CONNECTED"];
        
    }else{
        [timer invalidate];
        [SVProgressHUD dismiss];
        self.networkImageView.image = [UIImage imageNamed:@"BLUE_WIFI_DISCONNECTED"];
        [self.view makeToast:NSLocalizedString(@"Please check your wifi connection and Restart your app.", nil)];
    }
    
}
    
//-(void)onSocketCreate:(NSNotification *)notification
//{
//    
//    
//    [self getFirmwareVersionCallApi];
//    [[NSNotificationCenter defaultCenter] removeObserver:self name:SOCKET_CONNECTED object:nil];
//}
//    NSString *getFlag = [[NSUserDefaults standardUserDefaults]
//                         stringForKey:@"oneTime"];
//   
//    if ([getFlag isEqualToString:@"0"]) {
//       
//       // [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"oneTime"];
//    }
// else
// {
// [self getFirmwareVersionCallApi];
// }




//
//
-(void)connectionStatus:(NSNotification*)notification {
    //NSString *imageName;
    switch ([AppDelegate appDelegate].iswificonnectedint) {
            
        case 0:
            _networkImageView.image = [UIImage imageNamed:@"BLUE_WIFI_DISCONNECTED"];
            break;
            
        case 1:
            _networkImageView.image = [UIImage imageNamed:@"BLUE_WIFI_CONNECTED"];
            break;
            
        default:
            _networkImageView.image = [UIImage imageNamed:@"BLUE_WIFI_DISCONNECTED"];
            
            //  [self showAlertView];
            break;
    }

   // self.networkImageView.image = [UIImage imageNamed:imageName];
}

-(void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];

    [[AppDelegate appDelegate] setRootController];
    
}


-(void)showAlertViewForWifi{
    UIAlertController * alert=   [UIAlertController alertControllerWithTitle:@"Connect to Wifi" message:@""
                                                              preferredStyle:UIAlertControllerStyleAlert];
    [alert addTextFieldWithConfigurationHandler:^(UITextField *textField){
        textField.placeholder = @"Aquasmart wifi name";
    }];
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"Done" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){
        UITextField *textField = alert.textFields[0];
        [[WMSSettingsController sharedInstance]setWifiSsId:textField.text];
        NSLog(@"text was %@", textField.text);
        //
        CFArrayRef myArray = CNCopySupportedInterfaces();
        CFDictionaryRef myDict = CNCopyCurrentNetworkInfo(CFArrayGetValueAtIndex(myArray, 0));
        NSLog(@"Connected at:%@",myDict);
        NSDictionary *myDictionary = (__bridge_transfer NSDictionary*)myDict;
        NSString * BSSID = [myDictionary objectForKey:@"BSSID"];
        NSString *ssid = [myDictionary objectForKey:@"SSID"];
        NSLog(@"bssid is %@",BSSID);
        NSLog(@"ssid is %@",ssid);
        
        if (![ssid isEqualToString:[[WMSSettingsController sharedInstance]getWifiSsId]]) {
            
            [self showToast];

           [alert dismissViewControllerAnimated:YES completion:nil];
            
        }
        else
        {
            
            [[AppDelegate appDelegate]setRootController];
            [alert dismissViewControllerAnimated:YES completion:nil];
        }
        
        
        
    }];
    UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * action){
        [alert dismissViewControllerAnimated:YES completion:nil];
    }];
    [alert addAction:ok];
    [alert addAction:cancel];
    [[AppDelegate appDelegate].superVC presentViewController:alert animated:YES completion:nil];
}

-(void)showAlertForIpFail
{

    UIAlertView *alertView = [[UIAlertView alloc]
                              initWithTitle:nil
                              message:@"Router not in range or wrong network name provided or may be data is cleared.If wrong credentials entered please reset your device by holding the Pump-Switch for 10 seconds."
                              delegate:self
                              cancelButtonTitle:nil
                              otherButtonTitles:NSLocalizedString(@"OK", nil), nil];
    
    [alertView show];

}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 0) {
        [self performSelector:@selector(exitFromApp) withObject:nil afterDelay:5.0];
        
    }
    if (buttonIndex == 1) {
        
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

-(void)exitFromApp{
    [SVProgressHUD dismiss];
    exit(0);
}
-(void)showToast
{
   
    //[[AppDelegate appDelegate].superVC
    
[[AppDelegate appDelegate].superVC.view makeToast:NSLocalizedString(@"Please connect to AquaSmart or your router.", nil)];
}


-(void)viewWillDisappear:(BOOL)animated {
   
         [super viewWillDisappear:animated];
   
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)didRecieveError:(NSError *)error {
    [SVProgressHUD dismiss];
    [self.view makeToast:NSLocalizedString(@"Socket not connected", nil)];
    //[[TcpPacketClass sharedInstance]setup];
}


-(void)pushViewController:(UIViewController*)viewController {
    self.splashImageView.hidden = YES;
    self.neotechLogo.hidden = YES;
    self.headerView.hidden = NO;
    self.controller = viewController;
    NSArray *arr = self.childViewControllers;
    if (arr.count > 0) {
        UINavigationController *vc = [self.childViewControllers lastObject];
        for(UIViewController *tempVC in vc.viewControllers)
        {
            if([tempVC isKindOfClass:[UIViewController class]])
            {
                [tempVC removeFromParentViewController];
            }
        }
        [vc.view removeFromSuperview];
        [vc removeFromParentViewController];
    }
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:viewController];
    nav.navigationBar.hidden = ([viewController isKindOfClass:[MVYSideMenuController class]])?YES:NO;
    [self addChildViewController:nav];
    [self.view insertSubview:nav.view atIndex:0];
    [nav didMoveToParentViewController:self];
}
-(void)setHeaderViewData:(NSDictionary*)dict {
    if (dict) {
        
        
       if (![[dict valueForKey:@"IS_BACK_HIDDEN"] isEqualToString:@"Yes"]) {
            
            [self.backButton addTarget:self.controller action:NSSelectorFromString([dict valueForKey:@"BACK_SELECTOR"]) forControlEvents:UIControlEventTouchUpInside];
            self.backImageView.image = [UIImage imageNamed:[dict valueForKey:BACK_IMAGE]];
            self.backImageView.hidden = NO;
            self.backButton.hidden = NO;
        }
        else
        {
            self.backImageView.hidden = YES;
            self.backButton.hidden = YES;
            //self.notifyButton.hidden=YES;
        }
        
        if ([[dict valueForKey:@"NOTIFY_SELECTOR"] isEqualToString:@"Yes"]) {
            
            [self.notifyButton addTarget:self.controller action:NSSelectorFromString([dict valueForKey:@"NOTIFY_SELECTOR_HIDDEN"]) forControlEvents:UIControlEventTouchUpInside];
            
            self.notifyButton.hidden = NO;
           // [self setNotificationIconStatus];
            if ([[WMSSettingsController sharedInstance]getNotificationStatus]==YES) {
                [self.notifyButton setImage:[UIImage imageNamed:@"blue_warning_amber.png"] forState:UIControlStateNormal];
                
            }
            
            else
            {
            [self.notifyButton setImage:[UIImage imageNamed:@"unselected_alert.png"] forState:UIControlStateNormal];
            
            }
        } else {
            
            //self.backImageView.hidden = YES;
            self.notifyButton.hidden = YES;
        }
       
        
        self.titlelabel.text = [dict valueForKey:TITLE_TEXT];
        switch ([AppDelegate appDelegate].iswificonnectedint) {
                
            case 0:
                _networkImageView.image = [UIImage imageNamed:@"BLUE_WIFI_DISCONNECTED"];
                break;
                
            case 1:
                _networkImageView.image = [UIImage imageNamed:@"BLUE_WIFI_CONNECTED"];
                break;
                
            default:
                _networkImageView.image = [UIImage imageNamed:@"BLUE_WIFI_DISCONNECTED"];
                
                //  [self showAlertView];
                break;
        }
        
        [self.networkImageView setTranslatesAutoresizingMaskIntoConstraints:NO];
    }
}

-(void)updateHeadrLabel:(NSString*)screenName{
    self.titlelabel.text = screenName;
}


-(void)setNotificationIconStatus
{

    if ([[WMSSettingsController sharedInstance]getNotificationStatus]==YES) {
        [self.notifyButton setImage:[UIImage imageNamed:@"blue_warning_amber.png"] forState:UIControlStateNormal];
        
    }
    
    else
    {
        [self.notifyButton setImage:[UIImage imageNamed:@"unselected_alert.png"] forState:UIControlStateNormal];
        
    }

}
-(void)setWifiStatusIcon
{

    switch ([AppDelegate appDelegate].iswificonnectedint) {
            
        case 0:
            _networkImageView.image = [UIImage imageNamed:@"BLUE_WIFI_DISCONNECTED"];
            break;
            
        case 1:
            _networkImageView.image = [UIImage imageNamed:@"BLUE_WIFI_CONNECTED"];
            break;
            
        default:
            _networkImageView.image = [UIImage imageNamed:@"BLUE_WIFI_DISCONNECTED"];
            
            //  [self showAlertView];
            break;
    }
    
    [self.networkImageView setTranslatesAutoresizingMaskIntoConstraints:NO];

}
#pragma  mark - Get frimaware version
-(void)getFirmwareVersionCallApi
{
    [TcpPacketClass sharedInstance].delegate = self;
    OrderedDictionary *requestDict = [[OrderedDictionary alloc]initWithCapacity:3];
    [requestDict insertObject:_SIDVALUE forKey:_SID atIndex:0];
    //[requestDict insertObject:_BADVALUE forKey:_BAD atIndex:1];
    [requestDict insertObject:_SADVALUE forKey:_SAD atIndex:1];
    [requestDict insertObject:_8004 forKey:_CMD atIndex:2];
    NSError *writeError;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:requestDict options:kNilOptions error:&writeError];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
  // [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
   // [SVProgressHUD showWithStatus:@"Please Waitttfrimaware.."];
    
    //dispatch_async(dispatch_get_main_queue(), ^{
       [SVProgressHUD showWithStatus:@"Please Wait"];
   // });
    
    // [SVProgressHUD setOffsetFromCenter:UIOffsetMake(0, 120)];
    
    
   [[TcpPacketClass sharedInstance]writeOut:jsonString second:[requestDict objectForKey:_CMD]];
    
    timer = [NSTimer scheduledTimerWithTimeInterval:5.0 target:self selector:@selector(sendFirmwareAgain:) userInfo:nil repeats:NO];
    
}


- (void)sendFirmwareAgain:(NSTimer *)theTimer {
    
        int static ticks = 0;
        if (ticks<4) {
            [self getFirmwareVersionCallApi];
        }
        else if (ticks>3)
        {
            [theTimer invalidate];
            [SVProgressHUD dismiss];
            [self firmwareHitMoreThanFour];
           // [self.view makeToast:NSLocalizedString(@"Something went wrong.Please try again.", nil)];
            ticks = -1;
        }
        ticks++;
    

    
    
}

-(void)firmwareHitMoreThanFour
{

    UIAlertController * alert=   [UIAlertController alertControllerWithTitle:@"Nothing Responce From IOT,Restart App" message:@""
                                                              preferredStyle:UIAlertControllerStyleAlert];
    
    
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction * action){
        [alert dismissViewControllerAnimated:YES completion:nil];
    }];
    [alert addAction:ok];
    
    [[AppDelegate appDelegate].superVC presentViewController:alert animated:YES completion:nil];
}


-(void)didRecieveFirmwareVersionResponse:(NSDictionary *)jsonDict
{
    [timer invalidate];
    //[SVProgressHUD dismiss];
    NSLog(@"Response Recieved");
    HexFileVersion = @"110024";
    firmwareVersion = [jsonDict valueForKey:_DATA];
    [[WMSSettingsController sharedInstance]setCurrentFirmwareVersion:[jsonDict valueForKey:_DATA]];
    NSRange range = NSMakeRange(2,1);
    NSRange range1 = NSMakeRange(4,1);
    NSString *newText = [firmwareVersion stringByReplacingCharactersInRange:range withString:@""];
    NSString *FinalVersionString = [newText stringByReplacingCharactersInRange:range1 withString:@""];
    NSLog(@"%@",FinalVersionString);
    int FinalVersionInt = [FinalVersionString intValue];
    int HexFileVersionInt = [HexFileVersion intValue];
    

    
    if (FinalVersionInt<HexFileVersionInt) {
        
      
        dispatch_async(dispatch_get_main_queue(), ^{
            [SVProgressHUD dismiss];

        });
        
        
        
        
        
        
        UIStoryboard *mainStroyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        
        UIViewController *registrationViewController;
        registrationViewController = [mainStroyBoard instantiateViewControllerWithIdentifier:@"FirmwareUpgradeVC"];
        [[AppDelegate appDelegate].superVC pushViewController:registrationViewController];
        
    }

    else if (FinalVersionInt==HexFileVersionInt) {
        [self checkForRegistrationType];
       
    }
    
     else if(FinalVersionInt>HexFileVersionInt)
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [SVProgressHUD dismiss];
            
        });
        
        [self showAlertViewForAppUpdate ] ;
        
    }
    
}
#pragma mark - Visit Appstore Method-
-(void)gotoAppStore
{
    NSString *simple = @"itms-apps://itunes.apple.com";
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:simple]];
}


-(void)showAlertViewForAppUpdate{
    UIAlertController * alert=   [UIAlertController alertControllerWithTitle:@"Update Your App From App Store " message:@""
                                                              preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){
        
        
        [self gotoAppStore];
        
        
    }];
    UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * action){
        [self exitFromApp];
    }];
    [alert addAction:ok];
    [alert addAction:cancel];
    [[AppDelegate appDelegate].superVC presentViewController:alert animated:YES completion:nil];
}

#pragma mark - Check for Registration Type -
-(void)checkForRegistrationType {
    //creating  authentication packet to send to server
    OrderedDictionary *orderDictionary = [[OrderedDictionary alloc] initWithCapacity:5];
    [orderDictionary insertObject:_SIDVALUE forKey:_SID atIndex:0];
    [orderDictionary insertObject:_SADVALUE forKey:_SAD atIndex:1];
    [orderDictionary insertObject:_8208 forKey:_CMD atIndex:2];
    [orderDictionary insertObject:[[WMSSettingsController sharedInstance] getUUID] forKey:_DATA atIndex:3];
    NSError *writeError;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:orderDictionary options:kNilOptions error:&writeError];
    NSString *jsonStr = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
   // [SVProgressHUD showWithStatus:@"Loading..."];
    timer = [NSTimer scheduledTimerWithTimeInterval:5.0 target:self selector:@selector(sendAuthPacket:) userInfo:nil repeats: NO];
    //[TcpPacketClass sharedInstance].delegate = self;
    [[TcpPacketClass sharedInstance]writeOut:jsonStr second:[orderDictionary objectForKey:_CMD]];
    
    
}

-(void)sendAuthPacket:(NSTimer *)theTimer
{

    [theTimer invalidate];
    [self checkForRegistrationType];
   // [SVProgressHUD dismiss];
}
-(void)didRecieveAuthenticationResponse:(NSDictionary *)jsonDict {
    [timer invalidate];
    //[self invalidateTimer];
   // [MBProgressHUD hideHUDForView:self.view animated:YES];
    //[SVProgressHUD resetOffsetFromCenter];
    [SVProgressHUD dismiss];
    UIStoryboard *mainStroyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    if(jsonDict != nil) {
        NSDictionary *dataDict = [jsonDict valueForKey:_DATA];
        
        if([[jsonDict valueForKey:@"data"] isKindOfClass:[NSDictionary class]]) {
            
            [[WMSSettingsController sharedInstance] setDeviceAuthenticationStatus:[dataDict valueForKey:_01]];
          
            [[AppDelegate appDelegate]pushSidePanelController];
            
            
            
        }
        else if([[jsonDict valueForKey:@"data"] isKindOfClass:[NSString class]])
        {
            [[WMSSettingsController sharedInstance] setDeviceAuthenticationStatus:[jsonDict valueForKey:@"data"]];
            // BuyerRegistrationVCOne  *prev = [[BuyerRegistrationVCOne alloc]init];
            if ([[jsonDict valueForKey:@"data"] isEqualToString:@"1"] ||[[jsonDict valueForKey:@"data"] isEqualToString:@"3"]) {
                
                UIViewController *registrationViewController;
                registrationViewController = [mainStroyBoard instantiateViewControllerWithIdentifier:@"BuyerRegistrationVCOne"];
                
               // [[WMSSettingsController sharedInstance]isFirstTimeRegistration:NO];
                [[AppDelegate appDelegate].superVC pushViewController:registrationViewController];
                
                
            }
            else if ([[jsonDict valueForKey:@"data"] isEqualToString:@"2"])
            {
                
                UIViewController *registrationViewController;
                registrationViewController = [mainStroyBoard instantiateViewControllerWithIdentifier:@"BuyerRegistrationVCOne"];
                //[[WMSSettingsController sharedInstance]isFirstTimeRegistration:YES];
                // [[AppDelegate appDelegate].navigationController pushViewController:registrationViewController animated:NO];
                [[AppDelegate appDelegate].superVC pushViewController:registrationViewController];
                
            }
            
//            else if ([[jsonDict valueForKey:@"data"] isEqualToString:@"3"])
//            {
//                //NSString *previousScreenValue = @"3";
//                //  BuyerRegistrationVCOne *prev = [[BuyerRegistrationVCOne alloc]init];
//                // prev = [mainStroyBoard instantiateViewControllerWithIdentifier:@"BuyerRegistrationVCOne"];
//                UIViewController *registrationViewController;
//                registrationViewController = [mainStroyBoard instantiateViewControllerWithIdentifier:@"BuyerRegistrationVCOne"];
//                
//                //[[WMSSettingsController sharedInstance]isFirstTimeRegistration:NO];
//                // [[AppDelegate appDelegate].navigationController pushViewController:registrationViewController animated:NO];
//                [[AppDelegate appDelegate].superVC pushViewController:registrationViewController];
//                
//            }
            
           
            
        }
    }
    
}






//-(void)startTimer{
//       _timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(onTick) userInfo:nil repeats:YES];
//    // ticks = 5;
//    }

//
//    -(void)invalidateTimer{
//        
//            [timer invalidate];
//            timer = nil;
//    
//    }
-(void)getAquaSmartMacID
{
    
    OrderedDictionary *requestDict = [[OrderedDictionary alloc]initWithCapacity:5];
    [requestDict insertObject:_SIDVALUE forKey:_SID atIndex:0];
    [requestDict insertObject:_SADVALUE forKey:_SAD atIndex:1];
    [requestDict insertObject:_8275 forKey:_CMD atIndex:2];
    NSError *writeError;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:requestDict options:kNilOptions error:&writeError];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
   //[SVProgressHUD setOffsetFromCenter:UIOffsetMake(100, 120)];
    
   // [SVProgressHUD setOffsetFromCenter:UIOffsetMake(self.view.frame.origin.x, 20)];

    
    //[SVProgressHUD showWithStatus:@"Please Wait"];
   // [SVProgressHUD setOffsetFromCenter:UIOffsetMake(0, 120)];
   
    
    //dispatch_async(dispatch_get_main_queue(), ^{
      // [SVProgressHUD showWithStatus:@"Please WaitStart............"];
        //[SVProgressHUD setOffsetFromCenter:UIOffsetMake(0, 120)];
    //});
    
    timer = [NSTimer scheduledTimerWithTimeInterval: 5.0 target:self selector:@selector(sendMacAgain:) userInfo:nil repeats: NO];
    [TcpPacketClass sharedInstance].delegate = self;
   [[TcpPacketClass sharedInstance]writeOut:jsonString second:[requestDict objectForKey:_CMD]];
    
}



-(void)sendMacAgain:(NSTimer *)theTimer
{
    
    int static ticks = 0;
    if (ticks<4) {
        [self getAquaSmartMacID];
    }
    else if (ticks>3)
    {
        [theTimer invalidate];
        [SVProgressHUD dismiss];
        [self.view makeToast:NSLocalizedString(@"Something went wrong.Please try again.", nil)];
        ticks = -1;
    }
    ticks++;
    

}
-(void)didRecieveWirelessMacidResponse:(NSDictionary *)jsonDict
{
    
    [timer invalidate];
   // [SVProgressHUD dismiss];
    NSString *data = [jsonDict valueForKey:_DATA];
    mutArray=[NSArray alloc];
    finStr = @"";
    
    mutArray=[data componentsSeparatedByString:@"-"];
    for (int i=0; i<=mutArray.count-1; i++) {
        NSString *hexStr;
        str1=[mutArray objectAtIndex:i];
        hexStr=[NSString stringWithFormat:@"%X",[str1 intValue]];
        if (hexStr.length<2) {
            hexStr=[@"0" stringByAppendingString:hexStr];
        }
        
        
        if(i<mutArray.count-1)
        {
            finalString=[hexStr stringByAppendingString:@":"];
        }
        
        else
        {
            finalString= hexStr;
            
            
            
        }
        
        finStr = [finStr stringByAppendingString:finalString];
        upperMacID = [finStr uppercaseString];
    }
    NSLog(@"%@",upperMacID);
   [[WMSSettingsController sharedInstance]setMacId:finStr];
    [[SplashScreenVC sharedInstance]getFirmwareVersionCallApi];
}






    @end

