//
//  TcpPacketClass.h
//  AquaSmart
//
//  Created by Neotech on 27/03/17.
//  Copyright © 2017 Mac User. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GCDAsyncSocket.h"
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/signal.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#import "WMSBlackBoxCommands.h"
#import "DatabaseModel.h"
#import "AppLoading.h"
#import "AppDelegate.h"
#import "WMSStorageManager.h"
@protocol PacketReciveProtocol <NSObject>
@optional
-(void)packetReceived;
-(void)didRecieveError:(NSError *)error;
-(void)packetReceived:(NSString *)ipAddress;
-(void)packetFailure;
-(void)didRecieveFirmwareUpgradeResponse:(NSDictionary *)jsonDict;
-(void)didRecieveFirmwareVersionResponse:(NSDictionary *)jsonDict;
-(void)didRecieveTankSettingResponse:(NSDictionary *)jsonDict;
-(void)didRecieveGetTankSettingResponse:(NSDictionary *)jsonDict;
-(void)didRecieveAuthenticationResponse:(NSDictionary *)jsonDict;
-(void)didRecieveLoginorRegistrationResponse:(NSDictionary *)jsonDict;
-(void)didRecieveNotificationResponse:(NSDictionary *)jsonDict;
-(void)didRegisteredUserResponse:(NSDictionary *)jsonDict;
-(void)didRecieveDeviceStatusResponse:(NSDictionary *)jsonDict;
-(void)didRecieveResetPasswordResponse:(NSDictionary *)jsonDict;
-(void)didRecieveLicenceUpdationResponse:(NSDictionary *)jsonDict;
-(void)didRecieveRemoveRegistrationKeyResponse:(NSDictionary *)jsonDict;
-(void)didRecieveDeviceNetworkResponse:(NSDictionary *)jsonDict;
-(void)getDeviceNetworkSettingsResponse:(NSDictionary *)jsonDict;
-(void)getDeviceDateandTimeResponse:(NSDictionary *)jsonDict;
-(void)getLutronLightResponse:(NSDictionary *)jsonDict;
-(void)setLutronLightResponse:(NSDictionary *)jsonDict;
-(void)getSytemHealthResponse:(NSDictionary *)jsonDict;
-(void)getDiagnosticResponse:(NSDictionary *)jsonDict;
-(void)getCurrentFirmwareVersionResponse:(NSDictionary *)jsonDict;
-(void)syncDateandTimeResponse:(NSDictionary *)jsonDict;
-(void)getLghvacResponse:(NSDictionary *)jsonDict;
-(void)getAdminLoginResponse:(NSDictionary *)jsonDict;
-(void)getRestoreFactorySettingsResponse:(NSDictionary *)jsonDict;
-(void)getProfileInfoResponse:(NSDictionary *)jsonDict;
-(void)getProfileImmediateSaveResponse:(NSDictionary *)jsonDict;
-(void)getProvisioningDumpResponse:(NSDictionary *)jsonDict;
-(void)getScheduleCreateResponse:(NSDictionary *)jsonDict;
-(void)getDeviceAuthenticationResponse:(NSDictionary *)jsonDict;
-(void)getNetworkSettingsResponse:(NSDictionary *)jsonDict;   //network settings response
-(void)getTankScheduleResponse:(NSArray *)jsonArray;
-(void)getCurrentWaterLevelStatus:(NSDictionary *)jsonDict;
-(void)getCurrentPumpStatus:(NSDictionary *)jsonDict;   //get pump status
-(void)getChangePasswordResponse:(NSDictionary *)jsonDict;
-(void)getScheduleEnableResponse:(NSDictionary *)jsonDict;
-(void)getScheduleDisableResponse:(NSDictionary *)jsonDict;
-(void)getScheduleDeleteResponse:(NSDictionary *)jsonDict;
-(void)callForgatePasswordApi:(NSDictionary *)jsonDict;
-(void)didRecieveMacIdResponse:(NSDictionary *)jsonDict;
-(void)didrecieveForgotPasswordResponse:(NSDictionary *)jsonDict;  // forgot passowrd
-(void)didRecievePumpSuccessResponse:(NSDictionary *)jsonDict;
-(void)didRecievePumpFailureResponse:(NSDictionary *)jsonDict;
-(void)didrecieveEnableScheduleError;   // error 19
-(void)didrecieveDisableScheduleError;   //error 20
-(void)didrecieveDeleteScheduleError;     //error 21
-(void)didrecieveNoScheduleFoundError;    //error 18
-(void)didRecieveBytesFromIOT;
-(void)didRecieveWirelessMacidResponse:(NSDictionary *)jsonDict;
-(void)getSingleTankScheduleResponse:(NSDictionary *)jsonDict;
-(void)getFirmwareUpgradeSuccessResponse;
-(void)didRecieveSuctionSensorResponse:(NSDictionary *)jsonDict;
-(void)getPumpSettingResponse:(NSDictionary *)jsonDict;
-(void)didRecieveSuctionSensorSettingResponse:(NSDictionary *)jsonDict;
-(void)didRecieveGetPumpSettingResponse:(NSDictionary *)jsonDict;
//to get suction sensor response
@end

@interface TcpPacketClass : NSObject<GCDAsyncSocketDelegate>
@property (nonatomic,weak) id<PacketReciveProtocol> delegate;
@property(nonatomic,strong)NSString *response;
@property(nonatomic,strong)NSMutableArray *delegatesArray;
@property(nonatomic,strong)NSMutableArray *connectedSockets;
@property(nonatomic,strong)NSError *socketError;
@property(nonatomic,strong)NSMutableDictionary *packDict;
@property(nonatomic,strong)NSUserDefaults *defaults;
@property(nonatomic,strong)DatabaseModel *datamodel;
+(instancetype)sharedInstance;
-(void)start;
-(void)writeOut:(NSString *)s second:(NSString *)cmd;
-(NSString *)appendJsonString:(NSString *)jsonString;
-(NSInteger)isTCPReceived;

@property (assign) BOOL isRunning;
-(void)reTryToConnectSocket;
- (BOOL)isSocketConnected;
@end
