
//
//  TcpPacketClass.m
//  AquaSmart
//
//  Created by Neotech on 27/03/17.
//  Copyright © 2017 Mac User. All rights reserved.
//

#import "TcpPacketClass.h"
#define READ_TIMEOUT 15.0
#define READ_TIMEOUT_EXTENSION 10.0
@implementation TcpPacketClass
{
    
    NSString *    parseString;
    NSString *tempString;
    NSString *responseString;
    NSInteger flag;
    GCDAsyncSocket *asyncSocket;
    NSTimer *timer;
    NSString *zeroIndex;
    int ticks;
    DatabaseModel *datamodel;
    NSString *temString;
    NSString *final1String;
}

+(instancetype)sharedInstance {
    
    
    static TcpPacketClass *packetReceiver;
    static dispatch_once_t once;
    dispatch_once(&once, ^{
        packetReceiver = [[self alloc] init];
       
    });
    
    return packetReceiver;
}

-(id)init {
    NSLog(@"Initializing TCp Connection..");
     datamodel = [[DatabaseModel alloc]init];
    [self start];
    
    return  self;
}

-(void)start
{
    
    NSString *hostname;
    asyncSocket = [[GCDAsyncSocket alloc] initWithDelegate:self delegateQueue:dispatch_get_main_queue()];
    NSError *error = nil;
    if([[[WMSSettingsController sharedInstance]getWifiSsId]containsString:@"AquaSmart-"])
    {
        hostname = @"192.168.11.254";

    }
//   else if([[[WMSSettingsController sharedInstance]getWifiSsId]containsString:@"AquaSmart-"]|| [[WMSSettingsController sharedInstance]getAPModeSsId])
//   {
//   
//   
//   }
    
    else
    
    {
    
        hostname  = [[WMSSettingsController sharedInstance]getIPAddress];
    }
    uint16_t port = 8080;
    
    
    
    if (![asyncSocket connectToHost:hostname onPort:port error:&error])
    {
        NSLog(@"Unable to connect to due to invalid configuration: %@", error);
    }
    else
    {
        NSLog(@"Connecting...");
    }
    
    
    
    
    
}


-(void)writeOut:(NSString *)s second:(NSString *)cmd
{
    if (s == nil) {
        return;
    }
    temString =s;
    NSLog(@"Sending this packet%@",s);
    NSData *myData = [s dataUsingEncoding:NSUTF8StringEncoding];
    // _packDict = [[NSMutableDictionary alloc]init];
    // [_packDict setValue:s forKey:cmd];
    //    NSNumber *value =[NSNumber numberWithBool:[asyncSocket isConnected]];
    //    NSInteger newRow = [value integerValue];
    if ([asyncSocket isConnected] == YES) {
        [asyncSocket writeData:myData withTimeout:-1 tag:0];
        
    }
    else
    {
        [self reTryToConnectSocket];
    }
}

-(void)socket:(GCDAsyncSocket *)sock didAcceptNewSocket:(GCDAsyncSocket *)newSocket
{
    NSLog(@"new socket accepted");
    asyncSocket = newSocket;
    
    //  [newSocket readDataWithTimeout:-1 tag:0];
    
}

- (void)socket:(GCDAsyncSocket *)sock didConnectToHost:(NSString *)host port:(UInt16)port
{
    // ticks = 5;
    if (tempString!=nil) {
        
        NSLog(@"Sending packet again %@",tempString);
        [self writeOut:tempString second:_CMD];
    }
    NSMutableDictionary *parameter = [NSMutableDictionary dictionaryWithCapacity:0];
    [parameter setObject:[NSNumber numberWithInt:0] forKey:@"status"];
    NSLog(@"socket:didConnectToHost:%@ port:%hu", host, port);
    
    
    [asyncSocket readDataWithTimeout:-1 tag:0];
}

- (void)socketDidDisconnect:(GCDAsyncSocket *)sock withError:(NSError *)err
{
    
    if (err)
        
    {
        NSLog(@"socketDidDisconnect:withError: \"%@\"", err);
        [self reTryToConnectSocket];
    }
}
//- (NSTimeInterval)socket:(GCDAsyncSocket *)sock shouldTimeoutReadWithTag:(long)tag
//                 elapsed:(NSTimeInterval)elapsed
//               bytesDone:(NSUInteger)length
//{
//    if (elapsed <= READ_TIMEOUT)
//    {
//        return READ_TIMEOUT_EXTENSION;
//    }
//
//    return 0.0;
//}

-(void)closeSocket{
    [asyncSocket disconnect];
    asyncSocket = nil;
}

-(void)reTryToConnectSocket{
    if (![self isSocketConnected]) {
        [self closeSocket];
        [self start];
    }
}

-(BOOL)isSocketConnected{
    return [asyncSocket isConnected];
}


-(NSInteger)isTCPReceived {
    
    return flag;
}


- (void)socket:(GCDAsyncSocket *)sock didWriteDataWithTag:(long)tag
{
    NSLog(@"write done successfuly");
}

- (void)socket:(GCDAsyncSocket *)sock didReadData:(NSData *)data withTag:(long)tag
{
    // sleep(2);
    
    NSLog(@"socket:didReadData:withTag:");
    NSDictionary *json;
    NSError *jsonError;
    _response   = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    NSLog(@"response is  %@",_response);
    NSLog(@"%lu",(unsigned long)_response.length);
    
    json = [NSJSONSerialization JSONObjectWithData:data
                                           options:NSJSONReadingMutableContainers
                                             error:&jsonError];
    NSLog(@"json is:%@",json);
    //  NSString *responseStrr;
    //NSMutableString * responseString ;
    
    
   if (_response.length == 6 && (![_response containsString:@"{"] || ![_response containsString:@"}"]))
   // if (_response.length==6)
    {
        NSLog(@"response is appended:%lu",(unsigned long)_response.length);
        [self readInFirmware:_response];
        
    }
    
    else if (_response.length == 8 && (![_response containsString:@"{"] || ![_response containsString:@"}"]) && ([_response containsString:@"~"] && [_response containsString:@"^"]))
    {
        [self.delegate getFirmwareUpgradeSuccessResponse];
        
    }
    
    else if ((_response.length >= 360 )&& [_response hasPrefix:@"{{"])
    {
        
        NSString *responseStr = [ self fullJson:_response];
        
        NSMutableArray *responseArray = [[NSMutableArray alloc]init];
        
        [responseArray addObject:responseStr];
        if (responseArray.count > 0) {
            for (int i = 0; i < responseArray.count; i++) {
                NSData *objectData = [[responseArray objectAtIndex:i] dataUsingEncoding:NSUTF8StringEncoding];
                NSArray *json = [NSJSONSerialization JSONObjectWithData:objectData
                                                                options:NSJSONReadingMutableContainers
                                                                  error:&jsonError];
                [self multiJson:json];
            }
        }
        
        
        
    }
    
    
    
    
    
    
    
    
   
    
    else
    {
        //containsString
        
       
       /* if (([_response localizedStandardContainsString:@"{\"sid\":\"9000\""] && ![_response localizedStandardContainsString:@",{\"sid\":\"9000\""]))
        {
            responseString = nil;
        }*/
       
        if (([_response containsString:@"{\"sid\":\"9000\""] && ![_response containsString:@",{\"sid\":\"9000\""]))
        {
            responseString = nil;
        }
        
        
        
        
        if(_response.length<200 && json)
            
        {
            [self readIn:json];
        }
        
        
        
        else
        {
            
            if (responseString==nil) {
                responseString = _response;
            }
            else
            {
                responseString = [responseString stringByAppendingString:_response];
                //        NSString *stringResponse = [responseString stringByTrimmingCharactersInSet:[NSCharacterSet newlineCharacterSet]];
            }
            if([responseString hasPrefix:@"{{"]&&[responseString hasSuffix:@"\r"])
                
            {
                
                NSString *responseStr = [self parseJsonString:responseString];
                
                NSMutableArray *responseArr = [[NSMutableArray alloc]init];
                
                [responseArr addObject:responseStr];
                if (responseArr.count > 0) {
                    for (int i = 0; i < responseArr.count; i++) {
                        NSData *objectData = [[responseArr objectAtIndex:i] dataUsingEncoding:NSUTF8StringEncoding];
                        NSArray *json = [NSJSONSerialization JSONObjectWithData:objectData
                                                                        options:NSJSONReadingMutableContainers
                                                                          error:&jsonError];
                        [self multiJson:json];
                    }
                }
                responseString = nil;
            }
            
            
        }
    }
    
    NSMutableData *mutData = [NSMutableData dataWithData:data];
    //[sock readDataToData:data withTimeout:-1 tag:0];
    
    
    [asyncSocket readDataWithTimeout:-1 buffer:mutData bufferOffset:0 maxLength:200 tag:0];
}

-(NSString *)fullJson:(NSString *)jsonString {
    if ([jsonString hasSuffix:@"}}}"])
    {
        
        
        NSString *someText = jsonString;
        NSRange range = NSMakeRange(0,1);
        NSString *newText1 = [someText stringByReplacingCharactersInRange:range withString:@"["];
        NSLog(@"%@",newText1);
        NSRange range2 = NSMakeRange(newText1.length-2,1);
        
        final1String  = [newText1 stringByReplacingCharactersInRange:range2 withString:@"]"];
        //    NSString *temp1String = [newText stringByAppendingString:@"}"];
        //    NSString *final1String = [temp1String stringByAppendingString:@"]"];
        NSLog(@"final string is%@",final1String);
        //name.stri
        
        //NSMutableArray *responseArray = [[NSMutableArray alloc]init];
        //[responseArray addObject:finalString];
    }
    else if ([jsonString hasSuffix:@"}}"])
    {
        
        NSString *someText = jsonString;
        NSRange range = NSMakeRange(0,1);
        NSString *newText1 = [someText stringByReplacingCharactersInRange:range withString:@"["];
        NSLog(@"%@",newText1);
        
        
        final1String  = [newText1 stringByAppendingString:@"]"];
        //    NSString *temp1String = [newText stringByAppendingString:@"}"];
        //    NSString *final1String = [temp1String stringByAppendingString:@"]"];
        NSLog(@"final string is%@",final1String);
        
    }
    
    else if ([jsonString hasSuffix:@"}"])
    {
        
        NSString *someText = jsonString;
        NSRange range = NSMakeRange(0,1);
        NSString *newText1 = [someText stringByReplacingCharactersInRange:range withString:@"["];
        NSLog(@"%@",newText1);
        
        NSString *secondText = [newText1 stringByAppendingString:@"}"];
        final1String  = [secondText stringByAppendingString:@"]"];
        //    NSString *temp1String = [newText stringByAppendingString:@"}"];
        //    NSString *final1String = [temp1String stringByAppendingString:@"]"];
        NSLog(@"final string is%@",final1String);
        
    }
    
    return  final1String;
    
    
}








-(void)readInFirmware:(NSString *)responseHexString
{
    
    [[FirmwareUpgradeVC sharedInstance]readHexFile:responseHexString];
    

    
}

- (void)readIn:(NSDictionary *)responseData {
    //DDLogVerbose(@"Reading in the following:");
    NSLog(@"Reading in the following");
    //DDLogVerbose(@"Response ==%@", responseData);
    NSLog(@"Response==%@",responseData);
    if (responseData) {
        //isMacId = true
        NSString *cmdValue = [responseData valueForKey:@"cmd"];
        
        
        if ([responseData valueForKey:@"cmd"]) {
            tempString = nil;
            //[_packDict removeObjectForKey:cmdValue];
        }
        
        NSString *errorValue = [responseData valueForKey:@"err"];
        if ([errorValue isEqualToString:_18]) {
            if (self.delegate!=nil  && [self.delegate respondsToSelector:@selector(didrecieveNoScheduleFoundError)])
            {
                [self.delegate didrecieveNoScheduleFoundError];
                
                
            }
            else if ([errorValue isEqualToString:_19]) {
                if (self.delegate!=nil  && [self.delegate respondsToSelector:@selector(didrecieveEnableScheduleError)])
                {
                    [self.delegate didrecieveEnableScheduleError];
                    
                    
                }
                
                
                
            }
            
            else if ([errorValue isEqualToString:_20]) {
                if (self.delegate!=nil  && [self.delegate respondsToSelector:@selector(didrecieveDisableScheduleError)])
                {
                    [self.delegate didrecieveDisableScheduleError];
                    
                    
                }
                
                
                
            }
            
            else if ([errorValue isEqualToString:_21]) {
                if (self.delegate!=nil  && [self.delegate respondsToSelector:@selector(didrecieveDeleteScheduleError)])
                {
                    [self.delegate didrecieveDeleteScheduleError];
                    
                    
                }
                
                
                
            }
        }
        
        
        
        
        
        if ([cmdValue isEqualToString: _8208]) {
            
            if (self.delegate!=nil  && [self.delegate respondsToSelector:@selector(didRecieveAuthenticationResponse:)])
            {
                [self.delegate didRecieveAuthenticationResponse:responseData];
                
                
            }
        }
        else if ([cmdValue isEqualToString: _8200]) {
            if (self.delegate != nil && [self.delegate respondsToSelector:@selector(didRecieveLoginorRegistrationResponse:)]) {
                [self.delegate didRecieveLoginorRegistrationResponse:responseData];
            }
        }
        
        else if ([cmdValue isEqualToString: _8510]) {
            
            
            if (self.delegate != nil && [self.delegate respondsToSelector:@selector(didRecievePumpFailureResponse:)]) {
                [self.delegate didRecievePumpFailureResponse:responseData];
            }
            
            else
            {
                [self onPumpFailureResponse:responseData];
                
            }
        }
        
        else if ([cmdValue isEqualToString: _8502]) {
            if (self.delegate != nil && [self.delegate respondsToSelector:@selector(didRecieveGetTankSettingResponse:)]) {
                [self.delegate didRecieveGetTankSettingResponse:responseData];
            }
        }
        
        else if ([cmdValue isEqualToString: _8019]) {
            if (self.delegate != nil && [self.delegate respondsToSelector:@selector(getNetworkSettingsResponse:)]) {
                [self.delegate getNetworkSettingsResponse:responseData];
            }
        }
        
        
        else if ([cmdValue isEqualToString: _8532]) {
            if (self.delegate != nil && [self.delegate respondsToSelector:@selector(didRecieveSuctionSensorResponse:)]) {
                [self.delegate didRecieveSuctionSensorResponse:responseData];
            }
        }
        else if ([cmdValue isEqualToString: _8531]) {
            if (self.delegate != nil && [self.delegate respondsToSelector:@selector(didRecieveGetPumpSettingResponse:)]) {
                [self.delegate didRecieveGetPumpSettingResponse:responseData];
            }
        }

        
        
        else if ([cmdValue isEqualToString: _8530]) {
            if (self.delegate != nil && [self.delegate respondsToSelector:@selector(getPumpSettingResponse:)]) {
                [self.delegate getPumpSettingResponse:responseData];
            }
        }

        
        
        else if ([cmdValue isEqualToString: _8533]) {
            if (self.delegate != nil && [self.delegate respondsToSelector:@selector(didRecieveSuctionSensorSettingResponse:)]) {
                [self.delegate didRecieveSuctionSensorSettingResponse:responseData];
            }
        }
        
        
        else if ([cmdValue isEqualToString: _2003]) {
            
            
            
            //            [[NSNotificationCenter defaultCenter] postNotificationName:@"PUMP_SUCCESS_RESPONSE" object:nil];
            
            if (self.delegate != nil && [self.delegate respondsToSelector:@selector(didRecievePumpSuccessResponse:)]) {
                [self.delegate didRecievePumpSuccessResponse:responseData];
            }
            
            else
            {
                
                [self onPumpSuccess:responseData];
            }
        }
        
        else if ([cmdValue isEqualToString: _8511]) {
            if (self.delegate != nil && [self.delegate respondsToSelector:@selector(didRecieveNotificationResponse:)]) {
                [self.delegate didRecieveNotificationResponse:responseData];
            }
        }
        else if ([cmdValue isEqualToString: _8210]) {
            if (self.delegate != nil && [self.delegate respondsToSelector:@selector(didRegisteredUserResponse:)]) {
                [self.delegate didRegisteredUserResponse:responseData];
            }
        }
        
        
        else if ([cmdValue isEqualToString: _8003]) {
            if (self.delegate != nil && [self.delegate respondsToSelector:@selector(didRecieveFirmwareUpgradeResponse:)]) {
                [self.delegate didRecieveFirmwareUpgradeResponse:responseData];
            }
        }
        else if ([cmdValue isEqualToString: _8275]) {
            if (self.delegate != nil && [self.delegate respondsToSelector:@selector(didRecieveWirelessMacidResponse:)]) {
                [self.delegate didRecieveWirelessMacidResponse:responseData];
            }
        }
        
        
        
        
        else if ([cmdValue isEqualToString: _8204]) {
            if (self.delegate != nil && [self.delegate respondsToSelector:@selector(getChangePasswordResponse:)]) {
                [self.delegate getChangePasswordResponse:responseData];
            }
        }
        else if ([cmdValue isEqualToString: _8213]) {
            if (self.delegate != nil && [self.delegate respondsToSelector:@selector(didrecieveForgotPasswordResponse:)]) {
                [self.delegate didrecieveForgotPasswordResponse:responseData];
            }
        }
        else if ([cmdValue isEqualToString: _8051]) {
            if (self.delegate != nil && [self.delegate respondsToSelector:@selector(didRecieveLicenceUpdationResponse:)]) {
                [self.delegate didRecieveLicenceUpdationResponse:responseData];
            }
        }
        else if ([cmdValue isEqualToString: _8211]) {
            if (self.delegate != nil && [self.delegate respondsToSelector:@selector(didRecieveRemoveRegistrationKeyResponse:)]) {
                [self.delegate didRecieveRemoveRegistrationKeyResponse:responseData];
            }
        }
        else if ([cmdValue isEqualToString: _8501]) {
            if (self.delegate != nil && [self.delegate respondsToSelector:@selector(didRecieveTankSettingResponse:)]) {
                [self.delegate didRecieveTankSettingResponse:responseData];
            }
        }
        
        else if ([cmdValue isEqualToString: _8005]) {
            if (self.delegate != nil && [self.delegate respondsToSelector:@selector(didRecieveDeviceNetworkResponse:)]) {
                [self.delegate didRecieveDeviceNetworkResponse:responseData];
            }
        }
        
        else if ([cmdValue isEqualToString: _8004]) {
            if (self.delegate != nil && [self.delegate respondsToSelector:@selector(didRecieveFirmwareVersionResponse:)]) {
                [self.delegate didRecieveFirmwareVersionResponse:responseData];
            }
        }
        else if ([cmdValue isEqualToString: _8008]) {
            if (self.delegate != nil && [self.delegate respondsToSelector:@selector(getDeviceNetworkSettingsResponse:)]) {
                [self.delegate getDeviceNetworkSettingsResponse:responseData];
            }
        }
        else if ([cmdValue isEqualToString: _8103]) {
            if (self.delegate != nil && [self.delegate respondsToSelector:@selector(getDeviceDateandTimeResponse:)]) {
                [self.delegate getDeviceDateandTimeResponse:responseData];
            }
        }
        
        else if ([cmdValue isEqualToString: _8106]) {
            if (self.delegate != nil && [self.delegate respondsToSelector:@selector(getSytemHealthResponse:)]) {
                [self.delegate getSytemHealthResponse:responseData];
            }
        }
        else if ([cmdValue isEqualToString: _8055]) {
            if (self.delegate != nil && [self.delegate respondsToSelector:@selector(getDiagnosticResponse:)]) {
                [self.delegate getDiagnosticResponse:responseData];
            }
        }
        else if ([cmdValue isEqualToString: _8015]) {
            if (self.delegate != nil && [self.delegate respondsToSelector:@selector(getCurrentFirmwareVersionResponse:)]) {
                [self.delegate getCurrentFirmwareVersionResponse:responseData];
            }
        }
        else if ([cmdValue isEqualToString: _8102]) {
            if (self.delegate != nil && [self.delegate respondsToSelector:@selector(syncDateandTimeResponse:)]) {
                [self.delegate syncDateandTimeResponse:responseData];
            }
        }
        
        else if ([cmdValue isEqualToString: _8212]) {
            if (self.delegate != nil && [self.delegate respondsToSelector:@selector(getAdminLoginResponse:)]) {
                [self.delegate getAdminLoginResponse:responseData];
            }
        }
        
        else if ([cmdValue isEqualToString: _8046]) {
            if (self.delegate != nil && [self.delegate respondsToSelector:@selector(getRestoreFactorySettingsResponse:)]) {
                [self.delegate getRestoreFactorySettingsResponse:responseData];
            }
        }
        else if ([cmdValue isEqualToString: _7001]) {
            if (self.delegate != nil && [self.delegate respondsToSelector:@selector(getProfileInfoResponse:)]) {
                [self.delegate getProfileInfoResponse:responseData];
            }
        }
        
        else if ([cmdValue isEqualToString: _7004]) {
            if (self.delegate != nil && [self.delegate respondsToSelector:@selector(getScheduleCreateResponse:)]) {
                [self.delegate getScheduleCreateResponse:responseData];
            }
            
            else if(self.delegate!=nil && [self.delegate respondsToSelector:@selector(getScheduleDisableResponse:)])
            {
                [self.delegate getScheduleDisableResponse:responseData];
                
                
            }
            
            else if(self.delegate!=nil && [self.delegate respondsToSelector:@selector(getScheduleEnableResponse:)])
            {
                [self.delegate getScheduleEnableResponse:responseData];
                
                
            }
            else if(self.delegate!=nil && [self.delegate respondsToSelector:@selector(getScheduleDeleteResponse:)])
            {
                [self.delegate getScheduleDeleteResponse:responseData];
                
                
            }
            
        }
        
        else  if ([cmdValue isEqualToString: _7009]) {
            
            if (self.delegate != nil && [self.delegate respondsToSelector:@selector(getSingleTankScheduleResponse:)])
            {
                [self.delegate getSingleTankScheduleResponse:responseData];
            }
        }
        
        
        else if ([cmdValue isEqualToString: _8519]) {
            if (self.delegate != nil && [self.delegate respondsToSelector:@selector(getCurrentWaterLevelStatus:)]) {
                [self.delegate getCurrentWaterLevelStatus:responseData];
            }
        }
        
        
        
        
        else if ([cmdValue isEqualToString: _8228]) {
            if (self.delegate != nil && [self.delegate respondsToSelector:@selector(getDeviceAuthenticationResponse:)]) {
                [self.delegate getDeviceAuthenticationResponse:responseData];
            }
        }
    }
}
#pragma mark - multi json parsing here
-(void)multiJson:(NSArray *)json
{
    
    
    NSLog(@"Reading in the following");
    //DDLogVerbose(@"Response ==%@", responseData);
    NSLog(@"Response==%@",json);
    if (json) {
        for (NSDictionary *dict in json) {
            
            NSString *cmdValue = [dict valueForKey:@"cmd"];
            
            if ([json valueForKey:@"cmd"]) {
                [_packDict removeObjectForKey:cmdValue];
            }
            if ([cmdValue isEqualToString: _7009]) {
                
                if (self.delegate != nil && [self.delegate respondsToSelector:@selector(getTankScheduleResponse:)])
                {
                    [self.delegate getTankScheduleResponse:json];
                }
            }
            
            break;
        }
        
        
    }
    
}




-(NSString *)parseJsonString:(NSString *)jsonString {
    NSString *someText = jsonString;
    NSRange range = NSMakeRange(0,1);
    NSString *newText = [someText stringByReplacingCharactersInRange:range withString:@"["];
    NSLog(@"%@",newText);
    
    NSRange range2 = NSMakeRange(newText.length-2,1);
    NSString *finalString = [newText stringByReplacingCharactersInRange:range2 withString:@"]"];
    NSLog(@"final string is%@",finalString);
    //name.stri
    
    //NSMutableArray *responseArray = [[NSMutableArray alloc]init];
    //[responseArray addObject:finalString];
    
    return  finalString;
}

//-(NSString *)appendJsonString:(NSString *)jsonString {
//    static int count=0;
//    tempString = nil;
//
//    if(count==0)
//    {
//        parseString = jsonString;
//
//
//    }
//    if (count==1) {
//        tempString = [parseString stringByAppendingString:jsonString];
//        NSLog(@"final string is:%@",tempString);
//
//        count=0;
//        count --;
//    }
//    count++;
//
//
//    return  tempString;
//}


//saving into database
-(void)onPumpSuccess:(NSDictionary *)jsonDict
{
    
    NSMutableArray *pumpSuccessArray = [[NSMutableArray alloc]init];
    [timer invalidate];
    [SVProgressHUD dismiss];
    NSDictionary *dataDict = [jsonDict valueForKey:_DATA];
    NSString *tank = [dataDict objectForKey:@"01"];
    NSString *pumpState = [dataDict objectForKey:@"02"];
    NSInteger pumpStateInt = [pumpState integerValue];
    NSString *date = [dataDict objectForKey:@"03"];
    NSString *formattedDateString = date;
    //10-03-17-11-55-00
    NSRange range = NSMakeRange(8,1);
    NSRange range1 = NSMakeRange(11,1);
    NSRange range2 = NSMakeRange(14, 1);
    
    NSString *newText = [formattedDateString stringByReplacingCharactersInRange:range withString:@" "];
    NSString *newText1 = [newText stringByReplacingCharactersInRange:range1 withString:@":"];
    NSString *finalString = [newText1 stringByReplacingCharactersInRange:range2 withString:@":"];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT"]];
    NSDate *DateFromString = [dateFormatter dateFromString:finalString];
    double timestamp = [DateFromString timeIntervalSince1970];
    NSNumber *timeInMilliseconds = [NSNumber numberWithDouble:(timestamp*1000)];
    NSString *timeinMillStr = [ NSString stringWithFormat:@"%@",timeInMilliseconds];
    datamodel.date = timeinMillStr;
    datamodel.tankNum = tank;
    datamodel.isRead = @"0";
    switch (pumpStateInt) {
        case 0:
            if ([[dataDict valueForKey:_04] isEqualToString:@"1"])
                
            {
                NSString *reason1 = @"Motor trigger from Switch";
                
                //datamodel.date = date;
                datamodel.reason = reason1;
                datamodel.pumpState = @"OFF";
                
                [pumpSuccessArray addObject:datamodel];
                [[WMSStorageManager sharedInstance]savePumpResponseInDB:pumpSuccessArray];
                //  //[self.view makeToast:NSLocalizedString(@"Motor trigger from Switch", nil)];
                
                
                
            }
            
            else if ([[dataDict valueForKey:_04]isEqualToString:@"2"])
            {
                
                NSString *reason1 = @"Motor trigger from App";
                
                
                //datamodel.date = date;
                datamodel.reason = reason1;
                datamodel.pumpState = @"OFF";
                [pumpSuccessArray addObject:datamodel];
                [[WMSStorageManager sharedInstance]savePumpResponseInDB:pumpSuccessArray];
                //    //[self.view makeToast:NSLocalizedString(@"Motor trigger from App", nil)];
                
            }
            
            else if ([[dataDict valueForKey:_04]isEqualToString:@"3"])
            {
                
                NSString *reason1 = @"Motor trigger from Schedule";
                
                
                //datamodel.date = date;
                //datamodel.tank = tank;
                datamodel.reason = reason1;
                datamodel.pumpState = @"OFF";
                [pumpSuccessArray addObject:datamodel];
                [[WMSStorageManager sharedInstance]savePumpResponseInDB:pumpSuccessArray];
                //[self checkForDuplicates];
                
                //  //[self.view makeToast:NSLocalizedString(@"Motor trigger from Schedule", nil)];
            }
            
            else if ([[dataDict valueForKey:_04]isEqualToString:@"4"])
            {
                
                NSString *reason1 = @"Motor trigger from Dry Run";
                
                
                //datamodel.date = date;
                //  datamodel.tank = tank;
                datamodel.reason = reason1;
                datamodel.pumpState = @"OFF";
                [pumpSuccessArray addObject:datamodel];
                [[WMSStorageManager sharedInstance]savePumpResponseInDB:pumpSuccessArray];                //[self checkForDuplicates];
                
                // //[self.view makeToast:NSLocalizedString(@"Motor trigger from Dry Run", nil)];
                
            }
            
            else if ([[dataDict valueForKey:_04]isEqualToString:@"5"])
            {
                
                // NSString *reason1 = @"Motor turn off due to water level 100%";
                NSString *reason1 = @"Motor trigger from Auto task";
                
                //datamodel.date = date;
                datamodel.reason = reason1;
                datamodel.pumpState = @"OFF";
                // self.pumpOnOff.on = NO;
                [pumpSuccessArray addObject:datamodel];
                [[WMSStorageManager sharedInstance]savePumpResponseInDB:pumpSuccessArray];                //[self checkForDuplicates];
                
                //  //[self.view makeToast:NSLocalizedString(@"Motor trigger from Auto task", nil)];
                
            }
            
            else if ([[dataDict valueForKey:_04]isEqualToString:@"6"])
                
                
            {
                
                NSString *reason1 = @"Motor trigger from Power Failure";
                
                
                //datamodel.date = date;
                
                datamodel.reason = reason1;
                datamodel.pumpState = @"OFF";
                [pumpSuccessArray addObject:datamodel];
                [[WMSStorageManager sharedInstance]savePumpResponseInDB:pumpSuccessArray];
                //[self checkForDuplicates];
                
                // //[self.view makeToast:NSLocalizedString(@"Motor trigger from Power Failure", nil)];
                
            }
            // [_pumpOnOff setOn:NO animated:YES];
            
            break;
            
            
        case 1 :
            
            if ([[dataDict valueForKey:_04] isEqualToString:@"1"])
                
            {
                NSString *reason1 = @"Motor trigger from Switch";
                
                
                
                //datamodel.date = date;
                
                datamodel.reason = reason1;
                datamodel.pumpState = @"ON";
                
                [pumpSuccessArray addObject:datamodel];
                [[WMSStorageManager sharedInstance]savePumpResponseInDB:pumpSuccessArray];
                //[self checkForDuplicates];
                
                //  //[self.view makeToast:NSLocalizedString(@"Motor trigger from Switch", nil)];
            }
            
            else if ([[dataDict valueForKey:_04]isEqualToString:@"2"])
            {
                
                NSString *reason1 = @"Motor trigger from App";
                
                
                //datamodel.date = date;
                
                datamodel.reason = reason1;
                datamodel.pumpState = @"ON";
                //   [_pumpOnOff setOn:YES animated:YES];
                [pumpSuccessArray addObject:datamodel];
                [[WMSStorageManager sharedInstance]savePumpResponseInDB:pumpSuccessArray];
                
                
                // //[self.view makeToast:NSLocalizedString(@"Motor trigger from App", nil)];
                
            }
            
            else if ([[dataDict valueForKey:_04]isEqualToString:@"3"])
            {
                
                NSString *reason1 = @"Motor trigger from Schedule";
                
                
                //datamodel.date = date;
                datamodel.reason = reason1;
                datamodel.pumpState = @"ON";
                
                [pumpSuccessArray addObject:datamodel];
                [[WMSStorageManager sharedInstance]savePumpResponseInDB:pumpSuccessArray];
                
                
                //  //[self.view makeToast:NSLocalizedString(@"Motor trigger from Schedule", nil)];
            }
            
            else if ([[dataDict valueForKey:_04]isEqualToString:@"4"])
            {
                
                NSString *reason1 = @"Motor trigger from Dry Run";
                
                
                //datamodel.date = date;
                datamodel.reason = reason1;
                datamodel.pumpState = @"ON";
                [pumpSuccessArray addObject:datamodel];
                [[WMSStorageManager sharedInstance]savePumpResponseInDB:pumpSuccessArray];
                
                
                
            }
            
            
            else if ([[dataDict valueForKey:_04]isEqualToString:@"5"])
            {
                
                
                NSString *reason1 = @"Motor trigger from Presence Sensor";
                //datamodel.date = date;
                //datamodel.tank = tank;
                datamodel.reason = reason1;
                datamodel.pumpState = @"ON";
                // [_pumpOnOff setOn:YES];
                [pumpSuccessArray addObject:datamodel];
                [[WMSStorageManager sharedInstance]savePumpResponseInDB:pumpSuccessArray];
                //[self checkForDuplicates];
                
                //  //[self.view makeToast:NSLocalizedString(@"Motor trigger from Presence Sensor", nil)];
                
            }
            
            else if ([[dataDict valueForKey:_04]isEqualToString:@"6"])
                
                
            {
                
                NSString *reason1 = @"Motor trigger from Power Failure";
                
                
                //datamodel.date = date;
                //datamodel.tank = tank;
                datamodel.reason = reason1;
                datamodel.pumpState = @"ON";
                //  [_pumpOnOff setOn:YES animated:YES];
                [pumpSuccessArray addObject:datamodel];
                [[WMSStorageManager sharedInstance]savePumpResponseInDB:pumpSuccessArray];
                //[self checkForDuplicates];
                
                //      //[self.view makeToast:NSLocalizedString(@"Motor trigger from Power Failure", nil)];
                
                
            }
            // [_pumpOnOff setOn:YES animated:YES];
            break;
            
            
        case 17:
            if ([[dataDict valueForKey:_04] isEqualToString:@"1"])
                
            {
                NSString *reason1 = @"Motor trigger from Switch";
                
                
                
                //datamodel.date = date;
                // datamodel.tank = tank;
                datamodel.reason = reason1;
                datamodel.pumpState = @"OFF";
                [pumpSuccessArray addObject:datamodel];
                [[WMSStorageManager sharedInstance]savePumpResponseInDB:pumpSuccessArray];
                //[self checkForDuplicates];
                
                ////[self.view makeToast:NSLocalizedString(@"Motor trigger from Switch", nil)];
            }
            
            else if ([[dataDict valueForKey:_04]isEqualToString:@"2"])
            {
                
                NSString *reason1 = @"Motor trigger from App";
                
                
                //datamodel.date = date;
                //datamodel.tank = tank;
                datamodel.reason = reason1;
                datamodel.pumpState = @"OFF";
                [pumpSuccessArray addObject:datamodel];
                [[WMSStorageManager sharedInstance]savePumpResponseInDB:pumpSuccessArray];
                //[self checkForDuplicates];
                
                //  //[self.view makeToast:NSLocalizedString(@"Motor trigger from App", nil)];
                
            }
            
            else if ([[dataDict valueForKey:_04]isEqualToString:@"3"])
            {
                
                NSString *reason1 = @"Motor trigger from Schedule";
                
                
                //datamodel.date = date;
                //datamodel.tank = tank;
                datamodel.reason = reason1;
                datamodel.pumpState = @"OFF";
                [pumpSuccessArray addObject:datamodel];
                [[WMSStorageManager sharedInstance]savePumpResponseInDB:pumpSuccessArray];
                //[self checkForDuplicates];
                
                // //[self.view makeToast:NSLocalizedString(@"Motor trigger from Schedule", nil)];
            }
            
            else if ([[dataDict valueForKey:_04]isEqualToString:@"4"])
            {
                
                NSString *reason1 = @"Motor trigger from Dry Run";
                
                
                //datamodel.date = date;
                //  datamodel.tank = tank;
                datamodel.reason = reason1;
                datamodel.pumpState = @"OFF";
                [pumpSuccessArray addObject:datamodel];
                [[WMSStorageManager sharedInstance]savePumpResponseInDB:pumpSuccessArray];                //[self checkForDuplicates];
                
                //  //[self.view makeToast:NSLocalizedString(@"Motor trigger from Dry Run", nil)];
                
            }
            
            else if ([[dataDict valueForKey:_04]isEqualToString:@"5"])
            {
                
                //NSString *reason1 = @"Motor turn off due to water level 100%";
                NSString *reason1 = @"Motor trigger from Auto task";
                
                //datamodel.date = date;
                //datamodel.tank = tank;
                datamodel.reason = reason1;
                datamodel.pumpState = @"OFF";
                [pumpSuccessArray addObject:datamodel];
                [[WMSStorageManager sharedInstance]savePumpResponseInDB:pumpSuccessArray];
                //[self checkForDuplicates];
                
                //  //[self.view makeToast:NSLocalizedString(@"Motor trigger from Auto task", nil)];
                
            }
            
            else if ([[dataDict valueForKey:_04]isEqualToString:@"6"])
                
                
            {
                
                NSString *reason1 = @"Motor trigger from Power Failure";
                
                
                //datamodel.date = date;
                //datamodel.tank = tank;
                datamodel.reason = reason1;
                datamodel.pumpState = @"OFF";
                [pumpSuccessArray addObject:datamodel];
                [[WMSStorageManager sharedInstance]savePumpResponseInDB:pumpSuccessArray];
                //[self checkForDuplicates];
                
                //  //[self.view makeToast:NSLocalizedString(@"Motor trigger from Power Failure", nil)];
                
                
            }
            //   [_pumpOnOff setOn:NO animated:YES];
            
            break;
            
            
        case 25:
            //pump state is off
            if ([[dataDict valueForKey:_04] isEqualToString:@"1"])
                
            {
                NSString *reason1 = @"Motor trigger from Switch";
                
                
                
                //datamodel.date = date;
                // datamodel.tank = tank;
                datamodel.reason = reason1;
                datamodel.pumpState = @"OFF";
                [pumpSuccessArray addObject:datamodel];
                [[WMSStorageManager sharedInstance]savePumpResponseInDB:pumpSuccessArray];
                //[self checkForDuplicates];
                
                // //[self.view makeToast:NSLocalizedString(@"Motor trigger from Switch", nil)];
            }
            
            else if ([[dataDict valueForKey:_04]isEqualToString:@"2"])
            {
                
                NSString *reason1 = @"Motor trigger from App";
                
                
                //datamodel.date = date;
                //datamodel.tank = tank;
                datamodel.reason = reason1;
                datamodel.pumpState = @"OFF";
                [pumpSuccessArray addObject:datamodel];
                [[WMSStorageManager sharedInstance]savePumpResponseInDB:pumpSuccessArray];
                //[self checkForDuplicates];
                
                //  //[self.view makeToast:NSLocalizedString(@"Motor trigger from App", nil)];
                
            }
            
            else if ([[dataDict valueForKey:_04]isEqualToString:@"3"])
            {
                
                NSString *reason1 = @"Motor trigger from Schedule";
                
                
                //datamodel.date = date;
                //datamodel.tank = tank;
                datamodel.reason = reason1;
                datamodel.pumpState = @"OFF";
                [pumpSuccessArray addObject:datamodel];
                [[WMSStorageManager sharedInstance]savePumpResponseInDB:pumpSuccessArray];
                //[self checkForDuplicates];
                
                // //[self.view makeToast:NSLocalizedString(@"Motor trigger from Schedule", nil)];
            }
            
            else if ([[dataDict valueForKey:_04]isEqualToString:@"4"])
            {
                
                NSString *reason1 = @"Motor trigger from Dry Run";
                
                
                //datamodel.date = date;
                //  datamodel.tank = tank;
                datamodel.reason = reason1;
                datamodel.pumpState = @"OFF";
                [pumpSuccessArray addObject:datamodel];
                [[WMSStorageManager sharedInstance]savePumpResponseInDB:pumpSuccessArray];
                //[self checkForDuplicates];
                
                ////[self.view makeToast:NSLocalizedString(@"Motor trigger from Dry Run", nil)];
                
            }
            
            else if ([[dataDict valueForKey:_04]isEqualToString:@"5"])
            {
                
                //   NSString *reason1 = @"Motor turn off due to water level 100%";
                NSString *reason1 = @"Motor trigger from Auto task";
                
                
                //datamodel.date = date;
                //datamodel.tank = tank;
                datamodel.reason = reason1;
                datamodel.pumpState = @"OFF";
                [pumpSuccessArray addObject:datamodel];
                [[WMSStorageManager sharedInstance]savePumpResponseInDB:pumpSuccessArray];
                //[self checkForDuplicates];
                
                // //[self.view makeToast:NSLocalizedString(@"Motor trigger from Auto task", nil)];
                
            }
            
            else if ([[dataDict valueForKey:_04]isEqualToString:@"6"])
                
                
            {
                
                NSString *reason1 = @"Motor trigger from Power Failure";
                
                
                //datamodel.date = date;
                //datamodel.tank = tank;
                datamodel.reason = reason1;
                datamodel.pumpState = @"OFF";
                [pumpSuccessArray addObject:datamodel];
                [[WMSStorageManager sharedInstance]savePumpResponseInDB:pumpSuccessArray];
                
                //[self checkForDuplicates];
                
                //  //[self.view makeToast:NSLocalizedString(@"Motor trigger from Power Failure", nil)];
                
                
            }
            // [_pumpOnOff setOn:NO animated:YES];
            
            break;
            
            
        default:
            break;
    }
    
    
    
}




#pragma mark - pump failure status response here
-(void)onPumpFailureResponse:(NSDictionary *)jsonDict
{
    NSMutableArray *pumpFailResponseArray = [[NSMutableArray alloc]init];
    [timer invalidate];
    [SVProgressHUD dismiss];
    NSDictionary *dataDict = [jsonDict valueForKey:_DATA];
    NSString *tank = [dataDict objectForKey:@"01"];
    NSString *notificationNumber = [dataDict objectForKey:@"02"];
    NSInteger notification=[notificationNumber intValue];
    NSString *notificationField = [dataDict objectForKey:@"03"];
    NSString *date = [dataDict objectForKey:@"04"];
    NSString *formattedDateString = date;
    //10-03-17-11-55-00
    NSRange range = NSMakeRange(8,1);
    NSRange range1 = NSMakeRange(11,1);
    NSRange range2 = NSMakeRange(14, 1);
    
    NSString *newText = [formattedDateString stringByReplacingCharactersInRange:range withString:@" "];
    NSString *newText1 = [newText stringByReplacingCharactersInRange:range1 withString:@":"];
    NSString *finalString = [newText1 stringByReplacingCharactersInRange:range2 withString:@":"];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT"]];
    NSDate *DateFromString = [dateFormatter dateFromString:finalString];
    double timestamp = [DateFromString timeIntervalSince1970];
    NSNumber *timeInMilliseconds = [NSNumber numberWithDouble:(timestamp*1000)];
    NSString *timeinMillStr = [ NSString stringWithFormat:@"%@",timeInMilliseconds];
    datamodel.date=timeinMillStr;
    datamodel.isRead = @"0";
    datamodel.tankNum =tank;
    
    switch (notification) {
            
            //notification number is 1
        case 1:
        {
            if ([notificationField isEqualToString:@"1"]) {
                NSString *notification1 = @"Sensor number 1 malfunction";
                datamodel.reason = notification1;
                datamodel.pumpState=@"";
                [pumpFailResponseArray addObject:datamodel];
                [[WMSStorageManager sharedInstance]savePumpResponseInDB:pumpFailResponseArray];
                ////[self.view makeToast:NSLocalizedString(@"Sensor number 1 malfunction", nil)];
                
            }
            else if ([notificationField isEqualToString:@"2"]) {
                NSString *notification1 = @"Sensor number 2 malfunction";
                datamodel.reason = notification1;
                datamodel.pumpState=@"";
                
                [pumpFailResponseArray addObject:datamodel];
                [[WMSStorageManager sharedInstance]savePumpResponseInDB:pumpFailResponseArray];
                // [self checkForDuplicates];
                
                // //[self.view makeToast:NSLocalizedString(@"Sensor number 2 malfunction", nil)];
            }
            else if ([notificationField isEqualToString:@"3"]) {
                NSString *notification1 = @"Sensor number 3 malfunction";
                datamodel.reason = notification1;
                datamodel.pumpState=@"";
                
                [pumpFailResponseArray addObject:datamodel];
                [[WMSStorageManager sharedInstance]savePumpResponseInDB:pumpFailResponseArray];
                //  [self checkForDuplicates];
                
                //[self.view makeToast:NSLocalizedString(@"Sensor number 3 malfunction", nil)];
                
            }
            
            else if ([notificationField isEqualToString:@"4"]) {
                NSString *notification1 = @"Sensor number 4 malfunction";
                datamodel.reason = notification1;
                datamodel.pumpState=@"";
                
                [pumpFailResponseArray addObject:datamodel];
                [[WMSStorageManager sharedInstance]savePumpResponseInDB:pumpFailResponseArray];
                
                //[self.view makeToast:NSLocalizedString(@"Sensor number 3 malfunction", nil)];
                
            }
            
            else if ([notificationField isEqualToString:@"1-2"]) {
                NSString *notification1 = @"Sensor number 1 and 2 malfunction";
                datamodel.reason = notification1;
                datamodel.pumpState=@"";
                
                [pumpFailResponseArray addObject:datamodel];
                [[WMSStorageManager sharedInstance]savePumpResponseInDB:pumpFailResponseArray];
                
                //[self.view makeToast:NSLocalizedString(@"Sensor number 1 and 2 malfunction", nil)];
                
            }
            
            else if ([notificationField isEqualToString:@"1-3"]) {
                NSString *notification1 = @"Sensor number 1 and 3 malfunction";
                datamodel.reason = notification1;
                datamodel.pumpState=@"";
                
                [pumpFailResponseArray addObject:datamodel];
                [[WMSStorageManager sharedInstance]savePumpResponseInDB:pumpFailResponseArray];
                //[self.view makeToast:NSLocalizedString(@"Sensor number 1 and 3 malfunction", nil)];
                
            }
            
            else if ([notificationField isEqualToString:@"1-4"]) {
                NSString *notification1 = @"Sensor number 1 and 4 malfunction";
                datamodel.reason = notification1;
                datamodel.pumpState=@"";
                
                [pumpFailResponseArray addObject:datamodel];
                [[WMSStorageManager sharedInstance]savePumpResponseInDB:pumpFailResponseArray];
                //[self.view makeToast:NSLocalizedString(@"Sensor number 1 and 4 malfunction", nil)];
                
            }
            
            else if ([notificationField isEqualToString:@"1-2-3"]) {
                NSString *notification1 = @"Sensor number 1,2 and 3 malfunction";
                datamodel.reason = notification1;
                datamodel.pumpState=@"";
                
                [pumpFailResponseArray addObject:datamodel];
                [[WMSStorageManager sharedInstance]savePumpResponseInDB:pumpFailResponseArray];
                //[self.view makeToast:NSLocalizedString(@"Sensor number 1,2 and 3 malfunction", nil)];
                
            }
            
            else if ([notificationField isEqualToString:@"1-2-3-4"]) {
                NSString *notification1 = @"Sensor number 1,2,3 and 4 malfunction";
                datamodel.reason = notification1;
                datamodel.pumpState=@"";
                
                [pumpFailResponseArray addObject:datamodel];
                [[WMSStorageManager sharedInstance]savePumpResponseInDB:pumpFailResponseArray];
                //[self.view makeToast:NSLocalizedString(@"Sensor number 1,2,3 and 4  malfunction", nil)];
                
            }
            
            else if ([notificationField isEqualToString:@"2-4"]) {
                NSString *notification1 = @"Sensor number 2 and 4 malfunction";
                datamodel.reason = notification1;
                datamodel.pumpState=@"";
                
                [pumpFailResponseArray addObject:datamodel];
                [[WMSStorageManager sharedInstance]savePumpResponseInDB:pumpFailResponseArray];
                
                //[self.view makeToast:NSLocalizedString(@"Sensor number 3 malfunction", nil)];
                
            }
            
            else if ([notificationField isEqualToString:@"3-4"]) {
                NSString *notification1 = @"Sensor number 3 and 4 malfunction";
                datamodel.reason = notification1;
                datamodel.pumpState=@"";
                
                [pumpFailResponseArray addObject:datamodel];
                [[WMSStorageManager sharedInstance]savePumpResponseInDB:pumpFailResponseArray];
                
                //[self.view makeToast:NSLocalizedString(@"Sensor number 3 and 4  malfunction", nil)];
                
            }
            
            else if ([notificationField isEqualToString:@"2-3-4"]) {
                NSString *notification1 = @"Sensor number 2,3 and 4 malfunction";
                datamodel.reason = notification1;
                datamodel.pumpState=@"";
                
                [pumpFailResponseArray addObject:datamodel];
                [[WMSStorageManager sharedInstance]savePumpResponseInDB:pumpFailResponseArray];
                
                //[self.view makeToast:NSLocalizedString(@"Sensor number 2,3 and 4  malfunction", nil)];
                
            }
        }
            
            break;
            
        case 2 :
        { NSString *notification2 = @"Sensor hits more than 6 times in a minute";
            datamodel.reason = notification2;
            datamodel.pumpState=@"OFF";
            
            [pumpFailResponseArray addObject:datamodel];
            [[WMSStorageManager sharedInstance]savePumpResponseInDB:pumpFailResponseArray];
            
            //[self.view makeToast:NSLocalizedString(@"Sensor hits more than 6 times in a minute", nil)];
        }
            break;
            
            
        case 3 :
        { NSString *notification3 = @"Pump state changes more than 4 times in a minute";
            datamodel.reason = notification3;
            datamodel.pumpState=@"OFF";
            
            [pumpFailResponseArray addObject:datamodel];
            [[WMSStorageManager sharedInstance]savePumpResponseInDB:pumpFailResponseArray];
            
            //[self.view makeToast:NSLocalizedString(@"Pump state changes more than 4 times in a minute", nil)];
        }
            break;
            
        case 4 :
        { NSString *notification4 = @"Pump wire disconnected";
            datamodel.reason = notification4;
            datamodel.pumpState=@"OFF";
            
            [pumpFailResponseArray addObject:datamodel];
            [[WMSStorageManager sharedInstance]savePumpResponseInDB:pumpFailResponseArray];
            
            
            //[self.view makeToast:NSLocalizedString(@"Pump wire disconnected", nil)];
        }
            break;
        case 5 :
        { NSString *notification5 = @"Pump wire connected";
            datamodel.reason = notification5;
            datamodel.pumpState=@"OFF";
            
            [pumpFailResponseArray addObject:datamodel];
            [[WMSStorageManager sharedInstance]savePumpResponseInDB:pumpFailResponseArray];
            
            //[self.view makeToast:NSLocalizedString(@"Pump wire connected", nil)];
            
        }
            break;
            
        case 6 :
        { NSString *notification6 = @"Pump can't be started due to wire  disconnected";
            datamodel.reason = notification6;
            datamodel.pumpState=@"OFF";
            
            [pumpFailResponseArray addObject:datamodel];
            [[WMSStorageManager sharedInstance]savePumpResponseInDB:pumpFailResponseArray];
            
            //[self.view makeToast:NSLocalizedString(@"Pump can't be started due to wire  disconnected", nil)];
        }
            break;
        case 7 :
        { NSString *notification7 = @"Pump can't be started due to ac mains off";
            datamodel.reason = notification7;
            datamodel.pumpState=@"OFF";
            
            [pumpFailResponseArray addObject:datamodel];
            [[WMSStorageManager sharedInstance]savePumpResponseInDB:pumpFailResponseArray];
            
            //[self.view makeToast:NSLocalizedString(@"Pump can't ne started due to ac mains off", nil)];
            
        }
            break;
        case 8 :
        { NSString *notification8 = @"Automated task Nnumber 1 executed";
            datamodel.reason = notification8;
            datamodel.pumpState=@"OFF";
            [pumpFailResponseArray addObject:datamodel];
            [[WMSStorageManager sharedInstance]savePumpResponseInDB:pumpFailResponseArray];
            
            //[self.view makeToast:NSLocalizedString(@"Automated task Nnumber 1 executed", nil)];
            
        }
            break;
        case 9 :
        { NSString *notification9 = @"Automated task Nnumber 2 executed";
            datamodel.reason = notification9;
            datamodel.pumpState=@"OFF";
            
            [pumpFailResponseArray addObject:datamodel];
            [[WMSStorageManager sharedInstance]savePumpResponseInDB:pumpFailResponseArray];
            
            //[self.view makeToast:NSLocalizedString(@"Automated task Nnumber 2 executed", nil)];
            
        }
            break;
        case 10 :
        { NSString *notification10 = @"Automated task Nnumber 3 executed";
            datamodel.reason = notification10;
            datamodel.pumpState=@"OFF";
            
            [pumpFailResponseArray addObject:datamodel];
            [[WMSStorageManager sharedInstance]savePumpResponseInDB:pumpFailResponseArray];
            
            //[self.view makeToast:NSLocalizedString(@"Automated task Nnumber 3 executed", nil)];
            
        }
            break;
            
            
            
        case 17 :
        {
            //
            //
            //
            NSString *notification11 = @"Schedule can't be run";
            datamodel.reason = notification11;
            datamodel.pumpState=@"OFF";
            
            [pumpFailResponseArray addObject:datamodel];
            [[WMSStorageManager sharedInstance]savePumpResponseInDB:pumpFailResponseArray];
            
            //[self.view makeToast:NSLocalizedString(@"Schedule can't be run", nil)];
            //            if ([[dataDict valueForKey:_03] isEqualToString:@"1"])
            //
            //            {
            //                NSString *reason1 = @"Motor trigger from Switch";
            //
            //
            //
            //                //datamodel.date = date;
            //                datamodel.reason = reason1;
            //                datamodel.pumpState = @"OFF";
            //                [pumpFailResponseArray addObject:datamodel];
            //                [[WMSStorageManager sharedInstance]savePumpResponseInDB:pumpFailResponseArray];
            //                //[self.view makeToast:NSLocalizedString(@"Motor trigger from Switch", nil)];
            //            }
            //
            //            else if ([[dataDict valueForKey:_03]isEqualToString:@"2"])
            //            {
            //
            //                NSString *reason1 = @"Motor trigger from App";
            //
            //
            //                //datamodel.date = date;
            //                //datamodel.tank = tank;
            //                datamodel.reason = reason1;
            //                datamodel.pumpState = @"OFF";
            //                [pumpFailResponseArray addObject:datamodel];
            //                [[WMSStorageManager sharedInstance]savePumpResponseInDB:pumpFailResponseArray];
            //
            //                //[self.view makeToast:NSLocalizedString(@"Motor trigger from App", nil)];
            //
            //            }
            //
            //            else if ([[dataDict valueForKey:_03]isEqualToString:@"3"])
            //            {
            //
            //                NSString *reason1 = @"Motor trigger from Schedule";
            //
            //
            //                //datamodel.date = date;
            //
            //                datamodel.reason = reason1;
            //                datamodel.pumpState = @"OFF";
            //                [pumpFailResponseArray addObject:datamodel];
            //                [[WMSStorageManager sharedInstance]savePumpResponseInDB:pumpFailResponseArray];                //[self.view makeToast:NSLocalizedString(@"Motor trigger from Schedule", nil)];
            //            }
            //
            //            else if ([[dataDict valueForKey:_03]isEqualToString:@"4"])
            //            {
            //
            //                NSString *reason1 = @"Motor trigger from Dry Run";
            //
            //
            //                //datamodel.date = date;
            //                //  datamodel.tank = tank;
            //                datamodel.reason = reason1;
            //                datamodel.pumpState = @"OFF";
            //                [pumpFailResponseArray addObject:datamodel];
            //                [[WMSStorageManager sharedInstance]savePumpResponseInDB:pumpFailResponseArray];
            //                //[self.view makeToast:NSLocalizedString(@"Motor trigger from Dry Run", nil)];
            //
            //            }
            //
            //            else if ([[dataDict valueForKey:_03]isEqualToString:@"5"])
            //            {
            //
            //               // NSString *reason1 = @"Motor turn off due to water level 100%";
            //
            //                NSString *reason1 = @"Motor trigger from Auto task";
            //
            //                //datamodel.date = date;
            //                //datamodel.tank = tank;
            //                datamodel.reason = reason1;
            //                datamodel.pumpState = @"OFF";
            //                [pumpFailResponseArray addObject:datamodel];
            //                [[WMSStorageManager sharedInstance]savePumpResponseInDB:pumpFailResponseArray];                //[self.view makeToast:NSLocalizedString(@"Motor trigger from Auto task", nil)];
            //
            //            }
            //
            //            else if ([[dataDict valueForKey:_03]isEqualToString:@"6"])
            //
            //
            //            {
            //
            //                NSString *reason1 = @"Motor trigger from Power Failure";
            //
            //
            //                //datamodel.date = date;
            //                //datamodel.tank = tank;
            //                datamodel.reason = reason1;
            //                datamodel.pumpState = @"OFF";
            //                [pumpFailResponseArray addObject:datamodel];
            //                [[WMSStorageManager sharedInstance]savePumpResponseInDB:pumpFailResponseArray];
            //
            //                //[self.view makeToast:NSLocalizedString(@"Motor trigger from Power Failure", nil)];
            //
            //
            //            }
            //
            //            else if ([[dataDict valueForKey:_03]isEqualToString:@"14"])
            //
            //
            //            {
            //
            //                NSString *reason1 = @"Motor can't be started due to tank Full";
            //
            //
            //                //datamodel.date = date;
            //                //datamodel.tank = tank;
            //                datamodel.reason = reason1;
            //                datamodel.pumpState = @"OFF";
            //                [pumpFailResponseArray addObject:datamodel];
            //                [[WMSStorageManager sharedInstance]savePumpResponseInDB:pumpFailResponseArray];
            //                //[self.view makeToast:NSLocalizedString(@"Motor can't be started due to tank Full", nil)];
            //
            //
            //            }
            //            [_pumpOnOff setOn:NO animated:YES];
        }
            
            break;
            
            
        case 18 :
        { NSString *notification18 = @"System on battery";
            datamodel.reason = notification18;
            datamodel.pumpState=@"OFF";
            [pumpFailResponseArray addObject:datamodel];
            [[WMSStorageManager sharedInstance]savePumpResponseInDB:pumpFailResponseArray];
            //[self.view makeToast:NSLocalizedString(@"System on battery", nil)];
            
        }
            break;
            
            
            
            
        case 15 :
        { NSString *notification18 = @"Motor can't be stared due to tank full";
            datamodel.reason = notification18;
            datamodel.pumpState=@"OFF";
            
            [pumpFailResponseArray addObject:datamodel];
            [[WMSStorageManager sharedInstance]savePumpResponseInDB:pumpFailResponseArray];
            
            //[self.view makeToast:NSLocalizedString(@"Motor can't be stared due to tank full", nil)];
            
        }
            break;
        case 19 :
        { NSString *notification19 = @"System on mains";
            datamodel.reason = notification19;
            datamodel.pumpState=@"OFF";
            
            [pumpFailResponseArray addObject:datamodel];
            [[WMSStorageManager sharedInstance]savePumpResponseInDB:pumpFailResponseArray];
            
            //[self.view makeToast:NSLocalizedString(@"System on mains", nil)];
            
        }
            break;
        case 24 :
        {
            
            //pump state is on
            if ([[dataDict valueForKey:_03] isEqualToString:@"1"])
                
            {
                NSString *reason1 = @"Motor trigger from Switch";
                
                
                
                //datamodel.date = date;
                datamodel.reason = reason1;
                datamodel.pumpState = @"ON";
                [pumpFailResponseArray addObject:datamodel];
                [[WMSStorageManager sharedInstance]savePumpResponseInDB:pumpFailResponseArray];
                //[self.view makeToast:NSLocalizedString(@"Motor trigger from Switch", nil)];
            }
            
            else if ([[dataDict valueForKey:_03]isEqualToString:@"2"])
            {
                
                NSString *reason1 = @"Motor trigger from App";
                
                
                //datamodel.date = date;
                //datamodel.tank = tank;
                datamodel.reason = reason1;
                datamodel.pumpState = @"ON";
                [pumpFailResponseArray addObject:datamodel];
                [[WMSStorageManager sharedInstance]savePumpResponseInDB:pumpFailResponseArray];
                
                //[self.view makeToast:NSLocalizedString(@"Motor trigger from App", nil)];
                
            }
            
            else if ([[dataDict valueForKey:_03]isEqualToString:@"3"])
            {
                
                NSString *reason1 = @"Motor trigger from Schedule";
                
                
                //datamodel.date = date;
                
                datamodel.reason = reason1;
                datamodel.pumpState = @"ON";
                [pumpFailResponseArray addObject:datamodel];
                [[WMSStorageManager sharedInstance]savePumpResponseInDB:pumpFailResponseArray];
                
                //[self.view makeToast:NSLocalizedString(@"Motor trigger from Schedule", nil)];
            }
            
            else if ([[dataDict valueForKey:_03]isEqualToString:@"4"])
            {
                
                NSString *reason1 = @"Motor trigger from Dry Run";
                
                
                //datamodel.date = date;
                //  datamodel.tank = tank;
                datamodel.reason = reason1;
                datamodel.pumpState = @"ON";
                [pumpFailResponseArray addObject:datamodel];
                [[WMSStorageManager sharedInstance]savePumpResponseInDB:pumpFailResponseArray];
                
                //[self.view makeToast:NSLocalizedString(@"Motor trigger from Dry Run", nil)];
                
            }
            
            else if ([[dataDict valueForKey:_03]isEqualToString:@"5"])
            {
                
                // NSString *reason1 = @"Motor turn off due to water level 100%";
                NSString *reason1 = @"Motor trigger from Auto task";
                
                //datamodel.date = date;
                //datamodel.tank = tank;
                datamodel.reason = reason1;
                datamodel.pumpState = @"ON";
                [pumpFailResponseArray addObject:datamodel];
                [[WMSStorageManager sharedInstance]savePumpResponseInDB:pumpFailResponseArray];
                
                //[self.view makeToast:NSLocalizedString(@"Motor trigger from Auto task", nil)];
                
            }
            
            else if ([[dataDict valueForKey:_03]isEqualToString:@"6"])
                
                
            {
                
                NSString *reason1 = @"Motor trigger from Power Failure";
                
                
                //datamodel.date = date;
                //datamodel.tank = tank;
                datamodel.reason = reason1;
                datamodel.pumpState = @"ON";
                [pumpFailResponseArray addObject:datamodel];
                [[WMSStorageManager sharedInstance]savePumpResponseInDB:pumpFailResponseArray];
                
                //[self.view makeToast:NSLocalizedString(@"Motor trigger from Power Failure", nil)];
                
                
            }
            
            else if ([[dataDict valueForKey:_03]isEqualToString:@"14"])
                
                
            {
                
                NSString *reason1 = @"Motor can't be started due to tank Full";
                
                
                //datamodel.date = date;
                //datamodel.tank = tank;
                datamodel.reason = reason1;
                datamodel.pumpState = @"OFF";
                [pumpFailResponseArray addObject:datamodel];
                [[WMSStorageManager sharedInstance]savePumpResponseInDB:pumpFailResponseArray];
                
                //[self.view makeToast:NSLocalizedString(@"Motor can't be started due to tank Full", nil)];
                
                
            }
            // [_pumpOnOff setOn:YES animated:YES];
            
            
            
        }
            break;
        case 25 :
        {
            
            if ([[dataDict valueForKey:_03] isEqualToString:@"1"])
                
            {
                NSString *reason1 = @"Motor trigger from Switch";
                
                
                
                //datamodel.date = date;
                datamodel.reason = reason1;
                datamodel.pumpState = @"OFF";
                [pumpFailResponseArray addObject:datamodel];
                [[WMSStorageManager sharedInstance]savePumpResponseInDB:pumpFailResponseArray];
                
                //[self.view makeToast:NSLocalizedString(@"Motor trigger from Switch", nil)];
            }
            
            else if ([[dataDict valueForKey:_03]isEqualToString:@"2"])
            {
                
                NSString *reason1 = @"Motor trigger from App";
                
                
                //datamodel.date = date;
                //datamodel.tank = tank;
                datamodel.reason = reason1;
                datamodel.pumpState = @"OFF";
                [pumpFailResponseArray addObject:datamodel];
                [[WMSStorageManager sharedInstance]savePumpResponseInDB:pumpFailResponseArray];
                
                //[self.view makeToast:NSLocalizedString(@"Motor trigger from App", nil)];
                
            }
            
            else if ([[dataDict valueForKey:_03]isEqualToString:@"3"])
            {
                
                NSString *reason1 = @"Motor trigger from Schedule";
                
                
                //datamodel.date = date;
                
                datamodel.reason = reason1;
                datamodel.pumpState = @"OFF";
                [pumpFailResponseArray addObject:datamodel];
                [[WMSStorageManager sharedInstance]savePumpResponseInDB:pumpFailResponseArray];
                
                //[self.view makeToast:NSLocalizedString(@"Motor trigger from Schedule", nil)];
            }
            
            else if ([[dataDict valueForKey:_03]isEqualToString:@"4"])
            {
                
                NSString *reason1 = @"Motor trigger from Dry Run";
                
                
                //datamodel.date = date;
                //  datamodel.tank = tank;
                datamodel.reason = reason1;
                datamodel.pumpState = @"OFF";
                [pumpFailResponseArray addObject:datamodel];
                [[WMSStorageManager sharedInstance]savePumpResponseInDB:pumpFailResponseArray];
                
                //[self.view makeToast:NSLocalizedString(@"Motor trigger from Dry Run", nil)];
                
            }
            
            else if ([[dataDict valueForKey:_03]isEqualToString:@"5"])
            {
                
                // NSString *reason1 = @"Motor turn off due to water level 100%";
                
                NSString *reason1 = @"Motor trigger from Auto task";
                //datamodel.date = date;
                //datamodel.tank = tank;
                datamodel.reason = reason1;
                datamodel.pumpState = @"OFF";
                [pumpFailResponseArray addObject:datamodel];
                [[WMSStorageManager sharedInstance]savePumpResponseInDB:pumpFailResponseArray];
                
                //[self.view makeToast:NSLocalizedString(@"Motor trigger from Auto task", nil)];
                
            }
            
            else if ([[dataDict valueForKey:_03]isEqualToString:@"6"])
                
                
            {
                
                NSString *reason1 = @"Motor trigger from Power Failure";
                
                
                //datamodel.date = date;
                //datamodel.tank = tank;
                datamodel.reason = reason1;
                datamodel.pumpState = @"OFF";
                [pumpFailResponseArray addObject:datamodel];
                [[WMSStorageManager sharedInstance]savePumpResponseInDB:pumpFailResponseArray];
                
                //[self.view makeToast:NSLocalizedString(@"Motor trigger from Power Failure", nil)];
                
                
            }
            
            else if ([[dataDict valueForKey:_03]isEqualToString:@"14"])
                
                
            {
                
                NSString *reason1 = @"Motor can't be started due to tank Full";
                
                
                //datamodel.date = date;
                //datamodel.tank = tank;
                datamodel.reason = reason1;
                datamodel.pumpState = @"OFF";
                [pumpFailResponseArray addObject:datamodel];
                [[WMSStorageManager sharedInstance]savePumpResponseInDB:pumpFailResponseArray];
                
                //[self.view makeToast:NSLocalizedString(@"Motor can't be started due to tank Full", nil)];
                
                
            }
            
        }
            break;
            
            
            
            
        default:
            break;
    }
    
    //  [_pumpOnOff setOn:NO animated:YES];
    
}









@end
/*
 
 //- (void)socket:(GCDAsyncSocket *)sock didReadPartialDataOfLength:(NSUInteger)partialLength tag:(long)tag
 //{
 //    NSLog(@"did read partial data ");
 //
 //}
 //
 //-(NSTimeInterval)socket:(GCDAsyncSocket *)sock shouldTimeoutReadWithTag:(long)tag elapsed:(NSTimeInterval)elapsed bytesDone:(NSUInteger)length
 //{
 //
 //    NSLog(@"read timeout ");
 //    return 10.0;
 //
 //}
 //-(NSTimeInterval)socket:(GCDAsyncSocket *)sock shouldTimeoutWriteWithTag:(long)tag elapsed:(NSTimeInterval)elapsed bytesDone:(NSUInteger)length
 //{
 //
 //    NSLog(@"Write Timeout happens");
 //    return 10.0;
 //
 //}
 
 
 
 
 //-(void)startTimer{
 //    timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(onTick) userInfo:nil repeats:YES];
 //   // ticks = 5;
 //}
 //-(void)invalidateTimer{
 //    if (timer && [timer isValid]) {
 //        [timer invalidate];
 //        timer = nil;
 //    }
 //}
 
 //-(void)onTick{
 //   // ticks--;
 //    if (ticks == 0) {
 //        //NSError *error;
 //      //  [self.udpSocket beginReceiving:&error];
 //        //  NSLog(@"UDP Receiving start %d",state);
 //    }
 //    if (![asyncSocket isConnected]) {
 //        //ticks = 5;
 //        [self reTryToConnectSocket];
 //
 //    }
 //}
 
 */
