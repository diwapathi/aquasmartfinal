//
//  ThemeManager.m
//  Homesmart
//
//  Created by Neotech on 27/04/16.
//  Copyright © 2016 Neotech. All rights reserved.
//

#import "ThemeManager.h"
//#import "Constants.h"
//#import "WMSSettingsController.h"
#if DEBUG
//static const int ddLogLevel = LOG_LEVEL_VERBOSE;
#else
//static const int ddLogLevel = LOG_LEVEL_WARN;
#endif

NSString *const BACKGROUND_COLOR = @"BACKGROUND_COLOR";
NSString *const HEADER_VIEW_BACKGROUND_COLOR = @"HEADER_VIEW_BACKGROUND_COLOR";
NSString *const BORDER_COLOR = @"BORDER_COLOR";
NSString *const USER_IMG = @"USER_IMG";
NSString *const USER_PROFILE_DEFAULT_IMG = @"USER_PROFILE_DEFAULT_IMG";
NSString *const TOP_MENU_BGCOLOR = @"TOP_MENU_BGCOLOR";
NSString *const TAB_BAR_BACKGROUNG_COLOR = @"TAB_BAR_BACKGROUNG_COLOR";
NSString *const BACK_IMG_SELECTED = @"BACK_IMG_SELECTED";
NSString *const MENU_UNSELECTED = @"MENU_UNSELECTED";
NSString *const MENU_SELECTED = @"MENU_SELECTED";
NSString *const LOGOUT_UNSELECTED = @"LOGOUT_UNSELECTED";
NSString *const LOGOUT_SELECTED = @"LOGOUT_SELECTED";
NSString *const ABOUT_UNSELECTED = @"ABOUT_UNSELECTED";
NSString *const ABOUT_SELECTED = @"ABOUT_SELECTED";
NSString *const WALKTHROUGH_UNSELECTED = @"WALKTHROUGH_UNSELECTED";
NSString *const WALKTHROUGH_SELECTED = @"WALKTHROUGH_SELECTED";
NSString *const SETTING_UNSELECTED = @"SETTING_UNSELECTED";
NSString *const SETTING_SELECTED = @"SETTING_SELECTED";
NSString *const THEME_UNSELECTED = @"THEME_UNSELECTED";
NSString *const THEME_SELECTED = @"THEME_SELECTED";
NSString *const MAINTENANCE_UNSELECTED = @"MAINTENANCE_UNSELECTED";
NSString *const NOTIFICATION_UNSELECTED = @"NOTIFICATION_UNSELECTED";
NSString *const NOTIFICATION_SELECTED = @"NOTIFICATION_SELECTED";
NSString *const BELL_UNSELECTED = @"BELL_UNSELECTED";
NSString *const BELL_SELECTED = @"BELL_SELECTED";
NSString *const CAMERA_UNSELECTED = @"CAMERA_UNSELECTED";
NSString *const CAMERA_SELECTED = @"CAMERA_SELECTED";
NSString *const USER_PROFILE_UNSELECTED = @"USER_PROFILE_UNSELECTED";
NSString *const USER_PROFILE_SELECTED = @"USER_PROFILE_SELECTED";
NSString *const HOME_UNSELECTED = @"HOME_UNSELECTED";
NSString *const HOME_SELECTED = @"HOME_SELECTED";
NSString *const NIGHT_UNSELECTED = @"NIGHT_UNSELECTED";
NSString *const NIGHT_SELECTED = @"NIGHT_SELECTED";
NSString *const LABEL_TEXT_COLOR = @"LABEL_TEXT_COLOR";
NSString *const UNSELECT_CHECKBOX_IMG = @"UNSELECT_CHECKBOX_IMG";
NSString *const SELECT_CHECKBOX_IMG = @"SELECT_CHECKBOX_IMG";
NSString *const TOGGLE_SLIDER_IMG = @"TOGGLE_SLIDER_IMG";
NSString *const TOGGLE_SLIDER_BAR_COLOR = @"TOGGLE_SLIDER_BAR_COLOR";
NSString *const ARRIVING_IMG_SELECTED = @"ARRIVING_IMG_SELECTED";
NSString *const ARRIVING_IMG_UNSELECTED = @"ARRIVING_IMG_UNSELECTED";
NSString *const LEAVING_IMG_SELECTED = @"LEAVING_IMG_SELECTED";
NSString *const LEAVING_IMG_UNSELECTED = @"LEAVING_IMG_UNSELECTED";
NSString *const MORNING_SELECTED = @"MORNING_SELECTED";
NSString *const MORNING_UNSELECTED = @"MORNING_UNSELECTED";
NSString *const TEXT_FIELD_BACKGROUND_COLOR = @"TEXT_FIELD_BACKGROUND_COLOR";
NSString *const SEPERATOR_VIEW = @"SEPERATOR_VIEW";
NSString *const HOME_CELL_BACKGROUND_COLOR = @"HOME_CELL_BACKGROUND_COLOR";
NSString *const LEFT_PANEL_BGCOLOR = @"LEFT_PANEL_BGCOLOR";
NSString *const SUBTITLE_TEXT_COLOR = @"SUBTITLE_TEXT_COLOR";
NSString *const TITLE_TEXT_COLOR = @"TITLE_TEXT_COLOR";
NSString *const LEFT_PANEL_LABEL_COLOR = @"LEFT_PANEL_LABEL_COLOR";
NSString *const LIVING_ROOM_UNSELECTED = @"LIVING_ROOM_UNSELECTED";
NSString *const LIVING_ROOM_SELECTED = @"LIVING_ROOM_SELECTED";
NSString *const DROP_ARROW = @"DROP_ARROW";
NSString *const FORWARD_ARROW = @"FORWARD_ARROW";
NSString *const BACK_ARROW = @"BACK_ARROW";
NSString *const HOME_LABEL_COLOR = @"HOME_LABEL_COLOR";
NSString *const LICENCE_SCANNER = @"LICENCE_SCANNER";
NSString *const SERIALNO_SCANNER = @"SERIALNO_SCANNER";
NSString *const SYSTEM_HEALTH_UNSEL = @"SYSTEM_HEALTH_UNSEL";
NSString *const MAID_SELECTED = @"MAID_SELECTED";
NSString *const HOLIDAY_SELECTED = @"HOLIDAY_SELECTED";
NSString *const RELAX_SELECTED = @"RELAX_SELECTED";
NSString *const PARTY_SELECTED = @"PARTY_SELECTED";
NSString *const STUDY_SELECTED = @"STUDY_SELECTED";
NSString *const MAID_UNSELECTED = @"MAID_UNSELECTED";
NSString *const HOLIDAY_UNSELECTED = @"HOLIDAY_UNSELECTED";
NSString *const RELAX_UNSELECTED = @"RELAX_UNSELECTED";
NSString *const PARTY_UNSELECTED = @"PARTY_UNSELECTED";
NSString *const STUDY_UNSELECTED = @"STUDY_UNSELECTED";
NSString *const S02 = @"S02";
NSString *const U02 = @"U02";
NSString *const S06 = @"S06";
NSString *const U06 = @"U06";
NSString *const S01 = @"S01";
NSString *const U01 = @"U01";
NSString *const S03 = @"S03";
NSString *const U03 = @"U03";
NSString *const S04 = @"S04";
NSString *const U04 = @"U04";
NSString *const S05 = @"S05";
NSString *const U05 = @"U05";
NSString *const TABLE_LAMP_UNSELECTED = @"TABLE_LAMP_UNSELECTED";
NSString *const TABLE_LAMP_SELECTED = @"TABLE_LAMP_SELECTED";
NSString *const WALL_LAMP_UNSELECTED = @"WALL_LAMP_UNSELECTED";
NSString *const WALL_LAMP_SELECTED = @"WALL_LAMP_SELECTED";
NSString *const CANDELIER_SELECTED = @"CANDELIER_SELECTED";
NSString *const CANDELIER_UNSELECTED = @"CANDELIER_UNSELECTED";
NSString *const BRIGHTNESS = @"BRIGHTNESS";
NSString *const CURTAIN_SELECTED = @"CURTAIN_SELECTED";
NSString *const CURTAIN_UNSELECTED = @"CURTAIN_UNSELECTED";
NSString *const TRASH_SELECTED = @"TRASH_SELECTED";
NSString *const TRASH_UNSELECTED = @"TRASH_UNSELECTED";
NSString *const ALL_OFF_UNSELECTED = @"ALL_OFF_UNSELECTED";
NSString *const HVAC_SELECTED = @"HVAC_SELECTED";
NSString *const HVAC_UNSELECTED = @"HVAC_UNSELECTED";
NSString *const SWING = @"SWING";
NSString *const VENT = @"VENT";
NSString *const EXPAND_ARROW = @"EXPAND_ARROW";
NSString *const CLOUD_CONNECTED = @"CLOUD_CONNECTED";
NSString *const CLOUD_DISCONNECTED = @"CLOUD_DISCONNECTED";

NSString *const AUTO_SELECTED = @"AUTO_SELECTED";
NSString *const AUTO_UNSELECTED = @"AUTO_UNSELECTED";
NSString *const COOL_SELECTED = @"COOL_SELECTED";
NSString *const COOL_UNSELECTED = @"COOL_UNSELECTED";
NSString *const FAN_SELECTED = @"FAN_SELECTED";
NSString *const FAN_UNSELECTED = @"FAN_UNSELECTED";
NSString *const HEAT_SELECTED = @"HEAT_SELECTED";
NSString *const HEAT_UNSELECTED = @"HEAT_UNSELECTED";
NSString *const DRY_SELECTED = @"DRY_SELECTED";
NSString *const DRY_UNSELECTED = @"DRY_UNSELECTED";
//NSString *const RADIO_ON = @"RADIO_ON";
//NSString *const RADIO_OFF = @"RADIO_OFF";
NSString *const PANICSWITCH_UNSELECTED = @"PANICSWITCH_UNSELECTED";
NSString *const PANICSWITCH_SELECTED = @"PANICSWITCH_SELECTED";
NSString *const GEYSER_SELECTED = @"GEYSER_SELECTED";
NSString *const GEYSER_UNSELECTED = @"GEYSER_UNSELECTED";
NSString *const SMOKEINPUT_UNSELECTED = @"SMOKEINPUT_UNSELECTED";
NSString *const SMOKEINPUT_SELECTED = @"SMOKEINPUT_SELECTED";
NSString *const DORBELL_SELECTED = @"DORBELL_SELECTED";
NSString *const DORBELL_UNSELECTED = @"DORBELL_UNSELECTED";
NSString *const LUTRON_LIGHT_UNSELECTED = @"LUTRON_LIGHT_UNSELECTED";
NSString *const LUTRON_LIGHT_SELECTED = @"LUTRON_LIGHT_SELECTED";
NSString *const ENTERTAINMENT_SELECTED = @"ENTERTAINMENT_SELECTED";
NSString *const ENTERTAINMENT_UNSELECTED = @"ENTERTAINMENT_UNSELECTED";
NSString *const GASINPUT_UNSELECTED = @"GASINPUT_UNSELECTED";
NSString *const GASINPUT_SELECTED = @"GASINPUT_SELECTED";
NSString *const LIGHT_SELECTED = @"LIGHT_SELECTED";
NSString *const LIGHT_UNSELECTED = @"LIGHT_UNSELECTED";
NSString *const GREEN_PANIC_SWITCH = @"GREEN_PANIC_SWITCH";
NSString *const YELLOW_PANIC_SWITCH = @"YELLOW_PANIC_SWITCH";
NSString *const RED_PANIC_SWITCH = @"RED_PANIC_SWITCH";
NSString *const PASSWORD_VIEW = @"PASSWORD_VIEW";
NSString *const DOORLATCH_LOCK = @"DOORLATCH_LOCK";
NSString *const DOORLATCH_UNLOCK = @"DOORLATCH_UNLOCK";
NSString *const FIREINPUT_SELECTED = @"FIREINPUT_SELECTED";
NSString *const FIREINPUT_UNSELECTED = @"FIREINPUT_UNSELECTED";
NSString *const SETTOPBOX_SELECTED = @"SETTOPBOX_SELECTED";
NSString *const SETTOPBOX_UNSELECTED = @"SETTOPBOX_UNSELECTED";
NSString *const ALERT_NOTIFY = @"ALERT_NOTIFY";
NSString *const WIFI_CONNECTED = @"WIFI_CONNECTED";
NSString *const WIFI_DISCONNECTED = @"WIFI_DISCONNECTED";
NSString *const ADD_ICON = @"ADD_ICON";
NSString *const UPARROW = @"UPARROW";
NSString *const TOGGLE_SWITCH_IMG = @"TOGGLE_SWITCH_IMG";
NSString *const SELECTED_TEXT_COLOR = @"SELECTED_TEXT_COLOR";
NSString *const SETTINGS_ABOUT = @"SETTINGS_ABOUT";
NSString *const SETTINGS_ADD_USER = @"SETTINGS_ADD_USER";
NSString *const SETTINGS_USER_PROFILE = @"SETTINGS_USER_PROFILE";
NSString *const SETTINGS_ADMIN_PASS = @"SETTINGS_ADMIN_PASS";
NSString *const SETTINGS_APP_FEEDBACK = @"SETTINGS_APP_FEEDBACK";
NSString *const SETTINGS_MANAGE_ROOM = @"SETTINGS_MANAGE_ROOM";
NSString *const SETTINGS_FORGOT_PASS = @"SETTINGS_FORGOT_PASS";
NSString *const SETTINGS_TECH_SUPPORT = @"SETTINGS_TECH_SUPPORT";
NSString *const SETTINGS_ADD_LICENSE = @"SETTINGS_ADD_LICENSE";
NSString *const SETTINGS_REG_DEVICES = @"SETTINGS_REG_DEVICES";
NSString *const SETTINGS_FIRMWARE_UPGRADE = @"SETTINGS_FIRMWARE_UPGRADE";
NSString *const SETTINGS_SYS_HEALTH = @"SETTINGS_SYS_HEALTH";
NSString *const SETTINGS_DATE_TIME = @"SETTINGS_DATE_TIME";
NSString *const SETTINGS_RESTORE_FACTORY = @"SETTINGS_RESTORE_FACTORY";
NSString *const SETTINGS_DIAGNOSIS = @"SETTINGS_DIAGNOSIS";
NSString *const SETTINGS_QOS = @"SETTINGS_QOS";
NSString *const SETTINGS_BACKUP = @"SETTINGS_BACKUP";
NSString *const SETTINGS_HOME_PROFILE = @"SETTINGS_HOME_PROFILE";
NSString *const SETTINGS_SYS_PROFILE = @"SETTINGS_SYS_PROFILE";
NSString *const SETTINGS_NEW_PROV = @"SETTINGS_NEW_PROV";
NSString *const SETTINGS_NETWORK_SET = @"SETTINGS_NETWORK_SET";
NSString *const SETTINGS_DOOR_UNIT = @"SETTINGS_DOOR_UNIT";


@implementation ThemeManager
@synthesize themePath;
+(ThemeManager *)sharedManger {
    static ThemeManager *themeManger;
    if (!themeManger) {
        themeManger = [[ThemeManager alloc] init];
    //    themeManger.themePath = [[WMSSettingsController sharedInstance]getThemeName];
    }
    return themeManger;
    
}

+(NSString *)getThemeName {
    
    switch (themeType) {
  //      case BLUE_THEME :
            return @"BlueTheme";
            
   //     case BLACK_THEME:
            return @"BlackTheme";
            
        default: return @"BlueTheme";
            break;
    }
    NSLog(@"wrong theme name");
    return @"";
}

-(NSString *)getImageNameForKey:(NSString *)key {
    NSString *path = [[NSBundle mainBundle] pathForResource:themePath ofType:@"plist"];
    NSDictionary *themeDict = [NSDictionary dictionaryWithContentsOfFile:path];
    return [themeDict valueForKey:key];
    
}
/*
-(UIColor *)colorFromKey:(NSString *)key {
    NSString *path = [[NSBundle mainBundle] pathForResource:themePath ofType:];
    NSDictionary *themeDict = [NSDictionary dictionaryWithContentsOfFile:path];
    
    get value for key
    NSString *colorStr = [themeDict valueForKey:key];
    if (colorStr == nil) {
        NSLog(@"Theme Manager: no specified key %@ ",key);
    } else {
        get array of color  
        NSArray *colorArray = [colorStr componentsSeparatedByString:@","];
        if (colorArray.count == 4) {
            NSString *redStr = [colorArray objectAtIndex:0];
            NSString *greenStr = [colorArray objectAtIndex:1];
            NSString *blueStr = [colorArray objectAtIndex:2];
            NSString *alphaStr = [colorArray objectAtIndex:3];
            float red = [redStr floatValue]/255.0f;
            float green = [greenStr floatValue]/255.0f;
            float blue = [blueStr floatValue]/255.0f;
            
            UIColor *color =[UIColor colorWithRed:red green:green blue:blue alpha:[alphaStr floatValue]];
            return color;
        }
    }
    DDLogError(@"Theme Manager:  array for color is != 4 ");
    return nil;
    
}
*/

@end
