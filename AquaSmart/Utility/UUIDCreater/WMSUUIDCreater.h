//
//  WMSUUIDCreater.h
//  AquaSmart
//
//  Created by Neotech on 18/12/15.
//  Copyright © 2015 Neotech. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WMSUUIDCreater : NSObject
+(instancetype)sharedInstance;
@property (nonatomic,strong)   NSMutableDictionary *cacheDict;
+ (NSString *)GetUUID;
@end
