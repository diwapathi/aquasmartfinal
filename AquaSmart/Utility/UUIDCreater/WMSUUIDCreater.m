//
//  WMSUUIDCreater.m
//  AquaSmart
//
//  Created by Neotech on 18/12/15.
//  Copyright © 2015 Neotech. All rights reserved.
//

#import "WMSUUIDCreater.h"
#import "OrderedDictionary.h"
@implementation WMSUUIDCreater
+(instancetype)sharedInstance {
    
    static WMSUUIDCreater *settingsController;
    static dispatch_once_t once;
    dispatch_once(&once, ^{
        settingsController = [[self alloc] init];
        settingsController.cacheDict = [[NSMutableDictionary alloc] init];
    });
    
    return settingsController;
}

+ (NSString *)GetUUID {
    
    CFUUIDRef theUUID = CFUUIDCreate(NULL);
    CFStringRef string = CFUUIDCreateString(NULL, theUUID);
    CFRelease(theUUID);
    return (__bridge_transfer NSString *)string;
}

//
//+(NSDictionary *)requestDictionary:(NSString *)cmdValue Data:(id)data {
//    
//    OrderedDictionary *orderDictionary = [[OrderedDictionary alloc] initWithCapacity:4];
////    [orderDictionary insertObject:_SIDVALUE forKey:_SID atIndex:0];
////    [orderDictionary insertObject:_SADVALUE forKey:_SAD atIndex:1];
////    [orderDictionary insertObject:_8502 forKey:_CMD atIndex:2];
////    [orderDictionary insertObject:data forKey:_DATA atIndex:3];
//    
//    return orderDictionary;
//    
//}
@end
