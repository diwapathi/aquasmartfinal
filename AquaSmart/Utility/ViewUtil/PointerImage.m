//
//  PointerImage.m
//  Homesmart
//
//  Created by Neotech on 01/07/16.
//  Copyright © 2016 Neotech. All rights reserved.
//

#import "PointerImage.h"

@implementation PointerImage
-(void)awakeFromNib {
    [self makeCircularImageView];
}

-(void)makeCircularImageView {
    CGFloat width = self.frame.size.width;
    [self setFrame:CGRectMake(self.frame.origin.x, self.frame.origin.y, width,width)];
    self.layer.cornerRadius = width/2.0;
    self.clipsToBounds = YES;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
