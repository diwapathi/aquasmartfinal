//
//  NSString+UrlEncoding.h
//  GaramMasala
//
//  Created by Aditya Aggarwal on 08/04/15.
//  Copyright (c) 2015 ClairvoyantMobileSolutions. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (UrlEncoding)
-(NSString*)urlEncodedString;

- (NSString *)saferStringValue;
@end
