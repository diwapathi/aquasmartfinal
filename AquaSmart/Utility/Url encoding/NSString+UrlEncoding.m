//
//  NSString+UrlEncoding.m
//  GaramMasala
//
//  Created by Aditya Aggarwal on 08/04/15.
//  Copyright (c) 2015 ClairvoyantMobileSolutions. All rights reserved.
//

#import "NSString+UrlEncoding.h"

@implementation NSString (UrlEncoding)

-(NSString*)urlEncodedString{
    NSString* unEncodedString = [self stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString* encodedString = [unEncodedString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    return encodedString;
}

- (NSString *)saferStringValue{
    if (self.length)
        return self;
    else
    { NSString *str = @"";
    
        return str;
    }
}

@end
