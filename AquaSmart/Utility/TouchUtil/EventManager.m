//
//  EventManager.m
//  EventManagementSample
//
//  Created by Neotech on 12/04/16.
//  Copyright © 2016 Neotech. All rights reserved.
//

#import "EventManager.h"
#import "TouchUtil.h"

@implementation EventManager
-(void)sendEvent:(UIEvent *)event {
    [super sendEvent:event];
   
    NSSet *allTouches = [event allTouches];
    if ([allTouches count] > 0)
    {
        UITouchPhase phase = ((UITouch *)[allTouches anyObject]).phase;
        if (phase == UITouchPhaseEnded)
        {
             NSLog(@"Event Caught");
            if ([[TouchUtil getInstance]isTimerStart]) {
                [[TouchUtil getInstance]restartTimer];
            }
            
        }
        
    }
}
@end
