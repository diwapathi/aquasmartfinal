//
//  TouchUtil.h
//  EventManagementSample
//
//  Created by Neotech on 12/04/16.
//  Copyright © 2016 Neotech. All rights reserved.
//

#import <Foundation/Foundation.h>
//#import "WMSSettingsController.h"
@interface TouchUtil : NSObject
@property(nonatomic,retain)NSTimer *myTimer;
+(TouchUtil*)getInstance;
@property (strong, nonatomic) NSTimer *timer;
@property (nonatomic) int timerCount;
@property (nonatomic) BOOL isTimerStart;
-(void)startTimer;
-(void)restartTimer;
-(void)stopTimer;

@end
