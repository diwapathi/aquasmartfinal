//
//  TouchUtil.m
//  EventManagementSample
//
//  Created by Neotech on 12/04/16.
//  Copyright © 2016 Neotech. All rights reserved.
//

#import "TouchUtil.h"

@implementation TouchUtil
@synthesize timer;
@synthesize timerCount;
@synthesize isTimerStart;
static TouchUtil *instance = nil;
+(TouchUtil*)getInstance {
    @synchronized(self)
    {
        if(instance==nil)
        {
            instance= [TouchUtil new];
        }
    }
    return instance;
}

-(void)startTimer {
    NSLog(@"Timer Start");
// NSInteger logoutTime = [[WMSSettingsController sharedInstance]getLogoutTime];
//    timerCount =  (int)(logoutTime);
    [[NSNotificationCenter defaultCenter] postNotificationName:@"TIMEOUT" object:nil];
    timerCount =  60;
    isTimerStart = YES;
    timer = [NSTimer scheduledTimerWithTimeInterval:1.0f
                                              target:self
                                            selector:@selector(updateTimerValue)
                                            userInfo:nil
                                             repeats:YES];
  
}

-(void)restartTimer {
    NSLog(@"Refresh Timer");
 //   NSInteger logoutTime = [[WMSSettingsController sharedInstance]getLogoutTime];
   // timerCount =  (int)(logoutTime);
     timerCount =  60;
}

-(void)updateTimerValue {
    if (isTimerStart) {
        timerCount -- ;
    }
    NSLog(@"timer count== %d",timerCount);
    if (timerCount <= 0) {
        isTimerStart = NO;
        //[self stopTimer];
        [self startTimer];
        return;
    }
}

-(void)stopTimer {
    
    [timer invalidate];
   // [[NSNotificationCenter defaultCenter] postNotificationName:@"TIMEOUT" object:nil];
}

@end
