//
//  EventManager.h
//  EventManagementSample
//
//  Created by Neotech on 12/04/16.
//  Copyright © 2016 Neotech. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AppDelegate.h"
@interface EventManager :UIApplication
-(void)sendEvent:(UIEvent *)event;

@end
