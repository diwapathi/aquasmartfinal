//
//  MenuCell.m
//  Homesmart
//
//  Created by Neotech on 26/04/16.
//  Copyright © 2016 Neotech. All rights reserved.
//

#import "MenuCell.h"
#import "ThemeManager.h"
@implementation MenuCell

- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setData:(NSString *)title andImage:(NSString *)imageName {
   // self.menuTitleLabel.textColor = [[ThemeManager sharedManger]colorFromKey:LEFT_PANEL_LABEL_COLOR];
   // self.seperatorView.backgroundColor =  [[ThemeManager sharedManger]colorFromKey:LEFT_PANEL_LABEL_COLOR];
    self.menuImageView.image = [UIImage imageNamed:imageName];
    self.menuTitleLabel.text = title;
}
@end
