//
//  ScheduleCell.m
//  Homesmart
//
//  Created by Neotech on 20/09/16.
//  Copyright © 2016 Neotech. All rights reserved.
//

#import "ScheduleCell.h"
#import "ThemeManager.h"

@implementation ScheduleCell

- (void)awakeFromNib {
    // Initialization code
}
-(void)setData:(Schedule *)schdule {
    self.forwardImageView.image = [UIImage imageNamed:[[ThemeManager sharedManger] getImageNameForKey:FORWARD_ARROW]];
   // self.timeLabel.textColor = [[ThemeManager sharedManger]colorFromKey:TITLE_TEXT_COLOR];
   // self.dayLabel.textColor = [[ThemeManager sharedManger]colorFromKey:SUBTITLE_TEXT_COLOR];
    NSMutableString *startString = [schdule.start mutableCopy];
    NSMutableString *stopString = [schdule.stop mutableCopy];
    [startString insertString:@":" atIndex:2];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"HH:mm";
    NSDate *startTime = [dateFormatter dateFromString:startString];
    dateFormatter.dateFormat = @"hh:mm a";
    NSString *startTimeString = [dateFormatter stringFromDate:startTime];
    if (stopString != nil && ![stopString isEqual:[NSNull null]] && ![stopString isEqualToString:@""]) {
        [stopString insertString:@":" atIndex:2];
        dateFormatter.dateFormat = @"HH:mm";
        NSDate *stopTime = [dateFormatter dateFromString:stopString];
        dateFormatter.dateFormat = @"hh:mm a";
        NSString *stopTimeString = [dateFormatter stringFromDate:stopTime];
        self.timeLabel.text = [NSString stringWithFormat:@"%@ - %@",startTimeString,stopTimeString];
    }
    else{
        self.timeLabel.text = [NSString stringWithFormat:@"%@ - XXXX",startTimeString];
    }
    NSArray *days = [self getWeekDays:schdule.days];
    NSString *daysStr = @"";
    if (days.count > 0) {
        NSString *filePath = [[NSBundle mainBundle]pathForResource:@"Weekdays" ofType:@"plist"];
        NSDictionary *dict = [NSDictionary dictionaryWithContentsOfFile:filePath];
        daysStr = @"(";
        for (int  i = 0; i < days.count; i++) {
            NSString *weeksday = [dict valueForKey:[days objectAtIndex:i]];
            if (i == days.count-1) {
                daysStr = [daysStr stringByAppendingString:[NSString stringWithFormat:@"%@",weeksday]];
            }
            else {
                daysStr = [daysStr stringByAppendingString:[NSString stringWithFormat:@"%@,",weeksday]];
            }
        }
        daysStr = [daysStr stringByAppendingString:@")"];
        
    }
    self.dayLabel.text = [NSString stringWithFormat:@"%@%@",schdule.days,daysStr];
}

-(NSArray *)getWeekDays:(NSString *)daylist {
    NSMutableArray *characters = [[NSMutableArray alloc] initWithCapacity:[daylist length]];
    for (int i=0; i < [daylist length]; i++) {
        NSString *ichar  = [NSString stringWithFormat:@"%c", [daylist characterAtIndex:i]];
        [characters addObject:ichar];
    }
    return [characters copy];
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
