//
//  MenuCell.h
//  Homesmart
//
//  Created by Neotech on 26/04/16.
//  Copyright © 2016 Neotech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *menuImageView;
@property (weak, nonatomic) IBOutlet UILabel *menuTitleLabel;
-(void)setData:(NSString *)title andImage:(NSString *)imageName;
@property (weak, nonatomic) IBOutlet UIView *seperatorView;
@end
