//
//  ScheduleCell.h
//  Homesmart
//
//  Created by Neotech on 20/09/16.
//  Copyright © 2016 Neotech. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Schedule.h"
@interface ScheduleCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *dayLabel;
@property (weak, nonatomic) IBOutlet UIImageView *forwardImageView;
-(void)setData:(Schedule *)schdule;
@end
