//
//  AppSettingsCell.h
//  Homesmart
//
//  Created by Neotech on 25/11/16.
//  Copyright © 2016 Neotech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppSettingsCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *settingsImageview;
-(void)setData:(NSDictionary *)dataDict;
@end
