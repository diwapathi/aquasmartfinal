//
//  AppSettingsCell.m
//  Homesmart
//
//  Created by Neotech on 25/11/16.
//  Copyright © 2016 Neotech. All rights reserved.
//

#import "AppSettingsCell.h"
#import "ThemeManager.h"

@implementation AppSettingsCell

- (void)awakeFromNib {
    // Initialization code
}

-(void)setData:(NSDictionary *)dataDict {
    self.titleLabel.text = [dataDict valueForKey:@"Name"];
    self.settingsImageview.image = [UIImage imageNamed:[[ThemeManager sharedManger]getImageNameForKey:[dataDict valueForKey:@"Image"]]];
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
