//
//  WMSBlackBoxCommands.m
//  AquaSmart
//
//  Created by Neotech on 12/01/16.
//  Copyright © 2016 Neotech. All rights reserved.
//

#import "WMSBlackBoxCommands.h"

@implementation WMSBlackBoxCommands
NSString *const _SID = @"sid";
NSString *const _SIDVALUE = @"9000";
NSString *const _BAD = @"bad";
NSString *const _SAD = @"sad";
NSString *const _SADVALUE = @"000";
NSString *const _SADVALUE1 = @"001";
NSString *const _BADVALUE = @"0100";
NSString *const _BADVALUE1 = @"0200";
NSString *const _CMD = @"cmd";
NSString *const _DATA = @"data";

NSString *const _12 = @"12";
NSString *const _18 = @"18";
NSString *const _19 = @"19";
NSString *const _20 = @"20";
NSString *const _21 = @"21";

NSString *const _01 = @"01";
NSString *const _02 = @"02";
NSString *const _03 = @"03";
NSString *const _04 = @"04";
NSString *const _05 = @"05";
NSString *const _06 = @"06";
NSString *const _07 = @"07";
NSString *const _08 = @"08";
 NSString *const _09 = @"09";
 NSString *const _10= @"10";
 NSString *const _11= @"11";
  NSString *const _13= @"13";
 NSString *const _14= @"14";
 NSString *const _15= @"15";
 NSString *const _16= @"16";
NSString *const _17= @"17";
NSString *const _8502= @"8502";
NSString *const _8019 = @"8019";
NSString *const _8275 = @"8275";
NSString *const _8532 = @"8532";
NSString *const _8531 = @"8531";




NSString *const _8211 = @"8211";
NSString *const _8212 = @"8212";    //get admin password
NSString *const _8210 = @"8210";
NSString *const _8208 = @"8208"; //check authentication type
NSString *const _8200 = @"8200"; //check registration type
NSString *const _8204 = @"8204"; // change admin password
NSString *const _8213 = @"8213"; //forgot admin password
NSString *const _1000 = @"1000"; //digital output single read
NSString *const _2000 = @"2000"; //digital output single on
NSString *const _2001 = @"2001"; //digital output single off
NSString *const _2002 = @"2002"; //digital output single toggle
NSString *const _2003 = @"2003"; //digital output single read
NSString *const _3000 = @"3000"; //analogue input single read
NSString *const _4000 = @"4000"; //analogue output single increment by n
NSString *const _4001 = @"4001"; //analogue output single decrement by n
NSString *const _4002 = @"4002"; //analogue outputset fixed value
NSString *const _5000 = @"5000"; //software input single read
NSString *const _6000 = @"6000"; //software output single read
NSString *const _8051 = @"8051"; //licence add
NSString *const _8008 = @"8008"; //Get network settings
NSString *const _8005 = @"8005"; //set network settings
NSString *const _8013 = @"8013"; //firmware upgrade
NSString *const _8014 = @"8014"; //provisioning upload
NSString *const _8015 = @"8015"; //get firmware version
NSString *const _9000 = @"9000"; //qos
NSString *const _8300 = @"8300"; //diagnosis
NSString *const _8103 = @"8103"; //read date and time
NSString *const _8102 = @"8102"; //set date and time
NSString *const _8101 = @"8101"; //set time
NSString *const _8100 = @"8100"; //rtc
NSString *const _8003 = @"8003"; //save bootloader data
NSString *const _8002 = @"8002"; //bootloader requ. chunk
NSString *const _8000 = @"8000"; //communication link reset
NSString *const _8510 = @"8510";  //pump not on/off reason
NSString *const _8511 = @"8511";  //get notification
NSString *const _7201 = @"7201"; //snooze
NSString *const _7200 = @"7200"; //alarms and alerts
NSString *const _7102 = @"7102"; //query
NSString *const _7101 = @"7101"; //terminate
NSString *const _7100 = @"7100"; //session
NSString *const _7005 = @"7005"; //profile immediate + schedule save
NSString *const _7004 = @"7004"; //profile schedule save
NSString *const _7003 = @"7003" ; //profile immediate save


NSString *const _7000 = @"7000"; //profile activate
NSString *const _6200 = @"6200"; //Software Output - Masked Read
NSString *const _6201 = @"6201"; //Software Output - Command + Param

NSString *const _5200 = @"5200"; //Software Input - Masked Read
NSString *const _4202 = @"4202"; //Analog Output - Masked Fixed Value
NSString *const _4201 = @"4201"; //Analog Output - Masked Decrement by n
NSString *const _4200 = @"4200"; //Analog Output - Masked Increment by n
NSString *const _3200 = @"3200"; //Analog Intput - Masked Read
NSString *const _2203 = @"2203"; //Digital Output - Masked Read
NSString *const _2202 = @"2202"; //Digital Output - Masked Toggle
NSString *const _2201 = @"2201"; //Digital Output - Masked Off
NSString *const _2200 = @"2200"; //Digital Output - Masked On
NSString *const _2100 = @"2100"; //Digital Output - Batch On
NSString *const _1100 = @"1100"; //Digital Input - Batch Read
NSString *const _2101 = @"2101"; //Digital Output - Batch Off
NSString *const _2102 = @"2102"; //Digital Output - Batch Toggle
NSString *const _2103 = @"2103"; //Digital Output - Batch Read
NSString *const _3100 = @"3100"; //Analog Input - Batch Read
NSString *const _4100 = @"4100"; //Analog Output - Batch Increment by n
NSString *const _4101 = @"4101"; //Analog Output - Batch Decrement by n
NSString *const _4102 = @"4102"; //Analog Output - Batch Set Fixed Value
NSString *const _5100 = @"5100"; //Software Input - Batch Read
NSString *const _6100 = @"6100"; //Software Output - Batch Read
NSString *const _8004 = @"8004"; //Read Device Status
NSString *const _8106 = @"8016"; //Read System health
NSString *const _8055 = @"8055";
NSString *const _8203 = @"8203";
NSString *const _8046 = @"8046";
NSString *const _7001 = @"7001";
NSString *const _8228 = @"8228";
NSString *const _8530 = @"8530";
NSString *const _7009 = @"7009";   // get tank schedule
NSString *const _8519 = @"8519";   //get current water level and pump status
NSString *const _8520 = @"8520";   // get current water level
NSString *const _8501 = @"8501";   //Tank Settings
NSString *const _8533 = @"8533";    // getting suction and reserve snsor settings 



//+(NSString *)getReasonStringByType:(ReasonType)reasonType {
//    
//    switch (reasonType) {
//            
//        case MOTOR_TRIGGER_FROM_SWITCH:
//            return @"sensor malfunctioning";
//        case MOTOR_TRIGGER_FROM_GUI:
//            return @"sensor hit more than 6 times in a minute";
//        case MOTOR_TRIGGER_FROM_SCHEDULE:
//            return @"pump state change more than 4 times in a minute";
//        case MOTOR_TRIGGER_FROM_DRY_RUN:
//            return @"pump wire disconnected";
//        case MOTOR_TRIGGER_FROM_AUTO_TASK:
//            return @"pump can’t be start due to wire disconnected";
//        case MOTOR_TRIGGER_FROM_POWER_FAILURE:
//            return @"";
//        default:
//            return @"know message";
//            
//    }
//}

@end
