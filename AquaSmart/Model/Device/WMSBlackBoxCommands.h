//
//  WMSBlackBoxCommands.h
//  AquaSmart
//
//  Created by Neotech on 12/01/16.
//  Copyright © 2016 Neotech. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WMSBlackBoxCommands : NSObject

//enum {
//    
//    SENSOR_MALFUNCTIONING = 1,
//    SENSOR_HIT_MORE_THAN_6_TIMES_IN_A_MINUTE,
//    PUMP_STATE_CHANGE_MORE_THAN_4_TIMES_IN_A_MINUTE,
//    PUMP_WIRE_DISCONNECTED,
//    PUMP_WIRE_CONNECTED,
//    PUMP_CANT_BE_START_DUE_TO_WIRE_DISCONNECTED,
//    PUMP_CANT_BE_START_DUE_TO_AC_MAINS_OFF,
//    AUTOMATED_TASK_NUMER_1_EXECUTED,
//    AUTOMATED_TASK_NUMER_2_EXECUTED,
//    AUTOMATED_TASK_NUMER_3_EXECUTED,
//    AUTOMATED_TASK_NUMER_4_EXECUTED,
//    AUTOMATED_TASK_NUMER_5_EXECUTED,
//    AUTOMATED_TASK_NUMER_6_EXECUTED,
//    SCHEDULE_CANT_BE_RUN,
//    SYSTEM_ON_BATTERY,
//    SYSTEM_ON_MAINS,
//    NEW_FIRMWARE_SUCCESSFULLY_UPGRADE,
//    NEW_FIRMWARE_UPGRADE_FAILURE,
//    BLACKBOX_NOT_CONNECTED_WITH_SERVER,
//    BLACKBOX_CONNECTED_WITH_SERVER,
//    
//};
//typedef unsigned int NotificationType;


//enum {
//    
//    MOTOR_TRIGGER_FROM_SWITCH = 1,
//    MOTOR_TRIGGER_FROM_GUI,
//    MOTOR_TRIGGER_FROM_SCHEDULE,
//    MOTOR_TRIGGER_FROM_DRY_RUN,
//    MOTOR_TRIGGER_FROM_AUTO_TASK,
//    MOTOR_TRIGGER_FROM_POWER_FAILURE,
//    
//};
//
//typedef unsigned int ReasonType;

extern NSString *const _SID;
extern NSString *const _SIDVALUE;
extern NSString *const _SAD;
extern NSString *const _SADVALUE;
extern NSString *const _BADVALUE;
extern NSString *const _SADVALUE1;
extern NSString *const _CMD;
extern NSString *const _DATA;
extern NSString *const _BAD;

extern NSString *const _12;
extern NSString *const _01;
extern NSString *const _02;
extern NSString *const _03;
extern NSString *const _04;
extern NSString *const _05;
extern NSString *const _06;
extern NSString *const _07;
extern NSString *const _08;
extern NSString *const _09;
extern NSString *const _10;
extern NSString *const _11;
extern NSString *const _12;
extern NSString *const _13;
extern NSString *const _14;
extern NSString *const _15;
extern NSString *const _16;
extern NSString *const _17;
extern NSString *const _18;


extern NSString *const _18;
extern NSString *const _19;
extern NSString *const _20;
extern NSString *const _21;
extern NSString *const _8211;
extern NSString *const _8212;

extern NSString *const _8200; //check registration type
extern NSString *const _8208; //check registration type
extern NSString *const _8204; // change admin password
extern NSString *const _8213; //forgot admin password
extern NSString *const _1000; //digital output single read
extern NSString *const _2000; //digital output single on
extern NSString *const _2001; //digital output single off
extern NSString *const _2002; //digital output single toggle
extern NSString *const _2003; //digital output single read
extern NSString *const _3000; //analogue input single read
extern NSString *const _4000; //analogue output single increment by n
extern NSString *const _4001; //analogue output single decrement by n
extern NSString *const _4002; //analogue outputset fixed value
extern NSString *const _5000; //software input single read
extern NSString *const _6000; //software output single read
extern NSString *const _8051; //licence add
extern NSString *const _8008; //Get network settings
extern NSString *const _8005; //set network settings
extern NSString *const _8013; //firmware upgrade
extern NSString *const _8015; //get firmware version
extern NSString *const _9000; //qos
extern NSString *const _8300; //diagnosis
extern NSString *const _8103; //read date and time
extern NSString *const _8102; //set date and time
extern NSString *const _8101; //set time
extern NSString *const _8100; //rtc
extern NSString *const _8003; //save bootloader data
extern NSString *const _8002; //bootloader requ. chunk
extern NSString *const _8000; //communication link reset
extern NSString *const _7201; //snooze
extern NSString *const _7200; //alarms and alerts
extern NSString *const _7102; //query
extern NSString *const _7101; //terminate
extern NSString *const _7100; //session
extern NSString *const _7005; //profile immediate + schedule save
extern NSString *const _7004; //profile schedule save
extern NSString *const _7003; //profile immediate save
extern NSString *const _7009;  //get tank schedule
extern NSString *const _8519;  //get current water level and pump status
extern NSString *const _8511;  //get notification
extern NSString *const _8520;//get current water level 
extern NSString *const _8510;    //get pump not on/off reason

extern NSString *const _7000; //profile activate
extern NSString *const _6200; //Software Output - Masked Read
extern NSString *const _6201; //Software Output - Command + Param

extern NSString *const _5200; //Software Input - Masked Read
extern NSString *const _4202; //Analog Output - Masked Fixed Value
extern NSString *const _4201; //Analog Output - Masked Decrement by n
extern NSString *const _4200; //Analog Output - Masked Increment by n
extern NSString *const _3200; //Analog Intput - Masked Read
extern NSString *const _2203; //Digital Output - Masked Read
extern NSString *const _2202; //Digital Output - Masked Toggle
extern NSString *const _2201; //Digital Output - Masked Off
extern NSString *const _2200; //Digital Output - Masked On

extern NSString *const _2100; //Digital Output - Batch On
extern NSString *const _1100; //Digital Input - Batch Read
extern NSString *const _2101; //Digital Output - Batch Off
extern NSString *const _2102; //Digital Output - Batch Toggle
extern NSString *const _2103; //Digital Output - Batch Read
extern NSString *const _3100; //Analog Input - Batch Read
extern NSString *const _4100; //Analog Output - Batch Increment by n
extern NSString *const _4101; //Analog Output - Batch Decrement by n
extern NSString *const _4102; //Analog Output - Batch Set Fixed Value
extern NSString *const _5100; //Software Input - Batch Read
extern NSString *const _6100; //Software Output - Batch Read
extern NSString *const _8210;
extern NSString *const _8004;
extern NSString *const _8106;
extern NSString *const _8055;
extern NSString *const _BADVALUE1;
extern NSString *const _8203;
extern NSString *const _8046;
extern NSString *const _7001;
extern NSString *const _8014;
extern NSString *const _8228;
extern NSString *const _8530;
extern NSString *const _8501;//set Tank Settings
extern NSString *const _8502;  //get tank settings
extern NSString *const _8019;  //get network settings
extern  NSString *const _8275;
extern  NSString *const _8532;
extern  NSString *const _8531;
extern  NSString *const _8533;  //get suction and reserve sensor settings 
//get iot mac id

 @end
