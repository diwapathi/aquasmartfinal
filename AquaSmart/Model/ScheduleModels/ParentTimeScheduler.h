//
//  ParentTimeScheduler.h
//  Homesmart
//
//  Created by Neotech on 19/09/16.
//  Copyright © 2016 Neotech. All rights reserved.
//

#import "JSONModel.h"

@interface ParentTimeScheduler : JSONModel
@property(nonatomic)int length;
@property (nonatomic,strong) NSMutableArray *timeSchedulers;
@property (nonatomic,strong) NSString *timeSchedulerInfo;
@property (nonatomic,strong) NSString *deviceAddr;
@end
