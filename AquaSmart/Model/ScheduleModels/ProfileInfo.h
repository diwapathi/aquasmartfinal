//
//  ProfileInfo.h
//  Homesmart
//
//  Created by Neotech on 30/08/16.
//  Copyright © 2016 Neotech. All rights reserved.
//

#import "JSONModel.h"

@interface ProfileInfo : JSONModel
@property (nonatomic,strong) NSString *profileName;
@property (nonatomic,strong) NSMutableArray *subProfiles;
-(id) copyWithZone:(NSZone *) zone;
@end
