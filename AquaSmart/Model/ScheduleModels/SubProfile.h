//
//  SubProfile.h
//  Homesmart
//
//  Created by Neotech on 30/08/16.
//  Copyright © 2016 Neotech. All rights reserved.
//

#import "JSONModel.h"

@interface SubProfile : JSONModel
@property (nonatomic,strong) NSString *subProfileName;
@property (nonatomic,strong) NSMutableArray *devices;
-(id) copyWithZone:(NSZone *) zone;
@end
