//
//  TimeScheduler.h
//  Homesmart
//
//  Created by Neotech on 19/09/16.
//  Copyright © 2016 Neotech. All rights reserved.
//

#import "JSONModel.h"

@interface TimeScheduler : JSONModel
@property (nonatomic,strong) NSString *time;
@property (nonatomic,strong) NSString *state;
@property (nonatomic,strong) NSString *port;
@property (nonatomic) int days;
@property (nonatomic,strong) NSString *deviceAddr;
@property (nonatomic,strong) NSString *day;
@property (nonatomic,strong) NSString *type;
@end
