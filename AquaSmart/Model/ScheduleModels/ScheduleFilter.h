//
//  ScheduleFilter.h
//  Homesmart
//
//  Created by Neotech on 19/09/16.
//  Copyright © 2016 Neotech. All rights reserved.
//

#import "JSONModel.h"

@interface ScheduleFilter : JSONModel
@property(nonatomic)int time;
@property (nonatomic,strong) NSString *days;
@property (nonatomic,strong) NSString *port;
@property (nonatomic,strong) NSString *startState;
@property (nonatomic,strong) NSString *stopState;
@end
