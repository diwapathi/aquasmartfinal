//
//  SubProfile.m
//  Homesmart
//
//  Created by Neotech on 30/08/16.
//  Copyright © 2016 Neotech. All rights reserved.
//

#import "SubProfile.h"
#import "Schedule.h"
//#import "SlaveInfo.h"
@implementation SubProfile
-(id) copyWithZone:(NSZone *) zone
{
    SubProfile *object = [[self class] allocWithZone:zone];
    object.subProfileName = [self.subProfileName copyWithZone:zone];
    NSMutableArray *subProfileArray = [[NSMutableArray alloc] init];
    for (id item in self.devices) {
        if ([item isKindOfClass:[Schedule class]]) {
            [subProfileArray addObject:[(Schedule *)item copyWithZone:zone]];
        }
//        if ([item isKindOfClass:[SlaveInfo class]]) {
//            [subProfileArray addObject:[(SlaveInfo *)item copyWithZone:zone]];
//        }
    }
    object.devices = [subProfileArray copyWithZone:zone];
    return object;
}
@end
