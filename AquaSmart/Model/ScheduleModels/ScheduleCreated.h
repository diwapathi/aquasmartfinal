//
//  ScheduleCreated.h
//  Homesmart
//
//  Created by Neotech on 03/10/16.
//  Copyright © 2016 Neotech. All rights reserved.
//

#import "JSONModel.h"

@interface ScheduleCreated : JSONModel
@property (nonatomic,strong) NSString *start;
@property (nonatomic,strong) NSString *stop;
@property (nonatomic,strong) NSString *days;
@property (nonatomic,strong) NSString *day;
@property (nonatomic,strong) NSString *profileName;
@end
