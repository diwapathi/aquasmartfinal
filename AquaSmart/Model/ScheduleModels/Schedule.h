//
//  Schedule.h
//  Homesmart
//
//  Created by Neotech on 30/08/16.
//  Copyright © 2016 Neotech. All rights reserved.
//

#import "JSONModel.h"
//#import "SubProfile.h"
@interface Schedule : JSONModel
@property(nonatomic,strong)NSString *sid;
@property (nonatomic,strong) NSString *start;
@property (nonatomic,strong) NSString *stop;
@property (nonatomic,strong) NSString *days;
//@property (nonatomic,strong) NSString *day;
//@property (nonatomic,strong) NSMutableArray *devices;

@end
