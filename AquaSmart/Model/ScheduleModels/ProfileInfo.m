//
//  ProfileInfo.m
//  Homesmart
//
//  Created by Neotech on 30/08/16.
//  Copyright © 2016 Neotech. All rights reserved.
//

#import "ProfileInfo.h"
#import "SubProfile.h"
@implementation ProfileInfo

-(id) copyWithZone:(NSZone *) zone
{
    ProfileInfo *object = [[self class] allocWithZone:zone];
    object.profileName = [self.profileName copyWithZone:zone];
    NSMutableArray *subProfileArray = [[NSMutableArray alloc] init];
    for (id item in self.subProfiles) {
        if ([item isKindOfClass:[SubProfile class]]) {
            [subProfileArray addObject:[(SubProfile *)item copyWithZone:zone]];
        }
    }
    object.subProfiles = [subProfileArray copyWithZone:zone];
    return object;
}
@end
