//
//  DatabaseModel.h
//  AquaSmart
//
//  Created by Neotech on 04/04/17.
//  Copyright © 2017 Mac User. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DatabaseModel : NSObject

@property (nonatomic,strong) NSString *date;
@property (nonatomic,strong) NSString *tankNum;
@property (nonatomic,strong) NSString *pumpState;
@property (nonatomic,strong) NSString *reason;
@property(nonatomic,strong) NSString *isRead;
@property(nonatomic,strong) NSString *NotificationField;
@end
