//
//  WMSStorageManager.m
//  AquaSmart
//
//  Created by Neotech on 02/02/16.
//  Copyright © 2016 Neotech. All rights reserved.
//

#import "WMSStorageManager.h"
#import "Device.h"
#import "Constants.h"
#import "DDLog.h"
#import "DatabaseModel.h"
#import "ScheduleCreated.h"
#import "WMSSettingsController.h"
#if DEBUG
static const DDLogLevel ddLogLevel = DDLogLevelVerbose;
#else
static const DDLogLevel ddLogLevel = DDLogLevelWarning;
#endif

#define AQUASMART_DB @"Aquasmart.db"
#define kEmptyString                   @" "

//Notification Table

#define NOTIFICATION_TABLE                      @"NOTIFICATION_TABLE"

#define DATE                                @"DATE"
#define TANK_NUM                            @"TANK_NUM"
#define PUMP_STATE                                @"PUMP_STATE"
#define REASON                              @"REASON"
#define ISREAD                               @"ISREAD"









//Notification Table
#define PROFILE_TABLE                      @"PROFILE_TABLE"
#define RESOURCE_TABLE                     @"RESOURCE_TABLE"
#define GUID                                @"GUID"
#define UID                                @"UID"
#define BUSID                              @"BUSID"
#define SLAVEID                            @"SLAVEID"
#define DEVICE_ID                          @"DEVICE_ID"
#define DEVICE_NAME                        @"DEVICE_NAME"
#define DEVICE_NO                          @"DEVICE_NO"
#define ROOM_ID                            @"ROOM_ID"
#define ROOM_NAME                          @"ROOM_NAME"
#define CATEGORY                           @"CATEGORY"
#define PORT1                              @"PORT1"
#define PORT2                              @"PORT2"
#define PORT3                              @"PORT3"
#define PORT4                              @"PORT4"
#define PORT1METHOD                        @"PORT1METHOD"
#define PORT2METHOD                        @"PORT2METHOD"
#define PORT3METHOD                        @"PORT3METHOD"
#define PORT4METHOD                        @"PORT4METHOD"
#define DEVICE_STATUS                      @"DEVICE_STATUS"
#define DEVICE_INTENSITY                   @"DEVICE_INTENSITY"
#define GADGET_NAME                        @"GADGET_NAME"
#define GADGET_ICON                        @"GADGET_ICON"
#define GADGET_ISHIDDEN                    @"GADGET_ISHIDDEN"

#define PROFILE_U_ID                @"PROFILE_U_ID"
#define PROFILE_ID                @"PROFILE_ID"
#define BAD                       @"BAD"
#define SAD                       @"SAD"
#define DO                        @"DO"
#define DI                        @"DI"
#define AO                        @"A0"
#define AI                        @"AI"
#define SO                        @"SO"
#define SI                        @"SI"


#define LUTRON_LIGHT_TABLE         @"LUTRON_LIGHT_TABLE"
#define PORT                       @"PORT"
#define INTENSITY                  @"INTENSITY"


#define LGHVAC_TABLE                @"LGHVAC_TABLE"
#define AC_STATUS                   @"AC_STATUS"
#define TEMPERATURE                 @"TEMPERATURE"
#define FAN_SPEED                   @"FAN_SPEED"
#define AC_MODE                     @"AC_MODE"
#define SWING                       @"SWING"
#define VENT                        @"VENT"
#define DI_DEVICE_TABLE             @"DI_DEVICE_TABLE"
#define DI_ROOM_LIST                @"DI_ROOM_LIST"
#define DI_ISACTIVATED              @"DI_ISACTIVATED"
#define DI_NOTIFICATION_TIME        @"DI_NOTIFICATION_TIME"


#define SCHEDULE_TABLE                @"SCHEDULE_TABLE"
#define DAY                         @"DAY"
#define DAYS                        @"DAYS"
#define START_TIME                  @"START_TIME"
#define END_TIME                    @"END_TIME"
#define PROFILE_NAME                    @"PROFILE_NAME"

#define ROOM_TABLE                    @"ROOM_TABLE"
#define ROOM_HIDDEN                   @"ROOM_HIDDEN"
@implementation WMSStorageManager

+(WMSStorageManager *)sharedInstance {
    
    static WMSStorageManager *sharedInstance;
    static dispatch_once_t once;
    
    dispatch_once(&once, ^{
            
        sharedInstance = [[WMSStorageManager alloc] init];
        
    });
    
    return sharedInstance;
}

-(id)init {
    
    if(self == [super init]) {
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsPath = [paths objectAtIndex:0];
        
        _databasePath = [documentsPath stringByAppendingPathComponent:@"Aquasmart.sqlite"];
        NSLog(@"Databse Path - %@",_databasePath);
        
        _database = [FMDatabase databaseWithPath:_databasePath];
        _databaseQueue = [FMDatabaseQueue databaseQueueWithPath:_databasePath];
        [_database open];
        [self createDatabase];
    }
    return self;
}

-(void)createDatabase {
    NSMutableArray *sqlStatements = [[NSMutableArray alloc] init];
    NSMutableString *notificationTableString = [NSMutableString stringWithFormat:@"CREATE TABLE IF NOT EXISTS '%@' ",NOTIFICATION_TABLE];
   // [notificationTableString appendFormat:@"( '%@' TEXT  NULL,",TANK_NUM];
    [notificationTableString appendFormat:@" ('%@'  TEXT NOT NULL,",DATE];
    [notificationTableString appendFormat:@" '%@' TEXT NOT NULL,",PUMP_STATE];
    [notificationTableString appendFormat:@" '%@' TEXT NOT NULL,",REASON];
    [notificationTableString appendFormat:@" '%@' INTEGER NOT NULL)",ISREAD];
   
    [sqlStatements addObject:notificationTableString];

   
    [self executeSQLStatements:sqlStatements];
}


- (void) executeSQLStatements:(NSArray*)sqlStatements
{
    NSUInteger statementsCount = [sqlStatements count];
    for (NSUInteger iSQLStatement = 0; iSQLStatement < statementsCount; ++iSQLStatement)
    {
        NSString *sql = [sqlStatements objectAtIndex:iSQLStatement];
        //  DASSERT(sql && [sql length] > 0);
        if ([_database executeUpdate:sql, nil])
        {
            
            NSLog(@"Successfully run SQL statement: '%@'", sql);
            
            DDLogVerbose(@"Successfully run SQL statement: '%@'", sql);
        }
        else
        {
            NSLog(@"Failed to run SQL statement: '%@' with error: '%@'", sql, [_database lastErrorMessage]);
            DDLogVerbose(@"Failed to run SQL statement: '%@' with error: '%@'", sql, [_database lastErrorMessage]);
        }
    }
}

/**
 *
 */
- (void) executeSQLStatement:(NSString *) sqlStatement
{
    NSArray *sqlStatements = [[NSArray alloc] initWithObjects:sqlStatement, nil];
    
    [self executeSQLStatements:sqlStatements];
    
}


#pragma mark - saving pump success and failure response
-(void)savePumpResponseInDB:(NSMutableArray *) pumpArray {
    [[WMSSettingsController sharedInstance]saveNotificationStatus:YES];
    if( [pumpArray count] == 0 )
        return;
    [_database beginDeferredTransaction];
    
    BOOL success = NO;
    for (NSInteger i = 0; i < [pumpArray count]; i++)
    {
        DatabaseModel *dataObj = [pumpArray objectAtIndex:i];
        NSMutableString*    statement   = [NSMutableString stringWithFormat:@"INSERT OR REPLACE INTO %@ (",NOTIFICATION_TABLE];
       // [statement appendFormat:@"%@,",TANK_NUM];
        [statement appendFormat:@"%@, ",DATE];
        [statement appendFormat:@"%@, ",PUMP_STATE];
        [statement appendFormat:@"%@, ",REASON];
        [statement appendFormat:@"%@ )",ISREAD ];
        [statement appendString:@" VALUES (?,?,?,?)"];
        
        NSMutableArray *args = [[NSMutableArray alloc] initWithObjects:dataObj.date,
                                dataObj.pumpState,dataObj.reason,dataObj.isRead,
                                nil];
        
        success = [_database executeUpdate:statement withArgumentsInArray:args];
        if (!success) {
            NSLog(@" Error on saving resources in database %@",[_database lastErrorMessage]);
            DDLogVerbose(@" Error on saving resources in database %@",[_database lastErrorMessage]);
            goto rollback;
        }
    }
    
rollback:
    
    if(success)
    {
        [_database commit];
        NSLog(@"Successfully saving pump state");
        DDLogVerbose(@"Successfully saving pump state");
    }
    else
    {
        [_database rollback];
        NSLog(@"error in saving pump state");
        DDLogVerbose(@"error in saving pump state");
    }
    
}

-(NSMutableArray *)fetchFromDB {
    NSMutableArray* ret = [[NSMutableArray alloc] init] ;
    NSMutableString* statement = [[NSMutableString alloc] initWithString:kEmptyString];
    [statement appendFormat:@"SELECT DISTINCT * FROM %@ ORDER BY DATE DESC", NOTIFICATION_TABLE];
    FMResultSet *rs = [_database executeQuery:statement];
    while ([rs next]) {
        DatabaseModel *data = [[DatabaseModel alloc]init];
        data.date = [rs stringForColumn:DATE];
        data.pumpState = [rs stringForColumn:PUMP_STATE];
        data.reason = [rs stringForColumn:REASON];
        data.isRead = [rs stringForColumn:ISREAD];
   
        [ret addObject:data];
    }
    return ret;
}


-(void)updateResponseInDB:(DatabaseModel *)data {
    if(data == nil) return;
    __block BOOL success = NO;
    NSMutableString* statement = [[NSMutableString alloc] initWithString:kEmptyString];
    [statement appendFormat:@"UPDATE %@ SET %@ = \'%@\'",NOTIFICATION_TABLE,ISREAD,data.isRead];
    [_databaseQueue inTransaction:^(FMDatabase *_db, BOOL *rollback) {
        success  = [_db executeUpdate:statement];
        if(!success) {
            NSLog(@" Error on updating profile in database %@",[_database lastErrorMessage]);
            DDLogVerbose(@" Error on updating profile in database %@",[_database lastErrorMessage]);
            *rollback = YES;
            return;
        }
        else {
            NSLog(@"Success in updating device status in database");
            DDLogVerbose(@"Success in updating device status in database");
        }
    }];
    
    //    BOOL success = NO;
    //    NSMutableString* statement = [[NSMutableString alloc] initWithString:kEmptyString];
    //    [statement appendFormat:@"UPDATE %@ SET %@ = \'%@\', %@ = '%@', %@ = '%@', %@ = '%@', %@ = '%@', %@ = '%@'  WHERE %@ = \'%@\'",PROFILE_TABLE,DO,profile.doStatus,DI,profile.diStatus,AO,profile.aoStatus,AI,profile.aiStatus,SO,profile.soStatus,SI,profile.siStatus,PROFILE_U_ID,profile.pUid];
    //    success = [_database executeUpdate:statement];
    //    if (!success) {
    //        DDLogVerbose(@" Error on updating profile in database %@",[_database lastErrorMessage]);
    //    }
    //    else {
    //       DDLogVerbose(@"Success in updating device status in database");
    //    }
}







@end
/*//-(NSMutableArray *)fetchScheduleList:(NSString *)profileName {
 //    NSMutableArray* ret = [[NSMutableArray alloc] init] ;
 //    NSMutableString* statement = [[NSMutableString alloc] initWithString:kEmptyString];
 //    [statement appendFormat:@"SELECT DISTINCT * FROM %@ WHERE %@ = \'%@\'",SCHEDULE_TABLE,PROFILE_NAME,profileName];
 //    FMResultSet *rs = [_database executeQuery:statement];
 //    while ([rs next]) {
 //        ScheduleCreated *schduleCreated = [[ScheduleCreated alloc]init];
 //        schduleCreated.day = [rs stringForColumn:DAY];
 //        schduleCreated.days = [rs stringForColumn:DAYS];
 //        schduleCreated.start = [rs stringForColumn:START_TIME];
 //        schduleCreated.stop = [rs stringForColumn:END_TIME];
 //        schduleCreated.profileName = [rs stringForColumn:PROFILE_NAME];
 //        [ret addObject:schduleCreated];
 //    }
 //    return ret;
 //
 //}
 //
 //-(void)deleteSchduleFromDB:(ScheduleCreated *)schduleCreated {
 //     BOOL success = NO;
 //    NSMutableString*    statement   = [[NSMutableString alloc] initWithString:kEmptyString] ;
 //    [statement appendFormat:@"DELETE FROM %@ WHERE %@ = \'%@\' AND %@ = \'%@\' AND %@ = \'%@\'",SCHEDULE_TABLE,START_TIME,schduleCreated.start,END_TIME,schduleCreated.stop,DAYS,schduleCreated.days];
 //    success = [_database executeUpdate:statement];
 //    DDLogVerbose(@"Success in deleting schedule");
 //    if (!success) {
 //        DDLogVerbose(@" Error in deleting schedule %@",[_database lastErrorMessage]);
 //    }
 //}
 //*/
