//
//  WMSStorageManager.h
//  AquaSmart
//
//  Created by Neotech on 02/02/16.
//  Copyright © 2016 Neotech. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FMDatabase.h"
#import "FMDatabaseQueue.h"
#import "DatabaseModel.h"
@class DiDevice;
@class Profile;
@class Room;
@class RoomDeviceInfo;
@class Device;
@class LGHVACDevice;
@class LutronDevice;
@class ScheduleCreated;
//#import "WMSNotification.h"
//#import "WMSGetEventModel.h"
@protocol DatabaseInsertionProtocol <NSObject>

-(void)dataInsertedIntoDB;
@end

@interface WMSStorageManager : NSObject {
    
    FMDatabase *_database;
    NSString *_databasePath;
    FMDatabaseQueue *_databaseQueue;
}

+(WMSStorageManager *)sharedInstance;
-(void)savePumpResponseInDB:(NSMutableArray *) pumpArray;

-(NSMutableArray *)fetchFromDB;

@property (nonatomic,weak) id<DatabaseInsertionProtocol> delegate;

-(void)insertResources:(NSMutableArray *) devicesArray;
-(NSMutableArray *)getAllUniqueRoomInfo;
-(void)insertRoomListInDB:(NSMutableArray *) roomArray;
-(NSMutableArray *)fetchResources;
-(NSMutableArray *)getAllRoomsInfo;
-(NSMutableDictionary *)getAllGadgetsInRoom:(NSArray*)roomList and :(NSArray *)deviceList;
-(NSMutableArray *)getBusSlaveAddressMap;
-(NSMutableArray *)getProfileListById:(NSString *)profileId;
//-(NSMutableArray *)getProfileList;
-(void)updateProfileResponseInDB:(Profile *)profile;
-(void)retrieveProfiledData;
-(void)insertLutronLightStatusInDatabase:(NSArray *) devicesArray;
-(NSMutableArray *)fetchLutronLightData;
-(void)updateRoomInfoInDeviceDB:(Room *)roomInfo;
-(void)updateGadgetsInfoInDeviceDB:(RoomDeviceInfo *)roomInfo;

-(void)insertLghvacStatusInDatabase:(NSArray *)devicesArray;
-(NSMutableArray *)fetchLghvacData;

-(void)saveDiDevice:(DiDevice *)device;
-(void)insertDiDevices:(NSMutableArray *) devicesArray;
-(NSMutableArray *)getDiDevices;
-(NSMutableArray *)getDiDevicesfromDeviceTable;
-(void)updateStatusOfLghvacInDB:(LGHVACDevice *)lghvac;
-(void)updateStatusOfLutronLightInDB:(LutronDevice *)lutron ;
-(void)insertNewSchedule:(ScheduleCreated *)schduleCreated;

-(void)updateResponseInDB:(DatabaseModel *)data;

@end
