//
//  BuyerRegistrationVCOne.m
//  AquaSmart
//
//  Created by Mac User on 09/01/17.
//  Copyright © 2017 Mac User. All rights reserved.
//

#import "BuyerRegistrationVCOne.h"
#import "BuyerRegistrationVCTwo.h"
#import "HSVCScanner.h"

@interface BuyerRegistrationVCOne ()<UITextFieldDelegate,PacketReciveProtocol,ScannerProtocol,UITextViewDelegate,UIGestureRecognizerDelegate>
@end

@implementation BuyerRegistrationVCOne
{
    NSTimer *timer;
    BOOL isTermAccepted;
}
- (void)viewDidLoad {
  //  NSString *notificationName = @"WIFI_AVAILABLE";
    isTermAccepted = NO;
    [super viewDidLoad];
    [self setupOutlets];
    //[self.submitBtn setEnabled:NO];
    
    _submitBtn.backgroundColor = [UIColor colorWithRed:192/255.0 green:192/255.0 blue:192/255.0 alpha:1];
    [_submitBtn setTintColor:[UIColor colorWithRed:192/255.0 green:192/255.0 blue:192/255.0 alpha:1]];
    
    //self.submitBtn.enabled = NO;
    //self.serialNumTF.text = @"ARE403100003";
   // self.licenseNumTF.text = @"S0FB3CC5TR4409C02K54F5aP";

    self.serialNumTF.text = @"ARE403100005";
    self.licenseNumTF.text = @"440BC04K54F7aS0FB3CC5TRF";
    
    
//    self.serialNumTF.text = @"ARE403100009";
//    self.licenseNumTF.text = @"FC08K54FBaS0FB6KC1UN740Z";
    
    //005
//    self.serialNumTF.text = @"";
//    self.licenseNumTF.text = @"";
    
    //sukant aqua
//    self.serialNumTF.text = @"ARE406100006";
//    self.licenseNumTF.text = @"740CC05K54F8aS0FB6KC1T0F";
    
//    self.serialNumTF.text = @"ARE406100007";
//    self.licenseNumTF.text = @"06K54F9aS0FB6KC1T0740DCA";

    //hides keyboard on tap
    UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    [self.view addGestureRecognizer:gestureRecognizer];
    
   self.navigationController.navigationBar.hidden = YES;  //hides the navigation bar
    // Do any additional setup after loading the view.
    
    
}
- (IBAction)serialCheckTF:(id)sender

{
    if (self.serialNumTF.text.length <= 0) {
        [self.view makeToast:@"Serial number should be of 12 characters."
                    duration:2.0
                    position:CSToastPositionCenter];
       // [self.view makeToast:NSLocalizedString(@"Please fill serial number.", nil)];
        [self.view endEditing:YES];
        return;
    
    
}
}
- (IBAction)licenseNumTF:(id)sender
{
    
    if (self.licenseNumTF.text.length <= 0) {
        [self.view makeToast:@"License number should be of 24 characters."
                    duration:2.0
                    position:CSToastPositionCenter];
       // [self.view makeToast:NSLocalizedString(@"Please fill license number.", nil)];
        [self.view endEditing:YES];
        return;
        
        
    }
    
}

//- (void)onNetWorkChange:(NSNotification *)notification{
//    
//      [[SplashScreenVC sharedInstance]setWifiStatusIcon];
//    
//}

- (void)customIOS7dialogButtonTouchUpInside: (CustomIOSAlertView *)alertView clickedButtonAtIndex: (NSInteger)buttonIndex
{
    // NSLog(@"Delegate: Button at position %d is clicked on alertView %d.", (int)buttonIndex, (int)[alertView tag]);
    [alertView close];
}

- (IBAction)termsAndConditionsButtonClicked:(id)sender
{
    
    UIAlertController * alert=[UIAlertController
                               
                             
                               alertControllerWithTitle:@""
                               message:NSLocalizedString(@"Term Condition",nil) preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* closeButton = [UIAlertAction
                                  actionWithTitle:@"DECLINE"
                                  style:UIAlertActionStyleDefault
                                  handler:^(UIAlertAction * action)
                                  {
                                      
                                    
                                      [self dismissViewControllerAnimated:YES completion:nil];
                                      
                                  }
                                  ];
    UIAlertAction* agreeButton = [UIAlertAction
                                  actionWithTitle:@"ACCEPT"
                                  style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
                                  {
                                      isTermAccepted = YES;
                                      //self.submitBtn.enabled = YES;
                                      _submitBtn.backgroundColor = [UIColor colorWithRed:90/255.0 green:154/255.0 blue:250/255.0 alpha:1];
                                  }
                                  
                                  ];
    
    [alert addAction:closeButton];
    [alert addAction:agreeButton];
    
    [self presentViewController:alert animated:YES completion:nil];
     

    
    
    // Here we need to pass a full frame
  /*  alertView = [[CustomIOSAlertView alloc] init];
    
    // Add some custom content to the alert view
    [alertView setContainerView:[self createDemoView]];
    
    // Modify the parameters
    //[alertView setButtonTitles:[NSMutableArray arrayWithObjects:@"Decline", @"Accept", nil]];
    [alertView setDelegate:self];
    
    // You may use a Block, rather than a delegate.
    [alertView setOnButtonTouchUpInside:^(CustomIOSAlertView *alertView, int buttonIndex) {
        NSLog(@"Block: Button at position %d is clicked on alertView %d.", buttonIndex, (int)[alertView tag]);
    }];
    
    [alertView setUseMotionEffects:true];
    
    // And launch the dialog
    [alertView show];*/
   
}




- (UIView *)createDemoView
{
 
    UITextView *myTextView = [[UITextView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 300)];
    myTextView.textColor = [UIColor colorWithRed:0/256.0 green:84/256.0 blue:129/256.0 alpha:1.0];
    myTextView.font = [UIFont fontWithName:@"Helvetica-Bold" size:12];
    myTextView.backgroundColor=[UIColor whiteColor];
    myTextView.layer.cornerRadius= 5;
    myTextView.editable=false;
    myTextView.text = @"Water is an integral part of land/soil productivity. Its misuse can cause both degradation and erosion of soils. Management of water resource is considered to necessary for well being of human life as well as crop yields.  There is a gap between the seasonal availability of water and its equitable supply throughout the year. Accordingly the storage of water and passage through soils is very important. It is well known that about 70% area in this country is subject to varying degree of water stress. There are areas with heavy rainfall but water problems become predominant during non-monsoon periods. It is thus a matter of concern to create a redistribution system and requirements as and when it is needed. Although there are many methods for water management, the two important methods are mentioned as follows. (1) Watershed management ,(2) Rainwater harvesting,(3)Watershed Management  A watershed is an area bounded by the divide line of water flow so that a distinct drainage basin of any small or big water course or stream can be identified. The rain falling over this area will flow through only one point of the whole watershed. In other words, the entire area will be drained only by one stream or water course. In this way we will have definitive water resource which can be assessed and analyzed for planning for the optimum utilization through ground water, wells, tube wells, small ponds, bigger tanks or reservoirs. Watershed management is very important for rainfall and resultant run-off. More than 900 watersheds of the flood prone rivers have been identified and are at present in operation. The development of delayed through propagation of water harvesting technology is also based on the concept of micro watershed.Himalayas are one of the most critical watersheds in the world.The damage to reservoirs and irrigation systems and misused Himalayan slopes is mounting as are the costs for the control for the control measures during the folood season every year.The vast hydroelectric power potential can be harnessed from the Himalayas watersheds on a sustainable basis.(2)Rain water Harvesting:Water harvesting technologies have established the economic and practical feasibility for inclusion in integrated watershed management plans.a number of such structures in the Hirakud catchment have revealed that these are desirable for protection of land,restoration of degraded land for creation of micro irrigation potential of reuse of water.This would also help in increased production based productivity for generation of employment of opportunities.The concept of watershed management has been extended to agro-industrial watersheds which take care of agro-industrial development.";
    myTextView.delegate = self;
    UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(10,myTextView.frame.size.height+myTextView.frame.origin.y+15, self.view.frame.size.width, 2)];
    lineView.backgroundColor = [UIColor blueColor];
    UILabel *urllbl=[[UILabel alloc]initWithFrame:CGRectMake(myTextView.frame.origin.x, myTextView.frame.size.height+myTextView.frame.origin.y+17, self.view.frame.size.width, 42)];
    urllbl.textColor = [UIColor blueColor];
    urllbl.font = [UIFont fontWithName:@"Helvetica-Bold" size:14];
    urllbl.textAlignment=NSTextAlignmentCenter;
    urllbl.text=@"www.neotechindia.com";
    [urllbl setUserInteractionEnabled:YES];
    UITapGestureRecognizer *tap= [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(openLink:)];//your action selector
    [tap setNumberOfTapsRequired:1];
    urllbl.userInteractionEnabled= YES;
    [urllbl addGestureRecognizer:tap];
    
    UIButton *saveBtn = [[UIButton alloc]initWithFrame:CGRectMake(20, myTextView.frame.size.height+myTextView.frame.origin.y+70, self.view.frame.size.width/2, 42)];
    [saveBtn setTitle:@"Decline" forState:UIControlStateNormal];
    [saveBtn addTarget:self action:@selector(declineClicked:) forControlEvents:UIControlEventTouchUpInside];
    saveBtn.layer.cornerRadius=5;
    saveBtn.layer.backgroundColor=[UIColor redColor].CGColor;
    UIButton *removeBtn = [[UIButton alloc]initWithFrame:CGRectMake(saveBtn.frame.size.width+saveBtn.frame.origin.x+50, myTextView.frame.size.height+ myTextView.frame.origin.y+70, self.view.frame.size.width/2, 42)];
    [removeBtn setTitle:@"Accept" forState:UIControlStateNormal];
    [removeBtn addTarget:self action:@selector(acceptClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    removeBtn.layer.cornerRadius=5;
    removeBtn.layer.backgroundColor=[UIColor redColor].CGColor;
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height/2)];
    [view addSubview:myTextView];
    [view addSubview:lineView];
    [view addSubview:urllbl];
    [view addSubview:saveBtn];
    [view addSubview:removeBtn];
    [self.view addSubview:view];
    return view;
}

-(void)openLink:(id)sender{
    //write your code action
  [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.stackoverflow.com"]];
    
}

-(void)declineClicked:(id)sender{
    //write your code action
    [alertView close];
}


-(void)acceptClicked:(id)sender{
    //write your code action
     isTermAccepted = YES;
     [alertView close];
}

-(void)backClicked {
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)viewDidLayoutSubviews {
    [self.constraintContentViewHeight setConstant:400-(self.view.frame.size.height)];
}
-(void)setTopHeaderView {
    NSDictionary *navDict = @{
                              TITLE_TEXT :NSLocalizedString(@"Registration", nil),
                              IS_BACK_HIDDEN:@"Yes",
                              
                              };
    [[AppDelegate appDelegate].superVC setHeaderViewData:navDict];
}
- (IBAction)licenseScanButtonClicked:(id)sender {
    UIStoryboard *mainStroyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    HSVCScanner *scannerVC = [mainStroyBoard instantiateViewControllerWithIdentifier:@"HSVCScanner"];
    scannerVC.delegate = self;
    scannerVC.isLicenceSelected = YES;
    [self.navigationController pushViewController:scannerVC animated:YES];
}

#pragma mark - IBActions -
- (IBAction)serialScanButtonClicked:(id)sender {
    UIStoryboard *mainStroyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    HSVCScanner *scannerVC = [mainStroyBoard instantiateViewControllerWithIdentifier:@"HSVCScanner"];
    scannerVC.delegate = self;
    scannerVC.isLicenceSelected = NO;
    [self.navigationController pushViewController:scannerVC animated:YES];
}
//Delegate methods for serial and license scan
-(void)serialScanComplete:(NSString *)codeString {
    self.serialNumTF.text = codeString;
}

-(void)licenceScanComplete:(NSString *)codeString {
    self.licenseNumTF.text = codeString;
}



-(void)changeNetworkStatus
{


}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - hides keyboard on touch of screen
-(void)hideKeyboard
{

    [_serialNumTF resignFirstResponder];
    [_licenseNumTF resignFirstResponder];


}
#pragma mark - set the textfield tag
- (void)setupOutlets
{
    self.serialNumTF.delegate = self;
    self.serialNumTF.tag = 1;
    
    self.licenseNumTF.delegate = self;
    self.licenseNumTF.tag = 2;
    
   
}

#pragma mark - moving cursor to the next textfield
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{

    NSInteger nextTag = textField.tag + 1;
    [self jumpToNextTextField:textField withTag:nextTag];
    return NO;

}

- (void)jumpToNextTextField:(UITextField *)textField withTag:(NSInteger)tag
{
   
    UIResponder *nextResponder = [self.view viewWithTag:tag];
    
    if ([nextResponder isKindOfClass:[UITextField class]]) {
        // If there is a next responder and it is a textfield, then it becomes first responder.
        [nextResponder becomeFirstResponder];
    }
    else {
        // If there is not then removes the keyboard.
        [textField resignFirstResponder];
    }
}

#pragma mark - setting length for texfields
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if (textField == self.serialNumTF ) {
        if (self.serialNumTF.text.length == 12){
            if ([string isEqualToString:@""]) {
                return YES;
            }
            else
                return NO;
        }
        else{
            if ([string isEqualToString:@""]) {
                return YES;
            }
            NSString *abnRegex = @"[A-Za-z0-9]+";
            NSPredicate *abnTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", abnRegex];
            return [abnTest evaluateWithObject:string];
        }
    }
    if (textField == self.licenseNumTF ) {
        if (self.licenseNumTF.text.length == 24){
            if ([string isEqualToString:@""]) {
                return YES;
            }
            else
                return NO;
        }
        else
            if ([string isEqualToString:@" "]) {
                return NO;
            }
        return YES;
    }
    return YES;
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}

-(void)setInitialConfiguration {
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(endEditing)];
    [tapRecognizer setNumberOfTapsRequired:1];
    [self.view addGestureRecognizer:tapRecognizer];
   
}

#pragma mark - submit button event
- (IBAction)submitButtonClicked:(id)sender {
    
    if (isTermAccepted == NO) {
        [self.view makeToast:NSLocalizedString(@"Please Accept Terms and Condition First", nil)];
    }
    else
    {
    if (self.serialNumTF.text.length <= 0 ) {
        //[self.serialNumTF becomeFirstResponder];
        [self.view makeToast:NSLocalizedString(@"Please fill serial number.", nil)];
         [self.view endEditing:YES];
       
        return;
    }
    if ([self checkIfStringContainsUppercase:self.serialNumTF.text]) {
        

        [self.view makeToast:NSLocalizedString(@"Serial number should contain uppercase characters.", nil)];
         [self.view endEditing:YES];
               return;
    }
      if (self.serialNumTF.text.length <12) {
         

        [self.view makeToast:NSLocalizedString(@"Serial number should contain atleast 12 characters", nil)];
          [self.view endEditing:YES];
        
        return;
    }
      if (self.licenseNumTF.text.length <= 0) {
        

        [self.view makeToast:NSLocalizedString(@"Please fill license number.", nil)];
          [self.view endEditing:YES];
       
        return;
         
    }
      if (self.licenseNumTF.text.length <24) {
                [self.view makeToast:NSLocalizedString(@"License number should contain atleast 24 characters", nil)];
          [self.view endEditing:YES];
       
        return;
    }
    
   
    
        [SVProgressHUD showWithStatus:@"Loading.."];
        [self callDeviceAuthenticationApi];

        return;

    
    }
}




-(BOOL)checkIfStringContainsUppercase:(NSString *)str {
    if ([[str uppercaseStringWithLocale:[NSLocale currentLocale]] isEqualToString:str])
    {   // ... s is uppercase ...
        return NO;
    }
    else
    {   // ... s is not all uppercase ...
        return YES;
    }
}

#pragma mark - Device Authentication Api call -
-(void)callDeviceAuthenticationApi{
    OrderedDictionary *dataDict = [[OrderedDictionary alloc]initWithCapacity:3];
    [dataDict insertObject:@"1" forKey:_01 atIndex:0];
    [dataDict insertObject:self.serialNumTF.text forKey:_02 atIndex:1];
    [dataDict insertObject:self.licenseNumTF.text forKey:_03 atIndex:2];
    
    OrderedDictionary *requestDict = [[OrderedDictionary alloc]initWithCapacity:5];
    [requestDict insertObject:_SIDVALUE forKey:_SID atIndex:0];
    //[requestDict insertObject:_BADVALUE forKey:_BAD atIndex:1];
    [requestDict insertObject:_SADVALUE forKey:_SAD atIndex:1];
    [requestDict insertObject:_8228 forKey:_CMD atIndex:2];
    [requestDict insertObject:dataDict forKey:_DATA atIndex:3];
    NSError *writeError;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:requestDict options:kNilOptions error:&writeError];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    [SVProgressHUD showWithStatus:@"Please Wait.."];

    [TcpPacketClass sharedInstance].delegate = self;
    
   
       [[TcpPacketClass sharedInstance]writeOut:jsonString second:[requestDict objectForKey:_CMD]];
    timer = [NSTimer scheduledTimerWithTimeInterval: 5.0 target:self selector:@selector(updateCounter:) userInfo:nil repeats: NO];

}

#pragma dismiss progress loader wether responce appear or not within 5 second:
- (void)updateCounter:(NSTimer *)theTimer {
    [theTimer invalidate];
    [SVProgressHUD dismiss];
    [self.view makeToast:NSLocalizedString(@"Something went wrong Please try Again!!!", nil)];
}

-(void)getDeviceAuthenticationResponse:(NSDictionary *)jsonDict
{
    [timer invalidate];
    [SVProgressHUD dismiss];
    NSDictionary *dataDict = [jsonDict valueForKey:_DATA];
    if ([[dataDict valueForKey:_02] isEqualToString:@"1"]) {
        [self.view makeToast:NSLocalizedString(@"Serial number does not match", nil)];
        return;
    }
    else if ([[dataDict valueForKey:_02] isEqualToString:@"2"]) {
        [self.view makeToast:NSLocalizedString(@"License number does not match", nil)];
        return;
    }
    else if ([[dataDict valueForKey:_02] isEqualToString:@"0"]) {
        
        UIStoryboard *mainStroyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        NSString *status = [[WMSSettingsController sharedInstance]getDeviceAuthenticationStatus];
    
        //Save serial number &license number
        [[WMSSettingsController sharedInstance] saveSerialNumber:self.serialNumTF.text];
        [[WMSSettingsController sharedInstance]saveLicenceNumber:self.licenseNumTF.text];
        
        
        //if first time registration
        if ([status isEqualToString:@"2"]) {
            BuyerRegistrationVCTwo *firstRegistration = [mainStroyBoard instantiateViewControllerWithIdentifier:@"BuyerRegistrationVCTwo"];
             firstRegistration.licenseNumber = self.licenseNumTF.text;
             firstRegistration.serialNumber = self.serialNumTF.text;
            [self.navigationController pushViewController:firstRegistration animated:YES];
        }
        else {
            DeviceRegistrationVC *clientRegistration = [mainStroyBoard instantiateViewControllerWithIdentifier:@"DeviceRegistrationVC"];
            clientRegistration.licenseNumber = self.licenseNumTF.text;
            clientRegistration.serialNumber = self.serialNumTF.text;
            [self.navigationController pushViewController:clientRegistration animated:YES];
        }


        

    }
    
    else
    {
    
     [self.view makeToast:NSLocalizedString(@"Error occurred", nil)];
    }

    }

-(NetworkStatus)checkForWifiConnection {
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    [reachability startNotifier];
    NetworkStatus status = [reachability currentReachabilityStatus];
    return status;
}



-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    NetworkStatus status = [self checkForWifiConnection];
    if(status == NotReachable || status == ReachableViaWWAN) {
        [self.navigationController.view makeToast:NSLocalizedString(@"Please check your wifi connection", nil)];
    }
    [self setTopHeaderView];
   
}

-(void)endEditing {
    [self.view endEditing:YES];
}



-(void)viewDidAppear:(BOOL)animated
{
  //[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onNetWorkChange:) name:NET_WORK_CHANGE object:nil];
}


/*
- (IBAction)submitButtonClicked:(id)sender {
}
 */
@end
