//
//  BuyerRegistrationVCTwo.m
//  AquaSmart
//
//  Created by Mac User on 09/01/17.
//  Copyright © 2017 Mac User. All rights reserved.
//

#import "BuyerRegistrationVCTwo.h"
#import "BuyerRegistrationVCOne.h"
#import "DeviceRegistrationVC.h"
//#import "TextFieldValidator.h"
@interface BuyerRegistrationVCTwo ()<UITextFieldDelegate,PacketReciveProtocol>
@end

@implementation BuyerRegistrationVCTwo
{
    NSTimer *timer;
}



#pragma mark - hides keyboard on touch of screen


- (void)viewDidLoad {
    
   
    
    [[WMSSettingsController sharedInstance]TankSettingsFirstTime:YES];
    [super viewDidLoad];
    [self setRootController];
    [self setupOutlets];
        self.serialNumberTF.text = _serialNumber;
   self.licenseNumberTF.text = _licenseNumber;
    
    
    _serialNumberTF.userInteractionEnabled = NO;
    _licenseNumberTF.userInteractionEnabled = NO;
    self.emailTF.text = @"";
    self.phoneNumberTF.text = @"";
    UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    [self.view addGestureRecognizer:gestureRecognizer];
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 30, 20)];
    self.phoneNumberTF.leftView = paddingView;
    self.phoneNumberTF.leftViewMode = UITextFieldViewModeAlways;
    
     self.navigationController.navigationBar.hidden = YES;  //hides the navigation bar
 
    
    
    // Do any additional setup after loading the view.
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)backClicked {
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)viewDidLayoutSubviews {
    [self.constraintContentViewHeight setConstant:400-(self.view.frame.size.height)];
}
-(void)setTopHeaderView {
    NSDictionary *navDict = @{
                              TITLE_TEXT :NSLocalizedString(@"Buyer Registration", nil),
                              IS_BACK_HIDDEN:@"Yes"
                              };
    [[AppDelegate appDelegate].superVC setHeaderViewData:navDict];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    NetworkStatus status = [self checkForWifiConnection];
    if(status == NotReachable || status == ReachableViaWWAN) {
        [self.navigationController.view makeToast:NSLocalizedString(@"Please check your wifi connection", nil)];
    }
    [self setTopHeaderView];
    
}

- (IBAction)agreeButtonClicked:(id)sender {
    if (checkBoxSelected == 0){
        [_agreeButton setSelected:YES];
        checkBoxSelected = 1;
    } else {
        [_agreeButton setSelected:NO];
        checkBoxSelected = 0;
    }

    
    UIAlertController * alert=[UIAlertController
                               
                               alertControllerWithTitle:@"Read carefully and Please agree to our Terms and Conditions"
                               message:@"Water is an integral part of land/soil productivity. Its misuse can cause both degradation and erosion of soils. Management of water resource is considered to " preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* closeButton = [UIAlertAction
                                actionWithTitle:@"Close"
                                style:UIAlertActionStyleDefault
                                  handler:^(UIAlertAction * action)
                                  {
                                      
                                      [_agreeButton setBackgroundColor:[UIColor whiteColor]];
                                      //  [myButton setImage:[UIImage imageNamed:@"image.png"] forState:UIControlStateNormal];
                                      
                                  }
];
    UIAlertAction* agreeButton = [UIAlertAction
                               actionWithTitle:@"Agree"
                                  style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
                                  {
                                      
                                    [_agreeButton setBackgroundImage:[UIImage imageNamed:@"checkbox-checked.png"] forState:UIControlStateSelected];
                                    //  [myButton setImage:[UIImage imageNamed:@"image.png"] forState:UIControlStateNormal];
                                      
                                  }
                                  ];
    
    [alert addAction:closeButton];
    [alert addAction:agreeButton];
    
    [self presentViewController:alert animated:YES completion:nil];
}

-(void)hideKeyboard
{
    [_userNameTF resignFirstResponder];
    [_phoneNumberTF resignFirstResponder];
    [_licenseNumberTF resignFirstResponder];
    [_serialNumberTF resignFirstResponder];
    [_adminPasswordTF resignFirstResponder];
    [_standardPasswordTF resignFirstResponder];
    [_emailTF resignFirstResponder];


}
 
#pragma mark - set the textfield tag
- (void)setupOutlets
{
    //self.userNameTF.delegate = self;
    self.userNameTF.tag = 1;
    
    //self.phoneNumberTF.delegate = self;
    self.phoneNumberTF.tag = 2;
    
   //self.emailIDTF.delegate = self;
    self.emailTF.tag = 3;
    
   // self.adminPasswordTF.delegate = self;
    self.adminPasswordTF.tag = 4;
    
   // self.standardPasswordTF.delegate = self;
    self.standardPasswordTF.tag = 5;
    
    //self.serialNumberTF.delegate = self;
    self.serialNumberTF.tag = 6;
    
   // self.licenseNumberTF.delegate = self;
    self.licenseNumberTF.tag = 7;
    
    
    
    
}

#pragma mark - moving cursor to the next textfield

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    NSInteger nextTag = textField.tag + 1;
    [self jumpToNextTextField:textField withTag:nextTag];
    return NO;
    
}

- (void)jumpToNextTextField:(UITextField *)textField withTag:(NSInteger)tag
{
    
    UIResponder *nextResponder = [self.view viewWithTag:tag];
    
    if ([nextResponder isKindOfClass:[UITextField class]]) {
        // If there is a next responder and it is a textfield, then it becomes first responder.
        [nextResponder becomeFirstResponder];
    }
    else {
        // If there is not then removes the keyboard.
        [textField resignFirstResponder];
    }
}
- (IBAction)buyerNameCheck:(id)sender
{
    if([_userNameTF.text length]<3) {
        [self.view makeToast:@"Buyer name should be of  minimum 3 characters."
                    duration:2.0
                    position:CSToastPositionCenter];
        [self.view endEditing:YES];
        return;

        //[self.view makeToast:NSLocalizedString(@"Buyer name should be of  minimum 3 characters.", nil)];
        
    }
    
    
}



- (IBAction)mobileNumberCheck:(id)sender {
    
    if([_phoneNumberTF.text length]<10) {
        //[self.view makeToast:NSLocalizedString(@"Phone number should be of 10 digits.", nil)];
        [self.view makeToast:@"Phone number should be of 10 digits."
                    duration:2.0
                    position:CSToastPositionCenter];
        [self.view endEditing:YES];
        return;
    }
}
- (IBAction)emailCheck:(id)sender {
    
    if([_emailTF.text length]<=0) {
        
        [self.view makeToast:@"Invalid email."
                    duration:2.0
                    position:CSToastPositionCenter];
        [self.view endEditing:YES];
        return;
        
      //  [self.view makeToast:NSLocalizedString(@"E-mail cannot be empty.", nil)];
        
    }
}

- (IBAction)adminPasswordCheck:(id)sender
{
    
    if([_adminPasswordTF.text length]<3) {
        //[self.view makeToast:NSLocalizedString(@"Please fill admin password.", nil)];
        [self.view makeToast:@"Please fill admin password."
                    duration:2.0
                    position:CSToastPositionCenter];
        [self.view endEditing:YES];
        return;
    }

    
}


- (IBAction)backButton:(id)sender {
    
    
    [self.navigationController popViewControllerAnimated:YES];
}

-(BOOL)checkIfStringContainsUppercase:(NSString *)str {
    if ([[str uppercaseStringWithLocale:[NSLocale currentLocale]] isEqualToString:str])
    {   // ... s is uppercase ...
        return NO;
    }
    else
    {   // ... s is not all uppercase ...
        return YES;
    }
}


#pragma mark - Text fields text lenght validations -
-(NSString *)getName :(int)lenght :(UITextField *)texfield {
    NSMutableString *finalUserName = [texfield.text mutableCopy] ;
    
    if (texfield.text.length == lenght) {
        finalUserName = [texfield.text mutableCopy];
    }
    else {
        int count = (int)texfield.text.length;
        for (int i = count; i < lenght; i++) {
            
            [finalUserName appendString:@" "];
        }
    }
    return finalUserName;
}



//{"sid":"9000","bad":"0100","sad":"000","cmd":"8228","data":{"01":"1","02":"admin pwd.","03":"standard user pwd","04":"Buyer's name","04":"Buyer's Phone no.","05":"Buyer's e-mail"}}



#pragma mark  - Registration Api call -
-(void)callBuyerRegistrationApi {
   // NSString *userName = self.userNameTF.text;
    NSString *userName = [self getName:30 :self.userNameTF];
    NSString *phoneNumber = [self getName:10 :self.phoneNumberTF];
    NSString *emailId = [self getName:40 :self.emailTF];
   
    NSString *adminPass = [self getName:12 :_adminPasswordTF];
    [[WMSSettingsController sharedInstance]saveAdminPassword:adminPass];
    
    OrderedDictionary *dataDict = [[OrderedDictionary alloc]initWithCapacity:6];
    [dataDict insertObject:@"2" forKey:_01 atIndex:0];
    [dataDict insertObject:adminPass forKey:_02 atIndex:1];
    [dataDict insertObject:userName forKey:_03 atIndex:2];
    [dataDict insertObject:phoneNumber forKey:_04 atIndex:3];
    [dataDict insertObject:emailId forKey:_05 atIndex:4];
    
    
    
    
    OrderedDictionary *requestDict = [[OrderedDictionary alloc]initWithCapacity:5];
    [requestDict insertObject:_SIDVALUE forKey:_SID atIndex:0];
   // [requestDict insertObject:_BADVALUE forKey:_BAD atIndex:1];
    [requestDict insertObject:_SADVALUE forKey:_SAD atIndex:1];
    [requestDict insertObject:_8228 forKey:_CMD atIndex:2];
    [requestDict insertObject:dataDict forKey:_DATA atIndex:3];
    NSError *writeError;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:requestDict options:kNilOptions error:&writeError];
    NSString *jsonStr = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    jsonStr = [jsonStr stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    [SVProgressHUD showWithStatus:@"Please Wait.."];

    [TcpPacketClass sharedInstance].delegate = self;
    [[TcpPacketClass sharedInstance]writeOut:jsonStr second:[requestDict objectForKey:_CMD]];
    timer = [NSTimer scheduledTimerWithTimeInterval: 5.0 target:self selector:@selector(updateCounter:) userInfo:nil repeats: NO];
}
- (void)updateCounter:(NSTimer *)theTimer {
    [theTimer invalidate];
    [SVProgressHUD dismiss];
    [self.view makeToast:NSLocalizedString(@"Something went wrong Please try Again!!!", nil)];
}
-(void)didRecieveError:(NSError *)error {
    [SVProgressHUD dismissWithDelay:5];
    [self.view makeToast:NSLocalizedString(@"Socket not connected", nil)];
  //   [[Communicator sharedInstance]setup];
   
}

-(void)getDeviceAuthenticationResponse:(NSDictionary *)jsonDict {
    [timer invalidate];
    [SVProgressHUD dismiss];
    NSDictionary *dataDict = [jsonDict valueForKey:_DATA];
    if ([[dataDict valueForKey:_02] isEqualToString:@"0"]) {
        // navigating to device registration
        UIStoryboard *mainStroyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        DeviceRegistrationVC *deviceRegistration = [mainStroyBoard instantiateViewControllerWithIdentifier:@"DeviceRegistrationVC"];
        deviceRegistration.licenseNumber = self.licenseNumberTF.text;
        deviceRegistration.serialNumber = self.serialNumberTF.text;
        [self.navigationController pushViewController:deviceRegistration animated:YES];
    }
    else if([[dataDict valueForKey:_01]isEqualToString:@"1"]) {
        [self.view makeToast:NSLocalizedString(@"Serial number does not match!", nil)];
    }
    else if([[dataDict valueForKey:_01]isEqualToString:@"2"]) {
        [self.view makeToast:NSLocalizedString(@"License number does not match!", nil)];
    }
    else{
        [self.view makeToast:NSLocalizedString(@"Buyer Registration failed!", nil)];
    
    }
}


-(void)showErrorStatus:(NSString *)msg {
    //[self disconnectFromFTP];
    [SVProgressHUD dismiss];
    [self.view makeToast:msg];
}





#pragma mark - setting length for texfields

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    if(textField == _phoneNumberTF) {
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        return (newLength > 11) ? NO : YES;
    }
    
    else if(textField == _userNameTF)
    {
        //do the same with different values
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        return (newLength > 15) ? NO : YES;
    }
    else if(textField == _serialNumberTF)
    {
        //do the same with different values
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        return (newLength > 12) ? NO : YES;
    }
    else if(textField == _licenseNumberTF)
    {
        //do the same with different values
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        return (newLength > 24) ? NO : YES;
    }
    else if(textField == _adminPasswordTF)
    {
        //do the same with different values
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        return (newLength >12) ? NO : YES;
    }
    else if(textField == _standardPasswordTF)
    {
        //do the same with different values
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        return (newLength >6) ? NO : YES;
    }

    return 0;
}

- (IBAction)registerButtonClicked:(id)sender
{
    
    NSString *userReg = @"^[A-Za-z0-9_-]{3,15}$";
    NSString *emailReg = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailReg];
    NSPredicate *usernameTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",userReg];
    
    
    if (self.userNameTF.text.length <= 0) {
        [self.view makeToast:NSLocalizedString(@"Please fill buyer name.", nil)];
        [self.view endEditing:YES];
        return;
    }
        else if ([usernameTest evaluateWithObject:self.userNameTF.text] == NO)
        {
      //      NSString *userReg = @"^[A-Za-z0-9_-]{3,15}$";
         //   NSPredicate *usernameTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", userReg];
            [self.view makeToast:NSLocalizedString(@"Please enter valid buyer name.", nil)];
            [self.view endEditing:YES];
            return ;
            
            
        }


    else if (self.phoneNumberTF.text.length <= 0) {
        [self.view makeToast:NSLocalizedString(@"Please fill phone number.", nil)];
        [self.view endEditing:YES];
        return;
    }
    else if (self.phoneNumberTF.text.length < 10) {
        
        
        [self.view makeToast:NSLocalizedString(@"Phone number should contain atleast 10 characters", nil)];
        [self.view endEditing:YES];
        
        return;
    }

    else  if ([self.phoneNumberTF.text rangeOfCharacterFromSet:[NSCharacterSet letterCharacterSet]].location != NSNotFound || [self.phoneNumberTF.text rangeOfCharacterFromSet:[NSCharacterSet symbolCharacterSet]].location != NSNotFound ||[self.phoneNumberTF.text rangeOfCharacterFromSet:[NSCharacterSet punctuationCharacterSet]].location != NSNotFound) {
        [self.view makeToast:NSLocalizedString(@"Invalid phone number", nil)];
        [self.view endEditing:YES];
        return;
    }


//     else if (self.emailTF.text.length <= 0) {
//        [self.view makeToast:NSLocalizedString(@"Please fill email ID.", nil)];
//         [self.view endEditing:YES];
//        return;
//    }
//     else if (![self.emailTF validate]) {
//        [self.view makeToast:NSLocalizedString(@"Please enter valid email id.", nil)];
//         [self.view endEditing:YES];
//
//        return;
  //  }
   
    
    
     else if (self.emailTF.text.length==0)
    {
      //  NSString *emailReg = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9-]+\\.[A-Za-z]{2,4}";
      //  NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailReg];
        [self.view makeToast:NSLocalizedString(@"Please enter valid email id.", nil)];
               [self.view endEditing:YES];
        return ;
    }
    
    else if ([emailTest evaluateWithObject:self.emailTF.text] == NO)
    {
      //  NSString *emailReg = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9-]+\\.[A-Za-z]{2,4}";
       // NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailReg];
        [self.view makeToast:NSLocalizedString(@"Please enter valid email id.", nil)];
        [self.view endEditing:YES];
        return ;

        
    }
    

    
     else if (self.adminPasswordTF.text.length <=0 ) {
         [self.view makeToast:NSLocalizedString(@"Please fill admin Password.", nil)];
         [self.view endEditing:YES];
         return;
     }
     else  if (self.adminPasswordTF.text.length <6||self.adminPasswordTF.text.length>12) {
         [self.view makeToast:NSLocalizedString(@"Admin password length should be 6-12 character long.", nil)];
         [self.view endEditing:YES];
         return;
     }
          else if (self.serialNumberTF.text.length <= 0) {
         [self.view makeToast:NSLocalizedString(@"Please fill serial number.", nil)];
         [self.view endEditing:YES];
         return;
     }
     else if (self.serialNumberTF.text.length <12) {
         [self.view makeToast:NSLocalizedString(@"Serial number should contain  12 characters", nil)];
         [self.view endEditing:YES];
         return;
     }
    
     else  if ([self checkIfStringContainsUppercase:self.serialNumberTF.text]) {
         [self.view makeToast:NSLocalizedString(@"Serial number should contain uppercase characters.", nil)];
         [self.view endEditing:YES];
         return;
     }



     else if (self.licenseNumberTF.text.length <= 0) {
         [self.view makeToast:NSLocalizedString(@"Please fill license number.", nil)];
         [self.view endEditing:YES];
         return;
     }
//     else  if([self checkIfStringContainsUppercase:self.licenseNumberTF.text]) {
//         [self.view makeToast:NSLocalizedString(@"License number should contain uppercase characters.", nil)];
//         return;
//     }

     else  if(self.licenseNumberTF.text.length < 24) {
         [self.view makeToast:NSLocalizedString(@"License number should contain atleast 24 characters", nil)];
         [self.view endEditing:YES];
         return;
     }
    
    
//     else if (self.agreeButton.selected==NO) {
//         [self.view makeToast:NSLocalizedString(@"Please check terms and conditions before proceeding", nil)];
//         [self.view endEditing:YES];
//         return;
//     }
    else
  {
  
      [self callBuyerRegistrationApi];
//      UIStoryboard *mainStroyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//      DeviceRegistrationVC *deviceRegistrationVC = [mainStroyBoard instantiateViewControllerWithIdentifier:@"DeviceRegistrationVC"];
//      
//      [self.navigationController pushViewController:deviceRegistrationVC animated:nil];
//      return;
      

  
  }
    
    
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - Load Root View  and finding ip address -
-(void)setRootController {
    //checking network connection initially
    NetworkStatus status = [self checkForWifiConnection];
    NSLog(@"value of status is %ld",(long)status);
    if(status == NotReachable || status == ReachableViaWWAN) {
        [self.navigationController.view makeToast:@"Please connect to network."];
        
    }
    
}
-(NetworkStatus)checkForWifiConnection {
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    [reachability startNotifier];
    NetworkStatus status = [reachability currentReachabilityStatus];
    return status;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
//    if (textField==_standardPasswordTF )
//    {
//        [self animateTextField: textField up: YES];
//        
//    }
    if (textField==_serialNumberTF )
    {
        [self animateTextField: textField up: YES];

    }
   if (textField==_licenseNumberTF)
    {
        [self animateTextField: textField up: YES];
    }
    
    
}
- (void)textFieldDidEndEditing:(UITextField *)textField
{
//    if (textField==_standardPasswordTF )
//    {
//        [self animateTextField: textField up: YES];
//        
//    }
      if (textField==_serialNumberTF)
    {
        [self animateTextField: textField up: NO];

    }
    if (textField==_licenseNumberTF)
    
    {
        [self animateTextField: textField up: NO];
        
    }
}
- (void) animateTextField: (UITextField*) textField up: (BOOL) up
{
    const int movementDistance = 120; // tweak as needed
    const float movementDuration = 0.3f; // tweak as needed
    int movement = (up ? -movementDistance : movementDistance);
    [UIView beginAnimations: @"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
}
//-(void)viewDidAppear:(BOOL)animated
//{
//    self.isRun = YES;
//    self.isPrepared = NO;
//    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
//        while(self.isRun){
//            if(!self.isPrepared){
//                [[PacketRecieverClass sharedInstance]performConnection];
//                
//            }
//            usleep(1000000);
//        }
//    });
//    
//}
-(void)endEditing {
    [self.view endEditing:YES];
}

@end
