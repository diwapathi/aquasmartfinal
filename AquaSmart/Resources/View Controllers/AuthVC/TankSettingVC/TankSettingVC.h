//
//  TankSettingVC.h
//  AquaSmart
//
//  Created by Neotech on 04/05/17.
//  Copyright © 2017 Mac User. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIView+Toast.h"
#import "WMSBlackBoxCommands.h"
#import "WMSSettingsController.h"
#import "OrderedDictionary.h"
#import "SVProgressHUD.h"
#import "Reachability.h"
#import "TcpPacketClass.h"
#import "AppDelegate.h"
#import "Reachability.h"

@interface TankSettingVC : UIViewController<PacketReciveProtocol,UITextFieldDelegate,UIPickerViewDelegate,UIPickerViewDataSource>
@property (weak, nonatomic) IBOutlet UITextField *tankNameTF;
@property (weak, nonatomic) IBOutlet UIButton *showTankVolume;
@property (weak, nonatomic) IBOutlet UITextField *tankVolumeTF;

@property (weak, nonatomic) IBOutlet UIPickerView *sensorPicker;

@property (weak, nonatomic) IBOutlet UISlider *firstSlider;
@property (weak, nonatomic) IBOutlet UISlider *secondSlider;
@property (weak, nonatomic) IBOutlet UISlider *thirdSlider;
@property (weak, nonatomic) IBOutlet UISlider *fourthSlider;
@property (weak, nonatomic) IBOutlet UILabel *firstSliderLabel;
@property (weak, nonatomic) IBOutlet UILabel *secondSliderLabel;
@property (weak, nonatomic) IBOutlet UILabel *thirdSliderLabel;
@property (weak, nonatomic) IBOutlet UILabel *fourthSliderLabel;
@property (weak, nonatomic) IBOutlet UILabel *fourthSliderLabelZero;
@property (weak, nonatomic) IBOutlet UILabel *fourthSliderHedderLabel;


@property (weak, nonatomic) IBOutlet UIButton *firsScheduleModeButton;
@property (weak, nonatomic) IBOutlet UIButton *secondScheduleModeButton;
@property (weak, nonatomic) IBOutlet UIButton *thirdSchedulModeButton;

@property (weak, nonatomic) IBOutlet UIButton *firstAutoModeButton;
@property (weak, nonatomic) IBOutlet UIButton *secondAutoModeButton;
@property (weak, nonatomic) IBOutlet UIButton *thirdAutoModeButton;


-(void)callTankSettingApi;
@end
