//
//  TankSettingVC.m
//  AquaSmart
//
//  Created by Neotech on 04/05/17.
//  Copyright © 2017 Mac User. All rights reserved.
//

#import "TankSettingVC.h"

@interface TankSettingVC ()

@end

@implementation TankSettingVC
{
    NSTimer *timer;
    BOOL checkBoxSelected;
    NSArray *sensorArray;
    NSString *SensorPickerSelectedRowTitle;
    NSString *SensorPositionFour;
    NSString *SensorPositionThree;
    NSString *SensorPositionTwo;
    NSString *SensorPositionFirst;
    NSString *finalFourSensorPosition;
    int sensorPickerSelected;
    NSString *finalThreeSensorPosition;
    
    
    NSString *tankName;
    NSString *tankVolume ;
    NSString *startingSensor ;
    NSString *endSensor ;
    NSString *sensorAtBottom ;
    NSString *sensorPos;

    
    
    NSArray *ArrayForThreeSensor;
    NSArray *ArrayForFourSensor;
    NSString *firstSensorValue;
    NSString *secondSensorValue;
    NSString *thirdSensorValue;
    NSString *fourthSensorValue;

    NSInteger SensorPosInteger;
    
    
    NSString *suctionSensorPosition;
    NSString *reserveSensorPosition;

}

- (void)viewDidLoad {
   
    [self getTankSettings];
    NSInteger selectedPickerRow =  [_sensorPicker selectedRowInComponent:0];
    if (selectedPickerRow == 0) {
        self.fourthSlider.hidden = YES;
        self.fourthSliderLabel.hidden =YES;
        self.fourthSliderLabelZero.hidden =YES;
        self.fourthSliderHedderLabel.hidden =YES;
        self.thirdSchedulModeButton.hidden =YES;
        self.thirdAutoModeButton.hidden = YES;

    }
    [self setImageOnButton];
    sensorArray = [[NSArray alloc]initWithObjects:@"3",@"4", nil];
    _tankVolumeTF.hidden=YES;
    [self setTopHeaderView];
    [super viewDidLoad];
    UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    [self.view addGestureRecognizer:gestureRecognizer];

}

-(void)setImageOnButton
{
    //setting image to radio buttons
    [self.firstAutoModeButton setImage:[UIImage imageNamed:@"BLUE_RADIO_SELECTOR_OFF"] forState:UIControlStateNormal];
    [self.firstAutoModeButton setImage:[UIImage imageNamed:@"BLUE_RADIO_SELECTOR_ON"] forState:UIControlStateSelected];
    [self.secondAutoModeButton setImage:[UIImage imageNamed:@"BLUE_RADIO_SELECTOR_OFF"] forState:UIControlStateNormal];
    [self.secondAutoModeButton setImage:[UIImage imageNamed:@"BLUE_RADIO_SELECTOR_ON"] forState:UIControlStateSelected];
    [self.secondAutoModeButton setImage:[UIImage imageNamed:@"BLUE_RADIO_SELECTOR_OFF"] forState:UIControlStateNormal];
    [self.secondAutoModeButton setImage:[UIImage imageNamed:@"BLUE_RADIO_SELECTOR_ON"] forState:UIControlStateSelected];
    [self.thirdAutoModeButton setImage:[UIImage imageNamed:@"BLUE_RADIO_SELECTOR_OFF"] forState:UIControlStateNormal];
    [self.thirdAutoModeButton setImage:[UIImage imageNamed:@"BLUE_RADIO_SELECTOR_ON"] forState:UIControlStateSelected];
    
    
    [self.firsScheduleModeButton setImage:[UIImage imageNamed:@"BLUE_RADIO_SELECTOR_OFF"] forState:UIControlStateNormal];
    [self.firsScheduleModeButton setImage:[UIImage imageNamed:@"BLUE_RADIO_SELECTOR_ON"] forState:UIControlStateSelected];
    
    [self.secondScheduleModeButton setImage:[UIImage imageNamed:@"BLUE_RADIO_SELECTOR_OFF"] forState:UIControlStateNormal];
    [self.secondScheduleModeButton setImage:[UIImage imageNamed:@"BLUE_RADIO_SELECTOR_ON"] forState:UIControlStateSelected];
    
    
    [self.thirdSchedulModeButton setImage:[UIImage imageNamed:@"BLUE_RADIO_SELECTOR_OFF"] forState:UIControlStateNormal];
    [self.thirdSchedulModeButton setImage:[UIImage imageNamed:@"BLUE_RADIO_SELECTOR_ON"] forState:UIControlStateSelected];
    
}
#pragma mark- picker view delegate methods
-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    
    
    return sensorArray.count;
}

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    
    
    return 1;
}


-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    
    
    NSString *title;
    
           title = [sensorArray objectAtIndex:row];
        
    
    
    
    return title;
    
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    
    
    
        SensorPickerSelectedRowTitle = [sensorArray objectAtIndex:[self.sensorPicker selectedRowInComponent:0]];
      sensorPickerSelected  = [SensorPickerSelectedRowTitle intValue];
    switch (sensorPickerSelected) {
        case 4:
        {
            
            [_fourthSlider setValue:100];
            self.fourthSliderLabel.text = [NSString stringWithFormat:@"%i",(int) self.fourthSlider.value];

            
            [_thirdSlider setValue:75];
            self.thirdSliderLabel.text = [NSString stringWithFormat:@"%i",(int) self.thirdSlider.value];
            
            [_secondSlider setValue:50];
            self.secondSliderLabel.text = [NSString stringWithFormat:@"%i",(int) self.secondSlider.value];
            
            [_firstSlider setValue:25];
            self.firstSliderLabel.text = [NSString stringWithFormat:@"%i",(int) self.firstSlider.value];

            //for uselected button when picker value changed
            [self.firsScheduleModeButton setSelected:NO];
            [self.secondScheduleModeButton setSelected:NO];
            [self.thirdSchedulModeButton setSelected:NO];
            
            [self.firstAutoModeButton setSelected:NO];
            [self.secondAutoModeButton setSelected:NO];
            [self.thirdAutoModeButton setSelected:NO];
            
            
            
            
//            [_fourthSlider setValue: [_thirdSlider value] +5 animated:NO];
//            self.fourthSliderLabel.text = [NSString stringWithFormat:@"%i",(int) self.fourthSlider.value];
            
            self.fourthSlider.hidden = NO;
            self.fourthSliderLabel.hidden =NO;
            self.fourthSliderLabelZero.hidden =NO;
            self.fourthSliderHedderLabel.hidden =NO;
            self.firstAutoModeButton.hidden =NO;
            self.firsScheduleModeButton.hidden = NO;
            self.secondScheduleModeButton.hidden =NO;
            self.secondAutoModeButton.hidden = NO;
            self.thirdAutoModeButton.hidden =NO;
            self.thirdSchedulModeButton.hidden = NO;
            SensorPositionFour = [NSString stringWithFormat:@"%@", self.fourthSliderLabel.text];
            SensorPositionThree = [NSString stringWithFormat:@"%@", self.thirdSliderLabel.text];
            SensorPositionTwo = [NSString stringWithFormat:@"%@", self.secondSliderLabel.text];
            SensorPositionFirst = [NSString stringWithFormat:@"%@", self.firstSliderLabel.text];
            finalFourSensorPosition = [@[SensorPositionFirst, SensorPositionTwo, SensorPositionThree,SensorPositionFour] componentsJoinedByString:@"-"];
            finalThreeSensorPosition =  [@[SensorPositionFirst, SensorPositionTwo, SensorPositionThree] componentsJoinedByString:@"-"];

        }
            break;
        case 3:
        {
            
            [_firsScheduleModeButton setSelected:NO];
            [_firstAutoModeButton setSelected:NO];
            if ([suctionSensorPosition isEqualToString:@"4"]&&[reserveSensorPosition isEqualToString:@"4"]) {
                suctionSensorPosition = nil;
                reserveSensorPosition = nil;
            }
            //
            [_thirdSlider setValue:100];
            self.thirdSliderLabel.text = [NSString stringWithFormat:@"%i",(int) self.thirdSlider.value];
            
            [_secondSlider setValue:70];
            self.secondSliderLabel.text = [NSString stringWithFormat:@"%i",(int) self.secondSlider.value];

            [_firstSlider setValue:35];
            self.firstSliderLabel.text = [NSString stringWithFormat:@"%i",(int) self.firstSlider.value];

            
            //for uselected button when picker value changed

            
            [self.secondScheduleModeButton setSelected:NO];
            [self.thirdSchedulModeButton setSelected:NO];
            
            
            [self.secondAutoModeButton setSelected:NO];
            [self.thirdAutoModeButton setSelected:NO];

            
            
            //
//            [_thirdSlider setValue: [_secondSlider value] +5 animated:NO];
//            self.thirdSliderLabel.text = [NSString stringWithFormat:@"%i",(int) self.thirdSlider.value];
            self.fourthSlider.hidden = YES;
            self.fourthSliderLabel.hidden =YES;
            self.fourthSliderLabelZero.hidden =YES;
            self.fourthSliderHedderLabel.hidden =YES;
            self.firsScheduleModeButton.hidden =YES;
            self.firstAutoModeButton.hidden = YES;
            self.thirdSchedulModeButton.hidden = NO;
            self.thirdAutoModeButton.hidden = NO;
            
            SensorPositionFour = [NSString stringWithFormat:@"%@", self.fourthSliderLabel.text];
            SensorPositionThree = [NSString stringWithFormat:@"%@", self.thirdSliderLabel.text];
            SensorPositionTwo = [NSString stringWithFormat:@"%@", self.secondSliderLabel.text];
            SensorPositionFirst = [NSString stringWithFormat:@"%@", self.firstSliderLabel.text];
            finalFourSensorPosition = [@[SensorPositionFirst, SensorPositionTwo, SensorPositionThree,SensorPositionFour] componentsJoinedByString:@"-"];
            finalThreeSensorPosition =  [@[SensorPositionFirst, SensorPositionTwo, SensorPositionThree] componentsJoinedByString:@"-"];

            
        }
            break;
            
            
    }

    
    }





-(void)setTopHeaderView {
    NSDictionary *navDict = @{
                              TITLE_TEXT :NSLocalizedString(@"Tank Settings", nil),
                              BACK_IMAGE :@"BackImage",
                              IS_BACK_HIDDEN:@"No"
                              };
    [[AppDelegate appDelegate].superVC setHeaderViewData:navDict];
}


-(void)hideKeyboard
{
    [_tankNameTF resignFirstResponder];
    [_tankVolumeTF resignFirstResponder];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




#pragma mark- sending suction pump and reserve sensor packet
-(void)callSensorApi
{
    
    
    OrderedDictionary *dataDict = [[OrderedDictionary alloc]initWithCapacity:2];
    [dataDict insertObject:suctionSensorPosition forKey:_01 atIndex:0];
    [dataDict insertObject:reserveSensorPosition forKey:_02 atIndex:1];
    
    
    
    OrderedDictionary *requestDict = [[OrderedDictionary alloc]initWithCapacity:3];
    [requestDict insertObject:_SIDVALUE forKey:_SID atIndex:0];
    [requestDict insertObject:_SADVALUE forKey:_SAD atIndex:1];
    [requestDict insertObject:_8532 forKey:_CMD atIndex:2];
    [requestDict insertObject:dataDict forKey:_DATA atIndex:3];
    NSError *writeError;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:requestDict options:kNilOptions error:&writeError];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    [SVProgressHUD showWithStatus:@"Please Wait..."];
    [TcpPacketClass sharedInstance].delegate = self;
    [[TcpPacketClass sharedInstance]writeOut:jsonString second:[requestDict objectForKey:_CMD]];
    timer = [NSTimer scheduledTimerWithTimeInterval: 5.0 target:self selector:@selector(sendSuctionSensorPacketAgain:) userInfo:nil repeats: NO];

}

- (void)sendSuctionSensorPacketAgain:(NSTimer *)theTimer {
    int static ticks = 0;
    if (ticks<4) {
        [self callSensorApi];
    }
    else if (ticks>3)
    {
        [theTimer invalidate];
        [SVProgressHUD dismiss];
        [self.view makeToast:NSLocalizedString(@"Error in saving settings.Please try again.", nil)];
        ticks = -1;
    }
    ticks++;
    
}

-(void)didRecieveSuctionSensorResponse:(NSDictionary *)jsonDict
{

    [timer invalidate];
    [SVProgressHUD dismiss];
    
    NSDictionary *dataDict = [jsonDict valueForKey:_DATA];
    NSString *suctionSensorPos = [dataDict valueForKey:_01];
    NSString *reserveSensorPos = [dataDict valueForKey:_02];
     [self.view makeToast:NSLocalizedString(@"Suction Settings Save Successfully", nil)];
    if ([suctionSensorPos isEqualToString:@"4"]) {
    
        [self.firsScheduleModeButton setSelected:YES];
        
    }
    
    else if([suctionSensorPos isEqualToString:@"3"])
    {
    
    [self.secondScheduleModeButton setSelected:YES];
    
    }
    
    else if([suctionSensorPos isEqualToString:@"2"])
    {
        
        [self.thirdSchedulModeButton setSelected:YES];
        
    }
    
    
    if ([reserveSensorPos isEqualToString:@"4"]) {
        [self.firstAutoModeButton setSelected:YES];
    }
    
    else if([reserveSensorPos isEqualToString:@"3"])
    {
        
        [self.secondAutoModeButton setSelected:YES];
        
    }
    
    else if([reserveSensorPos isEqualToString:@"2"])
    {
        
        [self.thirdAutoModeButton setSelected:YES];
        
    }
    
}

#pragma mark- get tank settings api
-(void)getTankSettings
{
    
    

    OrderedDictionary *requestDict = [[OrderedDictionary alloc]initWithCapacity:3];
    [requestDict insertObject:_SIDVALUE forKey:_SID atIndex:0];
    [requestDict insertObject:_SADVALUE forKey:_SAD atIndex:1];
    [requestDict insertObject:_8502 forKey:_CMD atIndex:2];
    NSError *writeError;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:requestDict options:kNilOptions error:&writeError];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    [SVProgressHUD showWithStatus:@"Please Wait..."];
    [TcpPacketClass sharedInstance].delegate = self;
    [[TcpPacketClass sharedInstance]writeOut:jsonString second:[requestDict objectForKey:_CMD]];
    timer = [NSTimer scheduledTimerWithTimeInterval: 5.0 target:self selector:@selector(getTankSettingsAgain:) userInfo:nil repeats: NO];


}

- (void)getTankSettingsAgain:(NSTimer *)theTimer {
    int static ticks = 0;
    if (ticks<4) {
        [self getTankSettings];
    }
    else if (ticks>3)
    {
        [theTimer invalidate];
        [SVProgressHUD dismiss];
        [self.view makeToast:NSLocalizedString(@"Error in getting tank settings.Please try again.", nil)];
       // [self.navigationController popViewControllerAnimated:YES];
        ticks = -1;
    }
    ticks++;
    
}
-(void)didRecieveGetTankSettingResponse:(NSDictionary *)jsonDict
{
    [timer invalidate];
    //[SVProgressHUD dismiss];
    if([[jsonDict valueForKey:@"data"] isKindOfClass:[NSString class]])
    {
        
        
        [self.view makeToast:NSLocalizedString(@"Fail to recieve tank settings!!", nil)];
        
        
    }
    
    else if([[jsonDict valueForKey:_DATA] isKindOfClass:[NSDictionary class]])
    {
        NSDictionary *dataDict = [jsonDict valueForKey:_DATA];
        NSString *tankName1 = [dataDict valueForKey:_03];
        tankName = [tankName1 stringByReplacingOccurrencesOfString:@" " withString:@""];
       // tankName = [dataDict valueForKey:_03];
       tankVolume = [dataDict valueForKey:_04];
       startingSensor =  [dataDict valueForKey:_07];
        endSensor =  [dataDict valueForKey:_08];
        SensorPickerSelectedRowTitle = endSensor;
        //sensorPickerSelected = [endSensor intValue];
        sensorAtBottom =   [dataDict valueForKey:_09];
        sensorPos = [dataDict valueForKey:_10];

        
        
        
        [[NSUserDefaults standardUserDefaults]setObject:[dataDict valueForKey:_03] forKey:@"TankName"];
        [[NSUserDefaults standardUserDefaults]setObject:[dataDict valueForKey:_04] forKey:@"TankVolume"];
        
        [self setSensorPositions];
        // [self.view makeToast:NSLocalizedString(@"Tank settings recieved..", nil)];
    }
    
    else
    {
        
        [self.view makeToast:NSLocalizedString(@"Error Occurred.", nil)];
    }
   [self getSuctionSensorSettings];
    
}


#pragma mark- set sensor position
-(void)setSensorPositions
{

    self.tankNameTF.text = tankName;
    [self.showTankVolume setSelected:YES];
    self.tankVolumeTF.hidden = NO;
    self.tankVolumeTF.text = tankVolume;
    
    if ([endSensor isEqualToString:@"3"]) {
        [_sensorPicker selectRow:0 inComponent:0 animated:YES];
        self.fourthSlider.hidden = YES;
        self.fourthSliderLabel.hidden =YES;
        self.fourthSliderLabelZero.hidden =YES;
        self.fourthSliderHedderLabel.hidden =YES;
        self.thirdSchedulModeButton.hidden =NO;
        self.thirdAutoModeButton.hidden = NO;
        self.firstAutoModeButton.hidden = YES;
        self.firsScheduleModeButton.hidden = YES;
        
        ArrayForThreeSensor = [sensorPos componentsSeparatedByString:@"-"];   //take the one array for split the string
        
        firstSensorValue=[ArrayForThreeSensor objectAtIndex:0];
        secondSensorValue=[ArrayForThreeSensor objectAtIndex:1];
        thirdSensorValue=[ArrayForThreeSensor objectAtIndex:2];
        
        
        [_firstSlider setValue:[firstSensorValue intValue] animated:NO];
        self.firstSliderLabel.text = [NSString stringWithFormat:@"%i",(int) self.firstSlider.value];
        [_secondSlider setValue:[secondSensorValue intValue] animated:NO];
        self.secondSliderLabel.text = [NSString stringWithFormat:@"%i",(int) self.secondSlider.value];
        
        [_thirdSlider setValue:[thirdSensorValue intValue] animated:NO];
        self.thirdSliderLabel.text = [NSString stringWithFormat:@"%i",(int) self.thirdSlider.value];
        
        
        
        //by default send value to packet  , setting finalSensorPostion fourth and third
        SensorPositionFour = [NSString stringWithFormat:@"%@", self.fourthSliderLabel.text];
        SensorPositionThree = [NSString stringWithFormat:@"%@", self.thirdSliderLabel.text];
        SensorPositionTwo = [NSString stringWithFormat:@"%@", self.secondSliderLabel.text];
        SensorPositionFirst = [NSString stringWithFormat:@"%@", self.firstSliderLabel.text];
        finalFourSensorPosition = [@[SensorPositionFirst, SensorPositionTwo, SensorPositionThree,SensorPositionFour] componentsJoinedByString:@"-"];
        finalThreeSensorPosition =  [@[SensorPositionFirst, SensorPositionTwo, SensorPositionThree] componentsJoinedByString:@"-"];
        
    }
    
    else if ([endSensor isEqualToString:@"4"])
    {
        [_sensorPicker selectRow:1 inComponent:0 animated:YES];
        
        self.fourthSlider.hidden = NO;
        self.fourthSliderLabel.hidden =NO;
        self.fourthSliderLabelZero.hidden =NO;
        self.fourthSliderHedderLabel.hidden =NO;
        self.thirdSchedulModeButton.hidden =NO;
        self.thirdAutoModeButton.hidden = NO;

        ArrayForFourSensor = [sensorPos componentsSeparatedByString:@"-"];   //take the one array for split the string
        
        firstSensorValue=[ArrayForFourSensor objectAtIndex:0];
        secondSensorValue=[ArrayForFourSensor objectAtIndex:1];
        thirdSensorValue=[ArrayForFourSensor objectAtIndex:2];
        fourthSensorValue=[ArrayForFourSensor objectAtIndex:3];

        
        
        [_firstSlider setValue:[firstSensorValue intValue] animated:NO];
        self.firstSliderLabel.text = [NSString stringWithFormat:@"%i",(int) self.firstSlider.value];
        [_secondSlider setValue:[secondSensorValue intValue] animated:NO];
        self.secondSliderLabel.text = [NSString stringWithFormat:@"%i",(int) self.secondSlider.value];
        
        [_thirdSlider setValue:[thirdSensorValue intValue] animated:NO];
        self.thirdSliderLabel.text = [NSString stringWithFormat:@"%i",(int) self.thirdSlider.value];
        
        [_fourthSlider setValue:[fourthSensorValue intValue] animated:NO];
        self.fourthSliderLabel.text = [NSString stringWithFormat:@"%i",(int) self.fourthSlider.value];
        
        
        
        
        
        SensorPositionFour = [NSString stringWithFormat:@"%@", self.fourthSliderLabel.text];
        SensorPositionThree = [NSString stringWithFormat:@"%@", self.thirdSliderLabel.text];
        SensorPositionTwo = [NSString stringWithFormat:@"%@", self.secondSliderLabel.text];
        SensorPositionFirst = [NSString stringWithFormat:@"%@", self.firstSliderLabel.text];
        finalFourSensorPosition = [@[SensorPositionFirst, SensorPositionTwo, SensorPositionThree,SensorPositionFour] componentsJoinedByString:@"-"];
        finalThreeSensorPosition =  [@[SensorPositionFirst, SensorPositionTwo, SensorPositionThree] componentsJoinedByString:@"-"];
    }



}

#pragma mark - getting suction sensor settings 
-(void)getSuctionSensorSettings
{
    OrderedDictionary *requestDict = [[OrderedDictionary alloc]initWithCapacity:3];
    [requestDict insertObject:_SIDVALUE forKey:_SID atIndex:0];
    [requestDict insertObject:_SADVALUE forKey:_SAD atIndex:1];
    [requestDict insertObject:_8533 forKey:_CMD atIndex:2];
    NSError *writeError;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:requestDict options:kNilOptions error:&writeError];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    [SVProgressHUD showWithStatus:@"Please Wait..."];
    [TcpPacketClass sharedInstance].delegate = self;
    [[TcpPacketClass sharedInstance]writeOut:jsonString second:[requestDict objectForKey:_CMD]];
    timer = [NSTimer scheduledTimerWithTimeInterval: 5.0 target:self selector:@selector(getSensorSuctionSettingsAgain:) userInfo:nil repeats: NO];
    
    
    
}

- (void)getSensorSuctionSettingsAgain:(NSTimer *)theTimer {
    int static ticks = 0;
    if (ticks<4) {
        [self getSuctionSensorSettings];
    }
    else if (ticks>3)
    {
        [theTimer invalidate];
        [SVProgressHUD dismiss];
        [self.view makeToast:NSLocalizedString(@"Error in getting suction settings settings.Please try again.", nil)];
        [self.navigationController popViewControllerAnimated:YES];
        ticks = -1;
    }
    ticks++;
    
}


-(void)didRecieveSuctionSensorSettingResponse:(NSDictionary *)jsonDict
{

  
    [timer invalidate];
    [SVProgressHUD dismiss];
    
    NSDictionary *dataDict = [jsonDict valueForKey:_DATA];
    NSString *suctionSensorPos = [dataDict valueForKey:_01];
    NSString *reserveSensorPos = [dataDict valueForKey:_02];
    [self.view makeToast:NSLocalizedString(@"Suction Settings Get Successfully", nil)];
    if ([suctionSensorPos isEqualToString:@"4"]) {
        
        [self.firsScheduleModeButton setSelected:YES];
        suctionSensorPosition = @"4";
        
        
    }
    
    else if([suctionSensorPos isEqualToString:@"3"])
    {
        
        [self.secondScheduleModeButton setSelected:YES];
        suctionSensorPosition = @"3";
    }
    
    else if([suctionSensorPos isEqualToString:@"2"])
    {
        
        [self.thirdSchedulModeButton setSelected:YES];
        suctionSensorPosition = @"2";
    }
    
    
    if ([reserveSensorPos isEqualToString:@"4"]) {
        [self.firstAutoModeButton setSelected:YES];
        reserveSensorPosition = @"4";
    }
    
    else if([reserveSensorPos isEqualToString:@"3"])
    {
        
        [self.secondAutoModeButton setSelected:YES];
        reserveSensorPosition = @"3";
    }
    
    else if([reserveSensorPos isEqualToString:@"2"])
    {
        
        [self.thirdAutoModeButton setSelected:YES];
        reserveSensorPosition = @"2";
    }

}





#pragma mark - sending tank settings packet
-(void)callTankSettingApi{
    NSString *sensorPosition;
    
    if ([SensorPickerSelectedRowTitle isEqualToString:@"3"]) {
       sensorPosition  = finalThreeSensorPosition;
    }
    
    else
    {
        sensorPosition = finalFourSensorPosition;
    }
    id  numberOfTanks;
    NSString *numberOfTankString = [numberOfTanks stringValue];
    if ([numberOfTankString isEqualToString:@"2"]) {
        numberOfTankString = @"2";
    }
    else
    {
       numberOfTankString = @"4";
    }
    OrderedDictionary *dataDict = [[OrderedDictionary alloc]initWithCapacity:20];
    [dataDict insertObject:@"1" forKey:_01 atIndex:0];
    [dataDict insertObject:@"1" forKey:_02 atIndex:1];
    [dataDict insertObject:[self getName:10 :self.tankNameTF] forKey:_03 atIndex:2];
    [dataDict insertObject:self.tankVolumeTF.text forKey:_04 atIndex:3];
    [dataDict insertObject:@"1" forKey:_05 atIndex:4];
    [dataDict insertObject:@"10" forKey:_06 atIndex:5];
    [dataDict insertObject:@"1" forKey:_07 atIndex:6];
    [dataDict insertObject:SensorPickerSelectedRowTitle forKey:_08 atIndex:7];
    [dataDict insertObject:@"0" forKey:_09 atIndex:8];
     [dataDict insertObject:sensorPosition forKey:_10 atIndex:9];
    if ([numberOfTankString isEqualToString:@"2"]) {
        [dataDict insertObject:@"2" forKey:_10 atIndex:1];
        [dataDict insertObject:@"1" forKey:_11 atIndex:1];

    }
    
    OrderedDictionary *requestDict = [[OrderedDictionary alloc]initWithCapacity:5];
    [requestDict insertObject:_SIDVALUE forKey:_SID atIndex:0];
    //[requestDict insertObject:_BADVALUE forKey:_BAD atIndex:1];
    [requestDict insertObject:_SADVALUE forKey:_SAD atIndex:1];
    [requestDict insertObject:_8501 forKey:_CMD atIndex:2];
    [requestDict insertObject:dataDict forKey:_DATA atIndex:3];
    NSError *writeError;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:requestDict options:kNilOptions error:&writeError];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    [SVProgressHUD showWithStatus:@"Please Wait..."];
    [TcpPacketClass sharedInstance].delegate = self;
    [[TcpPacketClass sharedInstance]writeOut:jsonString second:[requestDict objectForKey:_CMD]];
    timer = [NSTimer scheduledTimerWithTimeInterval: 5.0 target:self selector:@selector(updateCounter:) userInfo:nil repeats: NO];
    
}
- (void)updateCounter:(NSTimer *)theTimer {
    [self.view makeToast:NSLocalizedString(@"Something Went wrong.Try Again!!", nil)];
    [theTimer invalidate];
    [SVProgressHUD dismiss];
    
    
}

-(void)didRecieveTankSettingResponse:(NSDictionary *)jsonDict
{
    
    [timer invalidate];
    [SVProgressHUD dismiss];
    
    NSDictionary *dataDict = [jsonDict valueForKey:_DATA];
    if([[jsonDict valueForKey:_01] isEqualToString:@"1"])
    {
        [self.view makeToast:NSLocalizedString(@"Fail to set tank settings!!", nil)];
    }
    
    else
    {
        NSString *tankName = [dataDict valueForKey:_03];
        NSString *tankVolume = [dataDict valueForKey:_04];
        [[NSUserDefaults standardUserDefaults]setValue:tankName forKey:@"TankName"];
        [[NSUserDefaults standardUserDefaults]setValue:tankVolume forKey:@"TankVolume"];
        [self.view makeToast:NSLocalizedString(@"Tank configured Successfully", nil)];
         [self.navigationController popViewControllerAnimated:YES];
        
        
    }
    
   
     [self callSensorApi];
    
    
}

#pragma mark - setting length for texfields

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    if(textField == _tankNameTF) {
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        return (newLength > 10) ? NO : YES;
    }
    
    else if(textField == _tankVolumeTF)
    {
        //do the same with different values
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        return (newLength > 5) ? NO : YES;
    }
    return 0;
}


#pragma mark - Text fields text lenght validations -
-(NSString *)getName :(int)lenght :(UITextField *)texfield {
    NSMutableString *finalUserName = [texfield.text mutableCopy] ;
    
    if (texfield.text.length == lenght) {
        finalUserName = [texfield.text mutableCopy];
    }
    else {
        int count = (int)texfield.text.length;
        for (int i = count; i < lenght; i++) {
            
            [finalUserName appendString:@" "];
        }
    }
    return finalUserName;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)showVolumeButton:(id)sender
{
    _tankVolumeTF.hidden=YES;
    if (_tankVolumeTF.hidden) {
        
        _tankVolumeTF.hidden = NO;
        
    } else if (!_tankVolumeTF.hidden) {
        
        _tankVolumeTF.hidden = YES;
        
    }
    
    if (checkBoxSelected == 0){
        [_showTankVolume setSelected:YES];
        checkBoxSelected = 1;
    } else {
        [_showTankVolume setSelected:NO];
        self.tankVolumeTF.hidden = YES;
        checkBoxSelected = 0;
    }
    

}

- (IBAction)saveButtonClicked:(id)sender {
    
    
    
    if ([SensorPickerSelectedRowTitle isEqualToString:@"3"]) {
        if (![self.secondScheduleModeButton isSelected] && ![self.thirdSchedulModeButton isSelected]) {
            
            
            [self.view makeToast:NSLocalizedString(@"Please check schedule mode", nil)];
            [self.view endEditing:YES];
            return;
        }
        else if(![self.secondAutoModeButton isSelected] && ![self.thirdAutoModeButton isSelected])
        {
            [self.view makeToast:NSLocalizedString(@"Please check Auto mode.", nil)];
            [self.view endEditing:YES];
            return;
            
        }
    }
    else
    {
    
        if (![self.secondScheduleModeButton isSelected] && ![self.thirdSchedulModeButton isSelected]&& ![self.firsScheduleModeButton isSelected]) {
            
            
            [self.view makeToast:NSLocalizedString(@"Please check schedule mode", nil)];
            [self.view endEditing:YES];
            return;
        }
        else if(![self.secondAutoModeButton isSelected] && ![self.thirdAutoModeButton isSelected]&& ![self.firstAutoModeButton isSelected])
        {
            [self.view makeToast:NSLocalizedString(@"Please check Auto mode.", nil)];
            [self.view endEditing:YES];
            return;
            
        }

    
    
    }
    
            if (self.tankNameTF.text.length <= 0) {
            [self.view makeToast:NSLocalizedString(@"Please fill Tank name.", nil)];
            [self.view endEditing:YES];
            return;
        }
    
        else if(finalFourSensorPosition==nil)
        {
            [self.view makeToast:NSLocalizedString(@"Please configure all your sensors.", nil)];
            [self.view endEditing:YES];
            return;
        
        }
    
        else if(finalThreeSensorPosition==nil)
        {
            [self.view makeToast:NSLocalizedString(@"Please configure all your sensors.", nil)];
            [self.view endEditing:YES];
            return;
            
        }
    
        else if(reserveSensorPosition==nil)
        {
            [self.view makeToast:NSLocalizedString(@"Please configure all your mode.", nil)];
            [self.view endEditing:YES];
            return;
            
        }
    
        else if(suctionSensorPosition==nil)
        {
            [self.view makeToast:NSLocalizedString(@"Please configure all your mode.", nil)];
            [self.view endEditing:YES];
            return;
            
        }
        else if (self.tankVolumeTF.text.length <= 0) {
            [self.view makeToast:NSLocalizedString(@"Please fill Tank Volume number.", nil)];
            [self.view endEditing:YES];
            return;
        }
        else  if ([self.tankVolumeTF.text rangeOfCharacterFromSet:[NSCharacterSet letterCharacterSet]].location != NSNotFound || [self.tankVolumeTF.text rangeOfCharacterFromSet:[NSCharacterSet symbolCharacterSet]].location != NSNotFound ||[self.tankVolumeTF.text rangeOfCharacterFromSet:[NSCharacterSet punctuationCharacterSet]].location != NSNotFound) {
            [self.view makeToast:NSLocalizedString(@"Tank Volume should be numeric only", nil)];
            [self.view endEditing:YES];
            return;
        }
        else if (self.showTankVolume.selected==NO) {
            [self.view makeToast:NSLocalizedString(@"Please first check  Tank Volume", nil)];
            [self.view endEditing:YES];
            return;
        }
    
    
//        else  if(self.tankVolumeTF.text.length < 10) {
//            [self.view makeToast:NSLocalizedString(@"Tank name should contain atleast 10 characters", nil)];
//            [self.view endEditing:YES];
//            return;
//        }

    
    else
    {
    [self callTankSettingApi];
        
    }
}



- (IBAction)fourthSliderClicked:(id)sender {
    _fourthSlider.minimumValue = 15;
    _fourthSlider.maximumValue = 100;
    [_fourthSlider setValue:((int)((_fourthSlider.value + 2.5) / 5) * 5) animated:NO];
    if (_fourthSlider.value <= _thirdSlider.value)
    {
        _thirdSlider.value = _fourthSlider.value-5;
        self.thirdSliderLabel.text = [NSString stringWithFormat:@"%i",(int) self.thirdSlider.value];
        _secondSlider.value = _thirdSlider.value-5;
        self.secondSliderLabel.text = [NSString stringWithFormat:@"%i",(int) self.secondSlider.value];
        _firstSlider.value = _secondSlider.value-5;
        self.firstSliderLabel.text = [NSString stringWithFormat:@"%i",(int) self.firstSlider.value];
    }
    self.fourthSliderLabel.text = [NSString stringWithFormat:@"%i",(int) self.fourthSlider.value];
 
    
    SensorPositionFour = [NSString stringWithFormat:@"%@", self.fourthSliderLabel.text];
     SensorPositionThree = [NSString stringWithFormat:@"%@", self.thirdSliderLabel.text];
    SensorPositionTwo = [NSString stringWithFormat:@"%@", self.secondSliderLabel.text];
    SensorPositionFirst = [NSString stringWithFormat:@"%@", self.firstSliderLabel.text];
    finalFourSensorPosition = [@[SensorPositionFirst, SensorPositionTwo, SensorPositionThree,SensorPositionFour] componentsJoinedByString:@"-"];
}
- (IBAction)thirdSliderClicked:(id)sender {
    if ([SensorPickerSelectedRowTitle isEqualToString:@"4"])
    {
    _thirdSlider.minimumValue = _fourthSlider.minimumValue-5;
    _thirdSlider.maximumValue = _fourthSlider.maximumValue-5;
    [_thirdSlider setValue:((int)((_thirdSlider.value + 2.5) / 5) * 5) animated:NO];
    if (_thirdSlider.value >= _fourthSlider.value) {
        _thirdSlider.value = _fourthSlider.value-5;
        self.thirdSliderLabel.text = [NSString stringWithFormat:@"%i",(int) self.thirdSlider.value];
    }
    else if (_thirdSlider.value <= _secondSlider.value)
    {
        _secondSlider.value = _thirdSlider.value-5;
        self.secondSliderLabel.text = [NSString stringWithFormat:@"%i",(int) self.secondSlider.value];
        _firstSlider.value = _secondSlider.value-5;
        self.firstSliderLabel.text = [NSString stringWithFormat:@"%i",(int) self.firstSlider.value];
    }
    self.thirdSliderLabel.text = [NSString stringWithFormat:@"%i",(int) self.thirdSlider.value];
    }
    
    else if ([SensorPickerSelectedRowTitle isEqualToString:@"3"])
    {
        _thirdSlider.minimumValue = 10;
        _thirdSlider.maximumValue = 100;
        [_thirdSlider setValue:((int)((_thirdSlider.value + 2.5) / 5) * 5) animated:NO];
        if (_thirdSlider.value <= _secondSlider.value)
        {
            _secondSlider.value = _thirdSlider.value-5;
            self.secondSliderLabel.text = [NSString stringWithFormat:@"%i",(int) self.secondSlider.value];
            _firstSlider.value = _secondSlider.value-5;
            self.firstSliderLabel.text = [NSString stringWithFormat:@"%i",(int) self.firstSlider.value];
            
        }
        self.thirdSliderLabel.text = [NSString stringWithFormat:@"%i",(int) self.thirdSlider.value];
    }

    
    SensorPositionFour = [NSString stringWithFormat:@"%@", self.fourthSliderLabel.text];
    SensorPositionThree = [NSString stringWithFormat:@"%@", self.thirdSliderLabel.text];
    SensorPositionTwo = [NSString stringWithFormat:@"%@", self.secondSliderLabel.text];
    SensorPositionFirst = [NSString stringWithFormat:@"%@", self.firstSliderLabel.text];
    finalFourSensorPosition = [@[SensorPositionFirst, SensorPositionTwo, SensorPositionThree,SensorPositionFour] componentsJoinedByString:@"-"];
    finalThreeSensorPosition =  [@[SensorPositionFirst, SensorPositionTwo, SensorPositionThree] componentsJoinedByString:@"-"];
}
- (IBAction)secondSliderClicked:(id)sender {
    _secondSlider.minimumValue = _thirdSlider.minimumValue-5;
    _secondSlider.maximumValue = _thirdSlider.maximumValue-5;
    [_secondSlider setValue:((int)((_secondSlider.value + 2.5) / 5) * 5) animated:NO];
    if (_secondSlider.value >= _thirdSlider.value) {
        _secondSlider.value = _thirdSlider.value-5;
        self.secondSliderLabel.text = [NSString stringWithFormat:@"%i",(int) self.secondSlider.value];
    }
    else if (_secondSlider.value <= _firstSlider.value)
    {
        
        _firstSlider.value = _secondSlider.value-5;
        self.firstSliderLabel.text = [NSString stringWithFormat:@"%i",(int) self.firstSlider.value];
    }
    self.secondSliderLabel.text = [NSString stringWithFormat:@"%i",(int) self.secondSlider.value];
    
    SensorPositionFour = [NSString stringWithFormat:@"%@", self.fourthSliderLabel.text];
    SensorPositionThree = [NSString stringWithFormat:@"%@", self.thirdSliderLabel.text];
    SensorPositionTwo = [NSString stringWithFormat:@"%@", self.secondSliderLabel.text];
    SensorPositionFirst = [NSString stringWithFormat:@"%@", self.firstSliderLabel.text];
    finalFourSensorPosition = [@[SensorPositionFirst, SensorPositionTwo, SensorPositionThree,SensorPositionFour] componentsJoinedByString:@"-"];
    finalThreeSensorPosition =  [@[SensorPositionFirst, SensorPositionTwo, SensorPositionThree] componentsJoinedByString:@"-"];
}
- (IBAction)firstSliderClicked:(id)sender {
    _firstSlider.minimumValue =_secondSlider.minimumValue-5;
    _firstSlider.maximumValue = _secondSlider.maximumValue-5;
    [_firstSlider setValue:((int)((_firstSlider.value + 2.5) / 5) * 5) animated:NO];
    if (_firstSlider.value >= _secondSlider.value) {
        _firstSlider.value = _secondSlider.value-5;
        self.firstSliderLabel.text = [NSString stringWithFormat:@"%i",(int) self.firstSlider.value];
    }
    
    self.firstSliderLabel.text = [NSString stringWithFormat:@"%i",(int) self.firstSlider.value];
    
    SensorPositionFour = [NSString stringWithFormat:@"%@", self.fourthSliderLabel.text];
    SensorPositionThree = [NSString stringWithFormat:@"%@", self.thirdSliderLabel.text];
    SensorPositionTwo = [NSString stringWithFormat:@"%@", self.secondSliderLabel.text];
    SensorPositionFirst = [NSString stringWithFormat:@"%@", self.firstSliderLabel.text];
    finalFourSensorPosition = [@[SensorPositionFirst, SensorPositionTwo, SensorPositionThree,SensorPositionFour] componentsJoinedByString:@"-"];
  finalThreeSensorPosition =  [@[SensorPositionFirst, SensorPositionTwo, SensorPositionThree] componentsJoinedByString:@"-"];
}

- (IBAction)reserveSensorButtonClicked:(id)sender {
    
    UIAlertView *alertView = [[UIAlertView alloc]
                              initWithTitle:nil
                              message:@"1) Schedule Mode:\nIt is enabled by settings the Pumping Schedule.\n PUMP runs to pull water from the inlet source.\n \n  2)Auto Mode:\n The natural water presence makes the water available for pumping. "
                              delegate:self
                              cancelButtonTitle:nil
                              otherButtonTitles:NSLocalizedString(@"OK", nil), nil];
    
    [alertView show];

}
#pragma mark- sensor button methods
- (IBAction)firstScheduleModeButtonClicked:(id)sender {
    [self.firsScheduleModeButton setSelected:YES];
    [self.secondScheduleModeButton setSelected:NO];
    [self.thirdSchedulModeButton setSelected:NO];
    suctionSensorPosition = @"4";
    
    

}
- (IBAction)secondScheduleModeButtonClicked:(id)sender {
    
    [self.firsScheduleModeButton setSelected:NO];
    [self.secondScheduleModeButton setSelected:YES];
    [self.thirdSchedulModeButton setSelected:NO];
    suctionSensorPosition = @"3";
}
- (IBAction)thirdScheduleModeButtonClicked:(id)sender {
    [self.firsScheduleModeButton setSelected:NO];
    [self.secondScheduleModeButton setSelected:NO];
    [self.thirdSchedulModeButton setSelected:YES];
    
suctionSensorPosition = @"2";
}





- (IBAction)firstAutoModeButtonClicked:(id)sender {
    
    [self.firstAutoModeButton setSelected:YES];
    [self.secondAutoModeButton setSelected:NO];
    [self.thirdAutoModeButton setSelected:NO];

    reserveSensorPosition = @"4";
    
}
- (IBAction)secondAutoModeButtonClicked:(id)sender {
    
    [self.firstAutoModeButton setSelected:NO];
    [self.secondAutoModeButton setSelected:YES];
    [self.thirdAutoModeButton setSelected:NO];
   reserveSensorPosition = @"3";
}
- (IBAction)thirdAutoModeButtonClicked:(id)sender {
    [self.firstAutoModeButton setSelected:NO];
    [self.secondAutoModeButton setSelected:NO];
    [self.thirdAutoModeButton setSelected:YES];
    reserveSensorPosition = @"2";
}

@end
