//
//  DeviceRegistrationVC.h
//  AquaSmart
//
//  Created by Mac User on 09/01/17.
//  Copyright © 2017 Mac User. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIView+Toast.h"
#import "CustomTextfield.h"

#import "WMSSettingsController.h"
#import "OrderedDictionary.h"
#import "WMSBlackBoxCommands.h"
#import "TcpPacketClass.h"
#import "SVProgressHUD.h"
#import "RegisteredUserVC.h"
#import "WMSSettingsController.h"
@interface DeviceRegistrationVC : UIViewController
@property (strong, nonatomic) IBOutlet UITextField *userNameTF;
@property (strong, nonatomic) IBOutlet UITextField *phoneNumberTF;
@property (strong, nonatomic) IBOutlet UITextField *serialNumberTF;
@property (strong, nonatomic) IBOutlet UITextField *licenseNumberTF;
@property(nonatomic,strong)NSString *serialNumber;
@property(nonatomic,strong)NSString *licenseNumber;
-(void)callClientRegistrationApi;
@property(nonatomic,assign)BOOL isRun;
@property(nonatomic,assign)BOOL isPrepared;
@property (weak, nonatomic) IBOutlet UIView *popupView;
@property (weak, nonatomic) IBOutlet UITextField *passwordTF;
@property (weak, nonatomic) IBOutlet UIView *passwordView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintContentViewHeight;
@property(nonatomic,assign)int flag;
@property(nonatomic)BOOL timeSyncFirst;
@property(nonatomic)BOOL isTankSettingsFirst;
+ (instancetype)sharedInstance;
@end
