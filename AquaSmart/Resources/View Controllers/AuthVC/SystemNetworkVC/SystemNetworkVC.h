//
//  SystemNetworkVC.h
//  AquaSmart
//
//  Created by Mac User on 19/01/17.
//  Copyright © 2017 Mac User. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIView+Toast.h"
#import "DeviceRegistrationVC.h"
#import "WMSBlackBoxCommands.h"
#import "WMSSettingsController.h"
#import "OrderedDictionary.h"
#import "SVProgressHUD.h"
#import "Reachability.h"
#import "TcpPacketClass.h"
#import "DeviceRegistrationVC.h"
#import "AppDelegate.h"
#import "Reachability.h"
#import "RadioButton.h"
#import "NIDropDown.h"
extern NSString *const RADIO_ON;
extern NSString *const RADIO_OFF;
@interface SystemNetworkVC : UIViewController<UIPickerViewDelegate,UIPickerViewDataSource,UITextFieldDelegate,PacketReciveProtocol,NIDropDownDelegate>
extern NSString *const RADIO_ON;
extern NSString *const RADIO_OFF;
@property (weak, nonatomic) IBOutlet UIView *view1;
@property (weak, nonatomic) IBOutlet UIView *view2;
@property (weak, nonatomic) IBOutlet UIButton *showview;

@property (weak, nonatomic) IBOutlet UIButton *encryptionTypeButton;


@property (weak, nonatomic) IBOutlet UITextField *passwordTf;
@property (weak, nonatomic) IBOutlet UIButton *refreshBtn;
@property (weak, nonatomic) IBOutlet UIButton *showPassword;
@property (weak, nonatomic) IBOutlet RadioButton *localWifi;
@property (weak, nonatomic) IBOutlet RadioButton *internetWifi;
//@property (weak, nonatomic) IBOutlet UIButton *localWifi;
//@property (weak, nonatomic) IBOutlet UIButton *internetWifi;
@property (weak, nonatomic) IBOutlet UIPickerView *networkPicker;
@property (weak, nonatomic) IBOutlet UIButton *saveBtn;
@property(nonatomic)BOOL isFromLeftPanel;
@property (weak, nonatomic) IBOutlet UITextField *networkSsidTf;

-(void)hideKeyboard;
-(void)backClicked;
@end
