//
//  SystemNetworkVC.m
//  AquaSmart
//
//  Created by Mac User on 19/01/17.
//  Copyright © 2017 Mac User. All rights reserved.
//

#import "SystemNetworkVC.h"
NSString *const RADIO_ON = @"RADIO_ON";
NSString *const RADIO_OFF = @"RADIO_OFF";
@interface SystemNetworkVC ()
{
    NSMutableArray *WifiNetwork;
     BOOL checkBoxSelected;
    NSTimer *timer;
    int connectivityMode;
    NSString *connectModeString;
    NSInteger networkSsidLength;
    NSInteger passwordLength;
     NSString *netSsidString;
     NSString *netPasswordString;
    NIDropDown *dropDown;
    NSArray *encyptionarray;
    
    
    
}
@end

@implementation SystemNetworkVC

- (void)viewDidLoad {
    
    
    if ([[[WMSSettingsController sharedInstance]getWifiSsId] containsString:@"AquaSmart-"]) {
        [self.localWifi setSelected:YES];
        [self.internetWifi setSelected:NO];
        self.passwordTf.text = @"123456789";
        _passwordTf.userInteractionEnabled = NO;
        self.networkSsidTf.text = [[WMSSettingsController sharedInstance]getWifiSsId];
        //self.networkSsidTf.text = @"AquaSmart-005";
        _networkSsidTf.userInteractionEnabled = NO;

        
    }
    else
    {
    
        [self.localWifi setSelected:NO];
        [self.internetWifi setSelected:YES];
        self.passwordTf.text = [[WMSSettingsController sharedInstance]getStationModePass];
        self.networkSsidTf.text = [[WMSSettingsController sharedInstance]getStationModeSsId];
        _networkSsidTf.userInteractionEnabled = YES;
        _passwordTf.userInteractionEnabled = YES;
        
        
    }
    
    _view2.hidden = YES;
    
    WifiNetwork=[[NSMutableArray alloc]initWithObjects:@"E4_IOS",@"E4_ANDROID", nil];
    UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    
    [self.view addGestureRecognizer:gestureRecognizer];
    [super viewDidLoad];
    [_networkPicker setShowsSelectionIndicator:YES];
    if (dropDown != nil) {
        [self hideDropDwonMenu:self.encryptionTypeButton];
    }

    self.networkPicker.hidden = YES;
    // Do any additional setup after loading the view.
    
    [self.localWifi setImage:[UIImage imageNamed:@"BLUE_RADIO_SELECTOR_OFF"] forState:UIControlStateNormal];
    [self.localWifi setImage:[UIImage imageNamed:@"BLUE_RADIO_SELECTOR_ON"] forState:UIControlStateSelected];
    [self.internetWifi setImage:[UIImage imageNamed:@"BLUE_RADIO_SELECTOR_OFF"] forState:UIControlStateNormal];
    [self.internetWifi setImage:[UIImage imageNamed:@"BLUE_RADIO_SELECTOR_ON"] forState:UIControlStateSelected];
    
    
    
    
    
}
- (IBAction)showview2:(id)sender {
    if (_view2.hidden == YES) {
        _view2.hidden = NO;

    }
    else
    {
        _view2.hidden = YES;

    }
    }


- (IBAction)encryptionButtonClicked:(id)sender
{
    
    [self setDropDownDta:sender];
    
    
}


-(void)setDropDownDta:(id)sender {
    
   
        if(dropDown == nil) {
            CGFloat f = 160;
            CGRect frame = self.view1.frame;
            frame.size.height = 160;
            self.view1.frame = frame;
            UIButton *button = (UIButton *)sender;
            encyptionarray = [[NSArray alloc]initWithObjects:@"None",@"WEP SHARE" ,@"WEP OPEN",@"WPA_AES/WPA_TKIP",@"WPA2_AES/WPA2_TKIP",@"WPA/WPA2_AES",nil];
            dropDown = [[NIDropDown alloc]showDropDown:button :&f :encyptionarray :nil :@"down" :CGRectMake(0, 0, frame.size.width, 0)];
            dropDown.delegate = self;
            [self.view1 addSubview:dropDown];
           // [self.userTypeExpandArrow setImage:[UIImage imageNamed:[[ThemeManager sharedManger]getImageNameForKey:UPARROW]]];
        }
        else {
            UIButton *button = (UIButton *)sender;
            [self hideDropDwonMenu:button];
            //[self.userTypeExpandArrow setImage:[UIImage imageNamed:[[ThemeManager sharedManger]getImageNameForKey:EXPAND_ARROW]]];
        }
    }
//    else {
//        self.accessLevelDict = [[[WMSSettingsController sharedInstance]getUserList] copy];
//        if (!self.accessLevelDict) {
//            [self callUserTypesApiCall];
//        }
//    }
//}

- (void) niDropDownDelegateMethod: (UIButton *) sender :(NSString *)title {
    //[self.userTypeExpandArrow setImage:[UIImage imageNamed:[[ThemeManager sharedManger]getImageNameForKey:EXPAND_ARROW]]];
//    if ([title containsString:@"guest"]) {
//        [self.passwordLabel setHidden:YES];
//        [self.userPasswordTF setHidden:YES];
//        [self.view endEditing:YES];
//    }
//    else {
//        [self.passwordLabel setHidden:NO];
//        [self.userPasswordTF setHidden:NO];
//    }
   // self.userTypeTF.text = title;
    [self hideDropDwonMenu:sender];
    //    [dropDown hideDropDown:sender :CGRectMake(0, 0, self.userTypeView.frame.size.width, 0)];
    //    dropDown = nil;
}

-(void)hideDropDwonMenu :(UIButton*)sender{
    
    [dropDown hideDropDown:sender :CGRectMake(0, 0, self.view1.frame.size.width, 0)];
    dropDown = nil;
    CGRect frame = self.view1.frame;
    frame.size.height = 0;
    self.view1.frame = frame;
}


- (IBAction)localWifiButtonClicked:(id)sender {
    
    _networkSsidTf.userInteractionEnabled = NO;
    _passwordTf.userInteractionEnabled = NO;
    [self.localWifi setSelected:YES];
    [self.internetWifi setSelected:NO];
    //[self.networkSsid  setTitle:@"E4-IOS" forState:UIControlStateNormal];
    self.passwordTf.text = @"123456789";
    self.networkSsidTf.text = [[WMSSettingsController sharedInstance]getWifiSsId];
    

    
    
}

- (IBAction)internetWifiButtonClicked:(id)sender {
    
    [self.localWifi setSelected:NO];
    _networkSsidTf.userInteractionEnabled = YES;
    _passwordTf.userInteractionEnabled = YES;
    
    NSString *takeNetworkSsid = _networkSsidTf.text;
    if ([takeNetworkSsid containsString:@"AquaSmart-"]) {
        _networkSsidTf.text = nil;
        _passwordTf.text = nil;
        
    }
    else
    {
   // [self.networkSsid setTitle:@"" forState:UIControlStateNormal];
    
    [self.internetWifi setSelected:YES];
    self.passwordTf.text = [[WMSSettingsController sharedInstance]getStationModePass];
    self.networkSsidTf.text = [[WMSSettingsController sharedInstance]getStationModeSsId];
   
    }
}
        
        
        
//  -(void)getWirelessMacID
//{
//
//    OrderedDictionary *requestDict = [[OrderedDictionary alloc]initWithCapacity:5];
//    [requestDict insertObject:_SIDVALUE forKey:_SID atIndex:0];
//    [requestDict insertObject:_SADVALUE forKey:_SAD atIndex:1];
//    [requestDict insertObject:_8275 forKey:_CMD atIndex:2];
//    NSError *writeError;
//    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:requestDict options:kNilOptions error:&writeError];
//    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
//    [SVProgressHUD show];
//    timer = [NSTimer scheduledTimerWithTimeInterval: 5.0 target:self selector:@selector(updateCounter:) userInfo:nil repeats: NO];
//    [TcpPacketClass sharedInstance].delegate = self;
//    [[TcpPacketClass sharedInstance]writeOut:jsonString second:[requestDict objectForKey:_CMD]];
//
//}
//
//
//-(void)didRecieveWirelessMacidResponse:(NSDictionary *)jsonDict
//{
//
//
//    NSString *data = [jsonDict valueForKey:_DATA];
//    mutArray=[NSArray alloc];
//    finStr = @"";
//    
//    mutArray=[data componentsSeparatedByString:@"-"];
//    for (int i=0; i<=mutArray.count-1; i++) {
//        NSString *hexStr;
//        str1=[mutArray objectAtIndex:i];
//        hexStr=[NSString stringWithFormat:@"%x",[str1 intValue]];
//        if (hexStr.length<2) {
//            hexStr=[@"0" stringByAppendingString:hexStr];
//        }
//        
//        
//        if(i<mutArray.count-1)
//        {
//            finalString=[hexStr stringByAppendingString:@":"];
//        }
//        
//        else
//        {
//            finalString= hexStr;
//            
//            
//            
//        }
//        
//        finStr = [finStr stringByAppendingString:finalString];
//    }
//    NSLog(@"%@",finStr);
//    //set macid
//    
//    
//
////    if ([data isEqualToString:@"0"]) {
////        [self.view makeToast:NSLocalizedString(@"Mac ID Successfully Recieved!!",nil)];
////    }
////    else
////    {
////        
////        [self.view makeToast:NSLocalizedString(@"Failed to get Mac ID!!",nil)];
////    }
//
//
//}
//


//- (IBAction)networkSsidButtonClicked:(id)sender
//
//{
//    if (_networkPicker.hidden) {
//        self.networkPicker.hidden = NO;
//    }
//   
//    else
//    {self.networkPicker.hidden = YES;
//    }
//}


-(void)viewWillAppear:(BOOL)animated
{

    [self setTopHeaderView];
}
-(void)setTopHeaderView {
    NSDictionary *navDict = @{
                              TITLE_TEXT :NSLocalizedString(@"Network Settings", nil),
                              BACK_IMAGE :@"BackImage",
                              IS_BACK_HIDDEN:@"No"
                              };
    [[AppDelegate appDelegate].superVC setHeaderViewData:navDict];

}


-(void)backClicked {
    UIStoryboard *mainStroyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    MVYSideMenuController *sideMenuController = [self sideMenuController];
    MainViewController *homeVC = [mainStroyBoard instantiateViewControllerWithIdentifier:@"MainViewController"];
    [sideMenuController changeContentViewController:homeVC closeMenu:YES];
}
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView

{
    
    return 1;
    
}




- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component

{
    
    return [WifiNetwork count];
    
}


- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component

{
    
    return WifiNetwork[row];
    
}


- (void) pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component

{
    NSInteger selectedRow = [_networkPicker selectedRowInComponent:0];
    NSString *selectedPickerRow=[WifiNetwork objectAtIndex:selectedRow];
    //[self.networkSsidTf setTitle:[WifiNetwork objectAtIndex:row] forState:UIControlStateNormal];
    _networkSsidTf.text=selectedPickerRow;
    _networkPicker.hidden=YES;
}

-(void)hideKeyboard
{
    [_networkSsidTf resignFirstResponder];
    [_passwordTf resignFirstResponder];
    

}

#pragma mark - moving cursor to the next textfield
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    NSInteger nextTag = textField.tag + 1;
    [self jumpToNextTextField:textField withTag:nextTag];
    return NO;
    
}

- (void)jumpToNextTextField:(UITextField *)textField withTag:(NSInteger)tag
{
    
    UIResponder *nextResponder = [self.view viewWithTag:tag];
    
    if ([nextResponder isKindOfClass:[UITextField class]]) {
        // If there is a next responder and it is a textfield, then it becomes first responder.
        [nextResponder becomeFirstResponder];
    }
    else {
        // If there is not then removes the keyboard.
        [textField resignFirstResponder];
    }
}

#pragma mark - setting length for texfields
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
     if (textField==_networkSsidTf)
    {
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        return (newLength > 30) ? NO : YES;
    }
   
    else if(textField == _passwordTf) {
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        return (newLength >30) ? NO : YES;
    }
    return 0;
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}


- (IBAction)hidePasswordBtnClicked:(id)sender
{
    if (self.passwordTf.text) {
        [self.passwordTf setSecureTextEntry:NO];
    }
    [self performSelector:@selector(passButtonClicked) withObject:nil afterDelay:2.0];
}

-(void)passButtonClicked
{
    if (self.passwordTf.text) {
        [self.passwordTf setSecureTextEntry:YES];
    }
}
//- (IBAction)showpasswordBtnClicked:(id)sender {
//    if (self.passwordTf.text) {
//        [self.passwordTf setSecureTextEntry:NO];
//    }
//}


- (IBAction)refreshButtonClicked:(id)sender {
    

}

- (IBAction)saveButtonClicked:(id)sender
{
    
    //count the textfield charcter
    networkSsidLength = [_networkSsidTf.text length];
    passwordLength = [_passwordTf.text length];
    netSsidString = [NSString stringWithFormat:@"%ld",(long)networkSsidLength];
    netPasswordString = [NSString stringWithFormat:@"%ld",(long)passwordLength];
//      [[WMSSettingsController sharedInstance]setWifiSsId:self.networkSsidTf.text];
//    [[WMSSettingsController sharedInstance]setStationModePass:self.passwordTf.text];
    if (self.networkSsidTf.text.length <= 0) {
        [self.view makeToast:NSLocalizedString(@"Please fill network name.", nil)];
        [self.view endEditing:YES];
        return;
    }
    
    else if(self.networkSsidTf.text.length <6 && self.networkSsidTf.text.length <=30)
    {
        [self.view makeToast:NSLocalizedString(@"Network name should contain atleast 3 characters", nil)];
        [self.view endEditing:YES];
        return;
    }
    
     else if (self.passwordTf.text.length <= 0) {
        [self.view makeToast:NSLocalizedString(@"Please fill network password.", nil)];
        [self.view endEditing:YES];
        return;
    }
    
    
    
    /*if (![self.localWifi isSelected]) {
        [self.view makeToast:NSLocalizedString(@"Please select mode.", nil)];
        [self.view endEditing:YES];
        return;

    }
    
    if ([self.internetWifi isSelected]) {
        [self.view makeToast:NSLocalizedString(@"Please select mode.", nil)];
        [self.view endEditing:YES];
        return;
        
    }
     */
    
    //setting connectivity mode
    //mode =1 for Ap Mode,mode = 2 for Station mode
    if ([self.localWifi isSelected]) {
        connectivityMode = 1;
        connectModeString = [NSString stringWithFormat:@"%d",connectivityMode];
    }
    
    else
    {
        connectivityMode  = 2; //station mode
        connectModeString = [NSString stringWithFormat:@"%d",connectivityMode];
    }
    [self getNetworkSettingApi];
   
    
    
    
}

- (IBAction)networkPasswordCheck:(id)sender {
    
    if (self.passwordTf.text.length <= 0) {
        [self.view makeToast:@"Password should be atleast 6 characters"
                    duration:2.0
                    position:CSToastPositionCenter];
        // [self.view makeToast:NSLocalizedString(@"Please fill license number.", nil)];
        [self.view endEditing:YES];
        return;
}
}


- (IBAction)networkNameCheck:(id)sender
{
    
    if (self.networkSsidTf.text.length <= 0) {
        [self.view makeToast:@"Password should be atleast 6 characters"
                    duration:2.0
                    position:CSToastPositionCenter];
        // [self.view makeToast:NSLocalizedString(@"Please fill license number.", nil)];
        [self.view endEditing:YES];
        return;
        
        
    }
    
}

-(void)getNetworkSettingApi
{


    OrderedDictionary *dataDict = [[OrderedDictionary alloc]initWithCapacity:6];
    [dataDict insertObject:connectModeString forKey:_01 atIndex:0];
    [dataDict insertObject:[self getName:30 :self.networkSsidTf] forKey:_02 atIndex:1];
    [dataDict insertObject:[self getName:30 :self.passwordTf] forKey:_03 atIndex:2];
    [dataDict insertObject:netSsidString forKey:_04 atIndex:3];
    [dataDict insertObject:netPasswordString forKey:_05 atIndex:4];
    
    OrderedDictionary *requestDict = [[OrderedDictionary alloc]initWithCapacity:5];
    [requestDict insertObject:_SIDVALUE forKey:_SID atIndex:0];
    //[requestDict insertObject:_BADVALUE forKey:_BAD atIndex:1];
    [requestDict insertObject:_SADVALUE forKey:_SAD atIndex:1];
    [requestDict insertObject:_8019 forKey:_CMD atIndex:2];
    [requestDict insertObject:dataDict forKey:_DATA atIndex:3];
    NSError *writeError;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:requestDict options:kNilOptions error:&writeError];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    [SVProgressHUD show];
    timer = [NSTimer scheduledTimerWithTimeInterval: 5.0 target:self selector:@selector(getNetworkSettingAgain:) userInfo:nil repeats: NO];
    [TcpPacketClass sharedInstance].delegate = self;
    [[TcpPacketClass sharedInstance]writeOut:jsonString second:[requestDict objectForKey:_CMD]];


}

-(void)getNetworkSettingAgain:(NSTimer *)theTimer
{
    
    int static ticks = 0;
    if (ticks<4) {
        [self getNetworkSettingApi];
    }
    else if (ticks>3)
    {
        [theTimer invalidate];
        [SVProgressHUD dismiss];
        [self.view makeToast:NSLocalizedString(@"Something went wrong.Please try again.", nil)];
        ticks = -1;
    }
    ticks++;
    
    
}


-(void)getNetworkSettingsResponse:(NSDictionary *)jsonDict
{
    [timer invalidate];
    [SVProgressHUD dismiss];
    NSString *data = [jsonDict valueForKey:_DATA];
    if ([data isEqualToString:@"0"]) {
        
        
        [[WMSSettingsController sharedInstance]setStationModeSsId:self.networkSsidTf.text];
        [[WMSSettingsController sharedInstance]setStationModePass:self.passwordTf.text];
        
        
        UIAlertView *alertView = [[UIAlertView alloc]
                                  initWithTitle:nil
                                  message:@"Network Settings Save Successfully.Please restart your AquaSmart.Your application will close after some moment. "
                                  delegate:self
                                  cancelButtonTitle:nil
                                  otherButtonTitles:NSLocalizedString(@"OK", nil), nil];
        
        [alertView show];
        
        
    }
    else
    {
    
      [self.view makeToast:NSLocalizedString(@"Failed to save Network Settings!!",nil)];
    }

}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 0) {
        [self performSelector:@selector(exitFromApp) withObject:nil afterDelay:2.0];
        
    }
    if (buttonIndex == 1) {
        
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

-(void)exitFromApp{
    [SVProgressHUD dismiss];
    exit(0);
}
#pragma dismiss progress loader wether responce appear or not within 5 second:
- (void)updateCounter:(NSTimer *)theTimer {
    [theTimer invalidate];
    [SVProgressHUD dismiss];
    [self.navigationController popViewControllerAnimated:YES];

}
    


    
-(void)textFieldDidBeginEditing: (UITextField *)textField
{
    if (textField == _networkSsidTf)
    {
        //_networkSsidTf.textInputView =_networkPicker;
    
        _networkPicker.hidden=NO;
    }
    else
    {
        _networkPicker.hidden=YES;

    }
}


#pragma mark - Text fields text lenght validations -
-(NSString *)getName :(int)lenght :(UITextField *)texfield {
    NSMutableString *finalUserName = [texfield.text mutableCopy] ;
    
    if (texfield.text.length == lenght) {
        finalUserName = [texfield.text mutableCopy];
    }
    else {
        int count = (int)texfield.text.length;
        for (int i = count; i < lenght; i++) {
            
            [finalUserName appendString:@"#"];
        }
    }
    return finalUserName;
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
