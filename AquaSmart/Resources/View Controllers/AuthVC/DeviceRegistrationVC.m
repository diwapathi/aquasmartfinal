//
//  DeviceRegistrationVC.m
//  AquaSmart
//
//  Created by Mac User on 09/01/17.
//  Copyright © 2017 Mac User. All rights reserved.
//

#import "DeviceRegistrationVC.h"

@interface DeviceRegistrationVC ()<UITextFieldDelegate,PacketReciveProtocol>
{
    NSTimer *timer;
    
    NSString *jsonStr;
    NSString *userName,*phoneNumer;
}
@end

@implementation DeviceRegistrationVC
+(instancetype)sharedInstance {
    
    static DeviceRegistrationVC *deviceVc;
    static dispatch_once_t once;
    dispatch_once(&once, ^{
        deviceVc = [[self alloc] init];
    });
    
    return deviceVc;
}
- (void)viewDidLoad {
    
    
    [[WMSSettingsController sharedInstance]GetTankSettings:YES];
    self.serialNumberTF.text = _serialNumber;
    self.licenseNumberTF.text = _licenseNumber;
    
    _serialNumberTF.userInteractionEnabled = NO;
    _licenseNumberTF.userInteractionEnabled = NO;
    UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    [self.view addGestureRecognizer:gestureRecognizer];
    [self setupOutlets];
    
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 30, 20)];
    self.phoneNumberTF.leftView = paddingView;
    self.phoneNumberTF.leftViewMode = UITextFieldViewModeAlways;
    
    [super viewDidLoad];
   
    // Do any additional setup after loading the view.
}

- (IBAction)checkTextF:(id)sender {
    
    if([_userNameTF.text length]<3) {
        
        [self.view makeToast:@"Buyer name should be of  minimum 3 characters."
                    duration:3.0
                    position:CSToastPositionCenter];
       // [self.view makeToast:NSLocalizedString(@"Buyer name should be of  minimum 3 characters.", nil)];
 
    }
}
- (IBAction)mobileNumCheck:(id)sender
{
    if([_phoneNumberTF.text length]<10) {
        [self.view makeToast:@"Phone number should be of 10 digits."
                    duration:3.0
                    position:CSToastPositionCenter];
       // [self.view makeToast:NSLocalizedString(@"Phone number should be of 10 digits.", nil)];
        
    }
    
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)backClicked {
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)viewDidLayoutSubviews {
    [self.constraintContentViewHeight setConstant:400-(self.view.frame.size.height)];
}

#pragma mark - setting header view 
-(void)setTopHeaderView {
    NSDictionary *navDict = @{
                              TITLE_TEXT :NSLocalizedString(@"Device Registration", nil),
                              IS_BACK_HIDDEN:@"Yes"
                              };
    [[AppDelegate appDelegate].superVC setHeaderViewData:navDict];
}

-(void)viewWillAppear:(BOOL)animated {
    [self.popupView setHidden:YES];
    [self.passwordView setHidden:YES];
    [super viewWillAppear:animated];
    NetworkStatus status = [self checkForWifiConnection];
    if(status == NotReachable || status == ReachableViaWWAN) {
        [self.navigationController.view makeToast:NSLocalizedString(@"Please check your wifi connection", nil)];
    }
    [self setTopHeaderView];
    
}

-(NetworkStatus)checkForWifiConnection {
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    [reachability startNotifier];
    NetworkStatus status = [reachability currentReachabilityStatus];
    return status;
}


#pragma mark - hides keyboard on touch of screen
-(void)hideKeyboard
{
    
    [_userNameTF resignFirstResponder];
    [_phoneNumberTF resignFirstResponder];
    [_licenseNumberTF resignFirstResponder];
    [_serialNumberTF resignFirstResponder];
    
    
    
    
}
#pragma mark - set the textfield tag
- (void)setupOutlets
{
    //self.userNameTF.delegate = self;
    self.userNameTF.tag = 1;
    
    self.phoneNumberTF.delegate = self;
    self.phoneNumberTF.tag = 2;
    
    
    
    self.serialNumberTF.delegate = self;
    self.serialNumberTF.tag = 3;
    
    self.licenseNumberTF.delegate = self;
    self.licenseNumberTF.tag = 4;
    
    
    
    
}

#pragma mark - moving cursor to the next textfield
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    NSInteger nextTag = textField.tag + 1;
    [self jumpToNextTextField:textField withTag:nextTag];
    return NO;
    
}

- (void)jumpToNextTextField:(UITextField *)textField withTag:(NSInteger)tag
{
    
    UIResponder *nextResponder = [self.view viewWithTag:tag];
    
    if ([nextResponder isKindOfClass:[UITextField class]]) {
        
        [nextResponder becomeFirstResponder];
    }
    else {
        // If there is not then removes the keyboard.
        [textField resignFirstResponder];
    }
}
-(BOOL)checkIfStringContainsUppercase:(NSString *)str {
    if ([[str uppercaseStringWithLocale:[NSLocale currentLocale]] isEqualToString:str])
    {   // ... s is uppercase ...
        return NO;
    }
    else
    {   // ... s is not all uppercase ...
        return YES;
    }
}
#pragma mark - setting length for texfields
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    if(textField == _phoneNumberTF) {
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        return (newLength > 10) ? NO : YES;
    }
    else if(textField == _serialNumberTF)
    {
        //do the same with different values
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        return (newLength > 12) ? NO : YES;
    }
    else if (textField==_userNameTF)
    {
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        return (newLength > 15) ? NO : YES;
    }
    else if(textField == _licenseNumberTF)
    {
        //do the same with different values
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        return (newLength > 24) ? NO : YES;
    }
    else if(textField == _passwordTF) {
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        return (newLength >12) ? NO : YES;
    }
    return 0;
}


- (IBAction)registerbtnClicked:(id)sender {
    
    
     NSString *userReg = @"^[A-Za-z0-9_-]{3,15}$";
     NSPredicate *usernameTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",userReg];
    if (self.userNameTF.text.length <= 0) {
        
        [self.view makeToast:NSLocalizedString(@"Please fill buyer name.", nil)];
        [self.view endEditing:YES];
        return;
    }
    
    
    else if ([usernameTest evaluateWithObject:self.userNameTF.text] == NO)
    {
       // NSString *userReg = @"^[A-Za-z0-9_-]{3,15}$";
      //  NSPredicate *usernameTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", userReg];
        [self.view makeToast:NSLocalizedString(@"Please enter valid  buyer name.", nil)];
        [self.view endEditing:YES];
        return ;
        
        
    }
    else if(self.userNameTF.text.length <2 && self.userNameTF.text.length <=30)
    {
        [self.view makeToast:NSLocalizedString(@"Buyer name should contain atleast 3 characters", nil)];
        [self.view endEditing:YES];
        return;
    }
    
    else if (self.phoneNumberTF.text.length <= 0) {
        [self.view makeToast:NSLocalizedString(@"Please fill phone number.", nil)];
        [self.view endEditing:YES];
        return;
    }
    else if (self.phoneNumberTF.text.length < 10) {
        
        
        [self.view makeToast:NSLocalizedString(@"Phone number should contain atleast 10 characters", nil)];
        [self.view endEditing:YES];
        
        return;
    }
    
    else  if ([self.phoneNumberTF.text rangeOfCharacterFromSet:[NSCharacterSet letterCharacterSet]].location != NSNotFound || [self.phoneNumberTF.text rangeOfCharacterFromSet:[NSCharacterSet symbolCharacterSet]].location != NSNotFound ||[self.phoneNumberTF.text rangeOfCharacterFromSet:[NSCharacterSet punctuationCharacterSet]].location != NSNotFound) {
        [self.view makeToast:NSLocalizedString(@"Invalid phone number", nil)];
        [self.view endEditing:YES];
        return;
    }
    else if (self.serialNumberTF.text.length <= 0) {
        [self.view makeToast:NSLocalizedString(@"Please fill serial number.", nil)];
        [self.view endEditing:YES];
        return;
    }
    else if (self.serialNumberTF.text.length <12) {
        [self.view makeToast:NSLocalizedString(@"Serial number should contain atleast 12 characters", nil)];
        [self.view endEditing:YES];
        return;
    }

    
    else  if ([self checkIfStringContainsUppercase:self.serialNumberTF.text]) {
        [self.view makeToast:NSLocalizedString(@"Serial number should contain uppercase characters.", nil)];
        [self.view endEditing:YES];
        return;
    }
    
    
    
    else if (self.licenseNumberTF.text.length <= 0) {
        [self.view makeToast:NSLocalizedString(@"Please fill license number.", nil)];
        [self.view endEditing:YES];
        return;
    }
//    else  if([self checkIfStringContainsUppercase:self.licenseNumberTF.text]) {
//        [self.view makeToast:NSLocalizedString(@"License number should contain uppercase characters.", nil)];
//        return;
//    }
    
    else  if(self.licenseNumberTF.text.length < 24) {
        [self.view makeToast:NSLocalizedString(@"License number should contain atleast 24 characters", nil)];
        [self.view endEditing:YES];
        return;
    }
    
    else
    {
        
        [self callClientRegistrationApi];

        
        
        
    }
    
    
}
#pragma mark - Client Device Registration Api -
//{"sid":"9000","bad":"0100","sad":"000","cmd":"8200","data":{"01":"2","02:"App-UID"","03":"Name","04":"Phone no."}} // "2" in field no "01" indicates that the Black box(HC) is new.
-(void)callClientRegistrationApi {
    ///BOOL status = [[WMSSettingsController sharedInstance]getDeviceAuthenticationStatus];
    NSString *status = [[WMSSettingsController sharedInstance] getDeviceAuthenticationStatus];
    userName = [self getName:30 :self.userNameTF];
    phoneNumer = [self getName:10 :self.phoneNumberTF];
    OrderedDictionary *dataDict = [[OrderedDictionary alloc]initWithCapacity:4];
    if ([status isEqualToString:@"2"] || [status isEqualToString:@"3"]) {
        [dataDict insertObject:@"2" forKey:_01 atIndex:0];
    }
    else {
        [dataDict insertObject:@"1" forKey:_01 atIndex:0];
    }
    [dataDict insertObject:[[WMSSettingsController sharedInstance]getUUID]forKey:_02 atIndex:1];
    [dataDict insertObject:userName forKey:_03 atIndex:2];
    [dataDict insertObject:phoneNumer forKey:_04 atIndex:3];
    
    OrderedDictionary *requestDict = [[OrderedDictionary alloc]initWithCapacity:5];
    [requestDict insertObject:_SIDVALUE forKey:_SID atIndex:0];
    //[requestDict insertObject:_BADVALUE forKey:_BAD atIndex:1];
    [requestDict insertObject:_SADVALUE forKey:_SAD atIndex:1];
    [requestDict insertObject:_8200 forKey:_CMD atIndex:2];
    [requestDict insertObject:dataDict forKey:_DATA atIndex:3];
    NSError *writeError;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:requestDict options:kNilOptions error:&writeError];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    jsonStr = jsonString;
    [SVProgressHUD showWithStatus:@"Please Wait.."];
   timer = [NSTimer scheduledTimerWithTimeInterval: 5.0 target:self selector:@selector(updateCounter:) userInfo:nil repeats: NO];
    [TcpPacketClass sharedInstance].delegate = self;
 [[TcpPacketClass sharedInstance]writeOut:jsonString second:[requestDict objectForKey:_CMD]];
    
}
- (void)updateCounter:(NSTimer *)theTimer {
    [theTimer invalidate];
    [SVProgressHUD dismiss];
    [self.view makeToast:NSLocalizedString(@"Something went wrong Please try Again!!!", nil)];
}

-(void)didRecieveLoginorRegistrationResponse:(NSDictionary *)jsonDict {
    [timer invalidate];
    [SVProgressHUD dismiss];
    if ([[jsonDict valueForKey:_DATA] isEqualToString:@"0"])
    {
        
        [self.view makeToast:NSLocalizedString(@"User registered successfully!!", nil)];
        [[WMSSettingsController sharedInstance] setUserName:[self getName:30:self.userNameTF]];
        [[WMSSettingsController sharedInstance] commit];
        [[AppDelegate appDelegate]pushSidePanelController];
        
                     }
    else if ([[jsonDict valueForKey:_DATA] isEqualToString:@"1"]) {
        
        [[WMSSettingsController sharedInstance] setUserName:[self getName:30:self.userNameTF]];
        [self.view makeToast:NSLocalizedString(@"No more client can be registered.", nil)];
        UIAlertView *alertView = [[UIAlertView alloc]
                                  initWithTitle:nil
                                  message:@"Do you want to delete already registered user?"
                                  delegate:self
                                  cancelButtonTitle:NSLocalizedString(@"No", nil)
                                  otherButtonTitles:NSLocalizedString(@"Yes", nil), nil];
        
        [alertView show];
        
    }
    
    else if ([[jsonDict valueForKey:_DATA] isEqualToString:@"2"]) {
        [self.view makeToast:NSLocalizedString(@"User name already registred.Please sign in with different user name.", nil)];
        return;
    }
    
    else {
        [self.view makeToast:NSLocalizedString(@"Client registration failed", nil)];
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 0) {
       [self dismissViewControllerAnimated:YES completion:nil];
    }
    if (buttonIndex == 1) {
        
        [self.popupView setHidden:NO];
        [self.passwordView setHidden:NO];
//        UIStoryboard *mainStroyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//        
//        ViewRegistartionTVC  *settingsVC = [mainStroyBoard instantiateViewControllerWithIdentifier:@"ViewRegistartionTVC"];
//        
//        [self.navigationController pushViewController:settingsVC animated:YES];
        
    }
}
-(void)didRecieveError:(NSError *)error {
    [SVProgressHUD dismiss];
    [self.view makeToast:NSLocalizedString(@"Socket not connected", nil)];
    // [[Communicator sharedInstance]setup];
}
-(void)showErrorStatus:(NSString *)msg {
   // [self disconnectFromFTP];
    [SVProgressHUD dismiss];
    [self.view makeToast:msg];
}

#pragma mark - Admin Authentication -
- (IBAction)viewPaswordBtnClicked:(id)sender {
    if (self.passwordTF.text) {
        [self.passwordTF setSecureTextEntry:NO];
    }
}

- (IBAction)hidePasswordButtonClicked:(id)sender {
    if (self.passwordTF.text) {
        [self.passwordTF setSecureTextEntry:YES];
    }
}

- (IBAction)cancelButtonClicked:(id)sender {
    self.passwordTF.text = nil;
    [self.popupView setHidden:NO];
}




- (IBAction)okButtonClicked:(id)sender {
    
   // NSString *adminPass = [[NSUserDefaults standardUserDefaults]
    //                        stringForKey:@"adminPass"];
    if (self.passwordTF.text.length <= 0) {
        [self.view makeToast:NSLocalizedString(@"Please enter admin password.", nil)];
        return;
    }
    else if (self.passwordTF.text.length >6 || self.passwordTF.text.length >12) {
        //[self.view makeToast:NSLocalizedString(@"Password should contain characters between 6 and 12.", nil)];
         [self.view makeToast:NSLocalizedString(@"Admin password length should be 6-12 character long.", nil)];
        
        
        return;
        
        
    }
//    else if(![self.passwordTF.text isEqualToString:adminPass])
//    {
//        [self.view makeToast:NSLocalizedString(@"Password mismatch.", nil)];
//        return;
//    
//    }
    
    
    
    else
    {
        [self getAdminPasswordApiCall];
        self.passwordView.hidden = YES;
        
    }
    
}




-(void)getAdminPasswordApiCall
{
    NSString *passwordString = [self getName:12 :_passwordTF];
    OrderedDictionary *requestDict = [[OrderedDictionary alloc] initWithCapacity:5];
    [requestDict insertObject:_SIDVALUE forKey:_SID atIndex:0];
    // [requestDict insertObject:_BADVALUE forKey:_BAD atIndex:1];
    [requestDict insertObject:_SADVALUE1 forKey:_SAD atIndex:1];
    [requestDict insertObject:_8212 forKey:_CMD atIndex:2];
    [requestDict insertObject:passwordString forKey:_DATA atIndex:3];
    NSError *writeError;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:requestDict options:kNilOptions error:&writeError];
    NSString *jsonStr = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    [TcpPacketClass sharedInstance].delegate = self;
    [[TcpPacketClass sharedInstance]writeOut:jsonStr second:[requestDict objectForKey:_CMD]];
    timer = [NSTimer scheduledTimerWithTimeInterval: 5.0 target:self selector:@selector(showAlertAgain:) userInfo:nil repeats: NO];
    
}

#pragma dismiss progress loader wether responce appear or not within 5 second:
- (void)showAlertAgain:(NSTimer *)theTimer {
    [theTimer invalidate];
    [SVProgressHUD dismiss];
    [self.popupView setHidden:NO];
    [self.passwordView setHidden:NO];

}

-(void)getAdminLoginResponse:(NSDictionary *)jsonDict {
    [SVProgressHUD dismiss];
    // NSDictionary *dataDict = [jsonDict valueForKey:_DATA];
    if ([[jsonDict valueForKey:_DATA] isEqualToString:@"0"])  {
        
        [self.popupView setHidden:YES];
        [[WMSSettingsController sharedInstance]FromDeviceRegistration:YES];
        UIStoryboard *mainStroyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        
        RegisteredUserVC  *regUserVC = [mainStroyBoard instantiateViewControllerWithIdentifier:@"RegisteredUserVC"];
        regUserVC.jsonString = jsonStr;
        regUserVC.userName = userName;
        regUserVC.phoneNumber = phoneNumer;

        [self.navigationController pushViewController:regUserVC animated:YES];
        

        
        // [[TouchUtil getInstance] startTimer];
        
    }
    
    else if( [[jsonDict valueForKey:_DATA] isEqualToString:@"1"])
    {
        [self.view makeToast:NSLocalizedString(@"Admin password is not correct.!!", nil)];
        
    }
    
    else {
        // [self.view makeToast:NSLocalizedString(@"Admin password is not correct.!!", nil)];
    }
}

#pragma mark - Text fields text lenght validations -
-(NSString *)getName :(int)lenght :(UITextField *)texfield {
    NSMutableString *finalUserName = [texfield.text mutableCopy] ;
    
    if (texfield.text.length == lenght) {
        finalUserName = [texfield.text mutableCopy];
    }
    else {
        int count = (int)texfield.text.length;
        for (int i = count; i < lenght; i++) {
            
            [finalUserName appendString:@" "];
        }
    }
    return finalUserName;
}







//-(void)viewDidAppear:(BOOL)animated
//{
//    self.isRun = YES;
//    self.isPrepared = NO;
//    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
//        while(self.isRun){
//            if(!self.isPrepared){
//                [[PacketRecieverClass sharedInstance]performConnection];
//            }
//            usleep(1000000);
//        }
//    });
//    
//}
@end
