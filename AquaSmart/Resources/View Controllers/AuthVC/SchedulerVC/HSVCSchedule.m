//
//  HSVCSchedule.m
//  Homesmart
//
//  Created by Neotech on 19/08/16.
//  Copyright © 2016 Neotech. All rights reserved.
//

#import "HSVCSchedule.h"
//#import "ThemeManager.h"
//#import "TabNavigationView.h"


@interface HSVCSchedule () <ScheduleProtocol,PacketReciveProtocol>

{
    BOOL isFromTime;
    NSMutableArray *responseArray;
    NSTimer *timer;
    
}


@property(weak,nonatomic)NSString *from;
@property(weak,nonatomic)NSString *to;
@property(weak,nonatomic)NSString *stringMinutePacketTo;
@property(weak,nonatomic)NSString *stringMinutePacketFrom;
@property (weak, nonatomic) IBOutlet UIButton *setButton;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton1;
@property (weak, nonatomic) IBOutlet UILabel *dateHeaderLabel;
@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;
@property (weak, nonatomic) IBOutlet UIView *popUpView;
@property (weak, nonatomic) IBOutlet UIView *dateTimeView;
@property (weak, nonatomic) IBOutlet UILabel *schedulerLabel;
@property (weak, nonatomic) IBOutlet UILabel *scheduleTimings;
@property (weak, nonatomic) IBOutlet UILabel *fromLabel;
@property (weak, nonatomic) IBOutlet UIButton *fromTimingsButton;
@property (weak, nonatomic) IBOutlet UILabel *toTimingsLabel;
@property (weak, nonatomic) IBOutlet UIButton *toTimingsButton;
@property (weak, nonatomic) IBOutlet UIScrollView *mainScrollView;
@property (weak, nonatomic) IBOutlet UILabel *repeatLabel;
@property (weak, nonatomic) IBOutlet RadioButton *weekDaysBtn;
@property (weak, nonatomic) IBOutlet RadioButton *weekendsButton;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet RadioButton *dailyButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintContentviewHeight;
@property (weak, nonatomic) IBOutlet UILabel *weekdaysLabel;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (weak, nonatomic) IBOutlet UILabel *weekendsLabel;
@property (weak, nonatomic) IBOutlet UIButton *doneButton;
@property (weak, nonatomic) IBOutlet UILabel *dailyLabel;
@property (weak, nonatomic) IBOutlet RadioButton *weeklyButton;
@property (weak, nonatomic) IBOutlet UILabel *weeklyLabel;
@property (strong, nonatomic) IBOutlet UIView *mainView;
@property(strong,nonatomic)NSArray *weekDaysArray;

@property(strong,nonatomic)IBOutlet UITableView *schedulerTableView;
@property(nonatomic,strong)NSMutableArray *subProfileList;
@property(nonatomic,strong)NSArray *oldSubProfileList;

@end

@implementation HSVCSchedule
{
    DatabaseModel *datamodel;
}
@synthesize schedule,daystext,delegate;




- (void)viewDidLoad {
    
    _scheduleTimings.hidden=YES;

   // _scheduleArray = [[NSMutableArray alloc]init];
    [super viewDidLoad];
    
    [self setTopHeaderView];
    [self.dailyButton setSelected:YES];
    
    
    [self setInitialConfiguration];
    if ([self.dailyButton isSelected]==YES) {
        [self.weeklyButton setSelected:NO];
        for (int  i = 1; i <= 7; i++) {
            if (![self.selectedWeekDay containsObject:[NSNumber numberWithInteger:i]]) {
                [self.selectedWeekDay addObject:[NSNumber numberWithInteger:i]];
            }
        }
        [self.collectionView reloadData];
        self.collectionView.userInteractionEnabled = NO;
        
        //        [self.dailyButton setSelected:YES];
        //        for (int i = 1; i <= 7; i++) {
        //            [self.selectedWeekDay addObject:[NSNumber numberWithInt:i]];
        //        }
        
    }

    self.datePicker.datePickerMode = UIDatePickerModeTime;
    if (_scheduleIds==nil) {
        _scheduleIds = @"0";
    }
    if (_isScheduleEditing==YES) {
        
     
        [self.fromTimingsButton setTitle:_startTime forState:UIControlStateNormal];
        [self.toTimingsButton setTitle:_endTime forState:UIControlStateNormal];
        [[WMSSettingsController sharedInstance]fromScheduleEditingButton:NO];
    }
    else
    {
    
        [self.fromTimingsButton setTitle:@"Select" forState:UIControlStateNormal];
        [self.toTimingsButton setTitle:@"Select" forState:UIControlStateNormal];

    }
      // Do any additional setup after loading the view.
}
-(void)setTopHeaderView {
    NSDictionary *navDict = @{
                              TITLE_TEXT :NSLocalizedString(@"Schedule Creation", nil),
                              BACK_IMAGE :@"BackImage",
                              };
    [[AppDelegate appDelegate].superVC setHeaderViewData:navDict];
}

-(void)backClicked {
    UIStoryboard *mainStroyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    MVYSideMenuController *sideMenuController = [self sideMenuController];
    MainViewController *homeVC = [mainStroyBoard instantiateViewControllerWithIdentifier:@"MainViewController"];
    [sideMenuController changeContentViewController:homeVC closeMenu:YES];
}





-(void)viewdidAppear:(BOOL)animated{
    
        
        [self.navigationController setNavigationBarHidden:YES];
        
    
    //[[AppDelegate appDelegate].superViewController updateHeadrLabel:profileInfo.profileName];
}
- (IBAction)backButton:(id)sender {
    
    
    [self.navigationController popViewControllerAnimated:YES];
}


-(void)setInitialConfiguration {
    [self.popUpView setHidden:YES];
  // self.dateTimeView.backgroundColor = [[ThemeManager sharedManger]colorFromKey:BACKGROUND_COLOR];
    // drop shadow
    [self.dateTimeView.layer setShadowColor:[UIColor blackColor].CGColor];
    [self.dateTimeView.layer setShadowOpacity:0.8];
    [self.dateTimeView.layer setShadowRadius:3.0];
    [self.dateTimeView.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    [self.fromTimingsButton setTitle:NSLocalizedString(@"Select", nil) forState:UIControlStateNormal];
    [self.toTimingsButton setTitle:NSLocalizedString(@"Select", nil) forState:UIControlStateNormal];
    self.weekDaysArray = @[@"MO",@"TU",@"WE",@"THU",@"FR",@"SA",@"SU"];
    self.selectedWeekDay = [[NSMutableArray alloc]init];
  //  self.mainView.backgroundColor = [[ThemeManager sharedManger]colorFromKey:BACKGROUND_COLOR];
    self.schedulerLabel.text  = NSLocalizedString(@"Schedules", nil);
    self.scheduleTimings.text = daystext;
    
    self.fromLabel.text = NSLocalizedString(@"From", nil);
    self.toTimingsLabel.text = NSLocalizedString(@"To", nil);
  
    self.repeatLabel.text = NSLocalizedString(@"Repeat", nil);
   //self.repeatLabel.textColor= [[ThemeManager sharedManger]colorFromKey:SUBTITLE_TEXT_COLOR];
    [self.doneButton setTitle:NSLocalizedString(@"Done", nil) forState:UIControlStateNormal];
    [self.cancelButton setTitle:NSLocalizedString(@"Cancel", nil) forState:UIControlStateNormal];
   // [self.doneButton setTitleColor:[[ThemeManager sharedManger]colorFromKey:TITLE_TEXT_COLOR] forState:UIControlStateNormal];
    [self.doneButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.cancelButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
   

    [self.setButton setTitle:NSLocalizedString(@"Set", nil) forState:UIControlStateNormal];
    [self.cancelButton1 setTitle:NSLocalizedString(@"Cancel", nil) forState:UIControlStateNormal];
    [self.dailyButton setImage:[UIImage imageNamed:@"BLUE_RADIO_SELECTOR_OFF"] forState:UIControlStateNormal];
     [self.dailyButton setImage:[UIImage imageNamed:@"BLUE_RADIO_SELECTOR_ON"] forState:UIControlStateSelected];
    
     [self.weeklyButton setImage:[UIImage imageNamed:@"BLUE_RADIO_SELECTOR_ON"] forState:UIControlStateSelected];
     [self.weeklyButton setImage:[UIImage imageNamed:@"BLUE_RADIO_SELECTOR_OFF"] forState:UIControlStateNormal];
    
    [self.fromTimingsButton setTitleEdgeInsets:UIEdgeInsetsMake(0.0, 5.0, 0.0, 0.0)];
    [self.toTimingsButton setTitleEdgeInsets:UIEdgeInsetsMake(0.0, 5.0, 0.0, 0.0)];
    self.fromTimingsButton.layer.cornerRadius = 2.0;
    self.toTimingsButton.layer.cornerRadius = 2.0;
    [self.collectionView setBackgroundColor:[UIColor clearColor]];
    [self.collectionView setAllowsMultipleSelection:YES];
    self.fromTimingsButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    self.toTimingsButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    if (self.isScheduleEditing == YES) {
        [self setPrefillData];
    }
    //self.oldSubProfileList = profileInfo.subProfiles;
    //keeping an old profile xml file
  //  NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
   // NSString *documentsDirectory = [paths objectAtIndex:0];
     //  NSError *error = nil;
   
}

-(void)setPrefillData {
    
    //self.scheduleTimings.text = daystext;
    NSArray *scheduleTimeArray = [daystext componentsSeparatedByString:@"("];
    NSArray *timeArray = [[scheduleTimeArray objectAtIndex:0]componentsSeparatedByString:@"-"];
    [self.fromTimingsButton setTitle:[timeArray objectAtIndex:0] forState:UIControlStateNormal];
    if (![[timeArray objectAtIndex:1] isEqualToString:@"XXXX"]) {
        [self.toTimingsButton setTitle:[timeArray objectAtIndex:1] forState:UIControlStateNormal];
    }
    NSString *weekStr = [scheduleTimeArray objectAtIndex:1];
    weekStr = [weekStr stringByReplacingOccurrencesOfString:@")" withString:@""];
    if ([weekStr isEqualToString:@"Weekdays"]) {
        [self.weekDaysBtn setSelected:YES];
        for (int i = 1; i <= 5; i++) {
            [self.selectedWeekDay addObject:[NSNumber numberWithInt:i]];
        }
    }
    else if ([weekStr isEqualToString:@"Daily"]) {
        [self.dailyButton setSelected:YES];
        for (int i = 1; i <= 7; i++) {
            [self.selectedWeekDay addObject:[NSNumber numberWithInt:i]];
        }
    }
    else if ([weekStr isEqualToString:@"Weekend"]) {
        [self.weekendsButton setSelected:YES];
        [self.selectedWeekDay addObject:[NSNumber numberWithInt:6]];
        [self.selectedWeekDay addObject:[NSNumber numberWithInt:7]];
    }
    else if ([weekStr isEqualToString:@"Weekly"]) {
        NSArray *scheduleInfoArr = [schedule.sid componentsSeparatedByString:@"_"];
        NSString *daysStr = [scheduleInfoArr objectAtIndex:0];
        for (int i = 0; i < [daysStr length];++i)
        {
            int dayNumber = [[NSString stringWithFormat:@"%c",[daysStr characterAtIndex:i]] intValue];
            [self.selectedWeekDay addObject:[NSNumber numberWithInt:dayNumber]];
        }
        [self.weeklyButton setSelected:YES];
    }
    
}

#pragma mark  - Collection view data source and delegate -
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView*)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView*)collectionView numberOfItemsInSection:(NSInteger)section {
    return [self.weekDaysArray count];
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    HSWeekdayCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"WEEKDAY_CELL" forIndexPath:indexPath];
    NSInteger index = indexPath.row;
    cell.weekDayLabel.text = [self.weekDaysArray objectAtIndex:index];
  //  cell.selected = YES;
  //  [collectionView selectItemAtIndexPath:indexPath animated:NO scrollPosition:UICollectionViewScrollPositionNone];
    cell.backgroundColor = [UIColor whiteColor];
    //cell.backgroundColor = [[ThemeManager sharedManger]colorFromKey:TEXT_FIELD_BACKGROUND_COLOR];
    self.weekdaysLabel.textColor = [UIColor blackColor];
    //cell.weekDayLabel.textColor = [[ThemeManager sharedManger]colorFromKey:TITLE_TEXT_COLOR];
    cell.layer.cornerRadius = 2.0;
    if (self.selectedWeekDay.count) {
        if ([self.selectedWeekDay containsObject:[NSNumber numberWithInteger:(index+1)]]) {
            cell.backgroundColor = [UIColor greenColor];
        }
    }
    
       return cell;
}

-(void)collectionView:(UICollectionView *)collectionView
didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    HSWeekdayCell *cell = (HSWeekdayCell *)[collectionView cellForItemAtIndexPath:indexPath];
    cell.backgroundColor = [UIColor greenColor];
    NSInteger dayNum = indexPath.row+1;
    // Add the selected item into the array
    if (![self.selectedWeekDay containsObject:[NSNumber numberWithInteger:dayNum]]) {
        [self.selectedWeekDay addObject:[NSNumber numberWithInteger:dayNum]];
    }
    else {
         [self.selectedWeekDay removeObject:[NSNumber numberWithInteger:dayNum]];
    }
     [self.collectionView reloadData];
}

-(void)collectionView:(UICollectionView *)collectionView
didDeselectItemAtIndexPath:(NSIndexPath *)indexPath {
    HSWeekdayCell *cell = (HSWeekdayCell *)[collectionView cellForItemAtIndexPath:indexPath];
   // cell.backgroundColor = [[ThemeManager sharedManger]colorFromKey:TEXT_FIELD_BACKGROUND_COLOR];
    cell.backgroundColor = [UIColor whiteColor];
    NSInteger dayNum = indexPath.row+1;
    // remove the selected item into the array
    if (![self.selectedWeekDay containsObject:[NSNumber numberWithInteger:dayNum]]) {
        [self.selectedWeekDay removeObject:[NSNumber numberWithInteger:dayNum]];
    }
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
  //  return CGSizeMake(32, 32);
   return CGSizeMake((self.collectionView.frame.size.width)/9.3, self.collectionView.frame.size.height);
}


#pragma mark - IBActions -
- (IBAction)setTimeButtonSelected:(id)sender {
    [self.popUpView setHidden:YES];
    NSDateFormatter *dateFormatter3 = [[NSDateFormatter alloc] init];
    [dateFormatter3 setDateStyle:NSDateFormatterMediumStyle];
    [dateFormatter3 setDateFormat:@"HH:mm"];
    
    NSString *dateString = [dateFormatter3 stringFromDate:self.datePicker.date];
    if (isFromTime) {
        [[NSUserDefaults standardUserDefaults]setObject:dateString forKey:@"From"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        [self.fromTimingsButton setTitle:dateString forState:UIControlStateNormal];
    }
    else {
        
        [[NSUserDefaults standardUserDefaults]setObject:dateString forKey:@"To"];
        [[NSUserDefaults standardUserDefaults]synchronize];
        [self.toTimingsButton setTitle:dateString forState:UIControlStateNormal];
    }
}
- (IBAction)cancelTimeButtonSelected:(id)sender {
    [self.popUpView setHidden:YES];
}


- (IBAction)fromTimingsButtonSelected:(id)sender {
    [self.popUpView setHidden:NO];
    self.dateHeaderLabel.text = NSLocalizedString(@"Set \"From\" time", nil);
    isFromTime = YES;
}

- (IBAction)toTimingsButtonSelected:(id)sender {
    [self.popUpView setHidden:NO];
    self.dateHeaderLabel.text = NSLocalizedString(@"Set \"To\" time", nil);
    //self.toDatePicker.hidden = NO;
    isFromTime = NO;
}


- (IBAction)dailyButtonClicked:(id)sender {
   // [self.weekDaysBtn setSelected:NO];
   // [self.weekendsButton setSelected:NO];
    [self.weeklyButton setSelected:NO];
    for (int  i = 1; i <= 7; i++) {
        if (![self.selectedWeekDay containsObject:[NSNumber numberWithInteger:i]]) {
            [self.selectedWeekDay addObject:[NSNumber numberWithInteger:i]];
        }
    }
    [self.collectionView reloadData];
    self.collectionView.userInteractionEnabled = NO;
}

- (IBAction)weeklyButtonClicked:(id)sender {
    
    [self.dailyButton setSelected:NO];
    for (int  i = 1; i <= 7; i++) {
        
        if (i == 6 || i == 7) {
            if ([self.selectedWeekDay containsObject:[NSNumber numberWithInteger:i]]) {
                [self.selectedWeekDay removeObject:[NSNumber numberWithInteger:i]];
                 self.collectionView.userInteractionEnabled = NO;
            }
        }
        else {
            if (![self.selectedWeekDay containsObject:[NSNumber numberWithInteger:i]]) {
                [self.selectedWeekDay addObject:[NSNumber numberWithInteger:i]];
                 self.collectionView.userInteractionEnabled = YES;
            }
        }
        
    [self.collectionView reloadData];
      self.collectionView.userInteractionEnabled = YES;
}
}

- (IBAction)cancelButtonClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)doneButtonClicked:(id)sender {
    if (_isScheduleEditing==YES)
    {
        
        [self performScheduleEditing];
       
    }
    
    
    
    
    //if new schedule
    else
    {
        [SVProgressHUD showWithStatus:NSLocalizedString(@"Please wait...", nil)];
        if ([self validateSchedule]) {
            
            [self callSaveSchedulerApi];
            [self.view setUserInteractionEnabled:NO];
            if (schedule) {
                NSMutableArray *scheduleArray = [[NSMutableArray alloc]init];
                [scheduleArray addObject:schedule];
                
                
            }
        }
        else
        {
            [SVProgressHUD dismiss];
            NSLog(@"Schedule validation failure");
        }
    }
}


-(void)scheduleEditingSucccess {
    NSString *timeString = [NSString stringWithFormat:@"%@-%@(%@)",self.fromTimingsButton.titleLabel.text,self.toTimingsButton.titleLabel.text,schedule.days];
    if (self.delegate && [self.delegate respondsToSelector:@selector(schduleEdited:)])
    {
        [self.delegate schduleEdited:timeString];
        [self.navigationController popViewControllerAnimated:YES];
        
        
        //[self performSelector:@selector(backClicked) withObject:nil afterDelay:1.0];
    }
    [SVProgressHUD dismiss];
   // [self.view makeToast:NSLocalizedString(@"Schedule edited successfully.", nil)];
}

//   if (!schedule) {
//        schedule = [[Schedule alloc]init];
//          if ([self.weekDaysBtn isSelected]) {
//        schedule.days = NSLocalizedString(@"Weekdays", nil);
//    }
//
//    if ([self.dailyButton isSelected]) {
//        schedule.days = NSLocalizedString(@"Daily", nil);
//    }
//    else if ([self.weeklyButton isSelected]) {
//        schedule.days = NSLocalizedString(@"Weekly", nil);
//    }
//    if (self.selectedWeekDay.count) {
//        NSArray *sorted_Array = [self.selectedWeekDay sortedArrayUsingDescriptors:
//                                 @[[NSSortDescriptor sortDescriptorWithKey:@"intValue"
//                                                                 ascending:YES]]];
//        self.selectedWeekDay  = [sorted_Array mutableCopy];
//    }
//   
//
//    NSString *daysStr = @"" ;
//    for (int i = 0; i < self.selectedWeekDay.count; i++) {
//        daysStr = [daysStr stringByAppendingString:[NSString stringWithFormat:@"%d",[[self.selectedWeekDay objectAtIndex:i] intValue]]];
//    }
//    schedule.days = daysStr;
//
//    if (![self.fromTimingsButton.titleLabel.text isEqualToString:@"Select"]) {
//        _from = self.fromTimingsButton.titleLabel.text;
//        NSDateFormatter *dateFormatter3 = [[NSDateFormatter alloc] init];
//        [dateFormatter3 setDateStyle:NSDateFormatterMediumStyle];
//        [dateFormatter3 setDateFormat:@"HH:mm"];
//      // NSString *dateString = [dateFormatter3 stringFromDate:self.fromTimingsButton.titleLabel.text];
//        NSDate *date = [dateFormatter3 dateFromString:_from];
//        NSCalendar *calendar = [NSCalendar currentCalendar];
//        NSDateComponents *components = [calendar components:(NSCalendarUnitHour | NSCalendarUnitMinute) fromDate:date];
//        NSInteger hour = [components hour];
//        NSInteger minute = [components minute];
//        
//        NSUInteger minutePacketFrom = (hour * 60)+ minute;
//        _stringMinutePacketFrom = [NSString stringWithFormat:@"%lu", (unsigned long)minutePacketFrom];
//
//
//        schedule.start = _stringMinutePacketFrom;
//        
//        
//    }
//       
//    if (![self.toTimingsButton.titleLabel.text isEqualToString:@"Select"]) {
//        _to = self.toTimingsButton.titleLabel.text;
//        NSDateFormatter *dateFormatter3 = [[NSDateFormatter alloc] init];
//        [dateFormatter3 setDateStyle:NSDateFormatterMediumStyle];
//        [dateFormatter3 setDateFormat:@"HH:mm"];
//        NSDate *date = [dateFormatter3 dateFromString:_to];
//        
//        
//        NSCalendar *calendar = [NSCalendar currentCalendar];
//        NSDateComponents *components = [calendar components:(NSCalendarUnitHour | NSCalendarUnitMinute) fromDate:date];
//        NSInteger hour = [components hour];
//        NSInteger minute = [components minute];
//        
//        NSUInteger minutePacketTo = (hour * 60)+ minute;
//        _stringMinutePacketTo = [NSString stringWithFormat:@"%lu", (unsigned long)minutePacketTo];
//
////        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
////        dateFormatter.dateFormat = @"hh:mm a";
////        NSDate *date = [dateFormatter dateFromString:self.toTimingsButton.titleLabel.text];
////        dateFormatter.dateFormat = @"HH:mm";
////        NSString *stopTime24 = [dateFormatter stringFromDate:date];
////        stopTime24 = [stopTime24 stringByReplacingOccurrencesOfString:@":" withString:@""];
////        schedule.stop = stopTime24;
//        schedule.stop = _stringMinutePacketTo;
//            }
//       
//       NSInteger startNum = [schedule.start intValue];
//       NSInteger stopNum = [schedule.stop intValue];
//
//    if (schedule.start == nil || schedule.stop == nil) {
//        [self.view makeToast:NSLocalizedString(@"Please select start or stop time", nil)];
//        return;
//    }
//    if (schedule.days == nil || [schedule.days length] <= 0 || schedule.days == nil) {
//        [self.view makeToast:NSLocalizedString(@"Please select some days to create schedule", nil)];
//        return;
//    }
//    if ([schedule.start isEqualToString:schedule.stop]) {
//        [self.view makeToast:NSLocalizedString(@"Start and stop time cannot be same.", nil)];
//        return;
//    }
//       
//       if (startNum >=stopNum) {
//           [self.view makeToast:NSLocalizedString(@"Start time cannot be greater  than and equal to end time .", nil)];
//           return;
//       }
//       
//      
//       
//    NSString *startStr = [NSString stringWithFormat:@"%@",schedule.start];
//       NSString *stopStr = [NSString stringWithFormat:@"%@",schedule.stop];
//       schedule.sid = [NSString stringWithFormat:@"%@_%@_%@",schedule.days,schedule.start,schedule.stop];
//       BOOL status = [self checkForDuplicateSchedule:startStr stopString:stopStr];
//    if (status == YES)
//    {
//        [self.view makeToast:NSLocalizedString(@"Schedule already exist !", nil)];
//        return;
//    }
//       
//       BOOL OverlapSchedule =  [self checkForOverlapSchedule:startStr endString:stopStr];
//       if (OverlapSchedule==YES) {
//           [self.view makeToast:NSLocalizedString(@"Schedule Overlaps, Please delete or edit the existing schedule !", nil)];
//           return;
//       }
    
//       if (schedule) {
//           NSMutableArray *scheduleList = [[NSMutableArray alloc]init]; 
//           [scheduleList addObject:schedule];
//           
//       }
       
       
       
//       [self callSaveSchedulerApi];
    
  
   
    
    


#pragma mark - Schedule editing -
-(void)performScheduleEditing {
   // [SVProgressHUD showWithStatus:NSLocalizedString(@"Please wait...", nil)];
    if ([self validateSchedule])
    {
        
        [self callSaveSchedulerApi];
    }


}

-(BOOL)validateSchedule {
    schedule = (_isScheduleEditing==NO)?[[Schedule alloc]init]:schedule;
    if ([self.weekDaysBtn isSelected]) {
        schedule.days = NSLocalizedString(@"Weekdays", nil);
    }
    
    if ([self.dailyButton isSelected]) {
        schedule.days = NSLocalizedString(@"Daily", nil);
    }
    else if ([self.weeklyButton isSelected]) {
        schedule.days = NSLocalizedString(@"Weekly", nil);
    }
    if (self.selectedWeekDay.count) {
        NSArray *sorted_Array = [self.selectedWeekDay sortedArrayUsingDescriptors:
                                 @[[NSSortDescriptor sortDescriptorWithKey:@"intValue"
                                                                 ascending:YES]]];
        self.selectedWeekDay  = [sorted_Array mutableCopy];
    }
    
    
    NSString *daysStr = @"" ;
    for (int i = 0; i < self.selectedWeekDay.count; i++) {
        daysStr = [daysStr stringByAppendingString:[NSString stringWithFormat:@"%d",[[self.selectedWeekDay objectAtIndex:i] intValue]]];
    }
    schedule.days = daysStr;
    
    if (![self.fromTimingsButton.titleLabel.text isEqualToString:@"Select"]) {
        _from = self.fromTimingsButton.titleLabel.text;
        NSDateFormatter *dateFormatter3 = [[NSDateFormatter alloc] init];
        [dateFormatter3 setDateStyle:NSDateFormatterMediumStyle];
        [dateFormatter3 setDateFormat:@"HH:mm"];
        // NSString *dateString = [dateFormatter3 stringFromDate:self.fromTimingsButton.titleLabel.text];
        NSDate *date = [dateFormatter3 dateFromString:_from];
        NSCalendar *calendar = [NSCalendar currentCalendar];
        NSDateComponents *components = [calendar components:(NSCalendarUnitHour | NSCalendarUnitMinute) fromDate:date];
        NSInteger hour = [components hour];
        NSInteger minute = [components minute];
        
        NSUInteger minutePacketFrom = (hour * 60)+ minute;
        _stringMinutePacketFrom = [NSString stringWithFormat:@"%lu", (unsigned long)minutePacketFrom];
        
        
        schedule.start = _stringMinutePacketFrom;
        
        
    }
    
    if (![self.toTimingsButton.titleLabel.text isEqualToString:@"Select"]) {
        _to = self.toTimingsButton.titleLabel.text;
        NSDateFormatter *dateFormatter3 = [[NSDateFormatter alloc] init];
        [dateFormatter3 setDateStyle:NSDateFormatterMediumStyle];
        [dateFormatter3 setDateFormat:@"HH:mm"];
        NSDate *date = [dateFormatter3 dateFromString:_to];
        
        
        NSCalendar *calendar = [NSCalendar currentCalendar];
        NSDateComponents *components = [calendar components:(NSCalendarUnitHour | NSCalendarUnitMinute) fromDate:date];
        NSInteger hour = [components hour];
        NSInteger minute = [components minute];
        
        NSUInteger minutePacketTo = (hour * 60)+ minute;
        _stringMinutePacketTo = [NSString stringWithFormat:@"%lu", (unsigned long)minutePacketTo];
        
        //        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        //        dateFormatter.dateFormat = @"hh:mm a";
        //        NSDate *date = [dateFormatter dateFromString:self.toTimingsButton.titleLabel.text];
        //        dateFormatter.dateFormat = @"HH:mm";
        //        NSString *stopTime24 = [dateFormatter stringFromDate:date];
        //        stopTime24 = [stopTime24 stringByReplacingOccurrencesOfString:@":" withString:@""];
        //        schedule.stop = stopTime24;
        schedule.stop = _stringMinutePacketTo;
    }
    
    NSInteger startNum = [schedule.start intValue];
    NSInteger stopNum = [schedule.stop intValue];
    
    if (schedule.start == nil || schedule.stop == nil) {
        [self.view makeToast:NSLocalizedString(@"Please select start or stop time", nil)];
        return NO;
    }
    if (schedule.days == nil || [schedule.days length] <= 0 || schedule.days == nil) {
        [self.view makeToast:NSLocalizedString(@"Please select some days to create schedule", nil)];
        return NO;
    }
    if ([schedule.start isEqualToString:schedule.stop]) {
        [self.view makeToast:NSLocalizedString(@"Start and stop time cannot be same.", nil)];
        return NO;
    }
    
    if (startNum >=stopNum) {
        [self.view makeToast:NSLocalizedString(@"Start time cannot be greater  than and equal to end time .", nil)];
        return NO;
    }
    
    
    
    NSString *startStr = [NSString stringWithFormat:@"%@",schedule.start];
    NSString *stopStr = [NSString stringWithFormat:@"%@",schedule.stop];
    schedule.sid = [NSString stringWithFormat:@"%@_%@_%@",schedule.days,schedule.start,schedule.stop];
    BOOL status = [self checkForDuplicateSchedule:startStr stopString:stopStr];
    if (status == YES)
    {
        [self.view makeToast:NSLocalizedString(@"Schedule already exist !", nil)];
        return NO;
    }
    
    BOOL OverlapSchedule =  [self checkForOverlapSchedule:startStr endString:stopStr];
    if (OverlapSchedule==YES) {
        [self.view makeToast:NSLocalizedString(@"Schedule Overlaps, Please delete or edit the existing schedule !", nil)];
        return NO;
    }
    return YES;
}

-(void)callSaveSchedulerApi
{
    
    NSString *selectedWeekdayString = [[self.selectedWeekDay componentsJoinedByString:@","] mutableCopy];
    NSString *newString = [selectedWeekdayString stringByReplacingOccurrencesOfString:@"," withString:@""];
    OrderedDictionary *dataDict = [[OrderedDictionary alloc]initWithCapacity:8];
    [dataDict insertObject:@"1" forKey:_01 atIndex:0];
    [dataDict insertObject:_scheduleIds forKey:_02 atIndex:1];
    [dataDict insertObject:@"1" forKey:_03 atIndex:2];
    [dataDict insertObject:@"0" forKey:_04 atIndex:3];
//    [dataDict insertObject:@"600" forKey:_05 atIndex:4];
//    [dataDict insertObject:@"610" forKey:_06 atIndex:5];

   [dataDict insertObject:_stringMinutePacketFrom forKey:_05 atIndex:4];
    [dataDict insertObject:_stringMinutePacketTo forKey:_06 atIndex:5];
    [dataDict insertObject:newString forKey:_07 atIndex:6];
    
    
    OrderedDictionary *requestDict = [[OrderedDictionary alloc]initWithCapacity:5];
    [requestDict insertObject:_SIDVALUE forKey:_SID atIndex:0];
    //[requestDict insertObject:_BADVALUE forKey:_BAD atIndex:1];
    [requestDict insertObject:_SADVALUE forKey:_SAD atIndex:1];
    [requestDict insertObject:_7004 forKey:_CMD atIndex:2];
    [requestDict insertObject:dataDict forKey:_DATA atIndex:3];
    NSError *writeError;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:requestDict options:kNilOptions error:&writeError];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    [SVProgressHUD showWithStatus:@"Please Wait.."];

    [TcpPacketClass sharedInstance].delegate = self;
    [[TcpPacketClass sharedInstance]writeOut:jsonString second:[requestDict objectForKey:_CMD]];
    timer = [NSTimer scheduledTimerWithTimeInterval:5.0 target:self selector:@selector(updateCounter:) userInfo:nil repeats: NO];
}


- (void)updateCounter:(NSTimer *)theTimer {
    [self.view makeToast:NSLocalizedString(@"Something went wrong.Try Again!!", nil)];
    [theTimer invalidate];
    [SVProgressHUD dismiss];
    
    
    [self.navigationController popViewControllerAnimated:YES];
    

    
}






#pragma  mark - schedule create response
-(void)getScheduleCreateResponse:(NSDictionary *)jsonDict {
    
    [timer invalidate];
    [SVProgressHUD dismiss];
   // NSString *dictStr = [NSString stringWithFormat:@"%@",jsonDict];
    //[_scheduleArray addObject:dictStr];
    
    NSLog(@"RESPONSE");
//    if (![[jsonDict valueForKey:_DATA] isKindOfClass:[NSString class]]) {
//        NSDictionary *dataDict = [jsonDict valueForKey:_DATA];
//        if ([[dataDict valueForKey:_02] isEqualToString:@"1"]) {
//            UIStoryboard *mainStroyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//            
//           // [_scheduleArray addObject:schedule];
//                [self.view makeToast:NSLocalizedString(@"Schedule Created Successfully", nil)];
//                BeginScheduleVC  * beginscheduleVC = [[BeginScheduleVC alloc]init];
//                 beginscheduleVC = [mainStroyBoard instantiateViewControllerWithIdentifier:@"BeginScheduleVC"];
//            
//
//            beginscheduleVC.selectWeekDay = [NSMutableArray arrayWithArray:_selectedWeekDay];
//            [self.navigationController popViewControllerAnimated:YES];
//            
//                   }
//        
//        else if ([[dataDict valueForKey:_02] isEqualToString:@"2"]) {
//            UIStoryboard *mainStroyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//            [self.view makeToast:NSLocalizedString(@"Schedule Created Successfully", nil)];
//
//          //  [_scheduleArray addObject:schedule];
//            BeginScheduleVC  * beginscheduleVC = [[BeginScheduleVC alloc]init];
//            beginscheduleVC = [mainStroyBoard instantiateViewControllerWithIdentifier:@"BeginScheduleVC"];
//            
//            beginscheduleVC.selectWeekDay = [NSMutableArray arrayWithArray:_selectedWeekDay];
//            [self.navigationController popViewControllerAnimated:YES];
//            
//            
//            
//        }
//        
//        else
//        {
//            UIStoryboard *mainStroyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//            
//           // [_scheduleArray addObject:schedule];
//            BeginScheduleVC  * beginscheduleVC = [[BeginScheduleVC alloc]init];
//            beginscheduleVC = [mainStroyBoard instantiateViewControllerWithIdentifier:@"BeginScheduleVC"];
//            [self.view makeToast:NSLocalizedString(@"Schedule Created Successfully", nil)];
//
//            beginscheduleVC.selectWeekDay = [NSMutableArray arrayWithArray:_selectedWeekDay];
//            [self.navigationController popViewControllerAnimated:YES];
//            
//                    }
//        
//    }
    
    if (_isScheduleEditing ==YES) {
        [self scheduleEditingSucccess];
         _isScheduleEditing = NO;
    }
    
    else
    {
    [self performSelector:@selector(refreshScheduleList) withObject:nil afterDelay:1.5];
    }
}



-(void)refreshScheduleList {
    if (self.delegate && [self.delegate respondsToSelector:@selector(scheduleCreated)])
    {
        [self.delegate scheduleCreated];
        [self.navigationController popViewControllerAnimated:YES];
    }
    

    
}


-(BOOL)checkForDuplicateSchedule:(NSString *)start stopString:(NSString *)stop{
   
    
    NSString *starttimeinMinute;
    NSString *stoptimeinMinute;
    NSUInteger startminutePacketTo;
    for (Schedule *scheduleInfo in self.scheduleList) {
        if (scheduleInfo.start) {
            
        
        NSDateFormatter *dateFormatt = [[NSDateFormatter alloc] init];
            [dateFormatt setDateStyle:NSDateFormatterMediumStyle];
            [dateFormatt setDateFormat:@"HH:mm"];
        NSDate *date = [dateFormatt dateFromString:scheduleInfo.start ];
        
        
        NSCalendar *calendar = [NSCalendar currentCalendar];
        NSDateComponents *components = [calendar components:(NSCalendarUnitHour | NSCalendarUnitMinute) fromDate:date];
        NSInteger hour = [components hour];
        NSInteger minute = [components minute];
        
        startminutePacketTo = (hour * 60)+ minute;
      starttimeinMinute = [NSString stringWithFormat:@"%lu", (unsigned long)startminutePacketTo];
        }
        
        if (scheduleInfo.stop) {
            
        
        
            NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
            [dateFormat setDateStyle:NSDateFormatterMediumStyle];
            [dateFormat setDateFormat:@"HH:mm"];
            NSDate *date = [dateFormat dateFromString:scheduleInfo.stop ];
            
            
            NSCalendar *calendar = [NSCalendar currentCalendar];
            NSDateComponents *components = [calendar components:(NSCalendarUnitHour | NSCalendarUnitMinute) fromDate:date];
            NSInteger hour = [components hour];
            NSInteger minute = [components minute];
            
            NSUInteger stopminutePacketTo = (hour * 60)+ minute;
           stoptimeinMinute = [NSString stringWithFormat:@"%lu", (unsigned long)stopminutePacketTo];

        
        }

        if([starttimeinMinute isEqualToString:start]&&[stoptimeinMinute isEqualToString:stop]){
            return YES;
        }
        
    }
    return NO;

}


-(BOOL)checkForOverlapSchedule:(NSString *)begin endString:(NSString *)end
    {
    
        NSUInteger startminutePacketTo = 0;
        NSUInteger stopminutePacketTo = 0;
  
    NSInteger startCreate = [begin intValue];
    NSInteger stopCreate = [end intValue];
        
        
    NSString *starttimeinMinute;
    NSString *stoptimeinMinute;
    

    for (Schedule *scheduleInfo in self.scheduleList) {
        if (scheduleInfo.start) {
            
            
            NSDateFormatter *dateFormatt = [[NSDateFormatter alloc] init];
            [dateFormatt setDateStyle:NSDateFormatterMediumStyle];
            [dateFormatt setDateFormat:@"HH:mm"];
            NSDate *date = [dateFormatt dateFromString:scheduleInfo.start ];
            
            
            NSCalendar *calendar = [NSCalendar currentCalendar];
            NSDateComponents *components = [calendar components:(NSCalendarUnitHour | NSCalendarUnitMinute) fromDate:date];
            NSInteger hour = [components hour];
            NSInteger minute = [components minute];
            
            startminutePacketTo = (hour * 60)+ minute;
            starttimeinMinute = [NSString stringWithFormat:@"%lu", (unsigned long)startminutePacketTo];
        }
        
        if (scheduleInfo.stop) {
            
            
            
            NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
            [dateFormat setDateStyle:NSDateFormatterMediumStyle];
            [dateFormat setDateFormat:@"HH:mm"];
            NSDate *date = [dateFormat dateFromString:scheduleInfo.stop ];
            
            
            NSCalendar *calendar = [NSCalendar currentCalendar];
            NSDateComponents *components = [calendar components:(NSCalendarUnitHour | NSCalendarUnitMinute) fromDate:date];
            NSInteger hour = [components hour];
            NSInteger minute = [components minute];
            
            stopminutePacketTo = (hour * 60)+ minute;
            stoptimeinMinute = [NSString stringWithFormat:@"%lu", (unsigned long)stopminutePacketTo];
            
            
        }
        
    
        if (startminutePacketTo>=startCreate && startminutePacketTo<stopCreate) {
            
            return YES;
            
        }
        
        if (startminutePacketTo < startCreate && stopminutePacketTo> startCreate)
        
            {
            
                return  YES;
            }
        
    }
        return NO;
    }

-(void)didRecieveError:(NSError *)error {
    [SVProgressHUD dismiss];
    [self.view makeToast:NSLocalizedString(@"Socket not connected", nil)];
  //   [[Communicator sharedInstance]setup];
    }
-(void)showErrorStatus:(NSString *)msg {
    // [self disconnectFromFTP];
   //
 //   [SVProgressHUD dismiss];
    [self.view makeToast:msg];
}

-(void)viewWillAppear:(BOOL)animated
{
    
    
    [self.navigationController setNavigationBarHidden:YES];
   // [self getDeviceDateTimeApiCall];
    NetworkStatus status = [self checkForWifiConnection];
    if(status == NotReachable || status == ReachableViaWWAN) {
        [self.navigationController.view makeToast:NSLocalizedString(@"Please check your wifi connection", nil)];
    }
    [[AppDelegate appDelegate].superVC updateHeadrLabel:NSLocalizedString(@"Schedule Creation", nil)];

    
}
-(NetworkStatus)checkForWifiConnection {
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    [reachability startNotifier];
    NetworkStatus status = [reachability currentReachabilityStatus];
    return status;
}



- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    
}
 
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
