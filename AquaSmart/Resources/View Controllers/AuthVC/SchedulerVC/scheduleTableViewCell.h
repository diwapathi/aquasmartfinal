//
//  scheduleTableViewCell.h
//  AquaSmart
//
//  Created by Mac User on 23/01/17.
//  Copyright © 2017 Mac User. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Schedule.h"
@interface scheduleTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *serialNumber;
@property (strong, nonatomic) IBOutlet UILabel *pumpName;
@property (strong, nonatomic) IBOutlet UILabel *startTime;
@property (strong, nonatomic) IBOutlet UILabel *endTime;
@property (weak, nonatomic) IBOutlet UILabel *daysLabel;
@property (weak, nonatomic) IBOutlet UIButton *deleteScheduleButton;
@property (weak, nonatomic) IBOutlet UIButton *editScheduleButton;
@property (weak, nonatomic) IBOutlet UILabel *startTimeHide;
@property (weak, nonatomic) IBOutlet UILabel *endTimeHide;


@property (weak, nonatomic) IBOutlet UISwitch *scheduleEnableDisableSwitch;
-(void)showCellData:(NSArray *)dataArray;
@end
