//
//  BeginScheduleVC.m
//  AquaSmart
//
//  Created by Neotech on 21/02/17.
//  Copyright © 2017 Mac User. All rights reserved.
//

#import "BeginScheduleVC.h"
#import "Schedule.h"
NSString *value = @"0";
@interface BeginScheduleVC ()<PacketReciveProtocol,ScheduleProtocol,UIGestureRecognizerDelegate,UITableViewDelegate,UITableViewDataSource>
{
    NSTimer *timer;
    NSMutableArray *scheduleData;
    NSMutableArray *dataArray;
    NSUInteger  selectedIndex;
    NSIndexPath *indexPath1;
    NSMutableString *  appendedFinalString;
    NSMutableString * appendedEndString;
    scheduleTableViewCell *accessElement;
    NSString *value1;
    NSString *value2;
    NSString *value3;
    
    
    NSMutableArray *ScheduleArray;
    NSIndexPath *deleteindexPath;
    
    DatabaseModel *datamodel;
    
    // NSMutableArray *responseArray;
}

@end

@implementation BeginScheduleVC
+(instancetype)sharedInstance {
    
    static BeginScheduleVC *scheduleVC;
    static dispatch_once_t once;
    dispatch_once(&once, ^{
        scheduleVC = [[self alloc] init];
    });
    
    return scheduleVC;
}
- (void)viewDidLoad {
    
    
    [self setTopHeaderView];
    
    datamodel=[[DatabaseModel alloc]init];
    [super viewDidLoad];
    
    
    //[self setInitialConfiguration];
    
    scheduleData = [[NSMutableArray alloc] init];
    _hsvc = [[HSVCSchedule alloc]init];
    accessElement = [[scheduleTableViewCell alloc]init];
    _serialArray = [[NSMutableArray alloc]initWithObjects:@"1",@"2",@"3 ", nil];
    _startArray = [[NSMutableArray alloc]init];
    _daysArray = [[NSMutableArray alloc]init];
    
    _endArray = [[NSMutableArray alloc]init];
    _startHideArray = [[NSMutableArray alloc]init];
    _endHideArray = [[NSMutableArray alloc]init];
    
    // Do any additional setup after loading the view.
}

#pragma mark - Initial data load -
-(void)setTopHeaderView {
    NSDictionary *navDict = @{
                              TITLE_TEXT :NSLocalizedString(@"Pumping Schedule", nil),
                              BACK_IMAGE :@"BackImage",
                              IS_BACK_HIDDEN:@"No"
                              };
    [[AppDelegate appDelegate].superVC setHeaderViewData:navDict];
}

-(void)backClicked {
    UIStoryboard *mainStroyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    MVYSideMenuController *sideMenuController = [self sideMenuController];
    MainViewController *homeVC = [mainStroyBoard instantiateViewControllerWithIdentifier:@"MainViewController"];
    [sideMenuController changeContentViewController:homeVC closeMenu:YES];
}
-(void)viewWillAppear:(BOOL)animated {
    
    
    
    [self getScheduleApi];
    [super viewWillAppear:animated];
    NetworkStatus status = [self checkForWifiConnection];
    if(status == NotReachable || status == ReachableViaWWAN) {
        [self.navigationController.view makeToast:NSLocalizedString(@"Please check your wifi connection", nil)];
    }
    [[AppDelegate appDelegate].superVC updateHeadrLabel:NSLocalizedString(@"Pumping Schedule", nil)];
    
}

-(NetworkStatus)checkForWifiConnection {
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    [reachability startNotifier];
    NetworkStatus status = [reachability currentReachabilityStatus];
    return status;
}



//
//-(void)setInitialConfiguration {
//    UILongPressGestureRecognizer *lpgr = [[UILongPressGestureRecognizer alloc]
//                                          initWithTarget:self action:@selector(handleLongPress:)];
//    lpgr.minimumPressDuration = 1.0; //seconds
//    lpgr.delegate = self;
//    [self.scheduleList addGestureRecognizer:lpgr];
//    
//}






//-(void)handleLongPress:(UILongPressGestureRecognizer *)gestureRecognizer
//{
//    CGPoint p = [gestureRecognizer locationInView:self.scheduleList];
//    NSIndexPath *LPressindexPath = [self.scheduleList indexPathForRowAtPoint:p];
//    if (LPressindexPath == nil) {
//        NSLog(@"long press on table view but not on a row");
//    } else if( gestureRecognizer.state == UIGestureRecognizerStateBegan) {
//        selectedIndex  = LPressindexPath;
//        UIAlertView *alertView = [[UIAlertView alloc]
//                                  initWithTitle:nil
//                                  message:NSLocalizedString(@"Do you really want to delete this Schedule", nil)
//                                  delegate:self
//                                  cancelButtonTitle:NSLocalizedString(@"No", nil)
//                                  otherButtonTitles:NSLocalizedString(@"Yes", nil), nil];
//        
//        [alertView show];
//    }
//}

//- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
//    if (buttonIndex == 0) {
//        
//    }
//    if (buttonIndex == 1) {
//        [self callDeleteSchedulerApi];
//    }
//}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return scheduleData.count;
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"scheduleCell";
    scheduleTableViewCell *cell = [_scheduleList dequeueReusableCellWithIdentifier:cellIdentifier];
    
    Schedule *scheduleModel = [scheduleData objectAtIndex:indexPath.row];
    cell.serialNumber.text = [_serialArray objectAtIndex:indexPath.row];
    cell.startTime.text = scheduleModel.start;
    cell.endTime.text = scheduleModel.stop;
    cell.daysLabel.text = scheduleModel.days;
    //[cell.daysLabel setHidden:YES];
    [cell.scheduleEnableDisableSwitch addTarget:self action:@selector(switchOperation:) forControlEvents:UIControlEventValueChanged];
    [cell.deleteScheduleButton addTarget:self action:@selector(removeBtnClicked:) forControlEvents:(UIControlEventTouchUpInside)];
    [cell.editScheduleButton addTarget:self action:@selector(geteditIndexPath:) forControlEvents:(UIControlEventTouchUpInside)];
   
    cell.scheduleEnableDisableSwitch.on = YES;
    cell.startTimeHide.hidden = YES;
    cell.endTimeHide.hidden = YES;
    cell.daysLabel.hidden = YES;
    // NSInteger row=indexPath.row;
    
    
    
    if ([value1 isEqualToString:@"1"]&&indexPath.row==0) {
        cell.scheduleEnableDisableSwitch.on= YES;
    }
    
    
    else if([value1 isEqualToString:@"2"]&&indexPath.row==0)
    {
        cell.scheduleEnableDisableSwitch.on= NO;
        
    }
   else  if ([value2 isEqualToString:@"1"]&&indexPath.row==1) {
        cell.scheduleEnableDisableSwitch.on= YES;
    }
   else  if([value2 isEqualToString:@"2"]&&indexPath.row==1)
    {
        cell.scheduleEnableDisableSwitch.on= NO;
        
    }
   else if ([value3 isEqualToString:@"1"]&&indexPath.row==2) {
        cell.scheduleEnableDisableSwitch.on= YES;
    }
    if([value3 isEqualToString:@"2"]&&indexPath.row==2)
    {
        cell.scheduleEnableDisableSwitch.on= NO;
        
    }
    
    
   
    
    return cell;
    
}


//schedule protocol
-(void)schduleEdited:(NSString *)timeString {
    [self.scheduleList reloadData];
    self.timeString = timeString;
    [self.view makeToast:NSLocalizedString(@"Schedule edited successfully.", nil)];
    //self.scheduleTimeLabel.text = timeString;
}


-(void)scheduleCreated {

    
    [self.scheduleList reloadData];
    [self.view makeToast:NSLocalizedString(@"Schedule Created Successfully",nil)];

    
}

-(void)geteditIndexPath:(id)sender
{
    _isEditClicked = YES;
    
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.scheduleList];
    NSIndexPath *indexPath = [self.scheduleList indexPathForRowAtPoint:buttonPosition];
    selectedIndex =  (int)indexPath.row;
    scheduleTableViewCell *cell = (scheduleTableViewCell *)[self.scheduleList cellForRowAtIndexPath:indexPath];
     UIStoryboard *mainStroyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    HSVCSchedule  *scheduleVC = [mainStroyBoard instantiateViewControllerWithIdentifier:@"HSVCSchedule"];
     scheduleVC.delegate = self;
    scheduleVC.isScheduleEditing = _isEditClicked;
    _scheduleInfo = [scheduleData objectAtIndex:indexPath.row];
    scheduleVC.schedule = _scheduleInfo;
    scheduleVC.daystext = self.timeString;
    scheduleVC.scheduleList = [scheduleData copy];
    _scheduleId = cell.serialNumber.text;
    
  //  _startTime = cell.startTimeHide.text;
   // _endTime = cell.endTimeHide.text;
    _weekDay = cell.daysLabel.text;
    //added today
    _startTime = cell.startTime.text;
    _endTime = cell.endTime.text;
    
    scheduleVC.scheduleIds = _scheduleId;
  
    //added today
    scheduleVC.startTime = _startTime;
    scheduleVC.endTime = _endTime;
    
    [self.navigationController pushViewController:scheduleVC animated:YES];
    
}

//-(void)getIndexPath:(id)sender
//{
//    
//    
//    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.scheduleList];
//    deleteindexPath = [self.scheduleList indexPathForRowAtPoint:buttonPosition];
//    selectedIndex =  (int)deleteindexPath.row;
//    scheduleTableViewCell *cell = (scheduleTableViewCell *)[self.scheduleList cellForRowAtIndexPath:deleteindexPath];
//    
//    _scheduleId = cell.serialNumber.text;
//    _startTime = cell.startTimeHide.text;
//    _endTime = cell.endTimeHide.text;
//    _weekDay = cell.daysLabel.text;
//    
//    
//    
//    [self callDeleteSchedulerApi];
//}


#pragma mark - Remove schedule -
-(void)removeBtnClicked:(id)sender {
    
    
    
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.scheduleList];
    NSIndexPath *indexPath = [self.scheduleList indexPathForRowAtPoint:buttonPosition];
    selectedIndex = (int)indexPath.row;
    scheduleTableViewCell *cell = (scheduleTableViewCell *)[self.scheduleList cellForRowAtIndexPath:indexPath];
    _scheduleId = cell.serialNumber.text;
    _startTime = cell.startTimeHide.text;
    _endTime = cell.endTimeHide.text;
    _weekDay = cell.daysLabel.text;

    
    NSString *alertText;
   
        alertText = NSLocalizedString(@"Do you really want to remove the schedule?", nil);
    
    UIAlertView *alertView = [[UIAlertView alloc]
                              initWithTitle:nil
                              message:alertText
                              delegate:self
                              cancelButtonTitle:NSLocalizedString(@"No", nil)
                              otherButtonTitles:NSLocalizedString(@"Yes", nil), nil];
    
    [alertView show];
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 0) {
        
    }
    if (buttonIndex == 1) {
        [self callDeleteSchedulerApi];
        
    }
}
-(void)switchOperation:(id)sender
{
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.scheduleList];
    NSIndexPath *indexPath = [self.scheduleList indexPathForRowAtPoint:buttonPosition];
    selectedIndex =  (int)indexPath.row;
    scheduleTableViewCell *cell = (scheduleTableViewCell *)[self.scheduleList cellForRowAtIndexPath:indexPath];
    
    
    _scheduleId = cell.serialNumber.text;
    _startTime = cell.startTimeHide.text;
    _endTime = cell.endTimeHide.text;
    _weekDay = cell.daysLabel.text;
    // _weekDay = cell.daysLabel.text;
    //[hsvc getSelectWeekDay];
    
    
    if (cell.scheduleEnableDisableSwitch.on) {
        [self callEnableSchedulerApi ];
    }
    else
        
    {
        [self callDisableSchedulerApi ];
        
        
    }
    
}


-(UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewCellEditingStyleDelete;
}


- (IBAction)createScheduleEventClicked:(id)sender {
    
    UIStoryboard *mainStroyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    HSVCSchedule  *scheduleVC = [mainStroyBoard instantiateViewControllerWithIdentifier:@"HSVCSchedule"];
    scheduleVC.delegate = self;
    scheduleVC.scheduleList = [scheduleData mutableCopy];
    [self.navigationController pushViewController:scheduleVC animated:YES];
}


#pragma mark - edit schedule event


- (IBAction)showNotifyButtonClicked:(id)sender
{
    
    UIStoryboard *mainStroyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    NotificationViewController *notificationVC = [[NotificationViewController alloc]init];
    notificationVC = [mainStroyBoard instantiateViewControllerWithIdentifier:@"NotificationViewController"];
    notificationVC.notifyArray=[self.notification mutableCopy];
    [self.navigationController pushViewController:notificationVC animated:nil];
}


-(BOOL)tableView:(UITableView *)tableView shouldHighlightRowAtIndexPath:(NSIndexPath *)indexPath
{
    return NO;
}


//-(void)passSelectWeekDay:(NSMutableArray *)dataArray
//{
//
//    self.selectWeekDay = [NSMutableArray arrayWithArray:dataArray];
//}



#pragma mark - schedule Enable Api
-(void)callEnableSchedulerApi
{

    OrderedDictionary *dataDict = [[OrderedDictionary alloc]initWithCapacity:8];
    [dataDict insertObject:@"1" forKey:_01 atIndex:0];
    [dataDict insertObject:_scheduleId forKey:_02 atIndex:1];
    [dataDict insertObject:@"1" forKey:_03 atIndex:2];
    [dataDict insertObject:@"0" forKey:_04 atIndex:3];
    
    [dataDict insertObject:_startTime forKey:_05 atIndex:4];
    [dataDict insertObject:_endTime forKey:_06 atIndex:5];
    [dataDict insertObject:_weekDay  forKey:_07 atIndex:6];
    
    
    OrderedDictionary *requestDict = [[OrderedDictionary alloc]initWithCapacity:5];
    [requestDict insertObject:_SIDVALUE forKey:_SID atIndex:0];
    //[requestDict insertObject:_BADVALUE forKey:_BAD atIndex:1];
    [requestDict insertObject:_SADVALUE forKey:_SAD atIndex:1];
    [requestDict insertObject:_7004 forKey:_CMD atIndex:2];
    [requestDict insertObject:dataDict forKey:_DATA atIndex:3];
    NSError *writeError;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:requestDict options:kNilOptions error:&writeError];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    [SVProgressHUD showWithStatus:@"Please Wait.."];
    [TcpPacketClass sharedInstance].delegate = self;
    [[TcpPacketClass sharedInstance]writeOut:jsonString second:[requestDict objectForKey:_CMD]];
    timer = [NSTimer scheduledTimerWithTimeInterval:5.0 target:self selector:@selector(updateCounter:) userInfo:nil repeats: NO];
}


#pragma dismiss progress loader wether responce appear or not within 5 second:
- (void)updateCounter:(NSTimer *)theTimer {
    [theTimer invalidate];
    [SVProgressHUD dismiss];
    
    //[self showAlertView:theTimer];
    
    
}

-(void)showAlertView:(NSTimer *)myTimer

{
    [SVProgressHUD dismiss];
    [timer invalidate];
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@"Error"
                                  message:@"Something Went Wrong.Please press Retry "
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    
    UIAlertAction* reTry = [UIAlertAction
                            actionWithTitle:@"RETRY"
                            style:UIAlertActionStyleDefault
                            handler:^(UIAlertAction * action)
                            {
                                [self getScheduleApi];
                                
                                [alert dismissViewControllerAnimated:YES completion:nil];
                            }];
    
    [alert addAction:reTry];
    [self.navigationController presentViewController:alert animated:YES completion:nil];
    //[[AppDelegate appDelegate].superVC presentViewController:alert animated:YES completion:nil];
}







#pragma mark - schedule response here
-(void)getScheduleEnableResponse:(NSDictionary *)jsonDict
{
    [SVProgressHUD dismiss];
    NSLog(@"RESPONSE");
    if (![[jsonDict valueForKey:_DATA] isKindOfClass:[NSString class]]) {
        NSDictionary *dataDict = [jsonDict valueForKey:_DATA];
        if ([[dataDict valueForKey:_03] isEqualToString:@"1"]) {
            
            [self.view makeToast:NSLocalizedString(@"Schedule Enabled Successfully",nil)];
            
        }
        
        else if ([[dataDict valueForKey:_03] isEqualToString:@"2"]) {
            [self.view makeToast:NSLocalizedString(@"Schedule Disabled Successfully",nil)];
            
        }
        
        else if ([[dataDict valueForKey:_03] isEqualToString:@"3"]) {
            
            
            [scheduleData removeObjectAtIndex:deleteindexPath.row];
            [self.scheduleList reloadData];
            if (scheduleData.count<3) {
                self.createSchedule.hidden= NO;
            }
            else
            {
                self.createSchedule.hidden = YES;
            }
            
           
            
            
            [self.view makeToast:NSLocalizedString(@"Schedule deleted successfully", nil)];
            
            
        }
        
        else
        {
            [self.view makeToast:NSLocalizedString(@"Sorry!! Something went wrong",nil)];
            
            
        }
        
    }
}

#pragma mark - schedule Disable  Api
-(void)callDisableSchedulerApi
{
    self.selectWeekDay   = [NSMutableArray arrayWithArray:_hsvc.selectedWeekDay];
    
    
    OrderedDictionary *dataDict = [[OrderedDictionary alloc]initWithCapacity:8];
    
    [dataDict insertObject:@"1" forKey:_01 atIndex:0];
    [dataDict insertObject:_scheduleId forKey:_02 atIndex:1];
    [dataDict insertObject:@"2" forKey:_03 atIndex:2];
    [dataDict insertObject:@"0" forKey:_04 atIndex:3];
    
    [dataDict insertObject:_startTime forKey:_05 atIndex:4];
    [dataDict insertObject:_endTime forKey:_06 atIndex:5];
    [dataDict insertObject:_weekDay   forKey:_07 atIndex:6];
    
    
    OrderedDictionary *requestDict = [[OrderedDictionary alloc]initWithCapacity:5];
    [requestDict insertObject:_SIDVALUE forKey:_SID atIndex:0];
    //[requestDict insertObject:_BADVALUE forKey:_BAD atIndex:1];
    [requestDict insertObject:_SADVALUE forKey:_SAD atIndex:1];
    [requestDict insertObject:_7004 forKey:_CMD atIndex:2];
    [requestDict insertObject:dataDict forKey:_DATA atIndex:3];
    NSError *writeError;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:requestDict options:kNilOptions error:&writeError];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    [SVProgressHUD showWithStatus:@"Please Wait.."];
    [TcpPacketClass sharedInstance].delegate = self;
    [[TcpPacketClass sharedInstance]writeOut:jsonString second:[requestDict objectForKey:_CMD]];
    timer = [NSTimer scheduledTimerWithTimeInterval: 5.0 target:self selector:@selector(updateCounter:) userInfo:nil repeats: NO];
    
}

- (IBAction)deleteButtonClicked:(id)sender {
    
    [self callDeleteSchedulerApi];
    
}



#pragma mark- delete schedule api call
-(void)callDeleteSchedulerApi
{
    
    self.selectWeekDay   = [NSMutableArray arrayWithArray:_hsvc.selectedWeekDay];
    
    
    
    OrderedDictionary *dataDict = [[OrderedDictionary alloc]initWithCapacity:8];
    [dataDict insertObject:@"1" forKey:_01 atIndex:0];
    [dataDict insertObject:_scheduleId forKey:_02 atIndex:1];
    [dataDict insertObject:@"3" forKey:_03 atIndex:2];
    [dataDict insertObject:@"0" forKey:_04 atIndex:3];
    
    [dataDict insertObject:_startTime forKey:_05 atIndex:4];
    [dataDict insertObject:_endTime forKey:_06 atIndex:5];
    [dataDict insertObject:_weekDay forKey:_07 atIndex:6];
    
    
    OrderedDictionary *requestDict = [[OrderedDictionary alloc]initWithCapacity:5];
    [requestDict insertObject:_SIDVALUE forKey:_SID atIndex:0];
    //[requestDict insertObject:_BADVALUE forKey:_BAD atIndex:1];
    [requestDict insertObject:_SADVALUE forKey:_SAD atIndex:1];
    [requestDict insertObject:_7004 forKey:_CMD atIndex:2];
    [requestDict insertObject:dataDict forKey:_DATA atIndex:3];
    NSError *writeError;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:requestDict options:kNilOptions error:&writeError];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    [SVProgressHUD showWithStatus:@"Please Wait.."];
    [TcpPacketClass sharedInstance].delegate = self;
    [[TcpPacketClass sharedInstance]writeOut:jsonString second:[requestDict objectForKey:_CMD]];
    timer = [NSTimer scheduledTimerWithTimeInterval: 5.0 target:self selector:@selector(updateCounter:) userInfo:nil repeats: NO];
    
    
}







#pragma mark - get tank schedules

-(void)getScheduleApi
{
    OrderedDictionary *dataDict = [[OrderedDictionary alloc]initWithCapacity:8];
    [dataDict insertObject:@"1" forKey:_01 atIndex:0];
    
    
    OrderedDictionary *requestDict = [[OrderedDictionary alloc]initWithCapacity:5];
    [requestDict insertObject:_SIDVALUE forKey:_SID atIndex:0];
    [requestDict insertObject:_SADVALUE forKey:_SAD atIndex:1];
    [requestDict insertObject:_7009 forKey:_CMD atIndex:2];
    [requestDict insertObject:dataDict forKey:_DATA atIndex:3];
    NSError *writeError;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:requestDict options:kNilOptions error:&writeError];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    [SVProgressHUD showWithStatus:@"Please Wait.."];
    [TcpPacketClass sharedInstance].delegate = self;
    [[TcpPacketClass sharedInstance]writeOut:jsonString second:[requestDict objectForKey:_CMD]];
    timer = [NSTimer scheduledTimerWithTimeInterval:8.0 target:self selector:@selector(sendTankSchedulePacket:) userInfo:nil repeats: NO];
}


-(void)sendTankSchedulePacket:(NSTimer *)thetimer

{
    int static ticks = 0;
    if (ticks<4) {
        [self getScheduleApi];
    }
    else if (ticks>3)
    {
        [thetimer invalidate];
        [SVProgressHUD dismiss];
        [self.view makeToast:NSLocalizedString(@"Error in fetching.Please try again.", nil)];
        ticks = -1;
    }
    ticks++;
}

#pragma mark - get tank schedule response
-(void)getTankScheduleResponse:(NSArray *)jsonArray {
    
    [timer invalidate];
    [SVProgressHUD dismiss];
    NSLog(@"RESPONSE");
    
    if (jsonArray) {
        [scheduleData removeAllObjects];
        for (NSDictionary *dict in jsonArray) {
            NSDictionary *dataDictionary = [dict valueForKey:@"data"];
            
            // if ([[dataDictionary valueForKey:_02] isEqualToString:@"1"]) {
            Schedule *scheduleModel = [[Schedule alloc] init];
            scheduleModel.sid =[dataDictionary objectForKey:@"02"];
            scheduleModel.start =[dataDictionary objectForKey:@"05"];
            scheduleModel.stop =[dataDictionary objectForKey:@"06"];
            scheduleModel.days = [dataDictionary objectForKey:@"07"];
            
            // NSString *scheduleID = [dataDictionary objectForKey:@"02"];
            // NSString *startTime = [dataDictionary objectForKey:@"05"];
            //  NSString *endtime = [dataDictionary objectForKey:@"06"];
            //            if (startTime.length<=3) {
            int start = [scheduleModel.start intValue];
            int  Hour = (start)/60;
            NSMutableString *hourString  =[NSMutableString stringWithFormat:@"%d",Hour];
            if (hourString.length==1) {
                [hourString insertString:@"0" atIndex:0];
            }
            int minutes = (start)%60;
            NSMutableString *minuteString  =[NSMutableString stringWithFormat:@"%d",minutes];
            if (minuteString.length==1) {
                [minuteString insertString:@"0" atIndex:0];
            }
            appendedFinalString = hourString;
            [appendedFinalString appendString:minuteString];
            [appendedFinalString insertString:@":" atIndex:2];
            
            
            NSString *endTime = [dataDictionary objectForKey:@"06"];
            int end = [endTime intValue];
            int  hour = (end)/60;
            NSMutableString *hourEndString  =[NSMutableString stringWithFormat:@"%d",hour];
            if (hourEndString.length==1) {
                [hourEndString insertString:@"0" atIndex:0];
            }
            int Minutes = (end)%60;
            NSMutableString *minuteEndString  =[NSMutableString stringWithFormat:@"%d",Minutes];
            if (minuteEndString.length==1) {
                [minuteEndString insertString:@"0" atIndex:0];
            }
            appendedEndString = hourEndString;
            [appendedEndString appendString:minuteEndString];
            [appendedEndString insertString:@":" atIndex:2];
            
            //NSString *days = [dataDictionary objectForKey:@"07"];
            
            scheduleModel.start = appendedFinalString;
            scheduleModel.stop = appendedEndString;
            
    
            
            if ([[dataDictionary valueForKey:_03] isEqualToString:@"1"]&&[[dataDictionary valueForKey:_02 ]isEqualToString:@"1"]) {
                
                value1 = @"1";
//                scheduleTableViewCell *cell;
//                cell.scheduleEnableDisableSwitch.on = YES;
                
            }
            if ([[dataDictionary valueForKey:_03] isEqualToString:@"2"]&&[[dataDictionary valueForKey:_02 ]isEqualToString:@"1"]) {
                
                value1 = @"2";
//                scheduleTableViewCell *cell;
//                cell.scheduleEnableDisableSwitch.on = NO;

            }
            
            if([[dataDictionary valueForKey:_03] isEqualToString:@"1"]&&[[dataDictionary valueForKey:_02 ]isEqualToString:@"2"])
            {
                value2 = @"1";
//                scheduleTableViewCell *cell;
//                cell.scheduleEnableDisableSwitch.on = YES;
            }
            if([[dataDictionary valueForKey:_03] isEqualToString:@"2"]&&[[dataDictionary valueForKey:_02 ]isEqualToString:@"2"])
            {
                value2 = @"2";
//                scheduleTableViewCell *cell;
//                cell.scheduleEnableDisableSwitch.on = NO;

            }
            if([[dataDictionary valueForKey:_03] isEqualToString:@"1"]&&[[dataDictionary valueForKey:_02 ]isEqualToString:@"3"])
            {
                value3 = @"1";
//                scheduleTableViewCell *cell;
//                cell.scheduleEnableDisableSwitch.on = YES;
            }
            
            if([[dataDictionary valueForKey:_03] isEqualToString:@"2"]&&[[dataDictionary valueForKey:_02 ]isEqualToString:@"3"])
            {
                value3 = @"2";
//                scheduleTableViewCell *cell;
//                cell.scheduleEnableDisableSwitch.on = NO;
            }
            
           
            [scheduleData addObject:scheduleModel];
            
            
        }
        if(scheduleData.count<3)
        {
            
            // [self.createSchedule setUserInteractionEnabled:YES];
            self.createSchedule.hidden = NO;
            
        }
        
        else
        {
            // [self.createSchedule setEnabled:NO];
            self.createSchedule.hidden = YES;
            
            
        }
        [self.scheduleList reloadData];
       
    }
    
    else {
        [self.view makeToast:NSLocalizedString(@"Error in fetching.Please try again.", nil)];
    }
}

//error response here
-(void)didrecieveEnableScheduleError
{
    [timer invalidate];
    [SVProgressHUD dismiss];
    [self.view makeToast:NSLocalizedString(@"Schedule cant be created or enabled", nil)];
    
    
}

-(void)didrecieveDeleteScheduleError
{
    [timer invalidate];
    [SVProgressHUD dismiss];
    [self.view makeToast:NSLocalizedString(@"Schedule cant be deleted", nil)];
    
    
}
-(void)didrecieveDisableScheduleError
{
    [timer invalidate];
    [SVProgressHUD dismiss];
    [self.view makeToast:NSLocalizedString(@"Schedule cant be disabled", nil)];
    
    
}

-(void)didrecieveNoScheduleFoundError
{
    
    [timer invalidate];
    [SVProgressHUD dismiss];
    [self.scheduleList reloadData];
    [self.view makeToast:NSLocalizedString(@"NO Schedule Found...Please Create schedule", nil)];
    
}



-(void)didRecieveError:(NSError *)error {
    [SVProgressHUD dismiss];
    [self.view makeToast:NSLocalizedString(@"Socket not connected", nil)];
    //  [[Communicator sharedInstance]setup];
}





- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
