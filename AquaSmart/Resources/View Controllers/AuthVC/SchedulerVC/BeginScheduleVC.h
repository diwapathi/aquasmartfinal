//
//  BeginScheduleVC.h
//  AquaSmart
//
//  Created by Neotech on 21/02/17.
//  Copyright © 2017 Mac User. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "SchedulerViewController.h"

#import "scheduleTableViewCell.h"
#import "HSVCSchedule.h"
#import "SubProfile.h"
#import "ScheduleCreated.h"

#import "OrderedDictionary.h"
#import "WMSBlackBoxCommands.h"
#import "TcpPacketClass.h"
#import "UIView+Toast.h"
#import "SVProgressHUD.h"
#import "DatabaseModel.h"
#import "NotificationViewController.h"
#import "WMSSettingsController.h"
@class ProfileInfo;
@class HSVCSchedule;
@interface BeginScheduleVC : UIViewController
@property(nonatomic,strong)ProfileInfo *profileInfo;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *sidebarbutton;
@property (weak, nonatomic) IBOutlet UIButton *createSchedule;
@property (weak, nonatomic) IBOutlet UIButton *editSchedule;
@property (weak, nonatomic) IBOutlet UITableView *scheduleList;
@property (weak, nonatomic) IBOutlet UIButton *menuButton;
@property(nonatomic,strong)NSMutableArray *scheduledArray;
@property(nonatomic,strong)NSString *profileName;
@property (strong, nonatomic) NSIndexPath *indexPathToBeDeleted;
@property (strong,nonatomic) NSDictionary* dict;
@property (strong,nonatomic) NSDictionary* diction;
@property(strong,nonatomic)NSMutableArray *serialArray;
@property(strong,nonatomic)NSMutableArray *startArray;
@property(strong,nonatomic)NSMutableArray *endArray;
@property(strong,nonatomic)NSMutableArray *daysArray;
@property(strong,nonatomic)NSMutableArray *startHideArray;
@property(strong,nonatomic)NSMutableArray *endHideArray;
@property (weak, nonatomic) IBOutlet UIButton *deleteScheduleButton;
@property(nonatomic,strong)NSString *timeString;
@property(strong,nonatomic)NSString *scheduleId;
@property(strong,nonatomic)NSString *startTime;
@property(strong,nonatomic)NSString *endTime;
@property(strong,nonatomic)NSString *weekDay;
@property(strong,nonatomic)NSMutableArray *selectWeekDay;
@property(strong,nonatomic)HSVCSchedule *hsvc;
@property(strong,nonatomic)NSMutableArray *notification;
@property(strong,nonatomic)Schedule *scheduleInfo;
@property(nonatomic)BOOL isEditClicked;;
@property(nonatomic)BOOL isFromLeftPanel;
extern NSString *value;
-(void)backClicked;
+(instancetype)sharedInstance;
//-(void)callEnableSchedulerApi;
//-(void)callDisableSchedulerApi;

@end
