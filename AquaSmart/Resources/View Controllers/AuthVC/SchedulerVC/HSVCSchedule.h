//
//  HSVCSchedule.h
//  Homesmart
//
//  Created by Neotech on 19/08/16.
//  Copyright © 2016 Neotech. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Schedule.h"
#import "BeginScheduleVC.h"
#import "AppDelegate.h"
#import "DatabaseModel.h"
#import "RadioButton.h"
#import "HSWeekdayCell.h"
#import "ScheduleCreated.h"
#import "TcpPacketClass.h"
#import "UIView+Toast.h"
#import "SVProgressHUD.h"
#import "WMSBlackBoxCommands.h"
#import "SplashScreenVC.h"
#import "WMSSettingsController.h"
@class ProfileInfo;
@protocol ScheduleProtocol<NSObject>
@optional
-(void)schduleEdited:(NSString *)timeString;
-(void)scheduleCreated;
@end


@interface HSVCSchedule : UIViewController
@property(nonatomic,strong)NSString *daystext;
@property(nonatomic,strong)Schedule *schedule;
@property(nonatomic,strong)Schedule *scheduleInfo;
//@property(nonatomic,strong)ProfileInfo *profileInfo;
@property(nonatomic,strong)id<ScheduleProtocol>delegate;
@property(nonatomic)BOOL isScheduleEditing;

-(void)callSaveSchedulerApi;
- (void)endTimerChanged:(id)sender;
@property(strong,nonatomic)NSDictionary *diction;
@property(strong,nonatomic)NSMutableArray *selectedWeekDay;
@property (strong, nonatomic)NSMutableArray *scheduleList;
@property(strong,nonatomic)NSMutableArray  *notification;
@property(strong,nonatomic)NSString *scheduleIds;
@property(strong,nonatomic)NSString *startTime;
@property(strong,nonatomic)NSString *endTime;
@property(strong,nonatomic)NSString *weekDay;
-(void)callEnableSchedulerApi;
-(void)callDeleteSchedulerApi;
-(void)callDisableSchedulerApi;
-(void)getSelectWeekDay;

@end
