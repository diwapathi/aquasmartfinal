//
//  HSWeekdayCell.h
//  Homesmart
//
//  Created by Neotech on 23/08/16.
//  Copyright © 2016 Neotech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HSWeekdayCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *weekDayLabel;
@end
