//
//  BuyerRegistrationVCTwo.h
//  AquaSmart
//
//  Created by Mac User on 09/01/17.
//  Copyright © 2017 Mac User. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIView+Toast.h"
#import "TextFieldValidator.h"
#import "DeviceRegistrationVC.h"
@interface BuyerRegistrationVCTwo : UIViewController
{
    BOOL checkBoxSelected;
    
}
@property(nonatomic,strong)NSString *licenseNumber;
@property(nonatomic,strong)NSString *serialNumber;
@property (strong, nonatomic) IBOutlet UITextField *userNameTF;
@property (strong, nonatomic) IBOutlet UITextField *phoneNumberTF;
@property (strong, nonatomic) IBOutlet TextFieldValidator *emailTF;
@property (strong, nonatomic) IBOutlet UITextField *adminPasswordTF;
@property (strong, nonatomic) IBOutlet UITextField* standardPasswordTF;
@property (strong, nonatomic) IBOutlet UITextField *serialNumberTF;
@property (strong, nonatomic) IBOutlet UITextField *licenseNumberTF;
@property (strong, nonatomic) IBOutlet UIButton *agreeButton;
- (IBAction)agreeButtonClicked:(id)sender;
- (IBAction)registerButtonClicked:(id)sender;
- (IBAction)backButton:(id)sender;
-(void)callBuyerRegistrationApi;
@property(nonatomic,assign)BOOL isRun;
@property(nonatomic,assign)BOOL isPrepared;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintContentViewHeight;
@end
