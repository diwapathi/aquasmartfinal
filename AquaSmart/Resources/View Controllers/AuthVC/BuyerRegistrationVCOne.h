//
//  BuyerRegistrationVCOne.h
//  AquaSmart
//
//  Created by Mac User on 09/01/17.
//  Copyright © 2017 Mac User. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "CustomTextfield.h"
//#import "CustomButton.h"
#import "CustomIOSAlertView.h"
#import "UIView+Toast.h"
#import "DeviceRegistrationVC.h"
#import "WMSBlackBoxCommands.h"
#import "WMSSettingsController.h"
#import "OrderedDictionary.h"
#import "SVProgressHUD.h"
#import "Reachability.h"
#import "TcpPacketClass.h"
#import "DeviceRegistrationVC.h"
#import "AppDelegate.h"
#import "Reachability.h"
#import <SystemConfiguration/CaptiveNetwork.h>

@interface BuyerRegistrationVCOne : UIViewController<CustomIOSAlertViewDelegate>
{
    CustomIOSAlertView *alertView;
}
@property (weak, nonatomic) IBOutlet UIButton *serialNumberBtn;
@property (weak, nonatomic) IBOutlet UIButton *licenseNumberButton;
@property (strong, nonatomic) IBOutlet UITextField *licenseNumTF;
@property (strong, nonatomic) IBOutlet UITextField *serialNumTF;
@property (weak, nonatomic) IBOutlet UIImageView *networkImageView;
- (IBAction)submitButtonClicked:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *submitBtn;
@property(nonatomic,strong)NSString *previousValue;
@property(nonatomic,strong)NSString *licenseNumber;
@property(nonatomic,strong)NSString *serialNumber;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintContentViewHeight;
@property (weak, nonatomic) IBOutlet UIButton *termsConditionsButton;

-(void)changeNetworkStatus;
@end
