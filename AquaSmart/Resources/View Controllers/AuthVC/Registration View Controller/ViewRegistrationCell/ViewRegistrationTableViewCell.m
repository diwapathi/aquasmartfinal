//
//  ViewRegistrationTableViewCell.m
//  AquaSmart
//
//  Created by Mac User on 17/01/17.
//  Copyright © 2017 Mac User. All rights reserved.
//

#import "ViewRegistrationTableViewCell.h"

@implementation ViewRegistrationTableViewCell


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
-(void)setData:(NSString *)userName
{

    self.registeredUserLabel.text = userName;
    [self.removeButtonClicked setTitle:NSLocalizedString(@"UNREGISTER",nil) forState:UIControlStateNormal];

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
