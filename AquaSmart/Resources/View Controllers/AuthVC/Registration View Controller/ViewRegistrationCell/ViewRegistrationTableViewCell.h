//
//  ViewRegistrationTableViewCell.h
//  AquaSmart
//
//  Created by Mac User on 17/01/17.
//  Copyright © 2017 Mac User. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewRegistrationTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *registeredUserLabel;


@property (strong, nonatomic) IBOutlet UIButton *removeButtonClicked;
-(void)setData:(NSString *)userName;
@end
