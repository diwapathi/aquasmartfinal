//
//  ControlSettingsVC.m
//  AquaSmart
//
//  Created by Mac User on 18/01/17.
//  Copyright © 2017 Mac User. All rights reserved.
//

#import "ControlSettingsVC.h"
#import "AutomaticTaskTVC.h"
#import "TankSettingVC.h"
#import "BuzzerSettingsViewController.h"
#import "ThresholdSettingsViewController.h"
@interface ControlSettingsVC ()<UITableViewDataSource,UITableViewDelegate>
{

    NSMutableArray *controlSettingsArray;
}
@property (strong, nonatomic) IBOutlet UITableView *controlSettingTable;

@end

@implementation ControlSettingsVC

- (void)viewDidLoad {
    controlSettingsArray = [[NSMutableArray alloc]initWithObjects:@"Tank and Pump Settings",@"Automatic Task",@"Buzzer Settings",@"Threshold limit Settings", nil];
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    return [controlSettingsArray count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{

   static NSString *cellIdentifier = @"ControlCell";
    UITableViewCell *cell = [_controlSettingTable dequeueReusableCellWithIdentifier:cellIdentifier];
    cell.textLabel.text = [controlSettingsArray objectAtIndex:indexPath.row];
    return cell;

}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

    
    if (indexPath.row==0) {
        
        UIStoryboard *mainStroyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        
        TankSettingVC  *tankSettingVC = [mainStroyBoard instantiateViewControllerWithIdentifier:@"TankSettingVC"];
        [self.navigationController pushViewController:tankSettingVC animated:YES];
        
        
        
    }
    if (indexPath.row==1) {
        
         UIStoryboard *mainStroyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        
        AutomaticTaskTVC  *automaticTaskVC = [mainStroyBoard instantiateViewControllerWithIdentifier:@"AutomaticTaskTVC"];
        [self.navigationController pushViewController:automaticTaskVC animated:YES];
        

        
    }
    
    if (indexPath.row==2) {
        
        UIStoryboard *mainStroyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        
        BuzzerSettingsViewController  *buzzerSettingsVC = [mainStroyBoard instantiateViewControllerWithIdentifier:@"BuzzerSettingsViewController"];
        [self.navigationController pushViewController:buzzerSettingsVC animated:YES];
        
        
        
    }
    if (indexPath.row==3) {
        
        UIStoryboard *mainStroyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        
        ThresholdSettingsViewController  *thresholdSettingsVC = [mainStroyBoard instantiateViewControllerWithIdentifier:@"ThresholdSettingsViewController"];
        [self.navigationController pushViewController:thresholdSettingsVC animated:YES];
        
        
        
    }
    
    
    

 


}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
