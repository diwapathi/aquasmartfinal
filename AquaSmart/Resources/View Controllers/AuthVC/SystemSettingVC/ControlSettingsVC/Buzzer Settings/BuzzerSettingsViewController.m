//
//  BuzzerSettingsViewController.m
//  AquaSmart
//
//  Created by Mac User on 23/01/17.
//  Copyright © 2017 Mac User. All rights reserved.
//

#import "BuzzerSettingsViewController.h"

@interface BuzzerSettingsViewController ()<UIPickerViewDataSource,UIPickerViewDelegate>
{

    NSMutableArray *snoozeTime;
    NSMutableArray *setLevel;

}
@end

@implementation BuzzerSettingsViewController


#pragma mark - picker view delegate and data source method

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{

    
    if (pickerView==self.snoozeTimePicker) {
        return [snoozeTime count];
    }
    
    else
    {
        return [setLevel count];
    }
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if (pickerView==self.snoozeTimePicker) {
        return [snoozeTime objectAtIndex:row];
        
    }
     else
     {
        return [setLevel objectAtIndex:row];
    
}
}

#pragma mark - activate buzzer method
- (IBAction)activateBuzzerClicked:(id)sender {
       
    if (_activateButton.enabled==YES) {
        self.snoozeTimePicker.userInteractionEnabled=YES;
         self.setLevelPicker.userInteractionEnabled=YES;
        self.snoozeTimeLabel.textColor = [UIColor blackColor];
        self.setLevelLabel.textColor = [UIColor blackColor];
       
    }
    
}


- (void)viewDidLoad {
    [super viewDidLoad];
    self.snoozeTimeLabel.textColor = [UIColor whiteColor];
    self.setLevelLabel.textColor = [UIColor whiteColor];
    self.snoozeTimePicker.userInteractionEnabled=NO;
    self.setLevelPicker.userInteractionEnabled=NO;
    snoozeTime = [[NSMutableArray alloc]initWithObjects:@"0 mins",@"10mins",@"20mins",@"30mins",@"40mins",@"50mins",@"60mins",@"70mins",@"80mins",@"90mins",@"100mins",@"110mins",@"120mins", nil];
    setLevel = [[NSMutableArray alloc]initWithObjects:@"10%",@"20%",@"30%",@"40%",@"50%",@"60%",@"70%",@"80%",@"90%",@"100%", nil];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
