//
//  BuzzerSettingsViewController.h
//  AquaSmart
//
//  Created by Mac User on 23/01/17.
//  Copyright © 2017 Mac User. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BuzzerSettingsViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIButton *activateButton;
@property (strong, nonatomic) IBOutlet UIPickerView *snoozeTimePicker;
@property (strong, nonatomic) IBOutlet UIPickerView *setLevelPicker;
@property (strong, nonatomic) IBOutlet UIButton *saveButton;
@property (strong, nonatomic) IBOutlet UIButton *cancelButton;
@property (strong, nonatomic) IBOutlet UILabel *snoozeTimeLabel;
@property (strong, nonatomic) IBOutlet UILabel *setLevelLabel;

@end
