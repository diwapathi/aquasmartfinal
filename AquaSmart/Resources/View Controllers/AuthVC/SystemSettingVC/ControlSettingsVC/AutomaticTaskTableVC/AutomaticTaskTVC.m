//
//  AutomaticTaskTVC.m
//  AquaSmart
//
//  Created by Mac User on 18/01/17.
//  Copyright © 2017 Mac User. All rights reserved.
//

#import "AutomaticTaskTVC.h"

@interface AutomaticTaskTVC ()
{

    NSMutableArray  *automaticTaskArray;
}
-(void)stepperValueChanged:(UIStepper *)stepper;
@property (strong, nonatomic) IBOutlet UITableView *automaticTaskTable;
@property(strong,nonatomic)UILabel *stepperLabel;
@property(strong,nonatomic)UILabel *stepperLabelDouble;
@property(strong,nonatomic)UILabel *stepperLabelThird;
@property(strong,nonatomic)UILabel *stepperLabelFourth;

@property(strong,nonatomic) UIStepper* stepper;
@property(strong,nonatomic) UIStepper* stepperDouble;
@property(strong,nonatomic) UIStepper* stepperThird;
@property(strong,nonatomic) UIStepper* stepperFourth;
@end

@implementation AutomaticTaskTVC

#pragma mark- stepper  first method defination
-(void)stepperValueChanged:(UIStepper *)stepper
{
    
    stepper.maximumValue = 10.0;
    stepper.minimumValue = 0.0;
    stepper.value--;
    stepper.value ++;
    NSLog(@"%f",stepper.value);
    
    [_stepperLabel setText:[NSString stringWithFormat:@"%f",stepper.value]];
    
}
    
#pragma mark- stepper  Second method defination
 -(void)stepperDouble:(UIStepper *)stepper
{
    
    stepper.maximumValue = 100.0;
    stepper.minimumValue = 0.0;
    stepper.stepValue =5.0;
    stepper.value--;
    stepper.value ++;
    NSLog(@"%f",stepper.value);
    [_stepperLabelDouble setText:[NSString stringWithFormat:@"%f",_stepperDouble.value]];
    
}
#pragma mark- stepper  Third method defination


-(void)stepperThird:(UIStepper *)stepper
{
    
    stepper.maximumValue = 100.0;
    stepper.minimumValue = 0.0;
    stepper.stepValue =5.0;
    stepper.value--;
    stepper.value ++;
    NSLog(@"%f",stepper.value);
    [_stepperLabelThird setText:[NSString stringWithFormat:@"%f",_stepperThird.value]];
    
}
#pragma mark- stepper  Second method defination

-(void)stepperFourth:(UIStepper *)stepper
{
    
    stepper.maximumValue = 100.0;
    stepper.minimumValue = 0.0;
    stepper.stepValue =5.0;
    stepper.value--;
    stepper.value ++;
    NSLog(@"%f",stepper.value);
    [_stepperLabelFourth setText:[NSString stringWithFormat:@"%f",_stepperFourth.value]];
    
}


- (void)viewDidLoad {
    [super viewDidLoad];
    automaticTaskArray = [[NSMutableArray alloc]initWithObjects:@"Protect Tank 1 Pump From dry-run",@"Protect Tank 2 Pump From dry-run",@"Tank 2 Pump ON in presence of supply water",@"Presence water tank 2 if it is less than 25%",@"Presence water if overall availblity less than 30%",@"Tank 1 Pump ON when tank 2 i s 100% and tank 2 is below than 70%",@"Optimize overall water availaiblity", nil];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return [automaticTaskArray count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *cellIdentifier = @"AutomaticCell";
    UITableViewCell *cell = [_automaticTaskTable dequeueReusableCellWithIdentifier:cellIdentifier];
    cell.textLabel.text = [automaticTaskArray objectAtIndex:indexPath.row];
    UIFont *myFont = [ UIFont fontWithName: @"Arial" size: 13.0 ];
    cell.textLabel.font  = myFont;
    UISwitch *switchView = [[UISwitch alloc] initWithFrame:CGRectZero];
    cell.accessoryView = switchView;
    [switchView setOn:NO animated:NO];
    [switchView addTarget:self action:@selector(switchChanged:) forControlEvents:UIControlEventValueChanged];
    switchView.onImage = [UIImage imageNamed:@""];
    switchView.offImage = [UIImage imageNamed:@""];
    return cell;
    
}

#pragma mark - Switch event call here
- (void)switchChanged:(id)sender {
    UISwitch *switchControl = sender;
    
    CGPoint buttonPosition = [sender convertPoint:CGPointZero
                                           toView:self.automaticTaskTable];
    NSIndexPath *tappedIP = [self.automaticTaskTable indexPathForRowAtPoint:buttonPosition];
    self.indexPathToBeDeleted = tappedIP;
    
    NSLog(@"%@",tappedIP);
    
    
    if (tappedIP.row==0) {
        
    if (switchControl.on) {
        UIAlertController * alertController = [UIAlertController alertControllerWithTitle: @""
                                                                                  message: @"Tank 1 Pump Off if dry run is detected for \n\n\n\n" preferredStyle:UIAlertControllerStyleAlert];
        
        
        _stepper
        
        = [[UIStepper alloc] init];
        _stepperLabel
        
        
        
        = [[UILabel alloc]initWithFrame:CGRectMake(150.0, 60.0, 100, 20)];
        _stepper.frame = CGRectMake(40.0, 60.0, 100, 10);
        
        [_stepperLabel setText:[NSString stringWithFormat:@"%f",_stepper.value]];
        [_stepperLabel setTextColor:[UIColor blackColor]];
        
        NSLog(@"%f",_stepper.value);
        [_stepper addTarget:self action:@selector(stepperValueChanged:) forControlEvents:UIControlEventValueChanged];
        [alertController.view addSubview:_stepper];
        [alertController.view addSubview:_stepperLabel];
        //        [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        //                    textField.placeholder = @"Device Date and Time";
        //                    textField.textColor = [UIColor blueColor];
        //                    textField.clearButtonMode = UITextFieldViewModeWhileEditing;
        //                    textField.borderStyle = UITextBorderStyleRoundedRect;
        //                }];
        //                [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        //                    textField.placeholder = @"System Date and Time";
        //                    textField.textColor = [UIColor blueColor];
        //                    textField.clearButtonMode = UITextFieldViewModeWhileEditing;
        //                    textField.borderStyle = UITextBorderStyleRoundedRect;
        //                    textField.secureTextEntry = YES;
        //                }];
        [alertController addAction:[UIAlertAction actionWithTitle:@"Save" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            //                    NSArray * textfields = alertController.textFields;
            //                    UITextField * namefield = textfields[0];
            //                    UITextField * passwordfiled = textfields[1];
            //                    NSLog(@"%@:%@",namefield.text,passwordfiled.text);
            
        }]];
        [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            //                    NSArray * textfields = alertController.textFields;
            //                    UITextField * namefield = textfields[0];
            //                    UITextField * passwordfiled = textfields[1];
            //                    NSLog(@"%@:%@",namefield.text,passwordfiled.text);
            
        }]];
        
        [self presentViewController:alertController animated:YES completion:nil];
        

        
       

    
    NSLog( @"The switch is %@", switchControl.on ? @"ON" : @"OFF" );
    }
    else
    {
        //switch off code here
        
    }
        
    }

  else if (tappedIP.row==1 )
{
    
    if (switchControl.on) {
        
                       UIAlertController * alertController = [UIAlertController alertControllerWithTitle: @""
                                                                                          message: @"Tank 1 Pump Off if dry run is detected for \n\n\n\n" preferredStyle:UIAlertControllerStyleAlert];
        
        
        _stepper
        
        = [[UIStepper alloc] init];
        _stepperLabel
        
        
        
        = [[UILabel alloc]initWithFrame:CGRectMake(150.0, 60.0, 100, 20)];
        _stepper.frame = CGRectMake(40.0, 60.0, 100, 10);
        
        [_stepperLabel setText:[NSString stringWithFormat:@"%f",_stepper.value]];
        [_stepperLabel setTextColor:[UIColor blackColor]];
        
        NSLog(@"%f",_stepper.value);
        [_stepper addTarget:self action:@selector(stepperValueChanged:) forControlEvents:UIControlEventValueChanged];
        [alertController.view addSubview:_stepper];
        [alertController.view addSubview:_stepperLabel];
//        [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
//                    textField.placeholder = @"Device Date and Time";
//                    textField.textColor = [UIColor blueColor];
//                    textField.clearButtonMode = UITextFieldViewModeWhileEditing;
//                    textField.borderStyle = UITextBorderStyleRoundedRect;
//                }];
//                [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
//                    textField.placeholder = @"System Date and Time";
//                    textField.textColor = [UIColor blueColor];
//                    textField.clearButtonMode = UITextFieldViewModeWhileEditing;
//                    textField.borderStyle = UITextBorderStyleRoundedRect;
//                    textField.secureTextEntry = YES;
//                }];
                [alertController addAction:[UIAlertAction actionWithTitle:@"Save" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
//                    NSArray * textfields = alertController.textFields;
//                    UITextField * namefield = textfields[0];
//                    UITextField * passwordfiled = textfields[1];
//                    NSLog(@"%@:%@",namefield.text,passwordfiled.text);
        
                }]];
                [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
//                    NSArray * textfields = alertController.textFields;
//                    UITextField * namefield = textfields[0];
//                    UITextField * passwordfiled = textfields[1];
//                    NSLog(@"%@:%@",namefield.text,passwordfiled.text);
                    
                }]];
        
                [self presentViewController:alertController animated:YES completion:nil];
                
        
    }
    
    else
    {
        //switch off code here
        
    }
}
   if (tappedIP.row==2 )
  {
      
      if (switchControl.on) {
          
          UIAlertController * alertController = [UIAlertController alertControllerWithTitle: @""
                                                                                    message: @"Turn tank 2 pump ON if it is full and presence is detected in pipeline \n\n\n\n" preferredStyle:UIAlertControllerStyleAlert];
          
          
                    [alertController addAction:[UIAlertAction actionWithTitle:@"Save" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
              //                    NSArray * textfields = alertController.textFields;
              //                    UITextField * namefield = textfields[0];
              //                    UITextField * passwordfiled = textfields[1];
              //                    NSLog(@"%@:%@",namefield.text,passwordfiled.text);
              
          }]];
          [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
              //                    NSArray * textfields = alertController.textFields;
              //                    UITextField * namefield = textfields[0];
              //                    UITextField * passwordfiled = textfields[1];
              //                    NSLog(@"%@:%@",namefield.text,passwordfiled.text);
              
          }]];
          
          [self presentViewController:alertController animated:YES completion:nil];
          
          
      }
      
      else
      {
          //switch off code here
          
      }
  }
    if (tappedIP.row==3) {
        
        if (switchControl.on) {
            UIAlertController * alertController = [UIAlertController alertControllerWithTitle: @""
                                                                                      message: @"Turn Tank 1 Pump Off when tank goes below  25% \n\n\n\n" preferredStyle:UIAlertControllerStyleAlert];
            
            
            _stepper
            
            = [[UIStepper alloc] init];
            _stepperLabel
            
            
            
            = [[UILabel alloc]initWithFrame:CGRectMake(150.0, 70.0, 100, 20)];
            _stepper.frame = CGRectMake(40.0, 70.0, 100, 10);
            
            [_stepperLabel setText:[NSString stringWithFormat:@" %f ",_stepper.value]];
            [_stepperLabel setTextColor:[UIColor blackColor]];
            
            NSLog(@"%f",_stepper.value);
            [_stepper addTarget:self action:@selector(stepperValueChanged:) forControlEvents:UIControlEventValueChanged];
            [alertController.view addSubview:_stepper];
            [alertController.view addSubview:_stepperLabel];
            //        [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
            //                    textField.placeholder = @"Device Date and Time";
            //                    textField.textColor = [UIColor blueColor];
            //                    textField.clearButtonMode = UITextFieldViewModeWhileEditing;
            //                    textField.borderStyle = UITextBorderStyleRoundedRect;
            //                }];
            //                [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
            //                    textField.placeholder = @"System Date and Time";
            //                    textField.textColor = [UIColor blueColor];
            //                    textField.clearButtonMode = UITextFieldViewModeWhileEditing;
            //                    textField.borderStyle = UITextBorderStyleRoundedRect;
            //                    textField.secureTextEntry = YES;
            //                }];
            [alertController addAction:[UIAlertAction actionWithTitle:@"Save" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                //                    NSArray * textfields = alertController.textFields;
                //                    UITextField * namefield = textfields[0];
                //                    UITextField * passwordfiled = textfields[1];
                //                    NSLog(@"%@:%@",namefield.text,passwordfiled.text);
                
            }]];
            [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                //                    NSArray * textfields = alertController.textFields;
                //                    UITextField * namefield = textfields[0];
                //                    UITextField * passwordfiled = textfields[1];
                //                    NSLog(@"%@:%@",namefield.text,passwordfiled.text);
                
            }]];
            
            [self presentViewController:alertController animated:YES completion:nil];
            
            
            
            
        }
        
        else
        {
            //switch off code here
            
        }
        }
    if (tappedIP.row==4) {
        
        if (switchControl.on) {
            UIAlertController * alertController = [UIAlertController alertControllerWithTitle: @""
                                                                                      message: @"Prevent automatic task from turning tank 1 pump ON if overall availablity is below than \n\n\n\n" preferredStyle:UIAlertControllerStyleAlert];
            
            
            _stepper= [[UIStepper alloc] init];
            _stepperLabel= [[UILabel alloc]initWithFrame:CGRectMake(150.0, 80.0, 100, 20)];
            _stepper.frame = CGRectMake(40.0, 80.0, 100, 10);
            
            [_stepperLabel setText:[NSString stringWithFormat:@"%f",_stepper.value]];
            [_stepperLabel setTextColor:[UIColor blackColor]];
            
            NSLog(@"%f",_stepper.value);
            [_stepper addTarget:self action:@selector(stepperValueChanged:) forControlEvents:UIControlEventValueChanged];
            [alertController.view addSubview:_stepper];
            [alertController.view addSubview:_stepperLabel];
            //        [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
            //                    textField.placeholder = @"Device Date and Time";
            //                    textField.textColor = [UIColor blueColor];
            //                    textField.clearButtonMode = UITextFieldViewModeWhileEditing;
            //                    textField.borderStyle = UITextBorderStyleRoundedRect;
            //                }];
            //                [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
            //                    textField.placeholder = @"System Date and Time";
            //                    textField.textColor = [UIColor blueColor];
            //                    textField.clearButtonMode = UITextFieldViewModeWhileEditing;
            //                    textField.borderStyle = UITextBorderStyleRoundedRect;
            //                    textField.secureTextEntry = YES;
            //                }];
            [alertController addAction:[UIAlertAction actionWithTitle:@"Save" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                //                    NSArray * textfields = alertController.textFields;
                //                    UITextField * namefield = textfields[0];
                //                    UITextField * passwordfiled = textfields[1];
                //                    NSLog(@"%@:%@",namefield.text,passwordfiled.text);
                
            }]];
            [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                //                    NSArray * textfields = alertController.textFields;
                //                    UITextField * namefield = textfields[0];
                //                    UITextField * passwordfiled = textfields[1];
                //                    NSLog(@"%@:%@",namefield.text,passwordfiled.text);
                
            }]];
            
            [self presentViewController:alertController animated:YES completion:nil];
            
            
            
            
        }
        
        else
        {
            //switch off code here
            
        }
    }
    if (tappedIP.row==5) {
        
        if (switchControl.on) {
            UIAlertController * alertController = [UIAlertController alertControllerWithTitle: @""   message: @"Turn Tank 1 ON when tank 2 fills up to \n\n\n\n\nand  tank 2 is below than \n\n\n" preferredStyle:UIAlertControllerStyleAlert];
            
            
            _stepper= [[UIStepper alloc] init];
            _stepperLabel= [[UILabel alloc]initWithFrame:CGRectMake(150.0, 70.0, 100, 20)];
            _stepper.frame = CGRectMake(40.0, 70.0, 100, 10);
            
            [_stepperLabel setText:[NSString stringWithFormat:@"%f",_stepper.value]];
            [_stepperLabel setTextColor:[UIColor blackColor]];
            
            NSLog(@"%f",_stepper.value);
            [_stepper addTarget:self action:@selector(stepperValueChanged:) forControlEvents:UIControlEventValueChanged];
            [alertController.view addSubview:_stepper];
            [alertController.view addSubview:_stepperLabel];
            
            
            _stepperDouble= [[UIStepper alloc] init];
           
            _stepperDouble.frame = CGRectMake(40.0, 160.0, 100, 10);
             _stepperLabelDouble= [[UILabel alloc]initWithFrame:CGRectMake(150.0, 170.0, 100, 20)];
            
            [_stepperLabelDouble setText:[NSString stringWithFormat:@"%f",_stepperDouble.value]];
            [_stepperLabelDouble setTextColor:[UIColor blackColor]];
            
            NSLog(@"%f",_stepperDouble.value);
            [_stepperDouble addTarget:self action:@selector(stepperDouble:) forControlEvents:UIControlEventValueChanged];
            [alertController.view addSubview:_stepperDouble];
            [alertController.view addSubview:_stepperLabelDouble];
            
            
            [alertController addAction:[UIAlertAction actionWithTitle:@"Save" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                //                    NSArray * textfields = alertController.textFields;
                //                    UITextField * namefield = textfields[0];
                //                    UITextField * passwordfiled = textfields[1];
                //                    NSLog(@"%@:%@",namefield.text,passwordfiled.text);
                
            }]];
            [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                //                    NSArray * textfields = alertController.textFields;
                //                    UITextField * namefield = textfields[0];
                //                    UITextField * passwordfiled = textfields[1];
                //                    NSLog(@"%@:%@",namefield.text,passwordfiled.text);
                
            }]];
            
            [self presentViewController:alertController animated:YES completion:nil];
            
            
            
            
        }
        
        else
        {
            //switch off code here
            
        }
    }
    
    
    
    

    if (tappedIP.row==6) {
        
        if (switchControl.on) {
UIAlertController * alertController = [UIAlertController alertControllerWithTitle: @""message: @"Turn Tank 1 pump on if (Tank less )than\n\n\n\nTank 2 greater than\n\n\nTurn Tank 1 pump OFF when it is \n\n\n\n if Tank 2 less than\n\n\n\n"
                                                preferredStyle:UIAlertControllerStyleAlert];
            
#pragma mark-first stepper
            _stepper= [[UIStepper alloc] init];
            _stepperLabel= [[UILabel alloc]initWithFrame:CGRectMake(150.0, 70.0, 100, 20)];
            _stepper.frame = CGRectMake(40.0, 70.0, 100, 10);
            
            [_stepperLabel setText:[NSString stringWithFormat:@"%f",_stepper.value]];
            [_stepperLabel setTextColor:[UIColor blackColor]];
            
            NSLog(@"%f",_stepper.value);
            [_stepper addTarget:self action:@selector(stepperValueChanged:) forControlEvents:UIControlEventValueChanged];
            [alertController.view addSubview:_stepper];
            [alertController.view addSubview:_stepperLabel];
            
#pragma mark-Second stepper
            _stepperDouble= [[UIStepper alloc] init];
            _stepperLabelDouble= [[UILabel alloc]initWithFrame:CGRectMake(150.0, 140.0, 100, 20)];
            _stepperDouble.frame = CGRectMake(40.0, 140.0, 100, 10);
            
            [_stepperLabelDouble setText:[NSString stringWithFormat:@"%f",_stepperDouble.value]];
            [_stepperLabelDouble setTextColor:[UIColor blackColor]];
            
            NSLog(@"%f",_stepperDouble.value);
            [_stepperDouble addTarget:self action:@selector(stepperDouble:) forControlEvents:UIControlEventValueChanged];
            [alertController.view addSubview:_stepperDouble];
            [alertController.view addSubview:_stepperLabelDouble];
#pragma mark-Third stepper
            _stepperThird= [[UIStepper alloc] init];
            _stepperLabelThird= [[UILabel alloc]initWithFrame:CGRectMake(150.0, 200.0, 100, 20)];
            _stepperThird.frame = CGRectMake(40.0, 200.0, 100, 10);
            
            [_stepperLabelThird setText:[NSString stringWithFormat:@"%f",_stepperThird.value]];
            [_stepperLabelThird setTextColor:[UIColor blackColor]];
            
            NSLog(@"%f",_stepperThird.value);
            [_stepperThird addTarget:self action:@selector(stepperThird:) forControlEvents:UIControlEventValueChanged];
            [alertController.view addSubview:_stepperThird];
            [alertController.view addSubview:_stepperLabelThird];
#pragma mark-Fourth stepper
            _stepperFourth= [[UIStepper alloc] init];
            _stepperLabelFourth= [[UILabel alloc]initWithFrame:CGRectMake(150.0, 270.0, 100, 20)];
            _stepperFourth.frame = CGRectMake(40.0, 270.0, 100, 10);
            
            [_stepperLabelFourth setText:[NSString stringWithFormat:@"%f",_stepperFourth.value]];
            [_stepperLabelFourth setTextColor:[UIColor blackColor]];
            
            NSLog(@"%f",_stepperFourth.value);
            [_stepperFourth addTarget:self action:@selector(stepperFourth:) forControlEvents:UIControlEventValueChanged];
            [alertController.view addSubview:_stepperFourth];
            [alertController.view addSubview:_stepperLabelFourth];
            

            
            [alertController addAction:[UIAlertAction actionWithTitle:@"Save" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                //                    NSArray * textfields = alertController.textFields;
                //                    UITextField * namefield = textfields[0];
                //                    UITextField * passwordfiled = textfields[1];
                //                    NSLog(@"%@:%@",namefield.text,passwordfiled.text);
                
            }]];
            [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                //                    NSArray * textfields = alertController.textFields;
                //                    UITextField * namefield = textfields[0];
                //                    UITextField * passwordfiled = textfields[1];
                //                    NSLog(@"%@:%@",namefield.text,passwordfiled.text);
                
            }]];
            
            [self presentViewController:alertController animated:YES completion:nil];
            
            
            
            
        }
        
        
        else
        {
        
           // switch off code here
        }
    }
    }


@end
