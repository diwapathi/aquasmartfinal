//
//  SystemSettingVC.h
//  AquaSmart
//
//  Created by Mac User on 17/01/17.
//  Copyright © 2017 Mac User. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JNExpandableTableView.h"
#import "FirmwareUpgradeVC.h"
@interface SystemSettingVC : UITableViewController

@property (strong, nonatomic) IBOutlet UITableView *table1;
@property(strong,nonatomic)UIStepper *stepper;
@property(strong,nonatomic)UILabel *stepperLabel;

@end
