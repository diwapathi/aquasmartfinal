//
//  SystemSettingVC.m
//  AquaSmart
//
//  Created by Mac User on 17/01/17.
//  Copyright © 2017 Mac User. All rights reserved.
//

#import "SystemSettingVC.h"
#import "SystemNetworkVC.h"
@interface SystemSettingVC ()<UITableViewDelegate,UITableViewDataSource>
{
    NSMutableArray *dataArray;
   }

@end

@implementation SystemSettingVC

- (void)viewDidLoad
{
    
    [super viewDidLoad];
    dataArray = [[NSMutableArray alloc]initWithObjects:@"SysTimeSettings",@"SysNetSettings",@"AdminPassword",@"FirmwareUpgrade",@"PullInterval",@"LogoutTime", nil];
   }

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return dataArray.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    static NSString *cellID = @"SystemCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {        // Create new cell
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    
    }
    cell.textLabel.text = [dataArray objectAtIndex:indexPath.row];
    
    return cell;
    
    
    
    
    }
    
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

    if (indexPath.row==0) {
        UIAlertController * alertController = [UIAlertController alertControllerWithTitle: @"System Date And Time"
                                                                                  message: @""
                                                                           preferredStyle:UIAlertControllerStyleAlert];
        [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
            textField.placeholder = @"Device Date and Time";
            textField.textColor = [UIColor blueColor];
            textField.clearButtonMode = UITextFieldViewModeWhileEditing;
            textField.borderStyle = UITextBorderStyleRoundedRect;
        }];
        [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
            textField.placeholder = @"System Date and Time";
            textField.textColor = [UIColor blueColor];
            textField.clearButtonMode = UITextFieldViewModeWhileEditing;
            textField.borderStyle = UITextBorderStyleRoundedRect;
            
        }];
        [alertController addAction:[UIAlertAction actionWithTitle:@"Sync" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            NSArray * textfields = alertController.textFields;
            UITextField * namefield = textfields[0];
            UITextField * passwordfiled = textfields[1];
            NSLog(@"%@:%@",namefield.text,passwordfiled.text);
            
        }]];
        [self presentViewController:alertController animated:YES completion:nil];
        
  
    }
    else if (indexPath.row==1) {
        UIStoryboard *mainStroyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        SystemNetworkVC *systemNetworkVC = [mainStroyBoard instantiateViewControllerWithIdentifier:@"SystemNetworkVC"];
        [self.navigationController pushViewController:systemNetworkVC animated:YES];
        

    }

    
    else if (indexPath.row==2)
    {
        
        UIAlertController * alertController = [UIAlertController alertControllerWithTitle: @"Set Password "
                                                                                  message: @""
                                                                           preferredStyle:UIAlertControllerStyleAlert];
        
        [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
            textField.placeholder = @"Old Password";
            textField.textColor = [UIColor blueColor];
            textField.clearButtonMode = UITextFieldViewModeWhileEditing;
            textField.borderStyle = UITextBorderStyleRoundedRect;
        }];
        [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
            textField.placeholder = @"New Password";
            textField.textColor = [UIColor blueColor];
            textField.clearButtonMode = UITextFieldViewModeWhileEditing;
            textField.borderStyle = UITextBorderStyleRoundedRect;
        }];
        [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
            textField.placeholder = @"Confirm Password";
            textField.textColor = [UIColor blueColor];
            textField.clearButtonMode = UITextFieldViewModeWhileEditing;
            textField.borderStyle = UITextBorderStyleRoundedRect;
            textField.secureTextEntry = YES;
        }];
        [alertController addAction:[UIAlertAction actionWithTitle:@"Save" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            NSArray * textfields = alertController.textFields;
            UITextField * namefield = textfields[0];
            UITextField * passwordfiled = textfields[1];
            NSLog(@"%@:%@",namefield.text,passwordfiled.text);
            
        }]];
        [self presentViewController:alertController animated:YES completion:nil];

    }
    
    
    else if (indexPath.row==3)
    {
        UIStoryboard *mainStroyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        FirmwareUpgradeVC *firmwareUpgradeVC = [mainStroyBoard instantiateViewControllerWithIdentifier:@"FirmwareUpgradeVC"];
        [self.navigationController pushViewController:firmwareUpgradeVC animated:YES];
    
    }
    
    else if (indexPath.row==4)
    {
        
        UIAlertController * alertController = [UIAlertController alertControllerWithTitle: @"Pull Interval"
                                                                                  message: @""
                                                                           preferredStyle:UIAlertControllerStyleAlert];
        
//        //water level label
//        UILabel *waterLevelLabel = [[UILabel alloc]initWithFrame:CGRectMake(150.0, 130.0, 50.0,25.0)];
//        waterLevelLabel.text = @"sec";
//        waterLevelLabel.textColor = [UIColor blackColor];
//        
//        
//        
//        
//        //notification label
//        UILabel *NotificationLabel = [[UILabel alloc]initWithFrame:CGRectMake(500.0, 25.0, 50.0,25.0)];
//        waterLevelLabel.text = @"mins";
//        waterLevelLabel.textColor = [UIColor blackColor];
//          [alertController.view addSubview:NotificationLabel];
//        
//        [alertController.view addSubview:waterLevelLabel];
//
        
        
        
        [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
            textField.placeholder = @"Water Level(sec)";
            textField.textColor = [UIColor blueColor];
            //textField.frame = CGRectMake(5.0, 25.0, 40.0, 25.0);
            textField.clearButtonMode = UITextFieldViewModeWhileEditing;
            //textField.borderStyle = UITextBorderStyleRoundedRect;
        }];
        [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
            textField.placeholder = @"Notfication(mins)";
            textField.textColor = [UIColor blueColor];
           // textField.frame = CGRectMake(5.0, 35.0, 40.0, 25.0);
            textField.clearButtonMode = UITextFieldViewModeWhileEditing;
            //textField.borderStyle = UITextBorderStyleRoundedRect;
                    }];
        [alertController addAction:[UIAlertAction actionWithTitle:@"Save" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            NSArray * textfields = alertController.textFields;
            UITextField * namefield = textfields[0];
            UITextField * passwordfiled = textfields[1];
            NSLog(@"%@:%@",namefield.text,passwordfiled.text);
            
        }]];
        [self presentViewController:alertController animated:YES completion:nil];
        
    }
    
    else if (indexPath.row==5)
    {
        
        UIAlertController * alertController = [UIAlertController alertControllerWithTitle: @""
                                                                                  message: @"Auto Logout Timeout(in mins):\n\n\n"
                                                                           preferredStyle:UIAlertControllerStyleAlert];
        
        _stepper= [[UIStepper alloc] init];
        _stepperLabel= [[UILabel alloc]initWithFrame:CGRectMake(150.0, 60.0, 100, 20)];
        _stepper.frame = CGRectMake(40.0, 60.0, 100, 10);
        
        [_stepperLabel setText:[NSString stringWithFormat:@"%f",_stepper.value]];
        [_stepperLabel setTextColor:[UIColor blackColor]];
        
        NSLog(@"%f",_stepper.value);
        [_stepper addTarget:self action:@selector(stepperValueChanged:) forControlEvents:UIControlEventValueChanged];
        [alertController.view addSubview:_stepper];
        [alertController.view addSubview:_stepperLabel];

        [alertController addAction:[UIAlertAction actionWithTitle:@"Save" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            
            
        }]];
        [self presentViewController:alertController animated:YES completion:nil];
        
    }


    }

//   UITableViewCell  *cell = [tableView dequeueReusableCellWithIdentifier:@"SystemCell" forIndexPath:indexPath];
//    if (cell==nil)
//    {
//        cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"SystemCell"];
//    }
//    
//    cell.textLabel.text=[dataArray objectAtIndex:indexPath.row];
//    
//    return cell;

#pragma mark- stepper  first method defination
-(void)stepperValueChanged:(UIStepper *)stepper
{
    
    stepper.maximumValue = 60.0;
    stepper.minimumValue = 0.0;
    stepper.value--;
    stepper.value ++;
    NSLog(@"%f",stepper.value);
    
    [_stepperLabel setText:[NSString stringWithFormat:@"%f",stepper.value]];
    
}



#pragma mark JNExpandableTableView DataSource



/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
