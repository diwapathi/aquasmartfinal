//
//  FirmwareUpgradeVC.h
//  AquaSmart
//
//  Created by Mac User on 20/01/17.
//  Copyright © 2017 Mac User. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MVYSideMenuController.h"
#import "MainViewController.h"
#import "WMSBlackBoxCommands.h"
#import "SVProgressHUD.h"
#import "OrderedDictionary.h"
#import "TcpPacketClass.h"
#import "Constants.h"
#import "MBProgressHUD.h"
@interface FirmwareUpgradeVC : UIViewController<PacketReciveProtocol>
@property (strong, nonatomic) IBOutlet UILabel *firmwareVersionLabel;

@property (weak, nonatomic) IBOutlet UIProgressView *topProgressView;


@property (weak, nonatomic) IBOutlet UILabel *updateFileLines;


@property (strong, nonatomic) IBOutlet UILabel *downloadLabel;
@property (strong, nonatomic) IBOutlet UILabel *updateLabel;
@property (nonatomic, strong) NSTimer *myTimer;
@property (weak, nonatomic) IBOutlet UILabel *availableVersionLbl;
@property (weak, nonatomic) IBOutlet UIButton *firmwareUpgradeButton;
@property (weak, nonatomic) IBOutlet UILabel *currentVersionLbl;
@property (weak, nonatomic) IBOutlet UILabel *totalFileLabel;

-(void)getFirmwareVersionCallApi;
-(void)firmwareUpdateApi;
-(void)readHexFile:(NSString *)responseHexString;
-(void)showProgressbar;
+(instancetype)sharedInstance;
@end
