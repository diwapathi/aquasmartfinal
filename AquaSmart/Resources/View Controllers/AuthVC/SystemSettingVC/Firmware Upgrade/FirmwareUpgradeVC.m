//
//  FirmwareUpgradeVC.m
//  AquaSmart
//
//  Created by Mac User on 20/01/17.
//  Copyright © 2017 Mac User. All rights reserved.
//

#import "FirmwareUpgradeVC.h"
#import "THProgressView.h"
@interface FirmwareUpgradeVC ()
{
    
    NSTimer *timer;
    // NSString *HexFileVersion;
    NSString *firmwareVersion;
    //int value = [string intValue];
    int firmwareVersionInt ;
    NSString *savedValue;
    NSString *HexFileVersion;
    NSString *hexFileVersionWithDot;
    int timeinterval;
    NSArray *listItems;
}
@end

@implementation FirmwareUpgradeVC



+(instancetype)sharedInstance {
    
    
    static FirmwareUpgradeVC *firmware;
    static dispatch_once_t once;
    dispatch_once(&once, ^{
        firmware = [[self alloc] init];
    });
    
    return firmware;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    _firmwareUpgradeButton.backgroundColor = [UIColor colorWithRed:192/255.0 green:192/255.0 blue:192/255.0 alpha:1];
    [_firmwareUpgradeButton setTintColor:[UIColor colorWithRed:192/255.0 green:192/255.0 blue:192/255.0 alpha:1]];
    
   
    HexFileVersion = @"110024";
    hexFileVersionWithDot = @"11.00.24";
    
    self.currentVersionLbl.text = [[WMSSettingsController sharedInstance]getCurrentFirmwareVersion];
    self.availableVersionLbl.text = LATEST_HEX_FILE_VERSION;
    
    firmwareVersion =  [[WMSSettingsController sharedInstance]getCurrentFirmwareVersion];
    NSRange range = NSMakeRange(2,1);
    NSRange range1 = NSMakeRange(4,1);
    NSString *newText = [firmwareVersion stringByReplacingCharactersInRange:range withString:@""];
    NSString *CurrentVersionString = [newText stringByReplacingCharactersInRange:range1 withString:@""];
    int HexFileVersionInt = [HexFileVersion intValue];
    int CurrentVersionInt = [CurrentVersionString intValue];

    if (HexFileVersionInt==CurrentVersionInt) {
        [self.firmwareUpgradeButton setUserInteractionEnabled:NO];
        
    }
    
    else
    {
      
    [self.firmwareUpgradeButton setUserInteractionEnabled:YES];
        _firmwareUpgradeButton.backgroundColor = [UIColor colorWithRed:90/255.0 green:154/255.0 blue:250/255.0 alpha:1];
    }
    [self setTopHeaderView];
//    self.topProgressView.progressTintColor = [UIColor blueColor];
//    self.topProgressView.hidden = YES;
//    self.downloadLabel.hidden = NO;
   
}
-(void)setTopHeaderView {
    NSDictionary *navDict = @{
                              TITLE_TEXT :NSLocalizedString(@"Software Upgrade", nil),
                              BACK_IMAGE :@"BackImage",
                              IS_BACK_HIDDEN:@"No"
                              };
    [[AppDelegate appDelegate].superVC setHeaderViewData:navDict];
}



-(void)firmwareUpdateApi
{
    //
    NSError* error;
    NSString *path = [[NSBundle mainBundle] pathForResource:@"Aqua_E4_11.00.24" ofType:@"hex"];
    // NSURL *url = [NSURL fileURLWithPath:path];
    NSDictionary *fileDictionary = [[NSFileManager defaultManager] attributesOfItemAtPath:path error: &error];
    NSNumber *size = [fileDictionary objectForKey:NSFileSize];
    NSLog(@"%@",size);
    NSString *crc=@"0";
    OrderedDictionary *dataDict = [[OrderedDictionary alloc]initWithCapacity:3];
    [dataDict insertObject:crc forKey:_01 atIndex:0];
    [dataDict insertObject:hexFileVersionWithDot forKey:_02 atIndex:1];
    
    OrderedDictionary *requestDict = [[OrderedDictionary alloc]initWithCapacity:5];
    [requestDict insertObject:_SIDVALUE forKey:_SID atIndex:0];
    //[requestDict insertObject:_BADVALUE forKey:_BAD atIndex:1];
    [requestDict insertObject:_SADVALUE forKey:_SAD atIndex:1];
    [requestDict insertObject:_8003 forKey:_CMD atIndex:2];
    [requestDict insertObject:dataDict forKey:_DATA atIndex:3];
    NSError *writeError;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:requestDict options:kNilOptions error:&writeError];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    // [SVProgressHUD showWithStatus:@"Upgrading Firmware.."];
    
    [TcpPacketClass sharedInstance].delegate = self;
    [[TcpPacketClass sharedInstance]writeOut:jsonString second:[requestDict objectForKey:_CMD]];
    timer = [NSTimer scheduledTimerWithTimeInterval:10.0 target:self selector:@selector(sendFirmwareUpdate:) userInfo:nil repeats: NO];
    
}


- (void)sendFirmwareUpdate:(NSTimer *)theTimer {
    [theTimer invalidate];
    // [SVProgressHUD dismiss];
    [self firmwareUpdateApi];
}



-(void)didRecieveFirmwareUpgradeResponse:(NSDictionary *)jsonDict
{
    
   //` [timer invalidate];
    
    //[SVProgressHUD dismissWithDelay:120];
    NSLog(@"Response Recieved");
    
    if ([[jsonDict valueForKey:_DATA] isEqualToString:@"0"]) {
        
        
        //[self readHexFile];
        [self.view makeToast:NSLocalizedString(@"Upgradtion will start.Please connect again to AquaSmart", nil)];
        return;
    }
    
    else
    {
        [self.view makeToast:NSLocalizedString(@"Fail!!!", nil)];
    }
    
    
    
    
    
    
    
}
// reading hx file
-(void)readHexFile:(NSString *)responseHexString
{
    
    NSString *finalString;
    NSString *formattedString;
    NSError *error;
    NSString *path = [[NSBundle mainBundle] pathForResource:@"Aqua_E4_11.00.24" ofType:@"hex"];
    NSString *data = [NSString stringWithContentsOfFile:path encoding:NSASCIIStringEncoding error:&error];
    
    listItems = [data componentsSeparatedByString:@"\n"];
    
    NSString *togetlastLine =  responseHexString;
    
    
    
    NSRange range = [togetlastLine rangeOfString:@"^0*" options:NSRegularExpressionSearch];
    togetlastLine= [togetlastLine stringByReplacingCharactersInRange:range withString:@""];
    NSInteger number = [togetlastLine integerValue];
    NSString *hex= [listItems objectAtIndex:number-1];
    
    
    timeinterval = [hex intValue];      //for sending time interval to progress bar
    NSString *addResponseString=[@"~" stringByAppendingString:responseHexString];
    formattedString =[addResponseString stringByAppendingString:hex];
    finalString = [formattedString stringByAppendingString:@"^"];
    NSString *packetHexString = [finalString stringByReplacingOccurrencesOfString:@"\r" withString:@""];
    NSLog(@"%@",packetHexString);
    //[[TcpPacketClass sharedInstance]writeOut:packetHexString];
    [[TcpPacketClass sharedInstance]writeOut:packetHexString second:_CMD];
    
    [self performSelectorOnMainThread:@selector(makeMyProgressBarMoving)  withObject:nil
                        waitUntilDone:NO];
    
    
    
}



- (void)makeMyProgressBarMoving {
    static int count =0; count++;
    
    if (count <=4018)
    {
        NSLog(@"%d",count);
        
        NSString *totalLine = @"/4018";
        NSString *countString = [NSString stringWithFormat:@"Upgradtion in progress\n%d %%",(count*100)/4018];
//        [self.downloadLabel setText:countString];
//        [_totalFileLabel setText:totalLine];
//        [_topProgressView setProgress:(float)count/4018];
        [SVProgressHUD resetOffsetFromCenter];
        [SVProgressHUD showProgress:(float)count/4018 status:countString];
        
        
    }
}




-(void)getFirmwareUpgradeSuccessResponse
{
    [timer invalidate];
    [SVProgressHUD setSuccessImage:[UIImage imageNamed:@"taskFinish.png"]];
    UIAlertView *alertView = [[UIAlertView alloc]
                              initWithTitle:nil
                              message:@"Firmware upgraded successfully.Please restart the app and IOT device. "
                              delegate:self
                              cancelButtonTitle:nil
                              otherButtonTitles:NSLocalizedString(@"OK", nil), nil];
    
    [alertView show];
    
    
    
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 0) {
        [self performSelector:@selector(exitFromApp) withObject:nil afterDelay:5.0];
        
    }
    if (buttonIndex == 1) {
        
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}


-(void)exitFromApp{
    [SVProgressHUD dismiss];
    exit(0);
}




- (IBAction)softwareUpgradeButtonClicked:(id)sender {
    
    
    [self firmwareUpdateApi];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
