//
//  ForgatePassword.h
//  AquaSmart
//
//  Created by Mac User on 06/03/17.
//  Copyright © 2017 Mac User. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OrderedDictionary.h"
#import "WMSBlackBoxCommands.h"
#import "TcpPacketClass.h"
#import "SVProgressHUD.h"
#import "UIView+Toast.h"
#import "MVYSideMenuController.h"
#import "MainViewController.h"
#import "AppDelegate.h"
#import "Reachability.h"
#import "WMSSettingsController.h"
#import "HSVCScanner.h"
@interface ForgatePassword : UIViewController<PacketReciveProtocol,UITextFieldDelegate,ScannerProtocol>
-(void)callForgatePasswordApi;
-(void)hideKeyboard;
- (void)setupOutlets;
-(void)backClicked;
-(void)licenceScanComplete:(NSString *)codeString;

@property (weak, nonatomic) IBOutlet UIButton *menuButton;

@property (strong, nonatomic) IBOutlet UITextField *lincenseNumber;
@property (strong, nonatomic) IBOutlet UITextField *password;
@property (strong, nonatomic) IBOutlet UITextField *confirmPassword;
@property (strong, nonatomic) IBOutlet UIButton *saveButton;
@property (weak, nonatomic) IBOutlet UIButton *passwordViewButton;
@property (weak, nonatomic) IBOutlet UIButton *confirmPasswordViewButton;
@property(nonatomic)BOOL isFromLeftPanel;

@end
