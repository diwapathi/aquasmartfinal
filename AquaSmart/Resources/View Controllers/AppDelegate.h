//
//  AppDelegate.h
//  AquaSmart
//
//  Created by Mac User on 09/01/17.
//  Copyright © 2017 Mac User. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CFNetwork/CFNetwork.h>
#import <CoreData/CoreData.h>
#import "OrderedDictionary.h"
#import "WMSUUIDCreater.h"
#import "WMSBlackBoxCommands.h"
#import "NSString+UrlEncoding.h"
#import "AppLoading.h"
#import "MainViewController.h"
#import "Reachability.h"
#import "UIView+Toast.h"
#import "BuyerRegistrationVCOne.h"
#import "SplashScreenVC.h"
#import "TcpPacketClass.h"
#import "HSVCLeftPanel.h"
#import "FirmwareUpgradeVC.h"
#import "DatabaseModel.h"
#import "WMSSettingsController.h"


#define WIFI_CHANGED @"WIFI_CHANGED"
#define NET_WORK_CHANGE @"NET_WORK_CHANGE"
#define TIMEOUT @"TIMEOUT"
#define SOCKET_CONNECTED @"SOCKET_CONNECTED"
#define PUMP_SUCCESS_RESPONSE @"PUMP_SUCCESS_RESPONSE"
#define PUMP_FAILURE_RESPONSE @"PUMP_FAILURE_RESPONSE"

@interface AppDelegate : UIResponder <UIApplicationDelegate,NSObject,MainPresenterDelegate>
{
    
    
    DatabaseModel *datamodel;
}
@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@property(nonatomic,strong) SplashScreenVC *superVC;
@property (strong,nonatomic)Reachability *reach;
@property(nonatomic)NetworkStatus netStatus;
@property (strong, nonatomic) UIWindow *window;
@property (nonatomic,strong) UINavigationController *navigationController;
@property (readonly, strong) NSPersistentContainer *persistentContainer;

@property (strong, nonatomic) MainPresenter *presenter;
@property BOOL isUserInAdvanceSettings;
+(instancetype)appDelegate;
+ (BOOL)isActiveInternet;
-(void)setRootController;
-(void)pushSidePanelController;
@property (assign) BOOL isRun;
@property (assign) BOOL isPrepared;
@property (assign) BOOL isWifiConnected;
@property (assign) int iswificonnectedint;
@end

