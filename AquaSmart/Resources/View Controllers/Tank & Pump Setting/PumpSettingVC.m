//
//  PumpSettingVC.m
//  AquaSmart
//
//  Created by Neotech on 26/07/17.
//  Copyright © 2017 Mac User. All rights reserved.
//

#import "PumpSettingVC.h"

@interface PumpSettingVC ()

@end

@implementation PumpSettingVC
{

    NSMutableArray *dryPickerArray;
    NSMutableArray *reRunPickerArray;
    NSString *DryPickerSelectedRowTitle;
    NSString *ReRunPickerSelectedRowTitle;
    NSTimer *timer;
}
- (void)viewDidLoad {
    
    [self callGetPumpSettingsApi];
    [self setTopHeaderView];
    dryPickerArray = [[NSMutableArray alloc]initWithObjects:@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9",@"10",@"11",@"12",@"13",@"14",@"15", nil];
    //reRunPickerArray =  [[NSMutableArray alloc]initWithObjects:@"15",@"16",@"17",@"18",@"19",@"20",@"21",@"22",@"23",@"24",@"25",@"26",@"27",@"28",@"29",@"30",@"31",@"6",@"7",@"8",@"9",@"10",@"11",@"12",@"13",@"14",@"15",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9",@"10",@"11",@"12",@"13",@"14",@"15",@"120", nil];
    reRunPickerArray = [[NSMutableArray alloc]init];
    
    for (int i=15; i<=120; i++) {
        [reRunPickerArray addObject:[NSString stringWithFormat:@"%d",i]]
        ;
    }
    
    NSLog(@"my array is:%@",reRunPickerArray);
   // NSString *DryPickerSelectedRowTitle = [dryPickerArray objectAtIndex:[self.dryRunPicker selectedRowInComponent:0]];
   // NSString *ReRunPickerSelectedRowTitle = [reRunPickerArray objectAtIndex:[self.pumpReRunPicker selectedRowInComponent:0]];
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (IBAction)savePumpSettingButtonClicked:(id)sender {
    
    [self callPumpSettingsApi];
}

#pragma mark - Initial data load -
-(void)setTopHeaderView {
    NSDictionary *navDict = @{
                              TITLE_TEXT :NSLocalizedString(@"Pump Settings", nil),
                              BACK_IMAGE :@"BackImage",
                              IS_BACK_HIDDEN:@"No"
                              };
    [[AppDelegate appDelegate].superVC setHeaderViewData:navDict];
}

-(void)backClicked {
    UIStoryboard *mainStroyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    MVYSideMenuController *sideMenuController = [self sideMenuController];
    MainViewController *homeVC = [mainStroyBoard instantiateViewControllerWithIdentifier:@"MainViewController"];
    [sideMenuController changeContentViewController:homeVC closeMenu:YES];
}

-(void)viewWillAppear:(BOOL)animated {
    
    [[AppDelegate appDelegate].superVC updateHeadrLabel:NSLocalizedString(@"Pump Settings", nil)];
    
}


#pragma mark- picker view delegate methods 
-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if (pickerView.tag==1) {
        return dryPickerArray.count;
    }
    
     if(pickerView.tag==2)
        
    {
        return  reRunPickerArray.count;
    }
   
    return 0;
}

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
 
    
    return 1;
}


-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{

    
    NSString *title;
    
    if (pickerView.tag == 1) // this is otherPickerview
    {
        title = [dryPickerArray objectAtIndex:row]; //
        
    }
    else if (pickerView.tag == 2) // this is citysPickerview
    {
        title=[reRunPickerArray objectAtIndex:row]; //
        
    }
    
    
    return title;

}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{

    
    if (pickerView.tag==1) {
        DryPickerSelectedRowTitle = [dryPickerArray objectAtIndex:[self.dryRunPicker selectedRowInComponent:0]];
    }
    
    else if (pickerView.tag==2)
    {
     ReRunPickerSelectedRowTitle = [reRunPickerArray objectAtIndex:[self.pumpReRunPicker selectedRowInComponent:0]];
    }
}


#pragma mark - Device Authentication Api call -
-(void)callPumpSettingsApi{
    
    if (DryPickerSelectedRowTitle == nil) {
        DryPickerSelectedRowTitle = @"3";
    }
    if (ReRunPickerSelectedRowTitle ==nil) {
        ReRunPickerSelectedRowTitle = @"15";
    }
    OrderedDictionary *dataDict = [[OrderedDictionary alloc]initWithCapacity:3];
    [dataDict insertObject:DryPickerSelectedRowTitle forKey:_01 atIndex:0];
    [dataDict insertObject:ReRunPickerSelectedRowTitle forKey:_02 atIndex:1];
   
    
    OrderedDictionary *requestDict = [[OrderedDictionary alloc]initWithCapacity:5];
    [requestDict insertObject:_SIDVALUE forKey:_SID atIndex:0];
    [requestDict insertObject:_SADVALUE forKey:_SAD atIndex:1];
    [requestDict insertObject:_8530 forKey:_CMD atIndex:2];
    [requestDict insertObject:dataDict forKey:_DATA atIndex:3];
    NSError *writeError;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:requestDict options:kNilOptions error:&writeError];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    [SVProgressHUD showWithStatus:@"Please Wait.."];
    
    [TcpPacketClass sharedInstance].delegate = self;
    
    
    [[TcpPacketClass sharedInstance]writeOut:jsonString second:[requestDict objectForKey:_CMD]];
    timer = [NSTimer scheduledTimerWithTimeInterval: 5.0 target:self selector:@selector(updateCounter:) userInfo:nil repeats: NO];
    
}

#pragma dismiss progress loader wether responce appear or not within 5 second:
- (void)updateCounter:(NSTimer *)theTimer {
    [theTimer invalidate];
    [SVProgressHUD dismiss];
    [self.view makeToast:NSLocalizedString(@"Something went wrong Please try Again!!!", nil)];
}

-(void)getPumpSettingResponse:(NSDictionary *)jsonDict
{
 
     [ timer invalidate];
    [SVProgressHUD dismiss];
    
    NSDictionary *dataDict = [jsonDict valueForKey:_DATA];
//    NSString *dryRun = [dataDict valueForKey:_01];
//    NSString *reRun = [dataDict valueForKey:_02];
//    if (dryRun) {
//        int i = 0;
//        for(NSString* dryRunString in dryPickerArray)
//        {
//            if ([dryRunString isEqualToString:dryRun]){
//                [_dryRunPicker selectRow:i inComponent:0 animated:YES ];
//            }
//            i++;
//        }
//    }
//    
//    else
//    {
//    
//        int i = 0;
//        for(NSString* dryRunString in reRunPickerArray)
//        {
//            if ([dryRunString isEqualToString:dryRun]){
//                [_dryRunPicker selectRow:i inComponent:0 animated:YES ];
//            }
//            i++;
//        }
//
//    }
   
   // [self.dryRunPicker set];
   // [self.pumpReRunPicker selectedRowInComponent:1];
    [self.view makeToast:NSLocalizedString(@"Pump Settings Save Successfully", nil)];
    
    
    
    
}



-(void)callGetPumpSettingsApi

{

    OrderedDictionary *requestDict = [[OrderedDictionary alloc]initWithCapacity:5];
    [requestDict insertObject:_SIDVALUE forKey:_SID atIndex:0];
    [requestDict insertObject:_SADVALUE forKey:_SAD atIndex:1];
    [requestDict insertObject:_8531 forKey:_CMD atIndex:2];

    NSError *writeError;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:requestDict options:kNilOptions error:&writeError];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    [SVProgressHUD showWithStatus:@"Please Wait.."];
    
    [TcpPacketClass sharedInstance].delegate = self;
    
    
    [[TcpPacketClass sharedInstance]writeOut:jsonString second:[requestDict objectForKey:_CMD]];
    timer = [NSTimer scheduledTimerWithTimeInterval: 5.0 target:self selector:@selector(sendGetPumpSettingsAgain:) userInfo:nil repeats: NO];



}

#pragma dismiss progress loader wether responce appear or not within 5 second:
- (void)sendGetPumpSettingsAgain:(NSTimer *)theTimer {
    [theTimer invalidate];
    [SVProgressHUD dismiss];
    [self.view makeToast:NSLocalizedString(@"Something went wrong Please try Again!!!", nil)];
    //[self.navigationController popViewControllerAnimated:YES];
}
-(void)didRecieveGetPumpSettingResponse:(NSDictionary *)jsonDict
{


    [ timer invalidate];
    [SVProgressHUD dismiss];
    
    NSDictionary *dataDict = [jsonDict valueForKey:_DATA];
    NSString *dryRun = [dataDict valueForKey:_01];
    NSString *reRun = [dataDict valueForKey:_02];
    if (dryRun) {
        int i = 0;
        for(NSString* dryRunString in dryPickerArray)
        {
            if ([dryRunString isEqualToString:dryRun]){
                [_dryRunPicker selectRow:i inComponent:0 animated:YES ];
                DryPickerSelectedRowTitle = @"";
            }
            i++;
        }
    }
    
    if (reRun) {
        
    
    {
        
        int i = 0;
        for(NSString* reRunString in reRunPickerArray)
        {
            if ([reRunString isEqualToString:reRun]){
                [_pumpReRunPicker selectRow:i inComponent:0 animated:YES ];
                ReRunPickerSelectedRowTitle = @"";
            }
            i++;
        }
[self.view makeToast:NSLocalizedString(@"Pump Settings Get Successfully", nil)];
    
    
}
}
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
