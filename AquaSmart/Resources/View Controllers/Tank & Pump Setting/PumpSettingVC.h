//
//  PumpSettingVC.h
//  AquaSmart
//
//  Created by Neotech on 26/07/17.
//  Copyright © 2017 Mac User. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIView+Toast.h"
#import "WMSBlackBoxCommands.h"
#import "WMSSettingsController.h"
#import "OrderedDictionary.h"
#import "SVProgressHUD.h"
#import "Reachability.h"
#import "TcpPacketClass.h"
#import "AppDelegate.h"
#import "Reachability.h"

@interface PumpSettingVC : UIViewController<PacketReciveProtocol,UIPickerViewDelegate,UIPickerViewDataSource>
@property (weak, nonatomic) IBOutlet UIPickerView *dryRunPicker;
@property (weak, nonatomic) IBOutlet UIPickerView *pumpReRunPicker;

-(void)setTopHeaderView;
@end
