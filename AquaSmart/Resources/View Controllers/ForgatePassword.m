//
//  ForgatePassword.m
//  AquaSmart
//
//  Created by Mac User on 06/03/17.
//  Copyright © 2017 Mac User. All rights reserved.
//

#import "ForgatePassword.h"

@interface ForgatePassword ()

@end

@implementation ForgatePassword
{
    NSTimer *timer;
}

- (void)viewDidLoad {
    [self setTopHeaderView];
   // self.lincenseNumber.text = @"440BC04K54F7aS0FB3CC5TRF";
    
    UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    [self.view addGestureRecognizer:gestureRecognizer];
      [self setupOutlets];
    [super viewDidLoad];
    

    // Do any additional setup after loading the view.
}
- (IBAction)licenseNumTF:(id)sender
{
    
    if (self.lincenseNumber.text.length <= 0) {
        [self.view makeToast:@"License number should be of 24 characters."
                    duration:2.0
                    position:CSToastPositionCenter];
        // [self.view makeToast:NSLocalizedString(@"Please fill license number.", nil)];
        [self.view endEditing:YES];
        return;
        
        
    }
    
}

- (IBAction)passwordCheck:(id)sender
{
    
    
    if (self.password.text.length <= 0) {
        [self.view makeToast:@"Password should be atleast 6 characters"
                    duration:2.0
                    position:CSToastPositionCenter];
        // [self.view makeToast:NSLocalizedString(@"Please fill license number.", nil)];
        [self.view endEditing:YES];
        return;
        
        
    }
    
    
}

- (IBAction)confirmPassword:(id)sender
{
    
    if (self.confirmPassword.text.length <= 0) {
        [self.view makeToast:@"Password should be atleast 6 characters"
                    duration:2.0
                    position:CSToastPositionCenter];
        // [self.view makeToast:NSLocalizedString(@"Please fill license number.", nil)];
        [self.view endEditing:YES];
        return;
        
        
    }
    
}


#pragma mark - Initial data load -
-(void)setTopHeaderView {
    NSDictionary *navDict = @{
                              TITLE_TEXT :NSLocalizedString(@"Forgot Password", nil),
                              BACK_IMAGE :@"BackImage",
                              IS_BACK_HIDDEN:@"No"
                              };
    [[AppDelegate appDelegate].superVC setHeaderViewData:navDict];
}

-(void)backClicked {
    UIStoryboard *mainStroyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    MVYSideMenuController *sideMenuController = [self sideMenuController];
    MainViewController *homeVC = [mainStroyBoard instantiateViewControllerWithIdentifier:@"MainViewController"];
    [sideMenuController changeContentViewController:homeVC closeMenu:YES];
}

-(void)viewWillAppear:(BOOL)animated {
  
    [super viewWillAppear:animated];
    NetworkStatus status = [self checkForWifiConnection];
    if(status == NotReachable || status == ReachableViaWWAN) {
        [self.navigationController.view makeToast:NSLocalizedString(@"Please check your wifi connection", nil)];
    }
    [[AppDelegate appDelegate].superVC updateHeadrLabel:NSLocalizedString(@"Forgot Password", nil)];
    
}

-(NetworkStatus)checkForWifiConnection {
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    [reachability startNotifier];
    NetworkStatus status = [reachability currentReachabilityStatus];
    return status;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


#pragma mark - setting length for texfields
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    //NSString *newString = [self.serialNumTF.text ////stringByReplacingCharactersInRange:range withString:string];
    //return (newString.length<=12);
    if(textField == _lincenseNumber) {
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        return (newLength > 24) ? NO : YES;
    }
    else if(textField == _password)
    {
        //do the same with different values
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        return (newLength >12) ? NO : YES;
    }
    
    else if(textField == _confirmPassword)
    {
        //do the same with different values
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        return (newLength >12) ? NO : YES;
    }

    
    return 0;
}

- (IBAction)saveButtonClicked:(id)sender
{
    
  
    if (self.password.text.length <= 0) {
        [self.view makeToast:NSLocalizedString(@"Please fill Password.", nil)];
        [self.view endEditing:YES];
        return;
    }
    
    
    else if (![self.lincenseNumber.text isEqualToString:[[WMSSettingsController sharedInstance] getLicenseNumber]])
    {
    
        [self.view makeToast:NSLocalizedString(@"License Number do not match.", nil)];
        [self.view endEditing:YES];
        return;

        
    
    }
        else  if(self.lincenseNumber.text.length < 24) {
        [self.view makeToast:NSLocalizedString(@"License number should contain atleast 24 characters", nil)];
        [self.view endEditing:YES];
        return;
    }

    else if(self.lincenseNumber.text.length<=0){
        [self.view makeToast:NSLocalizedString(@"Please fill License number.", nil)];
        [self.view endEditing:YES];
        return;
    
    }
      else if (self.password.text.length < 6) {
        [self.view makeToast:NSLocalizedString(@" Password must not be less than 6 characters", nil)];
        [self.view endEditing:YES];
        return;
    }
    
    if (self.confirmPassword.text.length <= 0) {
        [self.view makeToast:NSLocalizedString(@"Please fill Confirm Password.", nil)];
        [self.view endEditing:YES];
        return;
    }
    if (![self.password.text isEqualToString:self.confirmPassword.text]) {
        [self.view makeToast:NSLocalizedString(@" Passwords do not match \n please retype", nil)];
        [self.view endEditing:YES];
        self.password.text = @"";
        self.confirmPassword.text = @"";
        return;
    }

 else{
    [self callForgatePasswordApi];
 }
}
//- (IBAction)viewPaswordBtnClicked:(id)sender {
//    if (self.password.text) {
//        [self.password setSecureTextEntry:NO];
//    }
//}

- (IBAction)hidePasswordButtonClicked:(id)sender {
    if (self.password.text) {
        [self.password setSecureTextEntry:NO];
    }
    [self performSelector:@selector(newPassButtonClicked) withObject:nil afterDelay:2.0];
}
//- (IBAction)showPasswordButtonClicked:(id)sender {
//    
//    if (self.password.text) {
//               [self.password setSecureTextEntry:YES];
//           }
//}


-(void)newPassButtonClicked
{
    if (self.password.text) {
        [self.password setSecureTextEntry:YES];
    }

}


//- (IBAction)viewConfirmPaswordBtnClicked:(id)sender {
//    if (self.confirmPassword.text) {
//        [self.confirmPassword setSecureTextEntry:NO];
//    }
//}

- (IBAction)hideConfirmPasswordButtonClicked:(id)sender {
    if (self.confirmPassword.text) {
        [self.confirmPassword setSecureTextEntry:NO];
    }
    [self performSelector:@selector(confPassButtonClicked) withObject:nil afterDelay:2.0];
}

-(void)confPassButtonClicked
{
    if (self.confirmPassword.text) {
        [self.confirmPassword setSecureTextEntry:YES];
    }
    
}


- (IBAction)cancelButtonClicked:(id)sender {
    self.password.text = nil;
    self.confirmPassword.text = nil;
    }

- (IBAction)backButton:(id)sender {
    
    
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark - schedule Disable  Api
-(void)callForgatePasswordApi
{
    NSString *passwordString = [self getName:12 :_password];
    OrderedDictionary *dataDict = [[OrderedDictionary alloc]initWithCapacity:8];
    
    [dataDict insertObject:self.lincenseNumber.text forKey:_01 atIndex:0];
    [dataDict insertObject:passwordString forKey:_02 atIndex:1];
    
    //    [dataDict insertObject:@"600" forKey:_05 atIndex:4];
    //    [dataDict insertObject:@"610" forKey:_06 atIndex:5];
    
    
    
    OrderedDictionary *requestDict = [[OrderedDictionary alloc]initWithCapacity:5];
    [requestDict insertObject:_SIDVALUE forKey:_SID atIndex:0];
    //[requestDict insertObject:_BADVALUE forKey:_BAD atIndex:1];
    [requestDict insertObject:_SADVALUE forKey:_SAD atIndex:1];
    [requestDict insertObject:_8213 forKey:_CMD atIndex:2];
    [requestDict insertObject:dataDict forKey:_DATA atIndex:3];
    NSError *writeError;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:requestDict options:kNilOptions error:&writeError];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    [SVProgressHUD showWithStatus:@"Please Wait.."];

    [TcpPacketClass sharedInstance].delegate = self;
   [[TcpPacketClass sharedInstance]writeOut:jsonString second:[requestDict objectForKey:_CMD]];
   timer = [NSTimer scheduledTimerWithTimeInterval: 3.0 target:self selector:@selector(updateCounter:) userInfo:nil repeats: NO];
}

- (void)updateCounter:(NSTimer *)theTimer {
    [theTimer invalidate];
    [SVProgressHUD dismiss];
}

-(void)didrecieveForgotPasswordResponse:(NSDictionary *)jsonDict
{
    [SVProgressHUD dismiss];
   // NSDictionary *dataDict = [jsonDict valueForKey:_DATA];
    if ([[jsonDict valueForKey:_DATA] isEqualToString:@"0"]) {
        [self.view makeToast:NSLocalizedString(@"Password Reset Successfully", nil)];
        return;
    }
    
    else
    {
      [self.view makeToast:NSLocalizedString(@"Failed to Rest Password!!!!", nil)];
    }
    
}

#pragma mark - Text fields text lenght validations -
-(NSString *)getName :(int)lenght :(UITextField *)texfield {
    NSMutableString *finalUserName = [texfield.text mutableCopy] ;
    
    if (texfield.text.length == lenght) {
        finalUserName = [texfield.text mutableCopy];
    }
    else {
        int count = (int)texfield.text.length;
        for (int i = count; i < lenght; i++) {
            
            [finalUserName appendString:@" "];
        }
    }
    return finalUserName;
}


-(void)didRecieveError:(NSError *)error {
    [SVProgressHUD dismiss];
    [self.view makeToast:NSLocalizedString(@"Socket not connected", nil)];
   // [[Communicator sharedInstance]setup];
    }


#pragma mark - hides keyboard on touch of screen
-(void)hideKeyboard
{
    
    [_lincenseNumber resignFirstResponder];
    [_password resignFirstResponder];
    [_confirmPassword resignFirstResponder];
    
    
    
    
}
#pragma mark - set the textfield tag
- (void)setupOutlets
{
//    self.lincenseNumber.delegate = self;
//    self.lincenseNumber.tag = 1;
//    
//    self.password.delegate = self;
//    self.password.tag = 2;
//    
//    
//    
//    self.confirmPassword.delegate = self;
//    self.confirmPassword.tag = 3;
    
    
    
    
    
}

#pragma mark - moving cursor to the next textfield
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    NSInteger nextTag = textField.tag + 1;
    [self jumpToNextTextField:textField withTag:nextTag];
    return NO;
    
}

- (void)jumpToNextTextField:(UITextField *)textField withTag:(NSInteger)tag
{
    
    UIResponder *nextResponder = [self.view viewWithTag:tag];
    
    if ([nextResponder isKindOfClass:[UITextField class]]) {
        
        [nextResponder becomeFirstResponder];
    }
    else {
        // If there is not then removes the keyboard.
        [textField resignFirstResponder];
    }
}
- (IBAction)licenseNumberScannerButtonClicked:(id)sender {
    
    
        UIStoryboard *mainStroyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        HSVCScanner *scannerVC = [mainStroyBoard instantiateViewControllerWithIdentifier:@"HSVCScanner"];
        scannerVC.delegate = self;
        scannerVC.isLicenceSelected = NO;
        [self.navigationController pushViewController:scannerVC animated:YES];
    
}
-(void)licenceScanComplete:(NSString *)codeString {
    self.lincenseNumber.text = codeString;
}



@end
