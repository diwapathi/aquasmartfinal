//
//  NotificationCell.h
//  AquaSmart
//
//  Created by Neotech on 08/03/17.
//  Copyright © 2017 Mac User. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NotificationCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *notificationNumber;
@property (weak, nonatomic) IBOutlet UILabel *date;
@property (weak, nonatomic) IBOutlet UILabel *tankNumber;
@property (weak, nonatomic) IBOutlet UILabel *pumpState;
@property (weak, nonatomic) IBOutlet UIImageView *pumpStateLabel;
@property (weak, nonatomic) IBOutlet UIImageView *notifyImage;
@property (weak, nonatomic) IBOutlet UILabel *reason;

@end
