//
//  NotificationViewController.m
//  AquaSmart
//
//  Created by Neotech on 07/04/17.
//  Copyright © 2017 Mac User. All rights reserved.
//

#import "NotificationViewController.h"
#import "WMSStorageManager.h"
@interface NotificationViewController ()<UITableViewDelegate,UITableViewDataSource,PacketReciveProtocol>
{
    
    DatabaseModel *datamodel;
    NSString *str;
    NSString *LastDate;
    NSTimer *timer;
   // NSString *finalString;
    NSMutableArray *reverseArray;
    NSMutableArray *finalNotifyArray;
    NSString *isReasonSensor;
}
@end

@implementation NotificationViewController

{
    NSMutableArray *notificationResponse;

}
+(instancetype)sharedInstance {
    
    static NotificationViewController *notificationVC;
    static dispatch_once_t once;
    dispatch_once(&once, ^{
        notificationVC = [[self alloc] init];
        
    });
    
    return notificationVC;
}




- (IBAction)getAllNotification:(id)sender
{
    [_notifyArray removeAllObjects];
    [self callNotificationApi];
    
    
}



- (void)viewDidLoad {
    
     notificationResponse= [[NSMutableArray alloc]init];
    [[WMSSettingsController sharedInstance]saveNotificationStatus:NO];
    [SVProgressHUD showWithStatus:@"Loading..."];
    
    timer = [NSTimer scheduledTimerWithTimeInterval:2.0 target:self selector:@selector(stopLoader:) userInfo:nil repeats:NO];
     _notifyArray = [[NSMutableArray alloc]init];
    datamodel = [[DatabaseModel alloc]init];

    [self setTopHeaderView];
    
     _notifyArray =  [[WMSStorageManager sharedInstance]fetchFromDB];
    
    isReasonSensor = datamodel.reason;
    datamodel.isRead = @"1";
    [[WMSStorageManager sharedInstance]updateResponseInDB:datamodel];
         
    
    [self.notifyTable setSeparatorColor:[UIColor colorWithRed:192/255.0 green:192/255.0 blue:192/255.0 alpha:1]];
    
    

     [super viewDidLoad];
    
    }
   

- (void)stopLoader:(NSTimer *)theTimer {
    [theTimer invalidate];
    [SVProgressHUD dismiss];
    
   
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)backButton:(id)sender {
    
    
    // [self.navigationController popViewControllerAnimated:YES];
    UIStoryboard *mainStroyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    MVYSideMenuController *sideMenuController = [self sideMenuController];
    MainViewController *homeVC = [mainStroyBoard instantiateViewControllerWithIdentifier:@"MainViewController"];
    [sideMenuController changeContentViewController:homeVC closeMenu:YES];
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return _notifyArray.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
     static NSString *cellIdentifier = @"notificationCell";
    NotificationCell *cell = [_notifyTable dequeueReusableCellWithIdentifier:cellIdentifier];
    
    NSLog(@"notify array is:%@",_notifyArray);
    DatabaseModel *dataModel = [self.notifyArray objectAtIndex:indexPath.row];
    NSString *dateString = [dataModel.date mutableCopy];
    NSTimeInterval _interval=[dateString doubleValue]/1000;
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:_interval];
    NSDateFormatter *_formatter=[[NSDateFormatter alloc]init];
    [_formatter setDateFormat:@" d LLLL yyyy HH:mm:ss"];
    [_formatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT"]];
    NSString *dateToPass  = [_formatter stringFromDate:date];
    cell.date.text = dateToPass;
    NSString *pumpStatus = [dataModel.pumpState mutableCopy];
    NSString *reason = [dataModel.reason mutableCopy];
    isReasonSensor = [dataModel.reason mutableCopy];
    
    if ([dataModel.reason containsString:@"Sensor number"]) {
        cell.pumpStateLabel.hidden = TRUE;
        cell.pumpState.text = @"";
    }
    
    else
        
    {
        cell.pumpStateLabel.hidden = NO;

        cell.pumpState.text = pumpStatus;
    }
    if ([dataModel.isRead isEqualToString:@"0"]) {
        cell.notifyImage.image = [UIImage imageNamed:@"blue_warning_amber.png"];
    }
    else
    {
        cell.notifyImage.hidden = YES;
    }
   // cell.date.text = dateString;
   
    cell.reason.text = reason;
    

    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([isReasonSensor isEqualToString:@"Sensor number"]) {
        return 60.0;
    }
    else
    {
    
        return 108.0;
    }


}

// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}



// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        [_notifyArray removeObjectAtIndex:indexPath.row];
        
        [_notifyTable deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath]
                            withRowAnimation:UITableViewRowAnimationAutomatic];
        //[_notifyTable setEditing:YES animated:YES];
        
        //        // refresh the table view
        [_notifyTable reloadData];
        
        
        
        
    }else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }
    
    [_notifyTable reloadData];
}
- (UITableViewCellEditingStyle)tableView:(UITableView *)aTableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return UITableViewCellEditingStyleDelete;
}



#pragma mark - Initial data load -
-(void)setTopHeaderView {
    NSDictionary *navDict = @{
                              TITLE_TEXT :NSLocalizedString(@"Notification", nil),
                              BACK_IMAGE :@"BackImage",
                              IS_BACK_HIDDEN:@"No"
                              };
    
    [[AppDelegate appDelegate].superVC setHeaderViewData:navDict];
}



-(void)backClicked {
    
    UIStoryboard *mainStroyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    MVYSideMenuController *sideMenuController = [self sideMenuController];
    MainViewController *homeVC = [mainStroyBoard instantiateViewControllerWithIdentifier:@"MainViewController"];
    [sideMenuController changeContentViewController:homeVC closeMenu:YES];
    
    //[self.navigationController popViewControllerAnimated:YES];
}


-(void)viewWillDisappear:(BOOL)animated {
    
   // [_notifyArray removeAllObjects];
}
-(void)viewWillAppear:(BOOL)animated {
    
   
    [super viewWillAppear:animated];
    NetworkStatus status = [self checkForWifiConnection];
    if(status == NotReachable || status == ReachableViaWWAN) {
        [self.navigationController.view makeToast:NSLocalizedString(@"Please check your wifi connection", nil)];
    }
    [[AppDelegate appDelegate].superVC updateHeadrLabel:NSLocalizedString(@"Notifications", nil)];
    
}

-(NetworkStatus)checkForWifiConnection {
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    [reachability startNotifier];
    NetworkStatus status = [reachability currentReachabilityStatus];
    return status;
}

-(void)viewDidLayoutSubviews {
    [self.constraintContentViewHeight setConstant:400-(self.view.frame.size.height)];
}
#pragma mark - core data
//- (NSManagedObjectContext *)managedObjectContext {
//    NSManagedObjectContext *context = nil;
//    context = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
//    [context setMergePolicy:NSMergeByPropertyObjectTrumpMergePolicy];
//    id delegate = [[UIApplication sharedApplication] delegate];
//    if ([delegate performSelector:@selector(managedObjectContext)]) {
//        context = [delegate managedObjectContext];
//    }
//    return context;
//}

#pragma  mark - Get frimaware version
-(void)getFirmwareVersionCallApi
{
    [TcpPacketClass sharedInstance].delegate = self;
    OrderedDictionary *requestDict = [[OrderedDictionary alloc]initWithCapacity:3];
    [requestDict insertObject:_SIDVALUE forKey:_SID atIndex:0];
    [requestDict insertObject:_SADVALUE forKey:_SAD atIndex:1];
    [requestDict insertObject:_8004 forKey:_CMD atIndex:2];
    NSError *writeError;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:requestDict options:kNilOptions error:&writeError];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    
    
    [[TcpPacketClass sharedInstance]writeOut:jsonString second:[requestDict objectForKey:_CMD]];
    
    timer = [NSTimer scheduledTimerWithTimeInterval:5.0 target:self selector:@selector(sendFirmwareAgain:) userInfo:nil repeats:NO];
    
}


- (void)sendFirmwareAgain:(NSTimer *)theTimer {
       int static ticks = 0;
    if (ticks<4) {
        [self getFirmwareVersionCallApi];
    }
    else if (ticks>3)
    {
        [theTimer invalidate];
        //[SVProgressHUD dismiss];
        //[self.view makeToast:NSLocalizedString(@"Something went wrong.Please try again.", nil)];
        ticks = -1;
    }
    ticks++;
    
    
    
    
}

-(void)didRecieveFirmwareVersionResponse:(NSDictionary *)jsonDict
{
    
        
    [timer invalidate];
    
    
}

-(void)callNotificationApi{
    
    
    [_notifyArray removeAllObjects];
    OrderedDictionary *requestDict = [[OrderedDictionary alloc]initWithCapacity:5];
    [requestDict insertObject:_SIDVALUE forKey:_SID atIndex:0];
    [requestDict insertObject:_SADVALUE forKey:_SAD atIndex:1];
    [requestDict insertObject:_8511 forKey:_CMD atIndex:2];
    [requestDict insertObject:@"00-00-00-00-00-00" forKey:_DATA atIndex:3];
    NSError *writeError;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:requestDict options:kNilOptions error:&writeError];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    [SVProgressHUD showWithStatus:@"Please Wait..."];
    [TcpPacketClass sharedInstance].delegate = self;
    [[TcpPacketClass sharedInstance]writeOut:jsonString second:[requestDict objectForKey:_CMD]];
    timer = [NSTimer scheduledTimerWithTimeInterval:5.0 target:self selector:@selector(updateCounter:) userInfo:nil repeats: NO];
}

#pragma dismiss progress loader wether responce appear or not within 5 second:
- (void)updateCounter:(NSTimer *)theTimer {
    int static ticks = 0;
    if (ticks<4) {
        [self callNotificationApi];
    }
    else if (ticks>3)
    {
        [theTimer invalidate];
        [SVProgressHUD dismiss];
        [self.view makeToast:NSLocalizedString(@"Error in fetching.Please try again.", nil)];
        ticks = -1;
    }
    ticks++;
    
}
-(void)didRecieveNotificationResponse:(NSDictionary *)jsonDict;
{
    
    
    
    [timer invalidate];
    [SVProgressHUD dismissWithDelay:30.0];
    NSDictionary *dataDict = [jsonDict valueForKey:_DATA];
    NSString *notificationNumber = [dataDict objectForKey:@"02"];
    NSInteger notification = [notificationNumber integerValue];
    NSString *notificationField = [dataDict objectForKey:@"03"];
    NSString *date = [dataDict objectForKey:@"04"];
    
    
    
    //to save date in DB in timestamp
    NSString *formattedDateString = date;
    //10-03-17-11-55-00
    NSRange range = NSMakeRange(8,1);
    NSRange range1 = NSMakeRange(11,1);
    NSRange range2 = NSMakeRange(14, 1);
    
    NSString *newText = [formattedDateString stringByReplacingCharactersInRange:range withString:@" "];
    NSString *newText1 = [newText stringByReplacingCharactersInRange:range1 withString:@":"];
    NSString *finalString = [newText1 stringByReplacingCharactersInRange:range2 withString:@":"];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT"]];
    NSDate *DateFromString = [dateFormatter dateFromString:finalString];
    double timestamp = [DateFromString timeIntervalSince1970];
    NSNumber *timeInMilliseconds = [NSNumber numberWithDouble:(timestamp*1000)];
    NSString *timeinMillStr = [ NSString stringWithFormat:@"%@",timeInMilliseconds];
    datamodel.date = timeinMillStr;
    datamodel.isRead = @"0";
    // get reason by notification field
    switch (notification) {
        case 1:
            if ([notificationField isEqualToString:@"1"]) {
                NSString *notification1 = @"Sensor number 1 malfunction";
               datamodel.reason = notification1;
                datamodel.pumpState=@"OFF";
                [notificationResponse addObject:datamodel];
                //[self checkForDuplicates];
                
                //[self saveData];
                //[self.view makeToast:NSLocalizedString(@"Sensor number 1 malfunction", nil)];
                
            }
            else if ([notificationField isEqualToString:@"2"]) {
                NSString *notification1 = @"Sensor number 2 malfunction";
               datamodel.reason = notification1;
                datamodel.pumpState=@"OFF";
                [notificationResponse addObject:datamodel];
                //[self saveData];
                // [self checkForDuplicates];
                
                //[self.view makeToast:NSLocalizedString(@"Sensor number 2 malfunction", nil)];
            }
            else if ([notificationField isEqualToString:@"3"]) {
                NSString *notification1 = @"Sensor number 3 malfunction";
               datamodel.reason = notification1;
                datamodel.pumpState=@"OFF";
                [notificationResponse addObject:datamodel];
                // [self saveData];
                //  [self checkForDuplicates];
                
                //[self.view makeToast:NSLocalizedString(@"Sensor number 3 malfunction", nil)];
                
            }
            
            else if ([notificationField isEqualToString:@"4"]) {
                NSString *notification1 = @"Sensor number 4 malfunction";
               datamodel.reason = notification1;
                datamodel.pumpState=@"OFF";
                [notificationResponse addObject:datamodel];
                // [self saveData];
                //  [self checkForDuplicates];
                
                // [self.view makeToast:NSLocalizedString(@"Sensor number 3 malfunction", nil)];
                
            }
            
            else if ([notificationField isEqualToString:@"1-2"]) {
                NSString *notification1 = @"Sensor number 1 and 2 malfunction";
               datamodel.reason = notification1;
                datamodel.pumpState=@"OFF";
                [notificationResponse addObject:datamodel];
                // [self saveData];
                //  [self checkForDuplicates];
                
                //[self.view makeToast:NSLocalizedString(@"Sensor number 1 and 2 malfunction", nil)];
                
            }
            
            else if ([notificationField isEqualToString:@"1-3"]) {
                NSString *notification1 = @"Sensor number 1 and 3 malfunction";
               datamodel.reason = notification1;
                datamodel.pumpState=@"OFF";
                [notificationResponse addObject:datamodel];
                // [self saveData];
                //  [self checkForDuplicates];
                
                // [self.view makeToast:NSLocalizedString(@"Sensor number 1 and 3 malfunction", nil)];
                
            }
            
            else if ([notificationField isEqualToString:@"1-4"]) {
                NSString *notification1 = @"Sensor number 1 and 4 malfunction";
               datamodel.reason = notification1;
                datamodel.pumpState=@"OFF";
                [notificationResponse addObject:datamodel];
                // [self saveData];
                //  [self checkForDuplicates];
                
                //[self.view makeToast:NSLocalizedString(@"Sensor number 1 and 4 malfunction", nil)];
                
            }
            
            else if ([notificationField isEqualToString:@"1-2-3"]) {
                NSString *notification1 = @"Sensor number 1,2 and 3 malfunction";
               datamodel.reason = notification1;
                datamodel.pumpState=@"OFF";
                [notificationResponse addObject:datamodel];
                // [self saveData];
                //  [self checkForDuplicates];
                
                /// [self.view makeToast:NSLocalizedString(@"Sensor number 1,2 and 3 malfunction", nil)];
                
            }
            
            else if ([notificationField isEqualToString:@"1-2-3-4"]) {
                NSString *notification1 = @"Sensor number 1,2,3 and 4 malfunction";
               datamodel.reason = notification1;
                datamodel.pumpState=@"OFF";
                [notificationResponse addObject:datamodel];
                // [self saveData];
                //  [self checkForDuplicates];
                
                //[self.view makeToast:NSLocalizedString(@"Sensor number 1,2,3 and 4  malfunction", nil)];
                
            }
            
            else if ([notificationField isEqualToString:@"2-4"]) {
                NSString *notification1 = @"Sensor number 2 and 4 malfunction";
               datamodel.reason = notification1;
                datamodel.pumpState=@"OFF";
                [notificationResponse addObject:datamodel];
                // [self saveData];
                //  [self checkForDuplicates];
                
                // [self.view makeToast:NSLocalizedString(@"Sensor number 3 malfunction", nil)];
                
            }
            
            else if ([notificationField isEqualToString:@"3-4"]) {
                NSString *notification1 = @"Sensor number 3 and 4 malfunction";
               datamodel.reason = notification1;
                datamodel.pumpState=@"OFF";
                [notificationResponse addObject:datamodel];
                // [self saveData];
                //  [self checkForDuplicates];
                
                //[self.view makeToast:NSLocalizedString(@"Sensor number 3 and 4  malfunction", nil)];
                
            }
            
            else if ([notificationField isEqualToString:@"2-3-4"]) {
                NSString *notification1 = @"Sensor number 2,3 and 4 malfunction";
               datamodel.reason = notification1;
                datamodel.pumpState=@"OFF";
                [notificationResponse addObject:datamodel];
                // [self saveData];
                //  [self checkForDuplicates];
                
                //[self.view makeToast:NSLocalizedString(@"Sensor number 2,3 and 4  malfunction", nil)];
                
            }
            
            break;
            
        case 2 :
        { NSString *notification2 = @"Sensor hits more than 6 times in a minute";
            datamodel.reason = notification2;
            datamodel.pumpState=@"OFF";
            [notificationResponse addObject:datamodel];
            // [self saveData];
            // [self checkForDuplicates];
            
            // [self.view makeToast:NSLocalizedString(@"Sensor hits more than 6 times in a minute", nil)];
        }
            break;
            
            
        case 3 :
        { NSString *notification3 = @"Pump state changes more than 4 times in a minute";
            datamodel.reason = notification3;
            datamodel.pumpState=@"OFF";
            [notificationResponse addObject:datamodel];
            //[self saveData];
            // [self checkForDuplicates];
            
            //[self.view makeToast:NSLocalizedString(@"Pump state changes more than 4 times in a minute", nil)];
        }
            break;
            
        case 4 :
        { NSString *notification4 = @"Pump wire disconnected";
            datamodel.reason = notification4;
            datamodel.pumpState=@"OFF";
            [notificationResponse addObject:datamodel];
            //[self saveData];
            //[self checkForDuplicates];
            
            
            //[self.view makeToast:NSLocalizedString(@"Pump wire disconnected", nil)];
        }
            break;
        case 5 :
        { NSString *notification5 = @"Pump wire connected";
            datamodel.reason = notification5;
            datamodel.pumpState=@"OFF";
            [notificationResponse addObject:datamodel];
            //[self saveData];
            //[self checkForDuplicates];
            
            //[self.view makeToast:NSLocalizedString(@"Pump wire connected", nil)];
            
        }
            break;
            
        case 6 :
        { NSString *notification6 = @"Pump can't be started due to wire  disconnected";
            datamodel.reason = notification6;
            datamodel.pumpState=@"OFF";
            [notificationResponse addObject:datamodel];
            //[self saveData];
            // [self checkForDuplicates];
            
            //[self.view makeToast:NSLocalizedString(@"Pump can't be started due to wire  disconnected", nil)];
        }
            break;
        case 7 :
        { NSString *notification7 = @"Pump can't be started due to ac mains off";
            datamodel.reason = notification7;
            datamodel.pumpState=@"OFF";
            [notificationResponse addObject:datamodel];
            //[self saveData];
            //[self checkForDuplicates];
            
            // [self.view makeToast:NSLocalizedString(@"Pump can't ne started due to ac mains off", nil)];
            
        }
            break;
        case 8 :
        { NSString *notification8 = @"Automated task Nnumber 1 executed";
            datamodel.reason = notification8;
            datamodel.pumpState=@"OFF";
            [notificationResponse addObject:datamodel];
            // [self saveData];
            // [self checkForDuplicates];
            
            // [self.view makeToast:NSLocalizedString(@"Automated task Nnumber 1 executed", nil)];
            
        }
            break;
        case 9 :
        { NSString *notification9 = @"Automated task Nnumber 2 executed";
            datamodel.reason = notification9;
            datamodel.pumpState=@"OFF";
            [notificationResponse addObject:datamodel];
            // [self saveData];
            // [self checkForDuplicates];
            
            // [self.view makeToast:NSLocalizedString(@"Automated task Nnumber 2 executed", nil)];
            
        }
            break;
        case 10 :
        { NSString *notification10 = @"Automated task Nnumber 3 executed";
            datamodel.reason = notification10;
            datamodel.pumpState=@"OFF";
            [notificationResponse addObject:datamodel];
            //[self saveData];
            // [self checkForDuplicates];
            
            // [self.view makeToast:NSLocalizedString(@"Automated task Nnumber 3 executed", nil)];
            
        }
            break;
            
            
            
        case 17 :
        {
            
            if ([[dataDict valueForKey:_03] isEqualToString:@"1"])
                
            {
                NSString *reason1 = @"Motor trigger from Switch";
                
                
                
               // datamodel.date = date;
                datamodel.reason = reason1;
                datamodel.pumpState = @"OFF";
                [notificationResponse addObject:datamodel];
                
                
                // [self saveData];
                //[self checkForDuplicates];
                
                // [self.view makeToast:NSLocalizedString(@"Motor trigger from Switch", nil)];
            }
            
            else if ([[dataDict valueForKey:_03]isEqualToString:@"2"])
            {
                
                NSString *reason1 = @"Motor trigger from App";
                
                
               // datamodel.date = date;
                //datamodel.tank = tank;
                datamodel.reason = reason1;
                datamodel.pumpState = @"OFF";
                
                
                [notificationResponse addObject:datamodel];
                // [self saveData];
                //[self checkForDuplicates];
                
                //  [self.view makeToast:NSLocalizedString(@"Motor trigger from App", nil)];
                
            }
            
            else if ([[dataDict valueForKey:_03]isEqualToString:@"3"])
            {
                
                NSString *reason1 = @"Motor trigger from Schedule";
                
                
                //datamodel.date = date;
                
                datamodel.reason = reason1;
                datamodel.pumpState = @"OFF";
                
                [notificationResponse addObject:datamodel];
                //[self saveData];
                
                //[self.view makeToast:NSLocalizedString(@"Motor trigger from Schedule", nil)];
            }
            
            else if ([[dataDict valueForKey:_03]isEqualToString:@"4"])
            {
                
                NSString *reason1 = @"Motor trigger from Dry Run";
                
                
             //   datamodel.date = date;
                //  datamodel.tank = tank;
                datamodel.reason = reason1;
                datamodel.pumpState = @"OFF";
                [notificationResponse addObject:datamodel];
                
                // [self saveData];
                //[self checkForDuplicates];
                
                // [self.view makeToast:NSLocalizedString(@"Motor trigger from Dry Run", nil)];
                
            }
            
            else if ([[dataDict valueForKey:_03]isEqualToString:@"5"])
            {
                
                NSString *reason1 = @"Motor trigger from Auto Task";
                
                
               // datamodel.date = date;
                //datamodel.tank = tank;
                datamodel.reason = reason1;
                datamodel.pumpState = @"OFF";
                [notificationResponse addObject:datamodel];
                
                // [self saveData];
                //[self checkForDuplicates];
                
                // [self.view makeToast:NSLocalizedString(@"Motor turn off due to water level 100%", nil)];
                
            }
            
            else if ([[dataDict valueForKey:_03]isEqualToString:@"6"])
                
                
            {
                
                NSString *reason1 = @"Motor trigger from Power Failure";
                
                
               // datamodel.date = date;
                //datamodel.tank = tank;
                datamodel.reason = reason1;
                datamodel.pumpState = @"OFF";
                [notificationResponse addObject:datamodel];
                
                
                // [self saveData];
                //[self checkForDuplicates];
                
                // [self.view makeToast:NSLocalizedString(@"Motor trigger from Power Failure", nil)];
                
                
            }
            
            else if ([[dataDict valueForKey:_03]isEqualToString:@"14"])
                
                
            {
                
                NSString *reason1 = @"Due to tank Full";
                
                
               // datamodel.date = date;
                //datamodel.tank = tank;
                datamodel.reason = reason1;
                datamodel.pumpState = @"OFF";
                [notificationResponse addObject:datamodel];
                
                
                // [self saveData];
                //[self checkForDuplicates];
                
                //  [self.view makeToast:NSLocalizedString(@"Due to tank full", nil)];
                
                
            }
        }
            
            break;
            
            
        case 18 :
        { NSString *notification18 = @"System on battery";
            datamodel.reason = notification18;
            datamodel.pumpState=@"OFF";
            [notificationResponse addObject:datamodel];
            // [self saveData];
            // [self checkForDuplicates];
            
            //[self.view makeToast:NSLocalizedString(@"System on battery", nil)];
            
        }
            break;
            
            
            
            
        case 15 :
        { NSString *notification18 = @"Tank Full";
            datamodel.reason = notification18;
            datamodel.pumpState=@"OFF";
            [notificationResponse addObject:datamodel];
            //[self saveData];
            //[self checkForDuplicates];
            
            // [self.view makeToast:NSLocalizedString(@"Tank Full", nil)];
            
        }
            break;
        case 19 :
        { NSString *notification19 = @"System on mains";
            datamodel.reason = notification19;
            datamodel.pumpState=@"OFF";
            [notificationResponse addObject:datamodel];
            // [self saveData];
            //   [self checkForDuplicates];
            
            //[self.view makeToast:NSLocalizedString(@"System on mains", nil)];
            
        }
            break;
        case 24 :
        {
            
            
            if ([[dataDict valueForKey:_03] isEqualToString:@"1"])
                
            {
                NSString *reason1 = @"Motor trigger from Switch";
                
                
                
                //datamodel.date = date;
                datamodel.reason = reason1;
                datamodel.pumpState = @"ON";
                [notificationResponse addObject:datamodel];
                
                
                //[self saveData];
                //[self checkForDuplicates];
                
                //[self.view makeToast:NSLocalizedString(@"Motor trigger from Switch", nil)];
            }
            
            else if ([[dataDict valueForKey:_03]isEqualToString:@"2"])
            {
                
                NSString *reason1 = @"Motor trigger from App";
                
                
               // datamodel.date = date;
                //datamodel.tank = tank;
                datamodel.reason = reason1;
                datamodel.pumpState = @"ON";
                [notificationResponse addObject:datamodel];
                
                
             
                
            }
            
            else if ([[dataDict valueForKey:_03]isEqualToString:@"3"])
            {
                
                NSString *reason1 = @"Motor trigger from Schedule";
                
                
               // datamodel.date = date;
                
                datamodel.reason = reason1;
                datamodel.pumpState = @"ON";
                [notificationResponse addObject:datamodel];
                
                
                          }
            
            else if ([[dataDict valueForKey:_03]isEqualToString:@"4"])
            {
                
                NSString *reason1 = @"Motor trigger from Dry Run";
                
                
                               datamodel.reason = reason1;
                datamodel.pumpState = @"ON";
                
                [notificationResponse addObject:datamodel];
                
                
            }
            
            else if ([[dataDict valueForKey:_03]isEqualToString:@"5"])
            {
                
                //NSString *reason1 = @"Motor turn off due to water level 100%";
                
                NSString *reason1 = @"Motor trigger from Auto Task";
              
                datamodel.reason = reason1;
                datamodel.pumpState = @"OFF";
                
                [notificationResponse addObject:datamodel];
                
            }
            
            else if ([[dataDict valueForKey:_03]isEqualToString:@"6"])
                
                
            {
                
                NSString *reason1 = @"Motor trigger from Power Failure";
                
                
              
                datamodel.reason = reason1;
                datamodel.pumpState = @"ON";
                [notificationResponse addObject:datamodel];
                
                
                
            }
            
            else if ([[dataDict valueForKey:_03]isEqualToString:@"14"])
                
                
            {
                
                NSString *reason1 = @"Due to tank Full";
                
                
                             datamodel.reason = reason1;
                datamodel.pumpState = @"OFF";
                [notificationResponse addObject:datamodel];
                
            }
            
            
            
            
        }
            break;
        case 25 :
        {
            
            if ([[dataDict valueForKey:_03] isEqualToString:@"1"])
                
            {
                NSString *reason1 = @"Motor trigger from Switch";
                
                
                
                
                datamodel.reason = reason1;
                datamodel.pumpState = @"OFF";
                
                [notificationResponse addObject:datamodel];
                            }
            
            else if ([[dataDict valueForKey:_03]isEqualToString:@"2"])
            {
                
                NSString *reason1 = @"Motor trigger from App";
                
                               datamodel.reason = reason1;
                datamodel.pumpState = @"OFF";
                [notificationResponse addObject:datamodel];
                
                        }
            
            else if ([[dataDict valueForKey:_03]isEqualToString:@"3"])
            {
                
                NSString *reason1 = @"Motor trigger from Schedule";
                
                
                
                datamodel.reason = reason1;
                datamodel.pumpState = @"OFF";
                
                [notificationResponse addObject:datamodel];
               
            }
            
            else if ([[dataDict valueForKey:_03]isEqualToString:@"4"])
            {
                
                NSString *reason1 = @"Motor trigger from Dry Run";
                
                
                              datamodel.reason = reason1;
                datamodel.pumpState = @"OFF";
                [notificationResponse addObject:datamodel];
                
            }
            
            else if ([[dataDict valueForKey:_03]isEqualToString:@"5"])
            {
                
                NSString *reason1 = @"Motor trigger from Auto Task%";
                
                
              
                datamodel.reason = reason1;
                datamodel.pumpState = @"OFF";
                [notificationResponse addObject:datamodel];
                
                            }
            
            else if ([[dataDict valueForKey:_03]isEqualToString:@"6"])
                
                
            {
                
                NSString *reason1 = @"Motor trigger from Power Failure";
                
                
               
                datamodel.reason = reason1;
                datamodel.pumpState = @"OFF";
                
                [notificationResponse addObject:datamodel];
                
                
            }
            
            else if ([[dataDict valueForKey:_03]isEqualToString:@"14"])
                
                
            {
                
                NSString *reason1 = @"Due to tank Full";
                
                
                datamodel.reason = reason1;
                datamodel.pumpState = @"OFF";
                
                [notificationResponse addObject:datamodel];
                
                
                
                
            }
            
        }
            break;
            
            
            
            
        default:
            break;
    }
    
    [[WMSStorageManager sharedInstance]savePumpResponseInDB:notificationResponse];
    _notifyArray =  [[WMSStorageManager sharedInstance]fetchFromDB];
    [_notifyTable reloadData];
   
    
    
}



-(void)viewDidDisappear:(BOOL)animated
{
   // [_notifyArray removeAllObjects];

}
-(void)viewDidAppear:(BOOL)animated
{
   //_notifyArray =  [[WMSStorageManager sharedInstance]fetchFromDB];
}

//-(void)readData
//{

//    NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
//    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Notification"];
////    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"date > %@", 30];
////    fetchRequest.predicate = predicate;
//    self.notifyArray = [[managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
////    NSOrderedSet *orderedSet = [NSOrderedSet orderedSetWithArray:_notifyArray];
////    NSArray *arrayWithoutDuplicates = [orderedSet array];
//
//   // finalNotifyArray = [[NSMutableArray alloc]initWithArray:_notifyArray];
//
//         [_notifyTable reloadData];
//}


//-(void)saveData
//{
//
//    //DatabaseModel *databaseModel = [[DatabaseModel alloc]init];
////    NSManagedObjectContext *context = [self managedObjectContext];
////
////    // Create a new managed object
////    NSManagedObject *newDevice = [NSEntityDescription insertNewObjectForEntityForName:@"Notification" inManagedObjectContext:context];
//
//    NSString *dateString = datamodel.date;
//    NSString *formattedDateString = dateString;
//    //10-03-17-11-55-00
//    NSRange range = NSMakeRange(8,1);
//    NSRange range1 = NSMakeRange(11,1);
//    NSRange range2 = NSMakeRange(14, 1);
//
//    NSString *newText = [formattedDateString stringByReplacingCharactersInRange:range withString:@" "];
//    NSString *newText1 = [newText stringByReplacingCharactersInRange:range1 withString:@":"];
//    NSString *finString = [newText1 stringByReplacingCharactersInRange:range2 withString:@":"];
//
//    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
//    [dateFormatter setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
//    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT"]];
//    NSDate *DateFromString = [dateFormatter dateFromString:finString];
//    double timestamp = [DateFromString timeIntervalSince1970];
//    NSNumber *timeInMilliseconds = [NSNumber numberWithDouble:(timestamp*1000)];
//    NSString *timeinMillStr = [ NSString stringWithFormat:@"%@",timeInMilliseconds];
//    [newDevice setValue:timeinMillStr forKey:@"date"];
//    [newDevice setValue:datamodel.pumpState forKey:@"pumpState"];
//    [newDevice setValue:datamodel.reason forKey:@"reason"];
//    [newDevice setValue:datamodel.tank forKey:@"tankNumber"];
//    //    for (int i=0;i<_notifyArray.count;i++)
//    //    {
//    //        NSString *duplicate = [mutableFetchResults objectAtIndex:i];
//    //            NSString * timeStampString = [duplicate valueForKey:@"date"];
//    //    if ([[_notifyArray objectAtIndex:i]isEqualToString:[newDevice valueForKey:@"date"]]) {
//    //        NSLog(@"duplicate element");
//    //    }
//    //    else
//    //    {
//
//   // [_notifyArray addObject:newDevice];
//    //}
//    //  }
//    NSError *error = nil;
//    // Save the object to persistent store
//    if (![context save:&error]) {
//        NSLog(@"Can't Save! %@ %@", error, [error localizedDescription]);
//    }
//
//    else
//    {
//
//    NSLog(@"Notification Save Successfully");
//    }
//
//}
//
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */
//-(void)checkForDuplicates
//{
//    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Notification"
//                                              inManagedObjectContext:self.managedObjectContext];
//
//    NSFetchRequest *request = [[NSFetchRequest alloc] init];
//    [request setEntity:entity];
//
//    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"date"
//                                                                   ascending:NO];
//    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
//
//    [request setSortDescriptors:sortDescriptors];
//
//
//    NSError *Fetcherror;
//    NSMutableArray *mutableFetchResults = [[self.managedObjectContext
//                                            executeFetchRequest:request error:&Fetcherror] mutableCopy];
//
//    if (!mutableFetchResults) {
//        // error handling code.
//    }
//
////    if (mutableFetchResults.count==0) {
////        [self saveData];
////
////    }
//    //else
//   // {
//    for (int i = 0 ; i < [mutableFetchResults count] ; i++) {
//         NSString *duplicate = [mutableFetchResults objectAtIndex:i];
//
//
//        NSString * timeStampString = [duplicate valueForKey:@"date"];
//        NSString *timeStampString1 = [ NSString stringWithFormat:@"%@",timeStampString];
//
//
//        NSString *dateString = datamodel.date;
//        NSString *formattedDateString = dateString;
//        //10-03-17-11-55-00
//        NSRange range = NSMakeRange(8,1);
//        NSRange range1 = NSMakeRange(11,1);
//        NSRange range2 = NSMakeRange(14, 1);
//
//        NSString *newText = [formattedDateString stringByReplacingCharactersInRange:range withString:@" "];
//        NSString *newText1 = [newText stringByReplacingCharactersInRange:range1 withString:@":"];
//        NSString *finalString = [newText1 stringByReplacingCharactersInRange:range2 withString:@":"];
//
//        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
//        [dateFormatter setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
//        [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT"]];
//        NSDate *DateFromString = [dateFormatter dateFromString:finalString];
//        double timestamp = [DateFromString timeIntervalSince1970];
//        NSNumber *timeInMilliseconds = [NSNumber numberWithDouble:(timestamp*1000)];
//        NSString *timeString = [ NSString stringWithFormat:@"%@",timeInMilliseconds];
//
//
//        if (![timeStampString1 isEqualToString:timeString] && i== mutableFetchResults.count) {
//            NSLog(@"no duplicate entry!");
//            [self saveData];
//
//        }
//        else
//        {
//
//            NSLog(@"Duplicate element found");
//            //[self saveData];
//        }
//
//    }
//
//
//
//   // }
//
//
//
//
////    if (![mutableFetchResults
////          containsObject:datamodel.date]) {
////        //notify duplicates
////
////       // [self saveData];
////
////        return;
////    }
//}
@end
