//
//  NotificationViewController.h
//  AquaSmart
//
//  Created by Neotech on 07/04/17.
//  Copyright © 2017 Mac User. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DatabaseModel.h"
#import "NotificationCell.h"
#import <CoreData/CoreData.h>
#import "DatabaseModel.h"
#import "WMSBlackBoxCommands.h"
#import "OrderedDictionary.h"
#import "SVProgressHUD.h"
#import "TcpPacketClass.h"
#import "DatabaseModel.h"
#import "MainViewController.h"
@interface NotificationViewController : UIViewController
@property (nonatomic,strong) NSMutableArray *sortFetchArray;
@property (nonatomic,strong) NSMutableArray *notifyArray;
@property (nonatomic,strong) NSArray *arrayWithoutDuplicates;

@property (strong, nonatomic) IBOutlet UITableView *notifyTable;

@property (nonatomic,strong) NSMutableDictionary *notifDict;
-(void)callNotificationApi;
-(void)backClicked;
+(instancetype)sharedInstance;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintContentViewHeight;
@end
