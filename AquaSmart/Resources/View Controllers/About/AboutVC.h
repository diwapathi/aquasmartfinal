//
//  AboutVC.h
//  AquaSmart
//
//  Created by Neotech on 25/04/17.
//  Copyright © 2017 Mac User. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>

#import "MVYSideMenuController.h"
#import "MainViewController.h"
@interface AboutVC : UIViewController<MFMessageComposeViewControllerDelegate,MFMailComposeViewControllerDelegate>
@property (weak, nonatomic) IBOutlet UIButton *menuButton;
-(void)backClicked;
- (void)presentModalMailComposerViewController:(BOOL)animated;


@property(nonatomic)BOOL isFromLeftPanel;
@end
