//
//  ChangePasswordVC.h
//  AquaSmart
//
//  Created by Neotech on 25/04/17.
//  Copyright © 2017 Mac User. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIView+Toast.h"
#import "OrderedDictionary.h"
#import "SVProgressHUD.h"
#import "TcpPacketClass.h"
#import "AppDelegate.h"
#import "Reachability.h"
#import "MainViewController.h"
#import "MVYSideMenuController.h"
#import "WMSSettingsController.h"
@interface ChangePasswordVC : UIViewController<PacketReciveProtocol>
@property (weak, nonatomic) IBOutlet UIButton *oldPasswordViewButton;
@property (weak, nonatomic) IBOutlet UIButton *passwordViewButton;
@property (weak, nonatomic) IBOutlet UIButton *confirmPasswordViewButton;
@property (weak, nonatomic) IBOutlet UITextField *oldPassword;
@property (weak, nonatomic) IBOutlet UITextField *confirmPassword;
@property (weak, nonatomic) IBOutlet UITextField *password;
@property (weak, nonatomic) IBOutlet UIButton *saveButton;



@end
