//
//  ChangePasswordVC.m
//  AquaSmart
//
//  Created by Neotech on 25/04/17.
//  Copyright © 2017 Mac User. All rights reserved.
//

#import "ChangePasswordVC.h"

@interface ChangePasswordVC ()

@end

@implementation ChangePasswordVC
{
    NSTimer *timer;
}

- (void)viewDidLoad {
    UITapGestureRecognizer *gestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    [self.view addGestureRecognizer:gestureRecognizer];
   // [self setupOutlets];
    [self setTopHeaderView];
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}


#pragma mark - Initial data load -
-(void)setTopHeaderView {
    NSDictionary *navDict = @{
                              TITLE_TEXT :NSLocalizedString(@"Settings", nil),
                              BACK_IMAGE :@"BackImage",
                              };
    [[AppDelegate appDelegate].superVC setHeaderViewData:navDict];
}



- (IBAction)oldPassword:(id)sender
{
    
    
    if (self.oldPassword.text.length <= 0) {
        [self.view makeToast:@"Please enter your old password"
                    duration:2.0
                    position:CSToastPositionCenter];
        // [self.view makeToast:NSLocalizedString(@"Please fill license number.", nil)];
        [self.view endEditing:YES];
        return;
        
        
    }
    
    
}

- (IBAction)newPassword:(id)sender
{
    
    if (self.password.text.length <= 0) {
        [self.view makeToast:@"Password should be atleast 6 characters"
                    duration:2.0
                    position:CSToastPositionCenter];
        // [self.view makeToast:NSLocalizedString(@"Please fill license number.", nil)];
        [self.view endEditing:YES];
        return;
        
        
    }
    
}

- (IBAction)confirmPassword:(id)sender
{
    
    if (self.confirmPassword.text.length <= 0) {
        [self.view makeToast:@"Password should be atleast 6 characters"
                    duration:2.0
                    position:CSToastPositionCenter];
        // [self.view makeToast:NSLocalizedString(@"Please fill license number.", nil)];
        [self.view endEditing:YES];
        return;
        
        
    }
    
}
-(void)backClicked {
    UIStoryboard *mainStroyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    MVYSideMenuController *sideMenuController = [self sideMenuController];
    MainViewController *homeVC = [mainStroyBoard instantiateViewControllerWithIdentifier:@"MainViewController"];
    [sideMenuController changeContentViewController:homeVC closeMenu:YES];
}
#pragma mark - setting length for texfields
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    //NSString *newString = [self.serialNumTF.text ////stringByReplacingCharactersInRange:range withString:string];
    //return (newString.length<=12);
    if(textField == _oldPassword) {
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        return (newLength > 12) ? NO : YES;
    }
    else if(textField == _password)
    {
        //do the same with different values
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        return (newLength >12) ? NO : YES;
    }
    
    else if(textField == _confirmPassword)
    {
        //do the same with different values
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        return (newLength >12) ? NO : YES;
    }
    //    else if(textField == _confirmPassword)
    //    {
    //        //do the same with different values
    //        NSUInteger newLength = [textField.text length] + [string length] - range.length;
    //        return (newLength >12) ? NO : YES;
    //    }
    
    return 0;
}
-(void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    NetworkStatus status = [self checkForWifiConnection];
    if(status == NotReachable || status == ReachableViaWWAN) {
        [self.navigationController.view makeToast:NSLocalizedString(@"Please check your wifi connection", nil)];
    }
    [[AppDelegate appDelegate].superVC updateHeadrLabel:NSLocalizedString(@"Change Password", nil)];
    
}

-(NetworkStatus)checkForWifiConnection {
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    [reachability startNotifier];
    NetworkStatus status = [reachability currentReachabilityStatus];
    return status;
}


- (IBAction)saveButtonClicked:(id)sender
{
    
    //    if (![self.password.text isEqualToString:self.confirmPassword.text]) {
    //        [self.view makeToast:NSLocalizedString(@"Passwords do not match \n please retype",nil)];
    //    }
    //
    if (self.password.text.length <= 0) {
        [self.view makeToast:NSLocalizedString(@"Please fill Password.", nil)];
        [self.view endEditing:YES];
        return;
    }
    
    else if(self.oldPassword.text.length<=0)
    {
        [self.view makeToast:NSLocalizedString(@"Please fill old Password.", nil)];
        [self.view endEditing:YES];
        return;
    }
    
//    else if (![self.oldPassword.text isEqualToString:[[WMSSettingsController sharedInstance]getAdminPassword]]) {
//        [self.view makeToast:NSLocalizedString(@" Old Password do not match \n please retype", nil)];
//        [self.view endEditing:YES];
//        self.oldPassword.text = @"";
//        return;
//    }
    else if (self.password.text.length < 6) {
        [self.view makeToast:NSLocalizedString(@" Password must not be less than 6 characters", nil)];
        [self.view endEditing:YES];
        return;
    }
    
    else if (self.confirmPassword.text.length <= 0) {
        [self.view makeToast:NSLocalizedString(@"Please fill Confirm Password.", nil)];
        [self.view endEditing:YES];
        return;
    }
   else if (![self.password.text isEqualToString:self.confirmPassword.text]) {
        [self.view makeToast:NSLocalizedString(@" Passwords do not match \n please retype", nil)];
        [self.view endEditing:YES];
        self.password.text = @"";
        self.confirmPassword.text = @"";
        return;
    }
    
    else{
        [self callChangePasswordApi];
    }
}

//- (IBAction)viewOldPaswordBtnClicked:(id)sender {
//    if (self.oldPassword.text) {
//        [self.oldPassword setSecureTextEntry:NO];
//    }
//}

- (IBAction)hideOldPasswordButtonClicked:(id)sender {
    if (self.oldPassword.text) {
        [self.oldPassword setSecureTextEntry:NO];
    }
    [self performSelector:@selector(passButtonClicked) withObject:nil afterDelay:2.0];
}

-(void)passButtonClicked
{
    if (self.oldPassword.text) {
        [self.oldPassword setSecureTextEntry:YES];
    }
}


//- (IBAction)viewPaswordBtnClicked:(id)sender {
//    if (self.password.text) {
//        [self.password setSecureTextEntry:NO];
//    }
//}

- (IBAction)hidePasswordButtonClicked:(id)sender {
    if (self.password.text) {
        [self.password setSecureTextEntry:NO];
    }
    [self performSelector:@selector(hideButtonClicked) withObject:nil afterDelay:2.0];
}


-(void)hideButtonClicked
{
    if (self.password.text) {
        [self.password setSecureTextEntry:YES];
    }
}


//- (IBAction)viewConfirmPaswordBtnClicked:(id)sender {
//    if (self.confirmPassword.text) {
//        [self.confirmPassword setSecureTextEntry:NO];
//    }
//}

- (IBAction)hideConfirmPasswordButtonClicked:(id)sender {
    if (self.confirmPassword.text) {
        [self.confirmPassword setSecureTextEntry:NO];
    }
    [self performSelector:@selector(hideConfButtonClicked) withObject:nil afterDelay:2.0];
}

-(void)hideConfButtonClicked
{
    if (self.confirmPassword.text) {
        [self.confirmPassword setSecureTextEntry:YES];
    }
}


- (IBAction)cancelButtonClicked:(id)sender {
    self.password.text = nil;
    self.confirmPassword.text = nil;
}

- (IBAction)backButton:(id)sender {
    
    
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)hideKeyboard
{
    
    [_oldPassword resignFirstResponder];
    [_password resignFirstResponder];
    [_confirmPassword resignFirstResponder];
    
    
    
    
}
#pragma mark - schedule Disable  Api
-(void)callChangePasswordApi
{
    NSString *oldpasswordString = [self getName:12 :_oldPassword];
    NSString *newpasswordString = [self getName:12 :_password];
   // [[NSUserDefaults standardUserDefaults] setObject:_password forKey:@"newPass"];
    //[[NSUserDefaults standardUserDefaults] synchronize];
    OrderedDictionary *dataDict = [[OrderedDictionary alloc]initWithCapacity:8];
    
    [dataDict insertObject:oldpasswordString forKey:_01 atIndex:0];
    [dataDict insertObject:newpasswordString forKey:_02 atIndex:1];
    
    OrderedDictionary *requestDict = [[OrderedDictionary alloc]initWithCapacity:5];
    [requestDict insertObject:_SIDVALUE forKey:_SID atIndex:0];
  
    [requestDict insertObject:_SADVALUE forKey:_SAD atIndex:1];
    [requestDict insertObject:_8204 forKey:_CMD atIndex:2];
    [requestDict insertObject:dataDict forKey:_DATA atIndex:3];
    NSError *writeError;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:requestDict options:kNilOptions error:&writeError];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    [SVProgressHUD showWithStatus:@"Please Wait.."];
    
    [TcpPacketClass sharedInstance].delegate = self;
      [[TcpPacketClass sharedInstance]writeOut:jsonString second:[requestDict objectForKey:_CMD]];
    timer = [NSTimer scheduledTimerWithTimeInterval: 5.0 target:self selector:@selector(updateCounter:) userInfo:nil repeats: NO];
}

- (void)updateCounter:(NSTimer *)theTimer {
    [theTimer invalidate];
    [SVProgressHUD dismiss];
    [self.view makeToast:NSLocalizedString(@"Something went wrong.Try Again!!", nil)];
}

-(void)getChangePasswordResponse:(NSDictionary *)jsonDict
{
    [SVProgressHUD dismiss];
    // NSDictionary *dataDict = [jsonDict valueForKey:_DATA];
    if ([[jsonDict valueForKey:_DATA] isEqualToString:@"0"]) {
        [self.view makeToast:NSLocalizedString(@"Password Change Successfully", nil)];
        
        return;
    }
    
    if ([[jsonDict valueForKey:_DATA] isEqualToString:@"1"])     {
        [self.view makeToast:NSLocalizedString(@"Failed to Change Password!!!!", nil)];
    }
    
}

#pragma mark - Text fields text lenght validations -
-(NSString *)getName :(int)lenght :(UITextField *)texfield {
    NSMutableString *finalUserName = [texfield.text mutableCopy] ;
    
    if (texfield.text.length == lenght) {
        finalUserName = [texfield.text mutableCopy];
    }
    else {
        int count = (int)texfield.text.length;
        for (int i = count; i < lenght; i++) {
            
            [finalUserName appendString:@" "];
        }
    }
    return finalUserName;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
