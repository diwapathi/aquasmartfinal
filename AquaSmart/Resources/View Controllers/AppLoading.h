//
//  AppLoading.h
//  Homesmart
//
//  Created by Neotech on 07/06/16.
//  Copyright © 2016 Neotech. All rights reserved.
//
@protocol PacketReciveProtocol;
#import <Foundation/Foundation.h>
#import "AppDelegate.h"
#import "SplashScreenVC.h"
#import "BuyerRegistrationVCOne.h"
#import "Reachability.h"
#import <SystemConfiguration/SystemConfiguration.h>
#import "SystemTimeVC.h"
@class TcpPacketClass;


@interface AppLoading : NSObject<PacketReciveProtocol>
{
     int totalFilesDownloaded;
       
    NSTimer *timer;
}
+(instancetype)sharedInstance;
@property (nonatomic, assign, readonly ) BOOL              isReceiving;
@property (nonatomic, strong, readwrite) NSInputStream *   networkStream;
@property (nonatomic, copy,   readwrite) NSString *        filePath;
@property (nonatomic, strong, readwrite) NSOutputStream *  fileStream;

-(void)startAppLoadingActivity;
-(void)checkForRegistrationType;
//@property(nonatomic,strong)id<ApploadingProtocol>delegate;
@end
