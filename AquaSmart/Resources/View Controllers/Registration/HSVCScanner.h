//
//  HSVCScanner.h
//  Homesmart
//
//  Created by Neotech on 10/08/16.
//  Copyright © 2016 Neotech. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MVYSideMenuController.h"
@protocol ScannerProtocol <NSObject>
@optional
-(void)serialScanComplete:(NSString *)codeString;
-(void)licenceScanComplete:(NSString *)codeString;
@end
@interface HSVCScanner : UIViewController
@property(nonatomic,weak)id<ScannerProtocol>delegate;
@property(nonatomic)BOOL isLicenceSelected;
-(void)backClicked;
@end
