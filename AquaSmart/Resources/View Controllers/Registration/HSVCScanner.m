//
//  HSVCScanner.m
//  Homesmart
//
//  Created by Neotech on 10/08/16.
//  Copyright © 2016 Neotech. All rights reserved.
//

#import "HSVCScanner.h"
#import "MTBBarcodeScanner.h"
#import "ForgatePassword.h"
#import "UIView+Toast.h"
#import "AppDelegate.h"
@interface HSVCScanner ()

@property (nonatomic, weak) IBOutlet UIView *previewView;
@property (nonatomic, weak) IBOutlet UIButton *toggleScanningButton;
@property (nonatomic, weak) IBOutlet UIView *viewOfInterest;
@property (nonatomic, strong) MTBBarcodeScanner *scanner;
@property (nonatomic, strong) NSMutableDictionary *overlayViews;
@property (nonatomic, assign) BOOL didShowAlert;
@property (nonatomic, weak) IBOutlet UILabel *instructions;
@end

@implementation HSVCScanner
@synthesize delegate,isLicenceSelected;

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setTopHeaderView];
    [self setInitialConfiguration];
    // Do any additional setup after loading the view.
}

- (void)viewWillDisappear:(BOOL)animated {
   [super viewWillDisappear:animated];
    [self.scanner stopScanning];
}

-(void)setTopHeaderView {
    NSDictionary *navDict = @{
                              BACK_SELECTOR :@"backClicked",
                              TITLE_TEXT :NSLocalizedString(@"Scanner", nil),
                              BACK_IMAGE :@"BackImage",
                              IS_BACK_HIDDEN:@"No"
                              };
    [[AppDelegate appDelegate].superVC setHeaderViewData:navDict];
}

-(void)backClicked {
    [self.navigationController popViewControllerAnimated:YES];
//    UIStoryboard *mainStroyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//    MVYSideMenuController *sideMenuController = [self sideMenuController];
//    ForgatePassword *homeVC = [mainStroyBoard instantiateViewControllerWithIdentifier:@"ForgatePassword"];
//    [sideMenuController changeContentViewController:homeVC closeMenu:YES];
}

 
-(void)setInitialConfiguration {
    //[self.toggleScanningButton setTitleColor:[[ThemeManager sharedManger] colorFromKey:LABEL_TEXT_COLOR] forState:UIControlStateNormal];
    self.toggleScanningButton.layer.cornerRadius = 2.0;
   // self.toggleScanningButton.layer.borderColor = [[ThemeManager sharedManger]colorFromKey:BORDER_COLOR].CGColor;
    self.toggleScanningButton.layer.borderWidth = 1.0;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Scanner
- (MTBBarcodeScanner *)scanner {
    if (!_scanner) {
        _scanner = [[MTBBarcodeScanner alloc] initWithPreviewView:_previewView];
    }
    return _scanner;
}

#pragma mark - Overlay Views
- (NSMutableDictionary *)overlayViews {
    if (!_overlayViews) {
        _overlayViews = [[NSMutableDictionary alloc] init];
    }
    return _overlayViews;
}

#pragma mark - Scanning
- (void)startScanning {
    
    self.scanner.didStartScanningBlock = ^{
        NSLog(@"The scanner started scanning!");
    };
    
    self.scanner.didTapToFocusBlock = ^(CGPoint point){
        NSLog(@"The user tapped the screen to focus. \
              Here we could present a view at %@", NSStringFromCGPoint(point));
    };
    
    [self.scanner startScanningWithResultBlock:^(NSArray *codes) {
        [self drawOverlaysOnCodes:codes];
    }];
    
    // Optionally set a rectangle of interest to scan codes. Only codes within this rect will be scanned.
    self.scanner.scanRect = self.viewOfInterest.frame;
    
    [self.toggleScanningButton setTitle:@"Stop Scanning" forState:UIControlStateNormal];
    self.toggleScanningButton.backgroundColor = [UIColor redColor];
}

- (void)drawOverlaysOnCodes:(NSArray *)codes {
    // Get all of the captured code strings
    NSMutableArray *codeStrings = [[NSMutableArray alloc] init];
    for (AVMetadataMachineReadableCodeObject *code in codes) {
        if (code.stringValue) {
            [codeStrings addObject:code.stringValue];
        }
    }
    
    // Remove any code overlays no longer on the screen
    for (NSString *code in self.overlayViews.allKeys) {
        if ([codeStrings indexOfObject:code] == NSNotFound) {
            // A code that was on the screen is no longer
            // in the list of captured codes, remove its overlay
            [self.overlayViews[code] removeFromSuperview];
            [self.overlayViews removeObjectForKey:code];
        }
    }
    
    for (AVMetadataMachineReadableCodeObject *code in codes) {
        UIView *view = nil;
        NSString *codeString = code.stringValue;
        
        if (codeString) {
            if (self.overlayViews[codeString]) {
                // The overlay is already on the screen
                view = self.overlayViews[codeString];
                
                // Move it to the new location
                view.frame = code.bounds;
                
            } else {
                // First time seeing this code
                BOOL isValidCode = [self isValidCodeString:codeString];
                
                // Create an overlay
                UIView *overlayView = [self overlayForCodeString:codeString
                                                          bounds:code.bounds
                                                           valid:isValidCode];
                self.overlayViews[codeString] = overlayView;
                
                // Add the overlay to the preview view
                [self.previewView addSubview:overlayView];
                if (isValidCode) {
                    
                    if ([codeString containsString:@","]) {
                        NSArray *scannedCodeArray = [codeString componentsSeparatedByString:@","];
                        if(self.delegate && [self.delegate respondsToSelector:@selector(serialScanComplete:)]) {
                            [self.delegate serialScanComplete:[scannedCodeArray objectAtIndex:0]];
                        }
                        if(self.delegate && [self.delegate respondsToSelector:@selector(licenceScanComplete:)]) {
                            [self.delegate licenceScanComplete:[scannedCodeArray objectAtIndex:1]];
                        }
                    }
                    else {
                        if (codeString.length == 12) {
                            if(self.delegate && [self.delegate respondsToSelector:@selector(serialScanComplete:)]) {
                                [self.delegate serialScanComplete:codeString];
                            }
                        }
                        if (codeString.length == 24) {
                            if(self.delegate && [self.delegate respondsToSelector:@selector(licenceScanComplete:)]) {
                                [self.delegate licenceScanComplete:codeString];
                            }
                        }
                    }
                    
//                    if ([codeString containsString:@","]) {
//                        NSArray *scannedCodeArray = [codeString componentsSeparatedByString:@","];
//                        [self.delegate serialScanComplete:[scannedCodeArray objectAtIndex:0]];
//                        [self.delegate licenceScanComplete:[scannedCodeArray objectAtIndex:1]];
//                    }
                    [self.navigationController popViewControllerAnimated:YES];

                }
            }
        }
    }
}

- (BOOL)isValidCodeString:(NSString *)codeString {
    NSLog(@"%@",codeString);
    if (codeString) {
        [self.view makeToast:codeString];
        return YES;
    }
    BOOL stringIsValid = ([codeString rangeOfString:@"Valid"].location != NSNotFound);
    return stringIsValid;
}

- (UIView *)overlayForCodeString:(NSString *)codeString bounds:(CGRect)bounds valid:(BOOL)valid {
    UIColor *viewColor = valid ? [UIColor greenColor] : [UIColor redColor];
    UIView *view = [[UIView alloc] initWithFrame:bounds];
    UILabel *label = [[UILabel alloc] initWithFrame:view.bounds];
    
    // Configure the view
    view.layer.borderWidth = 5.0;
    view.backgroundColor = [viewColor colorWithAlphaComponent:0.75];
    view.layer.borderColor = viewColor.CGColor;
    
    // Configure the label
    label.font = [UIFont boldSystemFontOfSize:12];
    label.text = codeString;
    label.textColor = [UIColor blackColor];
    label.textAlignment = NSTextAlignmentCenter;
    label.numberOfLines = 0;
    
    // Add constraints to label to improve text size?
    
    // Add the label to the view
    [view addSubview:label];
    
    return view;
}

- (void)stopScanning {
    [self.scanner stopScanning];
    [self.toggleScanningButton setTitle:@"Start Scanning" forState:UIControlStateNormal];
    self.toggleScanningButton.backgroundColor = [UIColor clearColor];
    for (NSString *code in self.overlayViews.allKeys) {
        [self.overlayViews[code] removeFromSuperview];
    }
}

#pragma mark - Actions

- (IBAction)toggleScanningTapped:(id)sender {
    if ([self.scanner isScanning]) {
        [self stopScanning];
    } else {
        [MTBBarcodeScanner requestCameraPermissionWithSuccess:^(BOOL success) {
            if (success) {
                [self startScanning];
            } else {
                [self displayPermissionMissingAlert];
            }
        }];
    }
}


#pragma mark - Helper Methods
- (void)displayPermissionMissingAlert {
    NSString *message = nil;
    if ([MTBBarcodeScanner scanningIsProhibited]) {
        message = @"This app does not have permission to use the camera.";
    } else if (![MTBBarcodeScanner cameraIsPresent]) {
        message = @"This device does not have a camera.";
    } else {
        message = @"An unknown error occurred.";
    }
    [[[UIAlertView alloc] initWithTitle:@"Scanning Unavailable"
                                message:message
                               delegate:nil
                      cancelButtonTitle:@"Ok"
                      otherButtonTitles:nil] show];
}

//- (IBAction)switchCameraTapped:(id)sender {
//    [self.scanner flipCamera];
//}
//
//- (void)backTapped {
//    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
//}
//
//#pragma mark - Notifications
//
//- (void)deviceOrientationDidChange:(NSNotification *)notification {
//    self.scanner.scanRect = self.viewOfInterest.frame;
//}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
