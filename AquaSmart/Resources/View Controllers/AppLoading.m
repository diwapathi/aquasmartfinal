//
//  AppLoading.m
//  Aquasmart
//
//  Created by Neotech on 07/06/16.
//  Copyright © 2016 Neotech. All rights reserved.
//

#import "AppLoading.h"
#import "WMSSettingsController.h"
#import "WMSBlackBoxCommands.h"
#import "OrderedDictionary.h"
#import "UIView+Toast.h"
#include <CFNetwork/CFNetwork.h>


#if DEBUG
//static const DDLogLevel ddLogLevel = DDLogLevelVerbose;
#else
//static const DDLogLevel ddLogLevel = DDLogLevelWarning;
#endif

@implementation AppLoading

//@synthesize delegate;
+(instancetype)sharedInstance {
    
    static AppLoading *appLoading;
    static dispatch_once_t once;
    dispatch_once(&once, ^{
        appLoading = [[self alloc] init];
    });
    
    return appLoading;
}

-(void)startAppLoadingActivity {
    [self checkForRegistrationType];
}


#pragma mark - time sync packet 


-(void)syncDateandTimeResponse:(NSDictionary *)jsonDict
{
    [SVProgressHUD dismiss];
    NSLog(@"Response recieved:");
    if (jsonDict) {
        [SVProgressHUD dismiss];
        if ([[jsonDict valueForKey:_DATA]isEqualToString:@"0"]) {
            
            [self checkForRegistrationType];
        }
        
        else
        {
            
            
        }
    
    }
}

//#pragma mark - Check for Registration Type -
//-(void)checkForRegistrationType {
//    //creating  authentication packet to send to server
//    OrderedDictionary *orderDictionary = [[OrderedDictionary alloc] initWithCapacity:5];
//    [orderDictionary insertObject:_SIDVALUE forKey:_SID atIndex:0];
//    [orderDictionary insertObject:_SADVALUE forKey:_SAD atIndex:1];
//    [orderDictionary insertObject:_8208 forKey:_CMD atIndex:2];
//    [orderDictionary insertObject:[[WMSSettingsController sharedInstance] getUUID] forKey:_DATA atIndex:3];
//    NSError *writeError;
//    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:orderDictionary options:kNilOptions error:&writeError];
//    NSString *jsonStr = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
//    [SVProgressHUD showWithStatus:@"Loading..."];
//    timer = [NSTimer scheduledTimerWithTimeInterval: 5.0 target:self selector:@selector(showAlertView:) userInfo:nil repeats: NO];
//   //[Communicator sharedInstance].delegate = self;
//   // [[Communicator sharedInstance]writeOut:jsonStr];
//    [TcpPacketClass sharedInstance].delegate = self;
//   [[TcpPacketClass sharedInstance]writeOut:jsonStr second:[orderDictionary objectForKey:_CMD]];
//    
//    
//}
//-(void)showAlertView:(NSTimer *)myTimer
//
//{
//    [self invalidateTimer];
//    UIAlertController * alert=   [UIAlertController
//                                  alertControllerWithTitle:@"Error"
//                                  message:@"Sorry!....You are not connected to network "
//                                  preferredStyle:UIAlertControllerStyleAlert];
//    
//    UIAlertAction* ok = [UIAlertAction
//                         actionWithTitle:@"EXIT"
//                         style:UIAlertActionStyleDefault
//                         handler:^(UIAlertAction * action)
//                         {
//                             [alert dismissViewControllerAnimated:YES completion:nil];
//                             
//                             
//                         }];
//    
//    UIAlertAction* reTry = [UIAlertAction
//                            actionWithTitle:@"RETRY"
//                            style:UIAlertActionStyleDefault
//                            handler:^(UIAlertAction * action)
//                            {
//                                [self checkForRegistrationType];
//                                
//                                [alert dismissViewControllerAnimated:YES completion:nil];
//                            }];
//    [alert addAction:ok];
//    [alert addAction:reTry];
//    [self presentViewController:alert animated:YES completion:nil];
//    //  [[AppDelegate appDelegate].superVC presentViewController:alert animated:YES completion:nil];
//}
//
////- (void)updateCounter:(NSTimer *)theTimer {
////    [[SplashScreenVC sharedInstance]showAlertView:timer];
////    
////    [SVProgressHUD dismiss];
////    
////}
//-(void)invalidateTimer{
//    
//    [timer invalidate];
//    timer = nil;
//    
//}
//
//-(void)didRecieveAuthenticationResponse:(NSDictionary *)jsonDict {
//    [[SplashScreenVC sharedInstance]invalidateTimer];
//    [SVProgressHUD dismiss];
//    UIStoryboard *mainStroyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//    if(jsonDict != nil) {
//       
//        if([[jsonDict valueForKey:_DATA] isKindOfClass:[NSDictionary class]]) {
//         //   UIViewController *registrationViewController;
//
//            [[WMSSettingsController sharedInstance]isFirstTimeRegistration:NO];
//            [[AppDelegate appDelegate]pushSidePanelController];
//            
//            
//
//        }
//       else if([[jsonDict valueForKey:@"data"] isKindOfClass:[NSString class]])
//        {
//          // BuyerRegistrationVCOne  *prev = [[BuyerRegistrationVCOne alloc]init];
//            if ([[jsonDict valueForKey:@"data"] isEqualToString:@"1"]) {
//               
//            UIViewController *registrationViewController;
//            registrationViewController = [mainStroyBoard instantiateViewControllerWithIdentifier:@"BuyerRegistrationVCOne"];
//
//            [[WMSSettingsController sharedInstance]isFirstTimeRegistration:NO];
//            [[AppDelegate appDelegate].superVC pushViewController:registrationViewController];
//            
//    
//            }
//            else if ([[jsonDict valueForKey:@"data"] isEqualToString:@"2"])
//            {
//               
//               UIViewController *registrationViewController;
//              registrationViewController = [mainStroyBoard instantiateViewControllerWithIdentifier:@"BuyerRegistrationVCOne"];
//                [[WMSSettingsController sharedInstance]isFirstTimeRegistration:YES];
//                // [[AppDelegate appDelegate].navigationController pushViewController:registrationViewController animated:NO];
//                [[AppDelegate appDelegate].superVC pushViewController:registrationViewController];
//                
//            }
//            
//            else if ([[jsonDict valueForKey:@"data"] isEqualToString:@"3"])
//            {
//                //NSString *previousScreenValue = @"3";
//              //  BuyerRegistrationVCOne *prev = [[BuyerRegistrationVCOne alloc]init];
//                              // prev = [mainStroyBoard instantiateViewControllerWithIdentifier:@"BuyerRegistrationVCOne"];
//               UIViewController *registrationViewController;
//               registrationViewController = [mainStroyBoard instantiateViewControllerWithIdentifier:@"BuyerRegistrationVCOne"];
//
//                [[WMSSettingsController sharedInstance]isFirstTimeRegistration:NO];
//                // [[AppDelegate appDelegate].navigationController pushViewController:registrationViewController animated:NO];
//                [[AppDelegate appDelegate].superVC pushViewController:registrationViewController];
//                
//            }
//            
//            else
//            {
//              
//              
//
//            }
//
//        }
//            }
//    
//}
//

-(void)didRecieveError:(NSError *)error {
    [SVProgressHUD dismiss];
     [[AppDelegate appDelegate].window makeToast:NSLocalizedString(@"Socket not connected.Please try again..", nil)];
//    if (error.code == 61) {
//        [[AppDelegate appDelegate].window makeToast:NSLocalizedString(@"Connection Refused", nil)];
//    }
//    if (error.code == 57) {
//        [[AppDelegate appDelegate].window makeToast:NSLocalizedString(@"Socket not connected", nil)];
//    }
   // DDLogVerbose(@"Error occured ");
}



@end

















