//
//  RegisteredUserVC.h
//  AquaSmart
//
//  Created by Neotech on 28/04/17.
//  Copyright © 2017 Mac User. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OrderedDictionary.h"
#import "WMSBlackBoxCommands.h"
#import "TcpPacketClass.h"
#import "SVProgressHUD.h"
#import "UIView+Toast.h"
#import "AppDelegate.h"
#import "TouchUtil.h"
#import "AppLoading.h"
#import "ViewRegistrationTableViewCell.h"
#import "DatabaseModel.h"
#import "WMSSettingsController.h"
@interface RegisteredUserVC : UIViewController
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic)NSMutableArray *registeredUserArray;
@property(nonatomic)BOOL isResponseRecieved;
@property (nonatomic,strong)NSString *jsonString;
@property (nonatomic, strong) OrderedDictionary *requestDict;
@property (nonatomic,strong) NSString *userName;
@property (nonatomic,strong) NSString *phoneNumber;
-(void)viewRegistrationCallApi;
-(void)checkForRegistrationType;
@end
