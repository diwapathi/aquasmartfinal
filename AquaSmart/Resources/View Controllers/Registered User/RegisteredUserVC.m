//
//  RegisteredUserVC.m
//  AquaSmart
//
//  Created by Neotech on 28/04/17.
//  Copyright © 2017 Mac User. All rights reserved.
//

#import "RegisteredUserVC.h"

@interface RegisteredUserVC ()<PacketReciveProtocol,UITableViewDelegate,UITableViewDataSource>
{
    NSTimer *timer;
    NSString *registeredUserName;
    int selectedindex;
    DatabaseModel *datamodel;
    NSString *alertText;
}
@end

@implementation RegisteredUserVC

- (void)viewDidLoad {
    [super viewDidLoad];
    datamodel=[[DatabaseModel alloc]init];
    [super viewDidLoad];
    _registeredUserArray = [[NSMutableArray alloc]init];

    // Do any additional setup after loading the view.
}

#pragma mark - Initial data load -
-(void)setTopHeaderView {
    NSDictionary *navDict = @{
                              TITLE_TEXT :NSLocalizedString(@"Settings", nil),
                              BACK_IMAGE :@"BackImage",
                              };
    [[AppDelegate appDelegate].superVC setHeaderViewData:navDict];
}

-(void)backClicked {
    UIStoryboard *mainStroyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    MVYSideMenuController *sideMenuController = [self sideMenuController];
    MainViewController *homeVC = [mainStroyBoard instantiateViewControllerWithIdentifier:@"MainViewController"];
    [sideMenuController changeContentViewController:homeVC closeMenu:YES];
}

-(void)viewWillAppear:(BOOL)animated
{
    
    [self viewRegistrationCallApi];
    NetworkStatus status = [self checkForWifiConnection];
    if(status == NotReachable || status == ReachableViaWWAN) {
        [self.navigationController.view makeToast:NSLocalizedString(@"Please check your wifi connection", nil)];
    }
    [[AppDelegate appDelegate].superVC updateHeadrLabel:NSLocalizedString(@"View Registration", nil)];
}
-(NetworkStatus)checkForWifiConnection {
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    [reachability startNotifier];
    NetworkStatus status = [reachability currentReachabilityStatus];
    return status;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)backButton:(id)sender {
    
    
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_registeredUserArray count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"viewCell";
    ViewRegistrationTableViewCell *cell = (ViewRegistrationTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[ViewRegistrationTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                                    reuseIdentifier:CellIdentifier];
        
    }
    if (_registeredUserArray.count<=3) {
        NSString *titleStr = [self.registeredUserArray objectAtIndex:indexPath.row];
        [cell setData:titleStr];
    }
    
    if (_jsonString) {
        [cell.removeButtonClicked setTitle:NSLocalizedString(@"REPLACE", nil) forState:UIControlStateNormal];
    }
    [cell.removeButtonClicked addTarget:self action:@selector(removeBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
    return cell;}



// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        // [serialNumberArray removeObjectAtIndex:indexPath.row];
        [self.tableView deleteRowsAtIndexPaths:[NSMutableArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }
}



// Method to change the name to 30 char string
-(NSString *)getName:(NSString *)name {
    NSMutableString *finalUserName = [name mutableCopy] ;
    
    if (name.length == 30) {
        finalUserName = [name mutableCopy];
    }
    else {
        int count = (int)name.length;
        for (int i = count; i < 30; i++) {
            
            [finalUserName appendString:@" "];
        }
    }
    return finalUserName;
}


#pragma mark - Remove registered devices -
-(void)removeBtnClicked:(id)sender {
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:buttonPosition];
    selectedindex = (int)indexPath.row;
    ViewRegistrationTableViewCell *cell = (ViewRegistrationTableViewCell *)[self.tableView cellForRowAtIndexPath:indexPath];
    registeredUserName = cell.registeredUserLabel.text;
    NSString *alertText;
    if (_jsonString) {
        alertText = NSLocalizedString(@"Do you really want to replace the client?", nil);
    }
    else {
        alertText = NSLocalizedString(@"Do you really want to remove the client?", nil);
    }
    UIAlertView *alertView = [[UIAlertView alloc]
                              initWithTitle:nil
                              message:alertText
                              delegate:self
                              cancelButtonTitle:NSLocalizedString(@"No", nil)
                              otherButtonTitles:NSLocalizedString(@"Yes", nil), nil];
    
    [alertView show];
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 0) {
        
    }
    if (buttonIndex == 1) {
        [self removeRegiterationDataCall];
        
    }
}

#pragma  mark - remove registered user api call
-(void)removeRegiterationDataCall {
    OrderedDictionary *requestDict = [[OrderedDictionary alloc]initWithCapacity:4];
    [requestDict insertObject:_SIDVALUE forKey:_SID atIndex:0];
    // [requestDict insertObject:_BADVALUE forKey:_BAD atIndex:1];
    [requestDict insertObject:_SADVALUE forKey:_SAD atIndex:1];
    [requestDict insertObject:_8211 forKey:_CMD atIndex:2];
    [requestDict insertObject:registeredUserName forKey:_DATA atIndex:3];
    NSError *writeError;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:requestDict options:kNilOptions error:&writeError];
    if (jsonData) {
        NSString *jsonStr = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        [TcpPacketClass sharedInstance].delegate = self;
         [[TcpPacketClass sharedInstance]writeOut:jsonStr second:[requestDict objectForKey:_CMD]];    }
    [SVProgressHUD showWithStatus:@"Please Wait.."];
     timer = [NSTimer scheduledTimerWithTimeInterval: 5.0 target:self selector:@selector(updateCounter:) userInfo:nil repeats: NO];
}

- (void)updateCounter:(NSTimer *)theTimer {
    [theTimer invalidate];
    [SVProgressHUD dismiss];
    [self.tableView reloadData];
    //[self removeRegiterationDataCall];
    [self.view makeToast:NSLocalizedString(@"Something went wrong.Try Again!!!", nil)];
     [self.navigationController popViewControllerAnimated:YES];

}


-(void)didRecieveRemoveRegistrationKeyResponse:(NSDictionary *)jsonDict
{
    [timer invalidate];
    [SVProgressHUD dismiss];
    if ([[jsonDict valueForKey:_DATA] isEqualToString:@"0"])
    {
        
        if (_jsonString) {
            [SVProgressHUD show];
            [NSTimer scheduledTimerWithTimeInterval: 5.0 target:self selector:@selector(updateCounter:) userInfo:nil repeats: NO];
            // BOOL status = [[WMSSettingsController sharedInstance]getDeviceAuthenticationStatus];
            OrderedDictionary *dataDict = [[OrderedDictionary alloc]initWithCapacity:4];
            NSString *status = [[WMSSettingsController sharedInstance]getDeviceAuthenticationStatus];
            if ([status isEqualToString:@"2"] || [status isEqualToString:@"3"]) {
                [dataDict insertObject:@"2" forKey:_01 atIndex:0];
            }else{
                [dataDict insertObject:@"1" forKey:_01 atIndex:0];
            }
            
            [dataDict insertObject:[[WMSSettingsController sharedInstance]getUUID]forKey:_02 atIndex:1];
            [dataDict insertObject:self.userName forKey:_03 atIndex:2];
            [dataDict insertObject:self.phoneNumber forKey:_04 atIndex:3];
            
            OrderedDictionary *requestDict = [[OrderedDictionary alloc]initWithCapacity:5];
            [requestDict insertObject:_SIDVALUE forKey:_SID atIndex:0];
            [requestDict insertObject:_SADVALUE forKey:_SAD atIndex:1];
            [requestDict insertObject:_8200 forKey:_CMD atIndex:2];
            [requestDict insertObject:dataDict forKey:_DATA atIndex:3];
            NSError *writeError;
            NSData *jsonData = [NSJSONSerialization dataWithJSONObject:requestDict options:kNilOptions error:&writeError];
            NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
            [SVProgressHUD showWithStatus:@"Please Wait.."];
            
            [TcpPacketClass sharedInstance].delegate = self;
            
            
            [[TcpPacketClass sharedInstance]writeOut:jsonString second:[requestDict objectForKey:_CMD]];
//            timer = [NSTimer scheduledTimerWithTimeInterval: 5.0 target:self selector:@selector(updateCounter:) userInfo:nil repeats: NO];
        
        }
        else {
            [self.view makeToast:NSLocalizedString(@"User removed successfully.", nil)];
            NSString *name = [self getName:[[WMSSettingsController sharedInstance] getUserName]];
            if ([registeredUserName isEqualToString:name]) {
                //[self removeAllDataFromApp];
                //[[WMSSettingsController sharedInstance]setProvisionfileparseStatus:NO];
                [[WMSSettingsController sharedInstance]isFirstTimeRegistration:NO];
                [[TouchUtil getInstance].timer invalidate];
                [TouchUtil getInstance].isTimerStart = NO;
                [self temp];
                [[SplashScreenVC sharedInstance]getFirmwareVersionCallApi];
                //            UIStoryboard *mainStroyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                //            HSVCDeviceAuthentication *deviceAuthViewController = [mainStroyBoard instantiateViewControllerWithIdentifier:@"HSVCDeviceAuthentication"];
                //            [[AppDelegate appDelegate].superViewController pushViewController:deviceAuthViewController];
            }
            else {
                [self.registeredUserArray removeObjectAtIndex:selectedindex];
                [self.tableView reloadData];
            }
        }
        
        
//        [self.view makeToast:NSLocalizedString(@"User deleted Successfully.", nil)];
//        
//        if ([[WMSSettingsController sharedInstance]isFromDeviceRegistration]==YES)
//        
//        {
//            [self.registeredUserArray removeObjectAtIndex:selectedindex];
//            
//            [self.tableView reloadData];
//            [[WMSSettingsController sharedInstance]FromDeviceRegistration:NO];
//            //[self.navigationController popViewControllerAnimated:YES];
//            
//        }
//        
//        else
//        {
//            [self.registeredUserArray removeObjectAtIndex:selectedindex];
//            [self.tableView reloadData];
//            [self checkForRegistrationType];
//            
//        }
      
    }
    if ([[jsonDict valueForKey:_DATA] isEqualToString:@"1"]) {
        [self.view makeToast:NSLocalizedString(@"Failed to delete.", nil)];
    }
}



-(void)sendRegistrationPackAgain:(NSTimer *)thetimer
{
    
    [thetimer invalidate];
    [SVProgressHUD dismiss];
    // [self.view makeToast:NSLocalizedString(@"Something went wrong.Try Again!!", nil)];
    [self checkForRegistrationType];
    
}

#pragma mark - get mac id res
-(void)didRecieveLoginorRegistrationResponse:(NSDictionary *)jsonDict {
    [SVProgressHUD dismiss];
    if ([[jsonDict valueForKey:_DATA] isEqualToString:@"0"]) {
        [[WMSSettingsController sharedInstance] setUserName:self.userName];
        [[WMSSettingsController sharedInstance] commit];
        [[AppDelegate appDelegate]pushSidePanelController];
    }
    else if ([[jsonDict valueForKey:_DATA] isEqualToString:@"1"]) {
        [self.view makeToast:NSLocalizedString(@"No more client can be registered.Please remove any registered clients.", nil)];
        return;
    }
    else if ([[jsonDict valueForKey:_DATA] isEqualToString:@"2"]) {
        [self.view makeToast:NSLocalizedString(@"User name already registered !", nil)];
        return;
    }
    
    else {
        [self.view makeToast:NSLocalizedString(@"Client registration failed", nil)];
    }
}

-(void)temp{
    NSArray *navigation = [AppDelegate appDelegate].superVC.childViewControllers;
    
    UINavigationController *navigationcontroller = [navigation objectAtIndex:0];
    NSArray *viewControllers = navigationcontroller.viewControllers;
    for (UIViewController *anVC in viewControllers) {
        if ([anVC isKindOfClass:[MVYSideMenuController class]]) {
            [[AppDelegate appDelegate].superVC.backButton removeTarget:anVC action:NULL forControlEvents:UIControlEventAllEvents];
        }
    }
}


#pragma mark - View registration Api call
-(void)viewRegistrationCallApi

{
    
    OrderedDictionary *requestDict = [[OrderedDictionary alloc]initWithCapacity:3];
    [requestDict insertObject:_SIDVALUE forKey:_SID atIndex:0];
    [requestDict insertObject:_SADVALUE forKey:_SAD atIndex:1];
    [requestDict insertObject:_8210 forKey:_CMD atIndex:2];
    
    NSError *writeError;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:requestDict options:kNilOptions error:&writeError];
    //  NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    if (jsonData) {
        NSString *jsonStr = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
//        [NSTimer scheduledTimerWithTimeInterval: 5.0 target:self selector:@selector(updateCounter:) userInfo:nil repeats: NO];
        
        [SVProgressHUD showWithStatus:@"Fetching User List.."];
        [TcpPacketClass sharedInstance].delegate = self;
          [[TcpPacketClass sharedInstance]writeOut:jsonStr second:[requestDict objectForKey:_CMD]];
        
        
        
        timer = [NSTimer scheduledTimerWithTimeInterval:3.0 target:self selector:@selector(sendViewRegistrationPackAgain:) userInfo:nil repeats: NO];
            }
}


-(void)sendViewRegistrationPackAgain:(NSTimer *)thetimer
{
    int static ticks = 0;
    if (ticks<4) {
        [self viewRegistrationCallApi];
    }
    else if (ticks>3)
    {
        [thetimer invalidate];
        [SVProgressHUD dismiss];
        [self.view makeToast:NSLocalizedString(@"Something went wrong Please try again.", nil)];
        ticks = -1;
    }
    ticks++;
   

}
#pragma mark - registered user response
-(void)didRegisteredUserResponse:(NSDictionary *)jsonDict
{
    [timer invalidate];
    //_isResponseRecieved = YES;
    [SVProgressHUD dismiss];
    if (![[jsonDict valueForKey:_DATA] isKindOfClass:[NSString class]]) {
        NSDictionary *dataDict = [jsonDict valueForKey:_DATA];
        for (int i = 1; i <= dataDict.count; i++) {
            if (i%2 == 0) {
                [self.registeredUserArray addObject:[dataDict valueForKey:[NSString stringWithFormat:@"%02d",i]]];
            }
        }
        if (self.registeredUserArray.count <=3) {
            [self.tableView reloadData];
        }
        else {
            //[self.view makeToast:NSLocalizedString(@"Device limit reached.", nil)];
        }
    }
    else {
        [self.view makeToast:NSLocalizedString(@"No Registered devices.", nil)];
    }
}


-(void)didRecieveError:(NSError *)error {
    [SVProgressHUD dismiss];
    [self.view makeToast:NSLocalizedString(@"Error Occured.", nil)];
    // [[Communicator sharedInstance] setup];
}






- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    // Fetch the devices from persistent data store
    
    
    // [self.tableView reloadData];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
