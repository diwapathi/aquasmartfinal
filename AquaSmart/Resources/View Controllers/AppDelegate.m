//
//  AppDelegate.m
//  AquaSmart
//
//  Created by Mac User on 09/01/17.
//  Copyright © 2017 Mac User. All rights reserved.
//

#import "AppDelegate.h"
#import <SystemConfiguration/CaptiveNetwork.h>
@interface AppDelegate ()<PacketReciveProtocol>
@end

@implementation AppDelegate

- (NSString *)applicationDocumentsDirectory {
    return [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
}


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    

    
    //set logout
    if(![[WMSSettingsController sharedInstance] isAppAlreadyLanuched]) {
        //creation of mac id
        [AppDelegate uuidCreation];
        [[WMSSettingsController sharedInstance] setLogoutTime:(NSInteger)300];
        [[WMSSettingsController sharedInstance] AppAlreadyLanuched:YES];
        [[WMSSettingsController sharedInstance] commit];
    }
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSLog(@"path of database file%@",[paths objectAtIndex:0]);
    //check for internet connection
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityChanged:) name:kReachabilityChangedNotification object:nil];

    
    
    
    //Database creation
    [WMSStorageManager sharedInstance];
    

    
    //[[NSNotificationCenter defaultCenter] addObserver:self
     //                                        selector:@selector(onNetWorkChange:)
     //                                            name:kReachabilityChangedNotification object:nil];
    _reach = [Reachability reachabilityForInternetConnection];
    [_reach startNotifier];
    
    //[self checkNetworkStatus:nil];
    if(![[WMSSettingsController sharedInstance] isAppAlreadyLanuched]) {
        //creation of mac id
        [AppDelegate uuidCreation];
        [[WMSSettingsController sharedInstance] setLogoutTime:(NSInteger)900];
        [[WMSSettingsController sharedInstance] AppAlreadyLanuched:YES];
        [[WMSSettingsController sharedInstance] commit];
    }
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationDidTimeout:) name:@"TIMEOUT" object:nil];
    NSLog(@"%@",self.persistentStoreCoordinator);
    //[self uuidCreation];
        return YES;
}
- (void) reachabilityChanged:(NSNotification *)note
{
    Reachability* curReach = [note object];
    
    NSParameterAssert([curReach isKindOfClass:[Reachability class]]);
    self.netStatus = [curReach currentReachabilityStatus];
    
    NSMutableDictionary *parameter = [NSMutableDictionary dictionaryWithCapacity:0];
    [parameter setObject:[NSNumber numberWithInt:self.netStatus] forKey:@"status"];
    [[NSNotificationCenter defaultCenter] postNotificationName:NET_WORK_CHANGE
                                                        object:self
                                                      userInfo:parameter];
    
}





//- (void)checkNetworkStatus:(NSNotification *)notice
//{
//    
//    
//    _netStatus = [_reach currentReachabilityStatus];
//    if (_netStatus == NotReachable)
//    {
//        NSLog(@"The internet is down.");
//        
//        // do stuff when network gone.
//    }
//    else if(_netStatus == ReachableViaWiFi&&_isWifiConnected)
//    {
//        NSLog(@"The internet is working!");
//        
//        // do stuff when internet comes active
//       // [[TcpPacketClass sharedInstance]start];
//        [[NSNotificationCenter defaultCenter] postNotificationName:@"INTERNET_AVAILABLE" object:nil];
//            }
//}

//Creation of automatic uuid

+(void)uuidCreation {
    NSString *uuid = [[[[UIDevice currentDevice] identifierForVendor] UUIDString] saferStringValue];
    uuid = [uuid stringByReplacingOccurrencesOfString:@"-" withString:@""];
    NSString *macIdFromUUID = [uuid substringToIndex:16];
    NSString *validMacWithoutHypen = [macIdFromUUID stringByReplacingOccurrencesOfString:@"-" withString:@"A"];
    [[WMSSettingsController sharedInstance] setUUID:validMacWithoutHypen];
    [[WMSSettingsController sharedInstance] commit];
}


+ (BOOL)isActiveInternet
{
   // netStatus = [reach currentReachabilityStatus];
   // if (netStatus == NotReachable)
    {
        NSLog(@"The internet is down.");
        // do stuff when network gone.
        return FALSE;
    }
//    else
//    {
//        NSLog(@"The internet is working!");
//        // do stuff when internet comes active
//        [[NSNotificationCenter defaultCenter] postNotificationName:@"INTERNET_AVAILABLE" object:nil];
//        return TRUE;
//    }
}

+(instancetype)appDelegate {
    return (AppDelegate *)[[UIApplication sharedApplication] delegate];
}
#pragma mark - Load Root View  and finding ip address -

-(void)setRootController {
    
    // NetworkStatus status = [self checkForWifiConnection];
   // for scanning wifi and ssid
    CFArrayRef myArray = CNCopySupportedInterfaces();
    CFDictionaryRef myDict = CNCopyCurrentNetworkInfo(CFArrayGetValueAtIndex(myArray, 0));
    NSLog(@"Connected at:%@",myDict);
    NSDictionary *myDictionary = (__bridge_transfer NSDictionary*)myDict;
    NSString * BSSID = [myDictionary objectForKey:@"BSSID"];
    NSString *ssid = [myDictionary objectForKey:@"SSID"];
    NSLog(@"bssid is %@",BSSID);
    NSLog(@"ssid is %@",ssid);
    
    
    if ([[WMSSettingsController sharedInstance]getWifiSsId]==nil)
    {
        [[SplashScreenVC sharedInstance]showAlertViewForWifi];
    }
    
    else if ([ssid containsString:@"AquaSmart-"])
    {
    
        if ([ssid isEqualToString:[[WMSSettingsController sharedInstance]getWifiSsId]])
        {
            _isWifiConnected = YES;
            bool vIn = _isWifiConnected;
            _iswificonnectedint = vIn?1:0;
             //[SVProgressHUD showWithStatus:@"Please WaitStarttest............"];
            
            NSMutableDictionary *userInfoDict = [[NSMutableDictionary alloc]init];
            [userInfoDict setObject:[NSNumber numberWithBool:_isWifiConnected] forKey:@"wifi"];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"WIFI_AVAILABLE" object:nil userInfo:userInfoDict];
            NSLog(@"wifi available");
            if ([[WMSSettingsController sharedInstance]getMacId]==nil) {
               
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.5 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                    [SVProgressHUD showWithStatus:@"Please Wait"];
                    [[SplashScreenVC sharedInstance]getAquaSmartMacID];
                });
                
                
               
            }
            
            else
            {
                [[SplashScreenVC sharedInstance]getFirmwareVersionCallApi];
                
            }

        }
    
    }
    
    else
    {
        if ([ssid isEqualToString:[[WMSSettingsController sharedInstance]getStationModeSsId]])
        {
        self.presenter = [[MainPresenter alloc]initWithDelegate:self];
        
            
            dispatch_async(dispatch_get_main_queue(), ^{
               [SVProgressHUD showWithStatus:@"Please WaitTwo.."];
                
            });
            
        //IP lookup
        [self.presenter scanButtonClicked];
        
        }
        
    }
    
    
    
    
    /*
    if (![ssid isEqualToString:[[WMSSettingsController sharedInstance]getWifiSsId]]) {
        [[WMSSettingsController sharedInstance]setWifiSsId:nil];
        
//        NSString *appDomain = [[NSBundle mainBundle] bundleIdentifier];
//        [[NSUserDefaults standardUserDefaults] removePersistentDomainForName:appDomain];
    }
//    if (![[[WMSSettingsController sharedInstance]getWifiSsId]isEqualToString:ssid]) {
//        
//        //[[SplashScreenVC sharedInstance]showAlertViewForWifi];
//    }
    if ([[WMSSettingsController sharedInstance]getWifiSsId]==nil) {
        
        [[SplashScreenVC sharedInstance]showAlertViewForWifi];
        
        
    }

   // NSString *wifiName =  [[WMSSettingsController sharedInstance]getWifiSsId];
    if ([ssid isEqualToString:[[WMSSettingsController sharedInstance]getWifiSsId]] && [[[WMSSettingsController sharedInstance]getWifiSsId]containsString:@"AquaSmart-"]) {
        
     
       _isWifiConnected = YES;
    bool vIn = _isWifiConnected;
         _iswificonnectedint = vIn?1:0;


        NSMutableDictionary *userInfoDict = [[NSMutableDictionary alloc]init];
        [userInfoDict setObject:[NSNumber numberWithBool:_isWifiConnected] forKey:@"wifi"];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"WIFI_AVAILABLE" object:nil userInfo:userInfoDict];
        NSLog(@"wifi available");
        if ([[WMSSettingsController sharedInstance]getMacId]==nil) {
            [[SplashScreenVC sharedInstance]getAquaSmartMacID];
        }
        
        else
        {
        [[SplashScreenVC sharedInstance]getFirmwareVersionCallApi];
            
        }
    }
    
    
    else if([ssid isEqualToString:[[WMSSettingsController sharedInstance]getWifiSsId]] &&  ![[[WMSSettingsController sharedInstance]getWifiSsId]containsString:@"AquaSmart-"])
    {
        
         self.presenter = [[MainPresenter alloc]initWithDelegate:self];

        //[SVProgressHUD setOffsetFromCenter:UIOffsetMake(0, 300)];
        [SVProgressHUD showWithStatus:@"Please Wait.."];
     //IP lookup
        [self.presenter scanButtonClicked];

        
        
    
    }
    
//    else if([ssid isEqualToString:[[WMSSettingsController sharedInstance]getWifiSsId]] || [[WMSSettingsController sharedInstance]getAPModeSsId])
//    {
//        [[SplashScreenVC sharedInstance]getFirmwareVersionCallApi];
//    }
    else
    {
            [self.superVC.view makeToast:NSLocalizedString(@"Please connect to AquaSmart or your router.", nil)];
        //[self setRootController];
        _isWifiConnected = NO;
        
    }

*/

}



-(NetworkStatus)checkForWifiConnection {
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    [reachability startNotifier];
    NetworkStatus status = [reachability currentReachabilityStatus];
    return status;
}


#pragma mark - Presenter Delegates
//The delegates methods from Presenters.These methods help the MainPresenter to notify the MainVC for any kind of changes
-(void)mainPresenterIPSearchFinished:(NSString *)ipAddress {
    
    
    [[WMSSettingsController sharedInstance]setIPAddress:ipAddress];
    if (ipAddress==nil)
    {
        
        [[SplashScreenVC sharedInstance]showAlertForIpFail];
       
        
//        [[[UIAlertView alloc] initWithTitle:@"Failed to scan" message:[NSString stringWithFormat:@"Router not in range or wrong network name provided or may be data is cleared.If wrong credentials entered please reset your device by holding the Pump-Switch for 10 seconds."] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
//        
    }
    //setting wifi icon status
    _isWifiConnected = YES;
    bool vIn = _isWifiConnected;
    _iswificonnectedint = vIn?1:0;
    
    
    NSMutableDictionary *userInfoDict = [[NSMutableDictionary alloc]init];
    [userInfoDict setObject:[NSNumber numberWithBool:_isWifiConnected] forKey:@"wifi"];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"WIFI_AVAILABLE" object:nil userInfo:userInfoDict];
    NSLog(@"wifi available");
    [[SplashScreenVC sharedInstance]setWifiStatusIcon];
    [[SplashScreenVC sharedInstance]getFirmwareVersionCallApi];
    
    //[[[UIAlertView alloc] initWithTitle:@"Scan Finished" message:[NSString stringWithFormat:@"Number of devices connected to the Local Area Network : %lu", (unsigned long)self.presenter.connectedDevices.count] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
    
};

-(void)mainPresenterIPSearchFailed {
    
//    [[[UIAlertView alloc] initWithTitle:@"Failed to scan" message:[NSString stringWithFormat:@"Router not in range or wrong network name provided or may be data is cleared.If wrong credentials entered please reset your device by holding the Pump-Switch for 10 seconds."] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
};


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 0) {
        [self performSelector:@selector(exitFromApp) withObject:nil afterDelay:5.0];
        
    }
    if (buttonIndex == 1) {
        
        //[self dismissViewControllerAnimated:YES completion:nil];
    }
}

-(void)exitFromApp{
    [SVProgressHUD dismiss];
    exit(0);
}



-(void)mainPresenterIPSearchCancelled {
    
    
};


#pragma mark - KVO Observers
-(void)addObserversForKVO {
    
    [self.presenter addObserver:self forKeyPath:@"connectedDevices" options:NSKeyValueObservingOptionNew context:nil];
    [self.presenter addObserver:self forKeyPath:@"progressValue" options:NSKeyValueObservingOptionNew context:nil];
    [self.presenter addObserver:self forKeyPath:@"isScanRunning" options:NSKeyValueObservingOptionNew context:nil];
}

-(void)removeObserversForKVO {
    
    [self.presenter removeObserver:self forKeyPath:@"connectedDevices"];
    [self.presenter removeObserver:self forKeyPath:@"progressValue"];
    [self.presenter removeObserver:self forKeyPath:@"isScanRunning"];
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    
     //[[NSNotificationCenter defaultCenter] postNotificationName:@"WIFI_CHANGED" object:nil userInfo:nil];
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    
   // [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityChanged:) name:kReachabilityChangedNotification object:nil];
}

-(void)applicationDidTimeout:(NSNotification *) notif
{
    [[TouchUtil getInstance].timer invalidate];
    [TouchUtil getInstance].isTimerStart = NO;
    [[NSNotificationCenter defaultCenter] postNotificationName:@"POP_TO_ROOT_VIEW" object:nil];
    
}
- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    // Saves changes in the application's managed object context before the application terminates.
    
}


//push home screen
-(void)pushSidePanelController {
    
    UIStoryboard *mainStroyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    HSVCLeftPanel *menuVC = [mainStroyBoard instantiateViewControllerWithIdentifier:@"HSVCLeftPanel"];
    MainViewController  *homeScreen = [mainStroyBoard instantiateViewControllerWithIdentifier:@"MainViewController"];
    UINavigationController *contentNavigationController = [[UINavigationController alloc] initWithRootViewController:homeScreen];
    MVYSideMenuOptions *options = [[MVYSideMenuOptions alloc] init];
    options.contentViewScale = 1.0;
    options.contentViewOpacity = 0.05;
    options.shadowOpacity = 0.4;
    MVYSideMenuController *sideMenuController = [[MVYSideMenuController alloc] initWithMenuViewController:menuVC contentViewController:contentNavigationController  options:options];
    sideMenuController.menuFrame = CGRectMake(0, self.navigationController.navigationBar.frame.size.height+20, (self.window.frame.size.width/2)+40, self.window.bounds.size.height - 64.0);
    [self.superVC pushViewController:sideMenuController];


    

}



@end
