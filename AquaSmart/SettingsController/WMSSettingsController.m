//
//  WMSSettingsController.m
//  AquaSmart
//
//  Created by Neotech on 14/12/15.
//  Copyright © 2015 Neotech. All rights reserved.
//

#import "WMSSettingsController.h"

//#import "Device.h"
//#import "DDLog.h"

NSString *const BLACK_BOX_IP = @"BLACK_BOX_IP";
NSString *const WIFI_SSID = @"WIFI_SSID";
NSString *const AP_MODE_SSID = @"AP_MODE_SSID";
NSString *const STATION_MODE_SSID = @"STATION_MODE_SSID";
NSString *const STATION_MODE_PASS = @"STATION_MODE_PASS";

NSString *const IP_ADDRESS = @"IP_ADDRESS";

NSString *const MAC_ID = @"MAC_ID";
NSString *const SID = @"SID";
NSString *const SAD = @"SID";
NSString *const CMD = @"SID";
NSString *const APP_ALREADY_LAUNCH = @"APP_ALREADY_LAUNCH";
 NSString *const FIRST_SYNC = @"FIRST_SYNC";
    NSString *const GET_TANK_SETTINGS = @"GET_TANK_SETTINGS";
 NSString *const TANK_SETTINGS_FIRST = @"TANK_SETTINGS_FIRST";
 NSString *const ADMIN_PASSWORD = @"ADMIN_PASSWORD";
NSString *const FIRST_TIME_SPLASH = @"FIRST_TIME_SPLASH";
NSString *const WATER_LAVEL_FIRST_TIME = @"WATER_LAVEL_FIRST_TIME";
NSString *const FROM_SCHEDULE_EDIT = @"FROM_SCHEDULE_EDIT";
  NSString *const FROM_DEVICE_REG = @"FROM_DEVICE_REG";
NSString *const UUID = @"UUID";
NSString *const REGISTRATION_KEY = @"REGISTRATION_KEY";
NSString *const LOGOUT_TIME = @"LOGOUT_TIME";
NSString *const ACCESS_TOKEN = @"ACCESS_TOKEN";
NSString *const USER_NAME = @"USER_NAME";
NSString *const USER_EMAIL = @"USER_EMAIL";
NSString *const USER_PHONENO = @"USER_PHONENO";
NSString *const THEME_NAME = @"THEME_NAME";
NSString *const PROVISION_STATUS = @"PROVISION_STATUS";
NSString *const DEVICE_LIST = @"DEVICE_LIST";
NSString *const GADGET_LIST = @"GADGET_LIST";
NSString *const ROOM_LIST = @"ROOM_LIST";
NSString *const PROFILE_CODE = @"PROFILE_CODE";
NSString *const ADDRESS_LIST = @"ADDRESS_LIST";
NSString *const PROFILE_RESPONSE_LIST = @"PROFILE_RESPONSE_LIST";
NSString *const LUTRON_LIST = @"LUTRON_LIST";
NSString *const QOS_CONTENT = @"QOS_CONTENT";
NSString *const CURRENT_FIRMWARE_VERSION = @"CURRENT_FIRMWARE_VERSION";
NSString *const LATEST_FIRMWARE_VERSION = @"LATEST_FIRMWARE_VERSION";
NSString *const LGHVAC_LIST = @"LGHVAC_LIST";
NSString *const PANIC_SWITCH_STATUS = @"PANIC_SWITCH_STATUS";
NSString *const DEVICE_INFO = @"DEVICE_INFO";
NSString *const DI_DEVICE_LIST = @"DI_DEVICE_LIST";
NSString *const APP_PROFILE_LIST = @"APP_PROFILE_LIST";
NSString *const PROFILE_INFO_LIST = @"PROFILE_INFO_LIST";
NSString *const IMMEDIATE_LIST = @"IMMEDIATE_LIST";
NSString *const SCHEDULE_LIST = @"SCHEDULE_LIST";
NSString *const APP_USER_NAME = @"APP_USER_NAME";
NSString *const USER_ADDRESS = @"USER_ADDRESS";
NSString *const USER_IMAGE = @"USER_IMAGE";
NSString *const LICENSE_NUMBER = @"LICENSE_NUMBER";
NSString *const SERIAL_NUMBER = @"SERIAL_NUMBER";
NSString *const WIFI_NAME = @"WIFI_NAME";
NSString *const DEVICE_STATUS = @"DEVICE_STATUS";
NSString *const LOCATION = @"LOCATION";
NSString *const NOTIFICATION_STATUS = @"NOTIFICATION_STATUS";
@implementation WMSSettingsController
@synthesize cacheDict;
+(instancetype)sharedInstance {
    
    static WMSSettingsController *settingsController;
    static dispatch_once_t once;
    dispatch_once(&once, ^{
        settingsController = [[self alloc] init];
        settingsController.cacheDict = [[NSMutableDictionary alloc] init];
    });
    
    return settingsController;
}
//Private getter and setter
-(NSString *)getStringValueForKey:(NSString *)key {
    
    NSString *object = [[NSUserDefaults standardUserDefaults] valueForKey:key];
    
    return object;
    
}

-(void)setStringValue:(NSString *)stringValue ForKey:(NSString *)key {
    
    [[NSUserDefaults standardUserDefaults] setObject:stringValue forKey:key];
    
}


-(NSInteger)getIntegerValueForKey:(NSString *)key {
    
    NSInteger value = [[NSUserDefaults standardUserDefaults] integerForKey:key];
    return value;
    
}

-(void)setIntegerValue:(NSInteger)integerValue ForKey:(NSString *)key {
    
    [[NSUserDefaults standardUserDefaults] setInteger:integerValue forKey:key];
    
}

-(BOOL)getBOOLValueForKey:(NSString *)key {
    
    NSInteger value = [[NSUserDefaults standardUserDefaults] integerForKey:key];
    return value;
    
}

-(void)setBoolValue:(BOOL)status ForKey:(NSString *)key {
    
    [[NSUserDefaults standardUserDefaults] setBool:status forKey:key];
}

-(void)setBlackBoxIP:(NSString *)blackBoxIp {
    
    [self setStringValue:blackBoxIp ForKey:BLACK_BOX_IP];
    
}

-(NSString *)getBlackBoxIP {
    
    return [self getStringValueForKey:BLACK_BOX_IP];
    
}



-(NSString *)getMacId {
    
    return [self getStringValueForKey:MAC_ID];
    
}
-(void)setMacId:(NSString *)macId {
    
    [self setStringValue:macId ForKey:MAC_ID];
    
}

-(NSString *)getWifiSsId {
    
    return [self getStringValueForKey:WIFI_SSID];
    
}


-(void)setWifiSsId:(NSString *)wifiSsId {
    
    [self setStringValue:wifiSsId ForKey:WIFI_SSID];
    
}


//AP mode ssid

-(NSString *)getAPModeSsId {
    
    return [self getStringValueForKey:AP_MODE_SSID];
    
}


-(void)setAPModeSsId:(NSString *)ApModeSsId {
    
    [self setStringValue:ApModeSsId ForKey:AP_MODE_SSID];
    
}



//Station Mode SSId
-(NSString *)getStationModeSsId {
    
    return [self getStringValueForKey:STATION_MODE_SSID];
    
}


-(void)setStationModeSsId:(NSString *)StationModeSsId {
    
    [self setStringValue:StationModeSsId ForKey:STATION_MODE_SSID];
    
}
//Station Mode Pas


-(void)setStationModePass:(NSString *)StationModePass {
    
    [self setStringValue:StationModePass ForKey:STATION_MODE_PASS];
    
}

-(NSString *)getStationModePass {
    
    return [self getStringValueForKey:STATION_MODE_PASS];
    
}




#pragma mark - setting ip address for socket creation
-(NSString *)getIPAddress {
    
    return [self getStringValueForKey:IP_ADDRESS];
    
}


-(void)setIPAddress:(NSString *)ipAddress {
    
    [self setStringValue:ipAddress ForKey:IP_ADDRESS];
    
}


-(void)setBlackBoxSid:(NSString *)blackBoxIp {
    
    [self setStringValue:blackBoxIp ForKey:SID];
    
}

-(NSString *)getBlackBoxSid {
    
    return [self getStringValueForKey:SID];
    
}

-(void)setBlackBoxSad:(NSString *)blackBoxIp {
    
    [self setStringValue:blackBoxIp ForKey:SAD];
    
}

-(NSString *)getBlackBoxSad {
    
    return [self getStringValueForKey:SAD];
    
}

-(void)setBlackBoxCmd:(NSString *)blackBoxIp {
    
    [self setStringValue:blackBoxIp ForKey:CMD];
    
}

-(NSString *)getBlackBoxCmd {
    
    return [self getStringValueForKey:CMD];
    
}

//first time app launch check
-(BOOL)isAppAlreadyLanuched {
    
    return [[NSUserDefaults standardUserDefaults] boolForKey:APP_ALREADY_LAUNCH];
    
}

-(BOOL)isSplashFirstTime {
    
    return [[NSUserDefaults standardUserDefaults] boolForKey:FIRST_TIME_SPLASH];
    
}

-(void)SplashFirstTime:(BOOL)isSplashFirstTime {
    [[NSUserDefaults standardUserDefaults] setBool:isSplashFirstTime forKey:FIRST_TIME_SPLASH];
}






-(BOOL)isWaterLavelFirstTime {
    
    return [[NSUserDefaults standardUserDefaults] boolForKey:WATER_LAVEL_FIRST_TIME];
    
}

-(void)WaterLavelFirstTime:(BOOL)isWaterLavelFirstTime {
    [[NSUserDefaults standardUserDefaults] setBool:isWaterLavelFirstTime forKey:WATER_LAVEL_FIRST_TIME];
}



-(BOOL)isSyncFirstTime {
    
    return [[NSUserDefaults standardUserDefaults] boolForKey:FIRST_SYNC];
    
}

-(void)SyncFirstTime:(BOOL)isSyncFirstTime {
    [[NSUserDefaults standardUserDefaults] setBool:isSyncFirstTime forKey:FIRST_SYNC];
}



-(BOOL)isGetTankSettings {
    
    return [[NSUserDefaults standardUserDefaults] boolForKey:GET_TANK_SETTINGS];
    
}

-(void)GetTankSettings:(BOOL)isGetTankSettings {
    [[NSUserDefaults standardUserDefaults] setBool:isGetTankSettings forKey:GET_TANK_SETTINGS];
}

-(BOOL)isTankSettingsFirstTime {
    
    return [[NSUserDefaults standardUserDefaults] boolForKey:TANK_SETTINGS_FIRST];
    
}

-(void)TankSettingsFirstTime:(BOOL)isTankSettingsFirstTime {
    [[NSUserDefaults standardUserDefaults] setBool:isTankSettingsFirstTime forKey:TANK_SETTINGS_FIRST];
}


//saving admin password
-(void)saveAdminPassword:(NSString *)AdminPassword {
    [self setStringValue:AdminPassword ForKey:ADMIN_PASSWORD];
}
-(NSString *)getAdminPassword {
    return [self getStringValueForKey:ADMIN_PASSWORD];
}


//notification status
-(void)saveNotificationStatus:(BOOL)status {
    [self setBoolValue:status ForKey:NOTIFICATION_STATUS];
}

-(BOOL)getNotificationStatus {
    BOOL value = [self getBOOLValueForKey:NOTIFICATION_STATUS];
    return value;
}


-(BOOL)isFromDeviceRegistration;

{

    return [[NSUserDefaults standardUserDefaults] boolForKey:FROM_DEVICE_REG];

}

-(void)FromDeviceRegistration:(BOOL)isFromDeviceRegistration
{
[[NSUserDefaults standardUserDefaults] setBool:isFromDeviceRegistration forKey:FROM_DEVICE_REG];
   
}


-(BOOL)isFromScheduleEdit;

{
    
    return [[NSUserDefaults standardUserDefaults] boolForKey:FROM_SCHEDULE_EDIT];
    
}

-(void)fromScheduleEditingButton:(BOOL)isFromScheduleEdit
{
    [[NSUserDefaults standardUserDefaults] setBool:isFromScheduleEdit forKey:FROM_SCHEDULE_EDIT];
    
}


-(void)AppAlreadyLanuched:(BOOL)alreadyLaunch {
    [[NSUserDefaults standardUserDefaults] setBool:alreadyLaunch forKey:APP_ALREADY_LAUNCH];
}


-(void)setUUID:(NSString *)uuid {
    [self setStringValue:uuid ForKey:UUID];
}

-(NSString *)getUUID {
    
    NSString *uuid = [self getStringValueForKey:UUID];
    
    if(uuid == nil)
        uuid =  [WMSUUIDCreater GetUUID];
    
    return uuid;
    
}




-(NSString *)getRegistrationKey {
    
    NSString *registrationKey = [self getStringValueForKey:REGISTRATION_KEY];
    return registrationKey;
    
}
-(void)setRegistrationKey:(NSString *)registrationKey {
    
    [self setStringValue:registrationKey ForKey:REGISTRATION_KEY];
    
}

-(void)commit {
    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(BOOL)removeRegistrationKey {
    
    BOOL isRegistrationKeyRemove = false;
    
    if(self.getRegistrationKey != nil) {
        
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:REGISTRATION_KEY];
        isRegistrationKeyRemove= true;
        
    }
    return isRegistrationKeyRemove;
}

-(void)setLogoutTime:(NSInteger)time {
  [self setIntegerValue:time ForKey:LOGOUT_TIME];
}

-(NSInteger)getLogoutTime {
  return [self getIntegerValueForKey:LOGOUT_TIME];
}

-(void)setUserName:(NSString *)username
{
    [self setStringValue:username ForKey:USER_NAME];
}
-(NSString *)getUserName {
    NSString *userName = [self getStringValueForKey:USER_NAME];
    return userName;
}

-(void)setUserEmailid:(NSString *)emailid {
     [self setStringValue:emailid ForKey:USER_EMAIL];
}

-(NSString *)getUserEmailid {
    NSString *userEmail = [self getStringValueForKey:USER_EMAIL];
    return userEmail;
}

-(void)setUserPhoneNumber:(NSString *)phoneno {
    [self setStringValue:phoneno ForKey:USER_PHONENO];
}

-(NSString *)getUserPhoneNumber {
    NSString *userPhoneNumber = [self getStringValueForKey:USER_PHONENO];
    return userPhoneNumber;
}

-(void)setThemeName:(NSString *)themeName {
    [self setStringValue:themeName ForKey:THEME_NAME];
}

-(NSString *)getThemeName {
    NSString *themeName = [self getStringValueForKey:THEME_NAME];
    return themeName;

}

-(void)setProvisionfileparseStatus:(BOOL)status {
    [self setBoolValue:status ForKey:PROVISION_STATUS];
}

-(BOOL)getProvisionFileStatus {
    BOOL value = [self getBOOLValueForKey:PROVISION_STATUS];
    return value;
}

-(void)setDevicesList:(NSArray *)deviceArray {
    if (deviceArray) {
        if (!cacheDict) {
            cacheDict = [[NSMutableDictionary alloc] init];
        }
        [cacheDict setObject:deviceArray forKey:DEVICE_LIST
         ];
    }

}

-(NSArray *)getDevicesList {
    NSArray *deviceArray = [cacheDict objectForKey:DEVICE_LIST];
    if (deviceArray) {
        return deviceArray;
    }
    return nil;

}

-(void)setAllRooms:(NSArray *)roomList {
    if (roomList) {
        if (!cacheDict) {
            cacheDict = [[NSMutableDictionary alloc] init];
        }
        [cacheDict setObject:roomList forKey:ROOM_LIST
         ];
    }
}

-(NSArray *)getAllRooms {
    NSArray *roomArray = [cacheDict objectForKey:ROOM_LIST];
    if (roomArray) {
        return roomArray;
    }
    return nil;
}

-(void)setAddressMap:(NSArray *)addressArray {
    if (addressArray) {
        if (!cacheDict) {
            cacheDict = [[NSMutableDictionary alloc] init];
        }
        [cacheDict setObject:addressArray forKey:ADDRESS_LIST
         ];
    }
}
-(NSArray *)getAddressMap {
    NSArray *addressArray = [cacheDict objectForKey:ADDRESS_LIST];
    if (addressArray) {
        return addressArray;
    }
    return nil;
}

-(void)setGadgetList:(NSDictionary *)roomsGadgetDict {
    if (roomsGadgetDict) {
        if (!cacheDict) {
            cacheDict = [[NSMutableDictionary alloc] init];
        }
        [cacheDict setObject:roomsGadgetDict forKey:GADGET_LIST
         ];
    }
    
}

-(NSDictionary *)getGadgetList {
    NSDictionary *gadgetDict = [cacheDict objectForKey:GADGET_LIST];
    if (gadgetDict) {
        return gadgetDict;
    }
    return nil;
    
}


-(void)setActiveProfileCode:(int)profileCode {
   [self setIntegerValue:profileCode ForKey:PROFILE_CODE];
}

-(int)getActiveProfileCode {
    int profileCode = (int)[self getIntegerValueForKey:PROFILE_CODE];
    return profileCode;
}

-(void)saveProfileList:(NSMutableArray *)profileResposeModelArray {
    if (profileResposeModelArray) {
        if (!cacheDict) {
            cacheDict = [[NSMutableDictionary alloc] init];
        }
        [cacheDict setObject:profileResposeModelArray forKey:PROFILE_RESPONSE_LIST
         ];
    }

}

-(NSMutableArray *)getProfileResponseList {
    NSMutableArray *addressArray = [cacheDict objectForKey:PROFILE_RESPONSE_LIST];
    if (addressArray) {
        return addressArray;
    }
    return nil;
}

-(void)saveLutronLightList:(NSArray *)lutronArray {
    if (lutronArray) {
        if (!cacheDict) {
            cacheDict = [[NSMutableDictionary alloc] init];
        }
        [cacheDict setObject:lutronArray forKey:LUTRON_LIST
         ];
    }
}

-(NSArray *)getLutronLightList {
    NSArray *addressArray = [cacheDict objectForKey:LUTRON_LIST];
    if (addressArray) {
        return addressArray;
    }
    return nil;
}

-(void)saveLghvacList:(NSArray *)lghvacArray {
    if (lghvacArray) {
        if (!cacheDict) {
            cacheDict = [[NSMutableDictionary alloc] init];
        }
        [cacheDict setObject:lghvacArray forKey:LGHVAC_LIST
         ];
    }

}

-(NSArray *)getLghvacList {
    NSArray *addressArray = [cacheDict objectForKey:LGHVAC_LIST];
    if (addressArray) {
        return addressArray;
    }
    return nil;
}

+(NSString *)getBusId:(NSString *)busId {
    NSArray *busIdArray = [busId componentsSeparatedByString:@"-"];
    return [busIdArray objectAtIndex:1];
    
}

+(NSString *)getSlaveId:(NSString *)slaveId {
    NSArray *slaveIdArray = [slaveId componentsSeparatedByString:@"-"];
    return [slaveIdArray objectAtIndex:1];
}

-(void)saveQosFileContentStatus:(BOOL)status {
     [self setBoolValue:status ForKey:QOS_CONTENT];
}

-(BOOL)getQosFileContentStatus {
    NSInteger value = [[NSUserDefaults standardUserDefaults] integerForKey:QOS_CONTENT];
    return value;
}

-(void)setCurrentFirmwareVersion:(NSString *)version {
     [self setStringValue:version ForKey:CURRENT_FIRMWARE_VERSION];
}

-(NSString *)getCurrentFirmwareVersion {
    NSString *currentFirmwareVersion = [self getStringValueForKey:CURRENT_FIRMWARE_VERSION];
    return currentFirmwareVersion;

}
-(void)setLatestFirmwareVersion:(NSString *)version {
     [self setStringValue:version ForKey:LATEST_FIRMWARE_VERSION];
}
-(NSString *)getLatestFirmwareVersion {
    NSString *latestFirmwareVersion = [self getStringValueForKey:LATEST_FIRMWARE_VERSION];
    return latestFirmwareVersion;

}





-(void)setAppUserName:(NSString *)username {
    [self setStringValue:username ForKey:APP_USER_NAME];

}

-(NSString *)getAppUserName {
      return [self getStringValueForKey:APP_USER_NAME];
}
-(void)saveLicenceNumber:(NSString *)licenceNumber {     [self setStringValue:licenceNumber ForKey:LICENSE_NUMBER];
}

-(NSString *)getLicenseNumber {
    return [self getStringValueForKey:LICENSE_NUMBER];
}

//
-(void)saveSerialNumber:(NSString *)serialNumber {
     [self setStringValue:serialNumber ForKey:SERIAL_NUMBER];
}
-(NSString *)getSerialNUmber {
    return [self getStringValueForKey:SERIAL_NUMBER];
}
-(void)saveWifiName:(NSString *)saveWifiName {
    [self setStringValue:saveWifiName ForKey:WIFI_NAME];
}
//




-(NSString *)getWifiName {
    return [self getStringValueForKey:WIFI_NAME];
}
-(void)setUserAddress:(NSString *)address{
    [self setStringValue:address ForKey:USER_ADDRESS];

}

-(NSString *)getAppAddress{
      return [self getStringValueForKey:USER_ADDRESS];
}

-(void)setUserProfileImage:(UIImage *)image {
     [[NSUserDefaults standardUserDefaults] setObject:UIImagePNGRepresentation(image) forKey:USER_IMAGE];
}

-(UIImage *)getUserProfileImage {
    NSData* imageData = [[NSUserDefaults standardUserDefaults] objectForKey:USER_IMAGE];
    UIImage* image = [UIImage imageWithData:imageData];
    return image;
}

-(void)isFirstTimeRegistration:(BOOL)status{
    [self setBoolValue:status ForKey:DEVICE_STATUS];
}
//-(BOOL)getDeviceAuthenticationStatus {
//    BOOL value = [self getBOOLValueForKey:DEVICE_STATUS];
//    return value;
//}


-(void)setDeviceAuthenticationStatus:(NSString*)response{
    [self setStringValue:response ForKey:DEVICE_STATUS];
}
-(NSString*)getDeviceAuthenticationStatus {
    return  [self getStringValueForKey:DEVICE_STATUS];
}

-(void)setAcessToken:(NSString *)acessToken {
    [self setStringValue:acessToken ForKey:ACCESS_TOKEN];
}
-(NSString *)getAcessToken {
       return  [self getStringValueForKey:ACCESS_TOKEN];
}

-(void)setLastFetchedLocation:(NSDictionary *)location {
    if (location) {
        if (!cacheDict) {
            cacheDict = [[NSMutableDictionary alloc] init];
        }
        [cacheDict setObject:location forKey:LOCATION
         ];
    }

}
-(NSDictionary *)getLocation {
    NSDictionary *location = [cacheDict objectForKey:LOCATION];
    if (location) {
        return location;
    }
    return nil;

}

// private methods end

@end
