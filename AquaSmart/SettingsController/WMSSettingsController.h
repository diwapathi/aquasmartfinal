//
//  WMSSettingsController.h
//  AquaSmart
//
//  Created by Neotech on 14/12/15.
//  Copyright © 2015 Neotech. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WMSUUIDCreater.h"
#import <UIKit/UIKit.h>

@class Device;

@interface WMSSettingsController : NSObject
+(instancetype)sharedInstance;
@property (nonatomic,strong)   NSMutableDictionary *cacheDict;
//Black box ip getter and setter
-(NSString *)getBlackBoxIP;
-(void)setBlackBoxIP:(NSString *)blackBoxIp;

-(NSString *)getWifiSsId;
-(void)setWifiSsId:(NSString *)wifiSsId;

-(NSString *)getAPModeSsId;
-(void)setAPModeSsId:(NSString *)ApModeSsId;


//fetching ip address 
-(NSString *)getIPAddress;
-(void)setIPAddress:(NSString *)ipAddress;

-(NSString *)getStationModeSsId;
-(void)setStationModeSsId:(NSString *)StationModeSsId;

-(NSString *)getMacId;
-(void)setMacId:(NSString *)macId;

-(NSString *)getBlackBoxSid;
-(void)setBlackBoxSid:(NSString *)blackBoxIp;

-(NSString *)getBlackBoxSad;
-(void)setBlackBoxSad:(NSString *)blackBoxIp;

-(NSString *)getBlackBoxCmd;
-(void)setBlackBoxCmd:(NSString *)blackBoxIp;

//first time app launch check
-(BOOL)isAppAlreadyLanuched;
-(void)AppAlreadyLanuched:(BOOL)alreadyLaunch;

-(NSString *)getUUID;
-(void)setUUID:(NSString *)uuid;
-(BOOL)isSyncFirstTime;
-(void)SyncFirstTime:(BOOL)isSyncFirstTime;

//for setting tank first time
-(BOOL)isTankSettingsFirstTime;
-(void)TankSettingsFirstTime:(BOOL)isTankSettingsFirstTime;

//get tank settings
-(BOOL)isGetTankSettings;
-(void)GetTankSettings:(BOOL)isGetTankSettings;


//for edit in date picker
-(BOOL)isFromScheduleEdit;
-(void)fromScheduleEditingButton:(BOOL)isFromScheduleEdit;

-(BOOL)isWaterLavelFirstTime;
-(void)WaterLavelFirstTime:(BOOL)isWaterLabelFirstTime;
-(BOOL)isSplashFirstTime;
-(void)SplashFirstTime:(BOOL)isSplashFirstTime;

-(BOOL)isFromDeviceRegistration;
-(void)FromDeviceRegistration:(BOOL)isFromDeviceRegistration;

-(NSString *)getRegistrationKey;
-(void)setRegistrationKey:(NSString *)registrationKey;

-(void)saveNotificationStatus:(BOOL)status;
-(BOOL)getNotificationStatus;



//saving admin password
-(void)saveAdminPassword:(NSString *)AdminPassword;
-(NSString *)getAdminPassword;
////tanks settings for calculation
//-(NSInteger)getNumberOfOHTTanks;
//-(void)setNumberOFOHTTanks:(NSInteger)numberOfTanks;
//
//-(NSInteger)getOHTTotalVolume;
//-(void)setOHTTotalVolume:(NSInteger)totalTankVolume;
//
//-(BOOL)isUGTTankPresent;
//-(void)setUGTTankStatus:(BOOL)isUGTTankPresent;
//
//-(NSInteger)getUGTTotalVolume;
//-(void)setUGTTotalVolume:(NSInteger)totalTankVolume;
//
//-(void)setOHTTankPercentage:(NSInteger)ohtTankPercentage;
//-(NSInteger)getOHTTankPercentage;
//
//-(void)setUGTTankPercentage:(NSInteger)ugtTankPercentage;
//-(NSInteger)getUGTTankPercentage;
//
//-(void)setOhtPumpTimeStamp:(NSString *)timeStamp;
//-(NSString *)getOhtPumpTimeStamp;
//
//-(void)setUgtPumpTimeStamp:(NSString *)timeStamp;
//-(NSString *)getUgtPumpTimeStamp;
//
//-(void)setOhtPumpState:(BOOL)pumpStateValue;
//-(BOOL)getOhtPumpState;
//
//-(void)setUgtPumpState:(BOOL)pumpStateValue;
//-(BOOL)getUgtPumpState;
//
//-(NSArray *)getOhtTanksObjects;
//-(void)setOhtTanksObjects:(NSArray *)tanksArray;
//
//-(WMSTank *)getUgtTankObject;
//-(void)setUgtTankObject:(WMSTank *)tankModel;
//
////ugt and oht tank current volume calculative
//-(int)getOhtTankCurrentVolume;
//-(int)getUgtTankCurrentVolume;
//
-(void)setLogoutTime:(NSInteger)time;
//-(NSInteger)getLogoutTime;
//
//-(void)setWaterLevelPullInterval:(NSInteger)timeInSec;
//-(NSInteger)getWaterLevelPullInterval;
//
//-(void)setNotificationPullInterval:(NSInteger )timeInMins;
//-(NSInteger)getNotificationPullInterval;
//
//-(void)setLastNotificationPullTimestamp:(NSString *)lastNotificationPullTime;
//-(NSString *)getLastNotificationPullTimestamp;
//
//-(void)setLastEventPullTimestamp:(NSString *)lastEventPullTime;
//-(NSString *)getLastEventPullTimestamp;

//commit the changes
-(void)commit;

//Remove the Registration Key
-(BOOL)removeRegistrationKey;

//for stationmode pass
-(void)setStationModePass:(NSString *)StationModePass;
-(NSString *)getStationModePass;


-(void)setUserName:(NSString *)username;
-(NSString *)getUserName;

-(void)setUserEmailid:(NSString *)emailid;
-(NSString *)getUserEmailid;

-(void)setUserPhoneNumber:(NSString *)phoneno;
-(NSString *)getUserPhoneNumber;

-(void)setThemeName:(NSString *)themeName;
-(NSString *)getThemeName;

-(void)setProvisionfileparseStatus:(BOOL)status;
-(BOOL)getProvisionFileStatus;

-(void)setActiveProfileCode:(int)profileCode;
-(int)getActiveProfileCode;

-(void)setDevicesList:(NSArray *)deviceArray;
-(NSArray *)getDevicesList;

-(void)setAllRooms:(NSArray *)roomList;
-(NSArray *)getAllRooms;

-(void)setGadgetList:(NSDictionary *)roomsGadgetDict;
-(NSDictionary *)getGadgetList;

-(void)setAddressMap:(NSArray *)addressArray;
-(NSArray *)getAddressMap;

-(void)saveProfileList:(NSMutableArray *)profileResposeModelArray;
-(NSMutableArray *)getProfileResponseList;

+(NSString *)getBusId:(NSString *)busId ;
+(NSString *)getSlaveId:(NSString *)slaveId;

-(void)saveLutronLightList:(NSArray *)lutronArray;
-(NSArray *)getLutronLightList;
-(void)saveQosFileContentStatus:(BOOL)status;
-(BOOL)getQosFileContentStatus;
-(void)saveLghvacList:(NSArray *)lghvacArray;
-(NSArray *)getLghvacList;

-(void)setCurrentFirmwareVersion:(NSString *)version;
-(NSString *)getCurrentFirmwareVersion;
-(void)setLatestFirmwareVersion:(NSString *)version;
-(NSString *)getLatestFirmwareVersion;


-(void)setPanicSwitchStatus:(BOOL)status;
-(BOOL)getPanicSwitchStatus;

//-(void)storePanicSwitchData:(Device *)deviceInfo;
//-(NSString *)getPanicSwitchData;


-(void)saveDiDeviceList:(NSMutableArray *)diDeviceList;
-(NSMutableArray *)getdiDeviceList;

-(void)saveProfileListFromApi:(NSDictionary *)profileDict;
-(NSDictionary *)getProfileList;

-(void)saveProfileInfoListXml:(NSDictionary *)profileDict;
-(NSDictionary *)getProfileInfoList;
-(void)saveImmediateTextFileList:(NSArray *)immediateArray;
-(NSArray *)getImmediateTextFileList;

-(void)saveScheduleTextFileList:(NSArray *)scheduleArray;
-(NSArray *)getScheduleTextFileList;


-(void)setAppUserName:(NSString *)username;
-(NSString *)getAppUserName;

-(void)setUserAddress:(NSString *)address;
-(NSString *)getAppAddress;
-(void)setUserProfileImage:(UIImage *)image;
-(UIImage *)getUserProfileImage;

-(void)saveLicenceNumber:(NSString *)licenceNumber;
-(void)saveSerialNumber:(NSString *)serialNumber;
-(void)saveWifiName:(NSString *)saveWifiName;
-(NSString *)getLicenseNumber;
-(NSString *)getSerialNUmber;
-(NSString *)getWifiName;

-(void)isFirstTimeRegistration:(BOOL)status;
//-(BOOL)getDeviceAuthenticationStatus;


-(void)setDeviceAuthenticationStatus:(NSString*)response;
-(NSString*)getDeviceAuthenticationStatus;
-(void)setAcessToken:(NSString *)acessToken;
-(NSString *)getAcessToken;

-(void)setLastFetchedLocation:(NSDictionary *)location;
-(NSDictionary *)getLocation;

@end
