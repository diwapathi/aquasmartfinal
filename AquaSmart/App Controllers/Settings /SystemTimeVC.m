//
//  SystemTimeVC.m
//  AquaSmart
//
//  Created by Neotech on 09/03/17.
//  Copyright © 2017 Mac User. All rights reserved.
//

#import "SystemTimeVC.h"

@interface SystemTimeVC ()<PacketReciveProtocol>
{
    NSTimer* timer;
    NSString *finalString;
}
@end

@implementation SystemTimeVC
+(instancetype)sharedInstance {
    
    static SystemTimeVC *systemTime;
    static dispatch_once_t once;
    dispatch_once(&once, ^{
        systemTime = [[self alloc] init];
    });
    
    return systemTime;
}
- (void)viewDidLoad {
    
 //   myTimer = [NSTimer scheduledTimerWithTimeInterval:10.0 target: self
 //                                        selector: @selector(getDeviceDateTimeNow) userInfo: nil repeats: YES];
    NSDate *date = [NSDate date];
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    [dateFormatter setDateFormat:@"dd-MM-yy HH:mm:ss"];
    NSLog(@"%@",[dateFormatter stringFromDate:[NSDate date]]);
       NSString *dateString = [dateFormatter stringFromDate:date];
    NSString *formattedDateString = dateString;
    //10-03-17-11-55-00
    NSRange range = NSMakeRange(8,1);
    NSRange range1 = NSMakeRange(11,1);
    NSRange range2 = NSMakeRange(14, 1);

    NSString *newText = [formattedDateString stringByReplacingCharactersInRange:range withString:@"-"];
    NSString *newText1 = [newText stringByReplacingCharactersInRange:range1 withString:@"-"];
    finalString = [newText1 stringByReplacingCharactersInRange:range2 withString:@"-"];

    NSLog(@"%@",finalString);

    NSString *stringforlabel = [finalString stringByReplacingCharactersInRange:range withString:@","];
    self.systemDateTime.text = stringforlabel;
    [super viewDidLoad];
    [self setTopHeaderView];
    // Do any additional setup after loading the view.
}

#pragma mark - Initial data load -
-(void)setTopHeaderView {
    NSDictionary *navDict = @{
                              TITLE_TEXT :NSLocalizedString(@"Time Settings", nil),
                              BACK_IMAGE :@"BackImage",
                              };
    [[AppDelegate appDelegate].superVC setHeaderViewData:navDict];
}

-(void)backClicked {
    UIStoryboard *mainStroyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    MVYSideMenuController *sideMenuController = [self sideMenuController];
    MainViewController *homeVC = [mainStroyBoard instantiateViewControllerWithIdentifier:@"MainViewController"];
    [sideMenuController changeContentViewController:homeVC closeMenu:YES];
}

- (IBAction)showNotifyButtonClicked:(id)sender
{
    
    UIStoryboard *mainStroyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    NotificationViewController *notificationVC = [[NotificationViewController alloc]init];
    notificationVC = [mainStroyBoard instantiateViewControllerWithIdentifier:@"NotificationViewController"];
    
    [self.navigationController pushViewController:notificationVC animated:nil];
}

-(void)getDeviceDateTimeNow
{

    [self getDeviceDateTimeApiCall];
}

- (IBAction)syncButtonClicked:(id)sender
{
    
    [self setDeviceDateTimeApiCall];
    
    
}


-(void)viewWillAppear:(BOOL)animated
{
    NetworkStatus status = [self checkForWifiConnection];
    if(status == NotReachable || status == ReachableViaWWAN) {
        [self.navigationController.view makeToast:NSLocalizedString(@"Please check your wifi connection", nil)];
    }

        
        [self.navigationController setNavigationBarHidden:YES];
    [self getDeviceDateTimeApiCall];
     // [[AppDelegate appDelegate].superVC updateHeadrLabel:NSLocalizedString(@"Time Sync", nil)];
}
-(NetworkStatus)checkForWifiConnection {
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    [reachability startNotifier];
    NetworkStatus status = [reachability currentReachabilityStatus];
    return status;
}
#pragma mark - set Device Date and Time
-(void)getDeviceDateTimeApiCall
{
   
    
    OrderedDictionary *requestDict = [[OrderedDictionary alloc]initWithCapacity:5];
    [requestDict insertObject:_SIDVALUE forKey:_SID atIndex:0];
    //[requestDict insertObject:_BADVALUE forKey:_BAD atIndex:1];
    [requestDict insertObject:_SADVALUE forKey:_SAD atIndex:1];
    [requestDict insertObject:_8103 forKey:_CMD atIndex:2];
    NSError *writeError;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:requestDict options:kNilOptions error:&writeError];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    [SVProgressHUD showWithStatus:@"Please Wait.."];

    [TcpPacketClass sharedInstance].delegate = self;
     [[TcpPacketClass sharedInstance]writeOut:jsonString second:[requestDict objectForKey:_CMD]] ;
    timer = [NSTimer scheduledTimerWithTimeInterval:5.0 target:self selector:@selector(sendGetSyncTime:) userInfo:nil repeats: NO];


}


- (void)sendGetSyncTime:(NSTimer *)theTimer {
    
    int static ticks = 0;
    if (ticks<3) {
         [self getDeviceDateTimeNow];
    }
    else if (ticks>2)
    {
        [theTimer invalidate];
        [SVProgressHUD dismiss];
        [self.view makeToast:NSLocalizedString(@"Something went wrong Please try again.", nil)];
        ticks = -1;
    }
    ticks++;
    
    //[theTimer invalidate];
   
    
   // [SVProgressHUD dismiss];
    
    //[self.navigationController popViewControllerAnimated:YES];
    
}

#pragma mark - get device date and time response
-(void)getDeviceDateandTimeResponse:(NSDictionary *)jsonDict
{
    [SVProgressHUD dismiss];
    [timer invalidate];
    NSLog(@"RESPONSE");
            if(jsonDict != nil) {
                [SVProgressHUD dismiss];
            if([[jsonDict valueForKey:_DATA] isKindOfClass:[NSDictionary class]]) {

        NSDictionary *dataDict = [jsonDict valueForKey:_DATA];
        NSString *matchDateString = [dataDict objectForKey:_01];
        if ([[dataDict valueForKey:_01] isEqualToString:matchDateString])
        {
            NSString *dateTimeStr = [dataDict valueForKey:_01];
            //new
            NSRange range = NSMakeRange(8,1);
            NSRange range1 = NSMakeRange(11,1);
            NSRange range2 = NSMakeRange(14, 1);
            
            NSString *newText = [dateTimeStr stringByReplacingCharactersInRange:range withString:@"-"];
            NSString *newText1 = [newText stringByReplacingCharactersInRange:range1 withString:@"-"];
            NSString *DeviceDate = [newText1 stringByReplacingCharactersInRange:range2 withString:@"-"];
            
            NSLog(@"%@",DeviceDate);
            
            NSString *stringforlabel = [DeviceDate stringByReplacingCharactersInRange:range withString:@","];
            self.deviceDateTime.text = stringforlabel;
            
            
            
            
            
            
            //old
//            NSString *date = [dateTimeStr substringWithRange:NSMakeRange(0,2)];
//            NSString *month = [dateTimeStr substringWithRange:NSMakeRange(2,3)];
//            NSString *year = [dateTimeStr substringWithRange:NSMakeRange(5,3)];
//            NSString *hour = [dateTimeStr substringWithRange:NSMakeRange(9,2)];
//            NSString *min = [dateTimeStr substringWithRange:NSMakeRange(11,3)];
//            NSString *sec = [dateTimeStr substringWithRange:NSMakeRange(14,3)];
//            NSString *formattedDeviceDate =  [NSString stringWithFormat:@"%@ %@ %@ %@ %@ %@",date,month,year,hour,min,sec];
//            NSRange range = NSMakeRange(10, 1);
//            NSString *DeviceDate = [formattedDeviceDate stringByReplacingCharactersInRange:range withString:@","];
//            self.deviceDateTime.text = DeviceDate;
           // [self.view makeToast:NSLocalizedString(@"Device Time Get Successfully", nil)];
        }


}
}
}

#pragma  mark - device date and time api call
-(void)setDeviceDateTimeApiCall
{
  NSString *gmtString = @"                              ";
    NSUInteger length = [gmtString length];
    NSLog(@"%lu",(unsigned long)length);
    
    OrderedDictionary *dataDict = [[OrderedDictionary alloc]initWithCapacity:3];
    [dataDict insertObject:finalString forKey:_01 atIndex:0];
    [dataDict insertObject:@"                              " forKey:_02 atIndex:1];
    
    OrderedDictionary *requestDict = [[OrderedDictionary alloc]initWithCapacity:5];
    [requestDict insertObject:_SIDVALUE forKey:_SID atIndex:0];
    //[requestDict insertObject:_BADVALUE forKey:_BAD atIndex:1];
    [requestDict insertObject:_SADVALUE forKey:_SAD atIndex:1];
    [requestDict insertObject:_8102 forKey:_CMD atIndex:2];
    [requestDict insertObject:dataDict forKey:_DATA atIndex:3];
    NSError *writeError;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:requestDict options:kNilOptions error:&writeError];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
   [SVProgressHUD
    showWithStatus:@"Syncing Time..."];
    [TcpPacketClass sharedInstance].delegate = self;
     [[TcpPacketClass sharedInstance]writeOut:jsonString second:[requestDict objectForKey:_CMD]];
    timer = [NSTimer scheduledTimerWithTimeInterval: 5.0 target:self selector:@selector(updateCounter:) userInfo:nil repeats: NO];

}

- (void)updateCounter:(NSTimer *)theTimer {
    
    int static ticks = 0;
    if (ticks<3) {
        [self setDeviceDateTimeApiCall];
    }
    else if (ticks>2)
    {
        [theTimer invalidate];
        [SVProgressHUD dismiss];
       // [self.view makeToast:NSLocalizedString(@"Something went wrong Please try again.", nil)];
        ticks = -1;
    }
    ticks++;
    
//    [theTimer invalidate];
//    [SVProgressHUD dismiss];
    
    //[self.navigationController popViewControllerAnimated:YES];

}
#pragma mark- response of device date time here
-(void)syncDateandTimeResponse:(NSDictionary *)jsonDict
{
    [timer invalidate];
    if (jsonDict) {
        [SVProgressHUD dismiss];
        if ([[jsonDict valueForKey:_DATA]isEqualToString:@"0"]) {
            [self.view makeToast:NSLocalizedString(@"Sync Successfully", nil)];

        }
        
        else
        {
          [self.view makeToast:NSLocalizedString(@"Unable to Sync Successfully", nil)];
        
        }
        [self getDeviceDateTimeApiCall];
}
}

-(void)didRecieveError:(NSError *)error {
    [SVProgressHUD dismiss];
    [self.view makeToast:NSLocalizedString(@"Socket not connected", nil)];
   //  [[Communicator sharedInstance]setup];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
