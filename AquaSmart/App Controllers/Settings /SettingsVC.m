//
//  SettingsVC.m
//  AquaSmart
//
//  Created by Neotech on 03/04/17.
//  Copyright © 2017 Mac User. All rights reserved.
//

#import "SettingsVC.h"
#import "SKSTableView.h"
#import "SKSTableViewCell.h"
#import "AppSettingsCell.h"
#import "AppDelegate.h"


#import "SystemSettingVC.h"
#import "ControlSettingsVC.h"
#import "ForgatePassword.h"
#import "SystemTimeVC.h"
#import "WMSBlackBoxCommands.h"
#import "OrderedDictionary.h"
#import "NotificationViewController.h"
#import "ChangePasswordVC.h"
#import "TankSettingVC.h"
#import "FirmwareUpgradeVC.h"
@interface SettingsVC ()<SKSTableViewDelegate,PacketReciveProtocol>
{
    NSTimer *timer;
    DatabaseModel *datamodel;
    MainViewController *mainVC;
   
    int selectedRow;
    int selectedSection;
}
@property(nonatomic,strong)NSArray *mainSettingsArray;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *sidebarbutton;
@property(nonatomic,strong)NSArray *mainSettingsHeaderArray;
@property (weak, nonatomic) IBOutlet SKSTableView *settingsTableView;
@property (weak, nonatomic) IBOutlet UIView *popupView;
@property (weak, nonatomic) IBOutlet UIButton *okButton;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (weak, nonatomic) IBOutlet UITextField *passwordTF;
@property (weak, nonatomic) IBOutlet UILabel *passwordLabel;
@property (weak, nonatomic) IBOutlet UIView *passwordView;
@property (strong, nonatomic) IBOutlet UIButton *passwordViewButton;
@property (strong, nonatomic) IBOutlet UITextField *lincenceNumber;
@property (strong, nonatomic) IBOutlet UITextField *password;
@property (strong, nonatomic) IBOutlet UITextField *confirmPassword;

@property (strong, nonatomic) IBOutlet UIView *saveButton;

@end

@implementation SettingsVC


- (void)viewDidLoad {
    
    [self setTopHeaderView];
    datamodel=[[DatabaseModel alloc]init];
    [super viewDidLoad];
    self.lincenceNumber.text = @"OFBSK8E5A8340155JB54O3FP";
    [self.okButton setTitle:NSLocalizedString(@"Ok", nil) forState:UIControlStateNormal];
    [self.cancelButton setTitle:NSLocalizedString(@"Cancel", nil) forState:UIControlStateNormal];
    [self setInitialConfigurations];
    //for hiding the pop view dialog
    self.popupView.hidden = YES;
    selectedRow = -1 ;
    selectedSection = -1;
    self.passwordView.hidden = YES;
    

    // Do any additional setup after loading the view.
}

#pragma mark - Initial data load -
-(void)setTopHeaderView {
    NSDictionary *navDict = @{
                              TITLE_TEXT :NSLocalizedString(@"Settings", nil),
                              BACK_IMAGE :@"BackImage",
                              IS_BACK_HIDDEN:@"No"
                              };
    [[AppDelegate appDelegate].superVC setHeaderViewData:navDict];
}

-(void)backClicked {
    UIStoryboard *mainStroyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    MVYSideMenuController *sideMenuController = [self sideMenuController];
    MainViewController *homeVC = [mainStroyBoard instantiateViewControllerWithIdentifier:@"MainViewController"];
    [sideMenuController changeContentViewController:homeVC closeMenu:YES];
}

-(void)viewWillAppear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onSocketCreate:) name:SOCKET_CONNECTED object:nil];
    [self.popupView setHidden:NO];
    [self.passwordView setHidden:NO];
    [super viewWillAppear:animated];
    NetworkStatus status = [self checkForWifiConnection];
    if(status == NotReachable || status == ReachableViaWWAN) {
        [self.navigationController.view makeToast:NSLocalizedString(@"Please check your wifi connection", nil)];
    }
        [[AppDelegate appDelegate].superVC updateHeadrLabel:NSLocalizedString(@"Settings", nil)];
    
}

-(NetworkStatus)checkForWifiConnection {
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    [reachability startNotifier];
    NetworkStatus status = [reachability currentReachabilityStatus];
    return status;
}


-(void)setInitialConfigurations {
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(endEditing)];
    [tapRecognizer setNumberOfTapsRequired:1];
    tapRecognizer.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tapRecognizer];
    self.mainSettingsArray = [[NSArray alloc]init];
    NSString* plistPath = [[NSBundle mainBundle] pathForResource:@"dataSetting" ofType:@"plist"];
    self.mainSettingsArray =[NSArray arrayWithContentsOfFile:plistPath];
    self.mainSettingsHeaderArray = @[NSLocalizedString(@"System Settings", nil),NSLocalizedString(@"Registration", nil),NSLocalizedString(@"Tank and Pump Setting", nil)];
    self.settingsTableView.SKSTableViewDelegate = self;
        
    //self.popupView.hidden = YES;
    self.passwordLabel.text = NSLocalizedString(@"Please pppppprovide admin password", nil);
    [self.okButton setTitle:NSLocalizedString(@"Ok", nil) forState:UIControlStateNormal];
    [self.cancelButton setTitle:NSLocalizedString(@"Cancel", nil) forState:UIControlStateNormal];
   
    self.passwordView.layer.cornerRadius = 2.0;
    
    self.passwordViewButton.hidden = self.passwordTF.text?NO:YES;
    
}
- (IBAction)backButton:(id)sender {
    
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)showNotifyButtonClicked:(id)sender
{
    
    UIStoryboard *mainStroyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    NotificationViewController *notificationVC = [[NotificationViewController alloc]init];
    notificationVC.notifyArray = [_notification mutableCopy];
    notificationVC = [mainStroyBoard instantiateViewControllerWithIdentifier:@"NotificationViewController"];
    
    [self.navigationController pushViewController:notificationVC animated:nil];
}

-(void)endEditing {
    [self.view endEditing:YES];
}
#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.mainSettingsArray.count;
}

- (NSInteger)tableView:(SKSTableView *)tableView numberOfSubRowsAtIndexPath:(NSIndexPath *)indexPath
{
    NSArray *contentArray =  [self.mainSettingsArray objectAtIndex:indexPath.row];
    return contentArray.count;
}

- (BOOL)tableView:(SKSTableView *)tableView shouldExpandSubRowsOfCellAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == selectedSection && indexPath.row == selectedRow)
    {
        return YES;
    }
    
    return NO;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"SKSTableViewCell";
    
    SKSTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (!cell)
        cell = [[SKSTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    cell.textLabel.text = [self.mainSettingsHeaderArray objectAtIndex:indexPath.row];
    //cell.textLabel.textColor = [[ThemeManager sharedManger]colorFromKey:TITLE_TEXT_COLOR];
    cell.textLabel.font = [UIFont boldSystemFontOfSize:16];
    //cell.backgroundColor = [UIColor colorWithRed:51/255.0 green:171/255.0 blue:237/255.0 alpha:1];
    cell.expandable = YES;
    return cell;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForSubRowAtIndexPath:(NSIndexPath *)indexPath
{
    //SETTINGS_CELL
    selectedSection = (int)indexPath.section;
    selectedRow = (int)indexPath.row;
    
    static NSString *CellIdentifier = @"SETTINGS_CELL";
    AppSettingsCell *cell =  (AppSettingsCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (!cell)
        cell = [[AppSettingsCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    NSArray *contentArray =  [self.mainSettingsArray objectAtIndex:indexPath.row];
    NSDictionary *dataDict = [contentArray objectAtIndex:indexPath.subRow-1];
    [cell setData:dataDict];
    cell.backgroundColor = [UIColor colorWithRed:51/255.0 green:171/255.0 blue:237/255.0 alpha:1];
    
    return cell;
}

- (CGFloat)tableView:(SKSTableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50.0f;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 1) {
        //  self.popupView.hidden = NO;
        //  self.passwordView.hidden = NO;
        //  [self.settingsTableView collapseCurrentlyExpandedIndexPaths];
        NSLog(@"INDEX ROOT %ld ",(long)indexPath.row);
    }
    
}

- (void)tableView:(SKSTableView *)tableView didSelectSubRowAtIndexPath:(NSIndexPath *)indexPath
{
    UIStoryboard *mainStroyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    NSLog(@"INDEX %ld ",(long)indexPath.row);
    NSLog(@"INDEXSUB %ld ",(long)indexPath.subRow);
    if (indexPath.row == 0)
    {
        [AppDelegate appDelegate].isUserInAdvanceSettings = NO;
//        if (indexPath.subRow == 1) {
//            
//            TankSettingVC *TankSettingVC = [mainStroyBoard instantiateViewControllerWithIdentifier:@"TankSettingVC"];
//             [self.navigationController pushViewController:TankSettingVC animated:YES];
//        }
         if (indexPath.subRow == 1) {
            ChangePasswordVC *changePasswordVC = [mainStroyBoard instantiateViewControllerWithIdentifier:@"ChangePasswordVC"];
            [self.navigationController pushViewController:changePasswordVC animated:YES];
          
            
            
            
            
        }
        else if (indexPath.subRow == 2)
        {
            
            SystemTimeVC *systemTime = [mainStroyBoard instantiateViewControllerWithIdentifier:@"SystemTimeVC"];
            [self.navigationController pushViewController:systemTime animated:YES];
        }
        
        else if (indexPath.subRow ==3) {
            FirmwareUpgradeVC *firmwareVC = [mainStroyBoard instantiateViewControllerWithIdentifier:@"FirmwareUpgradeVC"];
            [self.navigationController pushViewController:firmwareVC animated:YES];
        }
        else if (indexPath.subRow == 6) {
        }
        
    }
    
    
    else if (indexPath.row == 1) {
        [AppDelegate appDelegate].isUserInAdvanceSettings = YES;
        
        if (indexPath.subRow == 1) {
            RegisteredUserVC *registrationVC = [mainStroyBoard instantiateViewControllerWithIdentifier:@"RegisteredUserVC"];
            [self.navigationController pushViewController:registrationVC animated:YES];
//            ControlSettingsVC *controlSettingVC = [mainStroyBoard instantiateViewControllerWithIdentifier:@"ControlSettingsVC"];
//            [self.navigationController pushViewController:controlSettingVC animated:YES];
            
            
            
            
            
        }        if (indexPath.subRow == 2) {
            
            
            
        }
        if (indexPath.subRow == 3) {
            
            SystemSettingVC *systemSettingVC = [mainStroyBoard instantiateViewControllerWithIdentifier:@"SystemSettingVC"];
            [self.navigationController pushViewController:systemSettingVC animated:YES];
            
        }
        else if (indexPath.subRow == 4) {
            // HSVCRegisteredDevices *registeredDevicesVC = [mainStroyBoard instantiateViewControllerWithIdentifier:@"HSVCRegisteredDevices"];
            // [self.navigationController pushViewController:registeredDevicesVC animated:YES];
        }
        else if (indexPath.subRow == 5) {
            // HSVCFirmwareUpgrade *firmwareUpgradeVC = [mainStroyBoard instantiateViewControllerWithIdentifier:@"HSVCFirmwareUpgrade"];
            // [self.navigationController pushViewController:firmwareUpgradeVC animated:YES];
        }
        else if (indexPath.subRow == 6) {
            // HSVCSystemHealth *systemHealthVC = [mainStroyBoard instantiateViewControllerWithIdentifier:@"HSVCSystemHealth"];
            // [self.navigationController pushViewController:systemHealthVC animated:YES];
        }
        else if (indexPath.subRow == 7) {
            // HSVCDateAndTime *dateAndTimeVC = [mainStroyBoard instantiateViewControllerWithIdentifier:@"HSVCDateAndTime"];
            // [self.navigationController pushViewController:dateAndTimeVC animated:YES];
        }
        
        
        else if (indexPath.subRow == 8) {
            // HSVCRestoreFactorySettings *restoreFactorySettingsVC = [mainStroyBoard instantiateViewControllerWithIdentifier:@"HSVCRestoreFactorySettings"];
            //  [self.navigationController pushViewController:restoreFactorySettingsVC animated:YES];
        }
        else if (indexPath.subRow == 9) {
            
            //  HSVCDiagnosis *diagnosisVC = [mainStroyBoard instantiateViewControllerWithIdentifier:@"HSVCDiagnosis"];
            // [self.navigationController pushViewController:diagnosisVC animated:YES];
        }
        else if (indexPath.subRow == 10) {
            
            //  HSVCQos *qosVC = [mainStroyBoard instantiateViewControllerWithIdentifier:@"HSVCQos"];
            // [self.navigationController pushViewController:qosVC animated:YES];
        }
        else if (indexPath.subRow == 11) {
            
            // HSVCBackupandRestore *backupandRestoreVC = [mainStroyBoard instantiateViewControllerWithIdentifier:@"HSVCBackupandRestore"];
            // [self.navigationController pushViewController:backupandRestoreVC animated:YES];
        }
        
        else if (indexPath.subRow == 12) {
            //     HSVCProfileSettings *profileSettingsVC = [mainStroyBoard instantiateViewControllerWithIdentifier:@"HSVCProfileSettings"];
            //  [self.navigationController pushViewController:profileSettingsVC animated:YES];
        }
        else if (indexPath.subRow == 14) {
            
            //  HSVCAdminPasswordChange *adminPasswordVC = [mainStroyBoard instantiateViewControllerWithIdentifier:@"HSVCAdminPasswordChange"];
            // [self.navigationController pushViewController:adminPasswordVC animated:YES];
        }
        else if (indexPath.subRow == 16) {
            // HSVCDeviceNetworkSettings *deviceNetworkSettingsVC = [mainStroyBoard instantiateViewControllerWithIdentifier:@"HSVCDeviceNetworkSettings"];
            //[self.navigationController pushViewController:deviceNetworkSettingsVC animated:YES];
        }
        
    }
    
    else if (indexPath.row == 2)
        
    {
        
         if (indexPath.subRow == 1) {
             TankSettingVC *TankSettingVC = [mainStroyBoard instantiateViewControllerWithIdentifier:@"TankSettingVC"];
            [self.navigationController pushViewController:TankSettingVC animated:YES];
        }
        
        else if (indexPath.subRow == 2) {
            PumpSettingVC *pumpSettingVC = [mainStroyBoard instantiateViewControllerWithIdentifier:@"PumpSettingVC"];
            [self.navigationController pushViewController:pumpSettingVC animated:YES];
        }
        
        

        
    }
}

#pragma mark - Admin Authentication -
- (IBAction)viewPaswordBtnClicked:(id)sender {
    if (self.passwordTF.text) {
        [self.passwordTF setSecureTextEntry:NO];
    }
}

- (IBAction)hidePasswordButtonClicked:(id)sender {
    if (self.passwordTF.text) {
        [self.passwordTF setSecureTextEntry:YES];
    }
}

- (IBAction)cancelButtonClicked:(id)sender {
    self.passwordTF.text = nil;
    [self.popupView setHidden:YES];
}

#pragma mark - setting length for texfields
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    //NSString *newString = [self.serialNumTF.text ////stringByReplacingCharactersInRange:range withString:string];
    //return (newString.length<=12);
    if(textField == _passwordTF) {
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        return (newLength >12) ? NO : YES;
    }
    return 0;
}


- (IBAction)okButtonClicked:(id)sender {
    
    if (self.passwordTF.text.length <= 0) {
        [self.view makeToast:NSLocalizedString(@"Please enter admin password.", nil)];
        return;
    }
    else if (self.passwordTF.text.length < 6) {
        [self.view makeToast:NSLocalizedString(@"Password should contain 6 characters.", nil)];
        return;
        
    }
    
    else
    {
        [self getAdminPasswordApiCall];
        self.passwordView.hidden = YES;
        
    }
    
}




-(void)getAdminPasswordApiCall
{
    NSString *passwordString = [self getName:12 :_passwordTF];
    OrderedDictionary *requestDict = [[OrderedDictionary alloc] initWithCapacity:5];
    [requestDict insertObject:_SIDVALUE forKey:_SID atIndex:0];
    // [requestDict insertObject:_BADVALUE forKey:_BAD atIndex:1];
    [requestDict insertObject:_SADVALUE1 forKey:_SAD atIndex:1];
    [requestDict insertObject:_8212 forKey:_CMD atIndex:2];
    [requestDict insertObject:passwordString forKey:_DATA atIndex:3];
    NSError *writeError;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:requestDict options:kNilOptions error:&writeError];
    NSString *jsonStr = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    [SVProgressHUD showWithStatus:@"Please Wait.."];
    [TcpPacketClass sharedInstance].delegate = self;
    [[TcpPacketClass sharedInstance]writeOut:jsonStr second:[requestDict objectForKey:_CMD]];
  
    timer = [NSTimer scheduledTimerWithTimeInterval: 5.0 target:self selector:@selector(updateCounter:) userInfo:nil repeats: NO];    
    
}

- (void)updateCounter:(NSTimer *)theTimer {
    [theTimer invalidate];
    [SVProgressHUD dismiss];
    [self.navigationController popViewControllerAnimated:YES];
}


-(void)viewDidAppear:(BOOL)animated
{


    NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Notification"];
     _notification = [[managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
}
-(void)getAdminLoginResponse:(NSDictionary *)jsonDict {
    [SVProgressHUD dismiss];
    // NSDictionary *dataDict = [jsonDict valueForKey:_DATA];
    if ([[jsonDict valueForKey:_DATA] isEqualToString:@"0"])  {
        
        [self.popupView setHidden:YES];
        [self reloadTableViewWithData:nil];
        // [[TouchUtil getInstance] startTimer];
        
    }
    
    else if( [[jsonDict valueForKey:_DATA] isEqualToString:@"1"])
    {
        [self.view makeToast:NSLocalizedString(@"Admin password is not correct.!!", nil)];
        
    }
    
    else {
        // [self.view makeToast:NSLocalizedString(@"Admin password is not correct.!!", nil)];
    }
}



#pragma mark - Text fields text lenght validations -
-(NSString *)getName :(int)lenght :(UITextField *)texfield {
    NSMutableString *finalUserName = [texfield.text mutableCopy] ;
    
    if (texfield.text.length == lenght) {
        finalUserName = [texfield.text mutableCopy];
    }
    else {
        int count = (int)texfield.text.length;
        for (int i = count; i < lenght; i++) {
            
            [finalUserName appendString:@" "];
        }
    }
    return finalUserName;
}



- (void)reloadTableViewWithData:(NSArray *)array
{
    [self.settingsTableView refreshDataWithScrollingToIndexPath:[NSIndexPath indexPathForItem:0 inSection:0]];
}


-(void)didRecieveError:(NSError *)error {
    [SVProgressHUD dismiss];
    [self.view makeToast:NSLocalizedString(@"Socket Disconnected", nil)];
    // [[Communicator sharedInstance]setup];
}

#pragma mark - Textfield delegate -
- (IBAction)editingChanged:(id)sender {
    if(sender == self.passwordTF)
        self.passwordViewButton.hidden = ([self.passwordTF.text length] > 0) ? NO : YES;
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

#pragma mark - core data
- (NSManagedObjectContext *)managedObjectContext {
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        context = [delegate managedObjectContext];
    }
    return context;
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)viewWillDisappear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NET_WORK_CHANGE object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:SOCKET_CONNECTED object:nil];
    [super viewWillDisappear:animated];
    //[self deRegisterFromNotification];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
