//
//  SystemTimeVC.h
//  AquaSmart
//
//  Created by Neotech on 09/03/17.
//  Copyright © 2017 Mac User. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WMSBlackBoxCommands.h"
#import "TcpPacketClass.h"
#import "SVProgressHUD.h"
#import "OrderedDictionary.h"
#import "UIView+Toast.h"
#import "AppDelegate.h"
#import "Reachability.h"
#import "MainViewController.h"
#import "MVYSideMenuController.h"
@interface SystemTimeVC : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *systemDateTime;
@property (weak, nonatomic) IBOutlet UILabel *deviceDateTime;
@property (weak, nonatomic) IBOutlet UIButton *syncButton;
@property(nonatomic)BOOL isresponseRecieved;
-(void)getDeviceDateTimeApiCall;
-(void)setDeviceDateTimeApiCall;
-(void)getDeviceDateTimeNow;
+(instancetype)sharedInstance;
@end
