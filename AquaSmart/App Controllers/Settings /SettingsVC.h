//
//  SettingsVC.h
//  AquaSmart
//
//  Created by Neotech on 03/04/17.
//  Copyright © 2017 Mac User. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DatabaseModel.h"
#import "RegisteredUserVC.h"
#import "SystemNetworkVC.h"
#import "AppDelegate.h"
#import "PumpSettingVC.h"
@interface SettingsVC : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *menuButton;
@property (strong) NSMutableArray *notification;
-(void)getAdminPasswordApiCall;
-(void)backClicked;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintContentViewHeight;
@property(nonatomic)BOOL isFromLeftPanel;

@end
