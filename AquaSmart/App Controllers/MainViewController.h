//
//  MainViewController.h
//  AquasmartiOS
//
//  Created by Mac User on 29/12/16.
//  Copyright © 2016 Mac User. All rights reserved.
//
#import "SplashScreenVC.h"
#import "AppDelegate.h"
#import <UIKit/UIKit.h>
#import "SettingsVC.h"
//#import "SWRevealViewController.h"
#import "WMSBlackBoxCommands.h"
#import "WMSSettingsController.h"
#import "SVProgressHUD.h"
#import "TcpPacketClass.h"
#import "UIView+Toast.h"
#import "OrderedDictionary.h"
#import "MBCircularProgressBarView.h"
#import "NotificationViewController.h"
#import "MVYSideMenuController.h"
#import "Reachability.h"
#import "TankSettingVC.h"
@interface MainViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *passwordText;
@property (weak, nonatomic) IBOutlet UILabel *systemDateTime;
@property (strong) NSArray *notification;
@property (weak, nonatomic) IBOutlet UIView *popupView;
@property (weak, nonatomic) IBOutlet UIView *passwordView;
@property(nonatomic)NetworkStatus netStatus;
@property (strong,nonatomic)Reachability *reach;
@property (assign) BOOL isWifiConnected;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *sidebarbutton;
@property (weak, nonatomic) IBOutlet UIProgressView *dropProgressView;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *notifyButton;
@property (weak, nonatomic) IBOutlet UISwitch *pumpOnOff;
@property (weak, nonatomic) IBOutlet UIImageView *dropImageView;
@property (weak, nonatomic) IBOutlet UILabel *percentageLabel;
@property (weak, nonatomic) IBOutlet MBCircularProgressBarView *progressBar;
@property (weak, nonatomic) IBOutlet UIButton *menuBUtton;
@property(weak,nonatomic)NSString *flag;
@property(strong,nonatomic) NSMutableArray *dateArray;
@property(strong,nonatomic)NSMutableArray *tankArray;
@property(strong,nonatomic)NSMutableArray *pumpStateArray;
@property(strong,nonatomic)NSMutableArray *notificationNmberArray;
@property(strong,nonatomic)NSMutableArray *reasonArray;
@property(strong,nonatomic)NSArray *sortedFetch;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintAccesslevelHeight;

@property(nonatomic)BOOL isFromLeftPanel;
#define DELAY_IN_SECONDS 3.0
//-(void)getCurrentWaterLevel;
-(void)getCurrentWaterLevelStatus;
-(void)callCurrentPumpStatusApi;
-(void)syncDateTime;
-(void)navigateToNotification;
-(void)setTopHeaderView;
-(void)menuButtonClicked;
- (void)timeOut:(NSNotification *)notification;
- (void)checkNetworkStatus:(NSNotification *)notice;
@property (weak, nonatomic) IBOutlet UILabel *tankName;
@property (weak, nonatomic) IBOutlet UILabel *tankVolume;
@property (weak, nonatomic) IBOutlet UIButton *longPresstankVolume;

@end
