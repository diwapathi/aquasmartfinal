//
//  MainViewController.m
//  AquasmartiOS
//
//  Created by Mac User on 29/12/16.
//  Copyright © 2016 Mac User. All rights reserved.
//

#import "MainViewController.h"
#import "MVYSideMenuController.h"
#import "SystemTimeVC.h"
#import "TouchUtil.h"
#import "WMSStorageManager.h"
@interface MainViewController ()<PacketReciveProtocol,UIGestureRecognizerDelegate,GCDAsyncSocketDelegate>
{

    NSTimer *timer;
    NSTimer *pumpTimer;
    DatabaseModel *datamodel;
     NSString *menuTitle;
    NSString *setTimeString;
    NSString *finalString;
    BOOL isSelf;
//    NSString *str;
//    NSString *str1;
//    
//    NSString *finalString;
//    NSArray *mutArray;
//    NSString *finStr;

}
@end

@implementation MainViewController

- (void)viewDidLoad {
    
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.5 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                  [SVProgressHUD showWithStatus:@"Loading.."];
                   [self getCurrentWaterLevelStatus];
            });
    
  self.passwordText.text = @"";
    //self.view.backgroundColor = [UIColor colorWithRed:74/255.0f green:181/255.0f blue:237/255.0f alpha:1.0];
    if(self.pumpOnOff.isOn)
    {
        [self.pumpOnOff setOnImage:[UIImage imageNamed:@"pump-1-on.png"]];
    
    }
    else
    {
      [self.pumpOnOff setOffImage:[UIImage imageNamed:@"pump-1-off.png"]];
    }
    
    
    [self.percentageLabel setHidden:YES];
    
     [self setTopHeaderViewInHomeScreen];
    [[TouchUtil getInstance]startTimer];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(timerStart:) name:TIMEOUT object:nil];
   // [self performSelector:@selector(getCurrentWaterLevel) withObject:nil afterDelay:20];

   // timer = [NSTimer scheduledTimerWithTimeInterval:30.0 target:self selector:@selector(getCurrentWaterLevel:) userInfo:nil repeats:YES];
    
    
    
//    if ([[WMSSettingsController sharedInstance]isWaterLavelFirstTime]==YES) {
//        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.5 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
//            [SVProgressHUD showWithStatus:@"Loading.."];
//            [self getCurrentWaterLevelStatus];
//        });
//        //[[WMSSettingsController sharedInstance]WaterLavelFirstTime:NO];    //to call water level one time
//    }
    
    
    if ([[WMSSettingsController sharedInstance]isGetTankSettings]==YES) {
        [self callGetTankSettingsApi];
        [[WMSSettingsController sharedInstance]GetTankSettings:NO];
    }
    
    //when first user register then show dialog of tank configuration
    if ([[WMSSettingsController sharedInstance]isTankSettingsFirstTime]==YES)
    {
     
        
        [[WMSSettingsController sharedInstance]TankSettingsFirstTime:NO];
        UIAlertView *alertView = [[UIAlertView alloc]
                                  initWithTitle:nil
                                  message:NSLocalizedString(@"Please configure Tank Settings.", nil)
                                  delegate:self
                                  cancelButtonTitle:NSLocalizedString(@"Cancel", nil)
                                  otherButtonTitles:NSLocalizedString(@"OK", nil), nil];
        
        [alertView show];
        
        
    }
    
   
   
    
    //retrieving from defaults
  NSString *WaterLevel = [[NSUserDefaults standardUserDefaults]stringForKey:@"WaterLevel"];
    if ([WaterLevel isEqualToString:@"000"])
    {
        [self.progressBar setValue:0.0];
        self.dropProgressView.progress = 0.0/10.0f;
        NSString *percentage = @"0 %";
        self.percentageLabel.text = percentage;
    }
    
    else if ([WaterLevel isEqualToString:@"025"])
    {
        self.dropProgressView.progress = 2.5/10.0f;
        // [self.dropProgressView setProgress:25.0];
        [self.progressBar setValue:25.0];
        NSString *percentage = @"25 %";
        self.percentageLabel.text = percentage;

    
    }
    
    else if ([WaterLevel isEqualToString:@"050"])
    {
        self.dropProgressView.progress = 5.0/10.0f;
        //[self.dropProgressView setProgress:50.0];
        [self.progressBar setValue:50.0];
        NSString *percentage = @"50 %";
        self.percentageLabel.text = percentage;
        
    }
    else if ([WaterLevel isEqualToString:@"075"])
    {
        self.dropProgressView.progress = 7.5/10.0f;
        //[self.dropProgressView setProgress:75.0];
        [self.progressBar setValue:75.0];
        NSString *percentage = @"75 %";
        self.percentageLabel.text = percentage;
        
    }
    else if ([WaterLevel isEqualToString:@"100"])
    {
        self.dropProgressView.progress = 10.0/10.0f;
        // [self.dropProgressView setProgress:100.0];
        [self.progressBar setValue:100.0];
        NSString *percentage = @"100 %";
        self.percentageLabel.text = percentage;
        
    }

    NSString *tankName = [[NSUserDefaults standardUserDefaults]stringForKey:@"TankName"];
    NSString *tankVolume = [[NSUserDefaults standardUserDefaults]stringForKey:@"TankVolume"];
    _tankName.text = tankName;
    _tankVolume.text = [tankVolume stringByAppendingString:@"  Ltrs"];
    
    [self.longPresstankVolume  setTitle: self.percentageLabel.text forState:UIControlStateNormal];
    [self.popupView setHidden:YES];
    [self.passwordView setHidden:YES];
    
    
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(endEditing)];
    [tapRecognizer setNumberOfTapsRequired:1];
    tapRecognizer.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tapRecognizer];
    
#pragma mark-code for convert decimal to hexadecimal:-
//    mutArray=[NSArray alloc];
//    finStr = @"";
//    
//    mutArray=[str componentsSeparatedByString:@"-"];
//    for (int i=0; i<=mutArray.count-1; i++) {
//        NSString *hexStr;
//        str1=[mutArray objectAtIndex:i];
//        hexStr=[NSString stringWithFormat:@"%x",[str1 intValue]];
//        if (hexStr.length<2) {
//            hexStr=[@"0" stringByAppendingString:hexStr];
//        }
//       
//
//        if(i<mutArray.count-1)
//            {
//                 finalString=[hexStr stringByAppendingString:@":"];
//            }
//        
//        else
//        {
//            finalString= hexStr;
//
//
//        
//        }
//        
//        finStr = [finStr stringByAppendingString:finalString];
//    }
//    NSLog(@"%@",finStr);
//    

    
   
     datamodel = [[DatabaseModel alloc]init];
    NSDate *date = [NSDate date];
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    [dateFormatter setDateFormat:@"dd-MM-yy HH:mm:SS"];
    NSLog(@"%@",[dateFormatter stringFromDate:[NSDate date]]);
    NSString *dateString = [dateFormatter stringFromDate:date];
    NSString *formattedDateString = dateString;
    //10-03-17-11-55-00
    NSRange range = NSMakeRange(8,1);
    NSRange range1 = NSMakeRange(11,1);
    NSRange range2 = NSMakeRange(14, 1);
    
    NSString *newText = [formattedDateString stringByReplacingCharactersInRange:range withString:@"-"];
    NSString *newText1 = [newText stringByReplacingCharactersInRange:range1 withString:@"-"];
    setTimeString = [newText1 stringByReplacingCharactersInRange:range2 withString:@"-"];
    NSLog(@"%@",setTimeString);
    /*_dateArray = [[NSMutableArray alloc]init];
    _tankArray = [[NSMutableArray alloc]init];
    _pumpStateArray = [[NSMutableArray alloc]init];
    _reasonArray = [[NSMutableArray alloc]init];
      _notificationNmberArray = [[NSMutableArray alloc]init];
     */
    [super viewDidLoad];
    [self.notifyButton setAction:@selector(navigateToNotification)];
  
    //[_pumpOnOff setOn:NO animated:YES];
    [self.dropProgressView setTrackTintColor:[UIColor whiteColor]];
    [self.dropProgressView setProgressTintColor:[UIColor colorWithRed:0/255.0f green:128/255.0f blue:255/255.0f alpha:1.0]];
    self.dropProgressView.transform = CGAffineTransformMakeRotation(-M_PI_2);
    UILongPressGestureRecognizer *longPressGesture = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongPress:)];
    longPressGesture.delegate = self;
    [self.dropProgressView addGestureRecognizer:longPressGesture];
   // self.pumpOnOff.on = NO;

    if ([[WMSSettingsController sharedInstance]isSyncFirstTime]==YES) {
        [self setDeviceDateTimeApiCall];
        [[WMSSettingsController sharedInstance]SyncFirstTime:NO];
    }

    }

- (void)timerStart:(NSNotification *)notification{
    
    [self getCurrentWaterLevelStatus];
}


- (IBAction)showTankVolume:(id)sender {



    NSString *tankVolume = [[NSUserDefaults standardUserDefaults]stringForKey:@"TankVolume"];
    NSInteger tankvolume = [tankVolume integerValue];
    NSInteger waterPercentage = [self.percentageLabel.text integerValue];
    NSInteger result = (tankvolume*waterPercentage)/100;
    NSString *showResult = [NSString stringWithFormat:@"%ld", (long)result];
    NSString *tankResultVolume = [showResult copy];
    [self.longPresstankVolume setTitle:[tankResultVolume stringByAppendingString:@"  Ltrs"] forState:UIControlStateNormal];
    //call method after 2 second
   [self performSelector:@selector(buttonClicked) withObject:nil afterDelay:2.0];

   
}

- (IBAction)hideTankVolume:(id)sender {
    
    [self.longPresstankVolume setTitle:self.percentageLabel.text forState:UIControlStateNormal];
    
}

-(void)buttonClicked{
    [self.longPresstankVolume setTitle:self.percentageLabel.text forState:UIControlStateNormal];

}


-(void)getCurrentWaterLevel:(NSTimer *)thetimer
{
    //[thetimer invalidate];
    [SVProgressHUD dismiss];
    
   [self getCurrentWaterLevelStatus];

}
#pragma set timesync packet
-(void)setDeviceDateTimeApiCall
{
    NSString *gmtString = @"                              ";
    NSUInteger length = [gmtString length];
    NSLog(@"%lu",(unsigned long)length);
    
    OrderedDictionary *dataDict = [[OrderedDictionary alloc]initWithCapacity:3];
    [dataDict insertObject:setTimeString forKey:_01 atIndex:0];
    [dataDict insertObject:@"                              " forKey:_02 atIndex:1];
    
    OrderedDictionary *requestDict = [[OrderedDictionary alloc]initWithCapacity:5];
    [requestDict insertObject:_SIDVALUE forKey:_SID atIndex:0];
    //[requestDict insertObject:_BADVALUE forKey:_BAD atIndex:1];
    [requestDict insertObject:_SADVALUE forKey:_SAD atIndex:1];
    [requestDict insertObject:_8102 forKey:_CMD atIndex:2];
    [requestDict insertObject:dataDict forKey:_DATA atIndex:3];
    NSError *writeError;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:requestDict options:kNilOptions error:&writeError];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
   // [SVProgressHUD showWithStatus:@"Syncing Time"];
    [TcpPacketClass sharedInstance].delegate = self;
    [[TcpPacketClass sharedInstance]writeOut:jsonString second:[requestDict objectForKey:_CMD]];
    timer = [NSTimer scheduledTimerWithTimeInterval:3.0 target:self selector:@selector(sendtimeDateAgain:) userInfo:nil repeats: NO];
    
}

- (void)sendtimeDateAgain:(NSTimer *)theTimer {
    
    [theTimer invalidate];
    //[SVProgressHUD dismiss];
    [self setDeviceDateTimeApiCall];

}
#pragma mark- response of device date time here
-(void)syncDateandTimeResponse:(NSDictionary *)jsonDict
{
    
    [timer invalidate];
   // [SVProgressHUD dismiss];
    if (jsonDict) {
        [SVProgressHUD dismiss];
        if ([[jsonDict valueForKey:_DATA]isEqualToString:@"0"]) {
            //[self.view makeToast:NSLocalizedString(@"Sync Successfully", nil)];
            
        }
        
        else
        {
            [self.view makeToast:NSLocalizedString(@"Unable to Sync Successfully", nil)];
            
        }
       
    }
}







- (void)checkNetworkStatus:(NSNotification *)notice
{
    
    
    _netStatus = [_reach currentReachabilityStatus];
    if (_netStatus == NotReachable)
    {
        NSLog(@"The internet is down.");
        
        // do stuff when network gone.
    }
    else if(_netStatus == ReachableViaWiFi&&_isWifiConnected)
    {
        NSLog(@"The internet is working!");
        
        // do stuff when internet comes active
        // [[TcpPacketClass sharedInstance]start];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"INTERNET_AVAILABLE" object:nil];
    }
}


-(void)menuButtonClicked{
    [[self sideMenuController]toggleMenu];
}

#pragma mark - Admin Authentication -
//- (IBAction)viewPaswordBtnClicked:(id)sender {
//    if (self.passwordText.text) {
//        [self.passwordText setSecureTextEntry:NO];
//    }
//    [self performSelector:@selector(passButtonClicked) withObject:nil afterDelay:10.0];
//   
//}

- (IBAction)hidePasswordButtonClicked:(id)sender {
    if (self.passwordText.text) {
        [self.passwordText setSecureTextEntry:NO];
    }
    [self performSelector:@selector(passButtonClicked) withObject:nil afterDelay:2.0];
}

-(void)passButtonClicked
{
    if (self.passwordText.text) {
        [self.passwordText setSecureTextEntry:YES];
    }
}

- (IBAction)cancelButtonClicked:(id)sender {
    self.passwordText.text = nil;
    [self.passwordView setHidden:YES];
}

#pragma mark - setting length for texfields
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    //NSString *newString = [self.serialNumTF.text ////stringByReplacingCharactersInRange:range withString:string];
    //return (newString.length<=12);
    if(textField == _passwordText) {
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        return (newLength >12) ? NO : YES;
    }
    return 0;
}


- (IBAction)okButtonClicked:(id)sender {
   
    if (self.passwordText.text.length <= 0) {
        [self.view makeToast:NSLocalizedString(@"Please enter admin password.", nil)];
        return;
    }
    
    else if (self.passwordText.text.length <6||self.passwordText.text.length >12) {
        [self.view makeToast:NSLocalizedString(@"Admin password length should be 6-12 character long.", nil)];
        return;
       
        
    }

    
    //    else if(![self.passwordTF.text isEqualToString:adminPass])
    //    {
    //        [self.view makeToast:NSLocalizedString(@"Password mismatch.", nil)];
    //        return;
    //
    //    }
    
    else
    {
        [self getAdminPasswordApiCall];
        self.passwordView.hidden = YES;
        
    }
    
}




-(void)getAdminPasswordApiCall
{
    NSString *passwordString = [self getName:12 :_passwordText];
    OrderedDictionary *requestDict = [[OrderedDictionary alloc] initWithCapacity:5];
    [requestDict insertObject:_SIDVALUE forKey:_SID atIndex:0];
    // [requestDict insertObject:_BADVALUE forKey:_BAD atIndex:1];
    [requestDict insertObject:_SADVALUE1 forKey:_SAD atIndex:1];
    [requestDict insertObject:_8212 forKey:_CMD atIndex:2];
    [requestDict insertObject:passwordString forKey:_DATA atIndex:3];
    NSError *writeError;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:requestDict options:kNilOptions error:&writeError];
    NSString *jsonStr = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.5 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [SVProgressHUD showWithStatus:@"Please Wait"];
    });
    self.popupView.hidden=YES;
    self.passwordView.hidden=YES;

    [TcpPacketClass sharedInstance].delegate = self;
    [[TcpPacketClass sharedInstance]writeOut:jsonStr second:[requestDict objectForKey:_CMD]];
    
    timer = [NSTimer scheduledTimerWithTimeInterval: 5.0 target:self selector:@selector(adminPacket:) userInfo:nil repeats: NO];
    
}
- (void)adminPacket:(NSTimer *)theTimer {
    int static ticks = 0;
    if (ticks<3) {
        [self getAdminPasswordApiCall];
    }
    else if (ticks>2)
    {
        [theTimer invalidate];
        [SVProgressHUD dismiss];
        self.passwordText.text = nil;
        [self.view makeToast:NSLocalizedString(@"Something went wrong Please try again.", nil)];
        ticks = -1;
    }
    ticks++;
//    [theTimer invalidate];
//    [SVProgressHUD dismiss];
//     [self.view makeToast:NSLocalizedString(@"Somthing went wrong Try again!!", nil)];
      // self.popupView.hidden=NO;
       //self.passwordView.hidden=NO;
}

- (void)pumpON:(NSTimer *)theTimer {
    
    [self.view makeToast:NSLocalizedString(@"Somthing went wrong Try again!!", nil)];
    if (_pumpOnOff.isOn) {
        _pumpOnOff.on = NO;
    }
    else
    {
        _pumpOnOff.on = YES;
    
    }
   
    [theTimer invalidate];
    [SVProgressHUD dismiss];
//    self.popupView.hidden=NO;
//    self.passwordView.hidden=NO;
}



-(void)getAdminLoginResponse:(NSDictionary *)jsonDict {
    
    [timer invalidate];
    [SVProgressHUD dismiss];
   
    [self.passwordView setHidden:YES];
    [self.popupView setHidden:YES];
    

    // NSDictionary *dataDict = [jsonDict valueForKey:_DATA];
    if ([[jsonDict valueForKey:_DATA] isEqualToString:@"0"])  {
        
        UIStoryboard *mainStroyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        SettingsVC *settingsVC = [mainStroyBoard instantiateViewControllerWithIdentifier:@"SettingsVC"];
        settingsVC.isFromLeftPanel = YES;
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:settingsVC];
        [[self sideMenuController] changeContentViewController:navigationController closeMenu:YES];
                //[self reloadTableViewWithData:nil];
       // [[TouchUtil getInstance] startTimer];
        
    }
    
    else if( [[jsonDict valueForKey:_DATA] isEqualToString:@"1"])
    {
        [self.view makeToast:NSLocalizedString(@"Admin password is not correct.!!", nil)];
        
    }
    
    else {
        // [self.view makeToast:NSLocalizedString(@"Admin password is not correct.!!", nil)];
    }
}

#pragma mark - Text fields text lenght validations -
-(NSString *)getName :(int)lenght :(UITextField *)texfield {
    NSMutableString *finalUserName = [texfield.text mutableCopy] ;
    
    if (texfield.text.length == lenght) {
        finalUserName = [texfield.text mutableCopy];
    }
    else {
        int count = (int)texfield.text.length;
        for (int i = count; i < lenght; i++) {
            
            [finalUserName appendString:@" "];
        }
    }
    return finalUserName;
}



//#setting notification button state
//-(void)setButtonsState:(NSString *)titlestring :(BOOL)state {
//
//    //if notification do not occur
//    if (![[WMSSettingsController sharedInstance]getNotificationStatus]) {
//        [self.notificationImageView setImage:[UIImage imageNamed:[[ThemeManager sharedManger]getImageNameForKey:NOTIFICATION_UNSELECTED]]];
//    }
//    else {
//        [self.notificationImageView setImage:[UIImage imageNamed:[[ThemeManager sharedManger]getImageNameForKey:ALERT_NOTIFY]]];
//    }
//    
//    //    self.notificationImageView.image = [UIImage imageNamed:[[ThemeManager sharedManger]getImageNameForKey:NOTIFICATION_UNSELECTED]];
//    self.bellImageView.image = [UIImage imageNamed:[[ThemeManager sharedManger]getImageNameForKey:BELL_UNSELECTED]];
//   
//    isNotification = !state;
//    
//    [[AppDelegate appDelegate].superViewController updateHeadrLabel:titlestring];
//    [self.view endEditing:YES];
//    self.profileCollectionView.hidden = state;
//    self.presetTableView.hidden = state;
//    if (dropDown != nil) {
//        [self hideDropDwonMenu:self.accessLevelBtn];
//    }
//    menuTitle = titlestring;
//}






-(void)setTopHeaderViewInHomeScreen {
    NSDictionary *navDict = @{
                              BACK_SELECTOR :@"backButtonClicked:",
                              TITLE_TEXT :NSLocalizedString(@"Home", nil),
                              BACK_IMAGE :@"BLUE_S_MENU",
                              IS_BACK_HIDDEN:@"No",
                              NOTIFY_SELECTOR:@"Yes",NOTIFY_SELECTOR_HIDDEN:@"navigateToNotificationVC:"
                              };
    [[AppDelegate appDelegate].superVC setHeaderViewData:navDict];
}


-(void)navigateToNotification
{
    UIStoryboard *mainStroyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    NotificationViewController *settingsVC = [mainStroyBoard instantiateViewControllerWithIdentifier:@"NotificationViewController"];
   // settingsVC.isFromLeftPanel = YES;
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:settingsVC];
    [[self sideMenuController] changeContentViewController:navigationController closeMenu:YES];

}

#pragma mark  view did appear
-(void)viewWillAppear:(BOOL)animated {
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(closetoggled) name:@"closetoggle" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(opentoggled) name:@"opentoggle" object:nil];
   

    [self setTopHeaderViewInHomeScreen];

    //[self callGetTankSettingsApi];
    //[[SystemTimeVC sharedInstance]setDeviceDateTimeApiCall];
}

#pragma mark - get tank settings
-(void)callGetTankSettingsApi
{
    OrderedDictionary *requestDict = [[OrderedDictionary alloc]initWithCapacity:3];
    [requestDict insertObject:_SIDVALUE forKey:_SID atIndex:0];
    [requestDict insertObject:_SADVALUE forKey:_SAD atIndex:1];
    [requestDict insertObject:_8502 forKey:_CMD atIndex:2];
    NSError *writeError;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:requestDict options:kNilOptions error:&writeError];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
   // [SVProgressHUD showWithStatus:@"Getting Tank Settings.."];
    [TcpPacketClass sharedInstance].delegate = self;
    [[TcpPacketClass sharedInstance]writeOut:jsonString second:[requestDict objectForKey:_CMD]];
    timer = [NSTimer scheduledTimerWithTimeInterval:5.0 target:self selector:@selector(sendTankSettingPacketAgain:) userInfo:nil repeats: NO];
}

- (void)sendTankSettingPacketAgain:(NSTimer *)theTimer {
    [theTimer invalidate];
    //[SVProgressHUD dismiss];
    [self callGetTankSettingsApi];
}

#pragma mark - get tank settings response
-(void)didRecieveGetTankSettingResponse:(NSDictionary *)jsonDict
{
    [timer invalidate];
    //[SVProgressHUD dismiss];
   // NSDictionary *dataDict = [jsonDict valueForKey:_DATA];
    if([[jsonDict valueForKey:@"data"] isKindOfClass:[NSString class]])
   {
       
       
       UIAlertView *alertView = [[UIAlertView alloc]
                                 initWithTitle:nil
                                 message:NSLocalizedString(@"Please configure Tank Settings.", nil)
                                 delegate:self
                                 cancelButtonTitle:NSLocalizedString(@"Cancel", nil)
                                 otherButtonTitles:NSLocalizedString(@"OK", nil), nil];
       
       [alertView show];


   }

   else if([[jsonDict valueForKey:_DATA] isKindOfClass:[NSDictionary class]])
    {
        NSDictionary *dataDict = [jsonDict valueForKey:_DATA];
        [[NSUserDefaults standardUserDefaults]setObject:[dataDict valueForKey:_03] forKey:@"TankName"];
        [[NSUserDefaults standardUserDefaults]setObject:[dataDict valueForKey:_04] forKey:@"TankVolume"];
   // NSString *tankName = [dataDict valueForKey:_03];
    //NSString *tankVolume = [dataDict valueForKey:_04];
        NSString *tankName = [[NSUserDefaults standardUserDefaults]stringForKey:@"TankName"];
        NSString *tankVolume = [[NSUserDefaults standardUserDefaults]stringForKey:@"TankVolume"];
    _tankName.text = tankName;
    _tankVolume.text = [tankVolume stringByAppendingString:@"  Ltrs"];
        
       // [self.view makeToast:NSLocalizedString(@"Tank settings recieved..", nil)];
    }
    
    else
    {
    
    [self.view makeToast:NSLocalizedString(@"Error Occurred.", nil)];
    }
    
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 0) {
       
        [alertView dismissWithClickedButtonIndex:0 animated:YES];
        
    }
    if (buttonIndex == 1) {
        
        
        UIStoryboard *mainStroyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        TankSettingVC *tankSettingsVC = [mainStroyBoard instantiateViewControllerWithIdentifier:@"TankSettingVC"];
        [self.navigationController pushViewController:tankSettingsVC animated:YES];
    }
}

//water percentage is in percentage
-(void)showCircularProgressBar:(int)waterPercentage {
    
    self.progressBar.value = waterPercentage;
    [self.dropProgressView setProgress:(waterPercentage/100.0)];
    self.percentageLabel.text = [[NSString stringWithFormat:@"%d",(int)self.progressBar.value] stringByAppendingString:@"%"];
    
}


#pragma mark- to hide keyboard
-(void)endEditing {
    [self.view endEditing:YES];
}


#pragma mark - Open and close left panel -
-(void)closetoggled {
    NSDictionary *navDict = @{
                              TITLE_TEXT :(menuTitle == nil)?NSLocalizedString(@"Home", nil):menuTitle,
                              BACK_IMAGE :@"BLUE_S_MENU",
                              IS_BACK_HIDDEN:@"No",
                              NOTIFY_SELECTOR:@"Yes"
                              };
    [[AppDelegate appDelegate].superVC setHeaderViewData:navDict];
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"SETTINGS"]==YES) {
        [self.popupView setHidden:NO];
        [self.passwordView setHidden:NO];
        //[self showAlertView];
        [[NSUserDefaults standardUserDefaults] setBool:FALSE forKey:@"SETTINGS"];
         //NSString *value = [[NSUserDefaults standardUserDefaults] stringForKey:@"SETTINGS"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}

-(void)opentoggled {
    // menuTitle = [AppDelegate appDelegate].superViewController.titleLabel.text;
    NSDictionary *navDict = @{
                              TITLE_TEXT :NSLocalizedString(@"Home", nil),
                              BACK_IMAGE :@"BackImage",
                              IS_BACK_HIDDEN:@"No",
                              NOTIFY_SELECTOR:@"Yes"
                              };
    [[AppDelegate appDelegate].superVC setHeaderViewData:navDict];
}


- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma  mark - handle long press
-(void)handleLongPress:(UILongPressGestureRecognizer *)gestureRecognizer
{
    if (gestureRecognizer.state != UIGestureRecognizerStateEnded) {
        return;
    }
  //  int currentTotalVolume = [[WMSSettingsController sharedInstance] getOhtTankCurrentVolume] + [[WMSSettingsController sharedInstance] getUgtTankCurrentVolume];
    
   // self.percentageLabel.text = [NSString stringWithFormat:@"%d ltrs",currentTotalVolume];
    [self.percentageLabel sizeToFit];
    
    int64_t delayInSeconds = DELAY_IN_SECONDS;
    
    dispatch_time_t pop_time = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
    dispatch_after(pop_time, dispatch_get_main_queue(), ^ (void) {
        
        self.percentageLabel.text = [[NSString stringWithFormat:@"%d",(int)self.progressBar.value] stringByAppendingString:@"%"];
        
    });
    
  
}
#pragma mark - get current water level
    -(void)getCurrentWaterLevelStatus {
    
        
        OrderedDictionary *requestDict = [[OrderedDictionary alloc]initWithCapacity:3];
        [requestDict insertObject:_SIDVALUE forKey:_SID atIndex:0];
        [requestDict insertObject:_SADVALUE forKey:_SAD atIndex:1];
        [requestDict insertObject:_8519 forKey:_CMD atIndex:2];
        NSError *writeError;
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:requestDict options:kNilOptions error:&writeError];
        NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
       
        [TcpPacketClass sharedInstance].delegate = self;
       [[TcpPacketClass sharedInstance]writeOut:jsonString second:[requestDict objectForKey:_CMD]];
        timer = [NSTimer scheduledTimerWithTimeInterval:3.0 target:self selector:@selector(sendPacketAgain:) userInfo:nil repeats: NO];
    }



-(void)sendPacketAgain:(NSTimer *)theTimer
{
    
    int static ticks = 0;
    if (ticks<4) {
        [self getCurrentWaterLevelStatus];
    }
    else if (ticks>3)
    {
        [theTimer invalidate];
        [SVProgressHUD dismiss];
        [self.view makeToast:NSLocalizedString(@"Something went wrong.Please try again.", nil)];
        ticks = -1;
    }
    ticks++;
    
    
}




//sending water level status packet again
//-(void)sendPacketAgain:(NSTimer *)thetimer
//{
//
//    [thetimer invalidate];
//   // [SVProgressHUD dismiss];
//
//    [self getCurrentWaterLevelStatus];
//
//    
//
//
//}




-(void)getCurrentWaterLevelStatus:(NSDictionary *)jsonDict
{
    [timer invalidate];
   [SVProgressHUD dismiss];
    NSLog(@"RESPONSE");
    NSDictionary *dataDict = [jsonDict valueForKey:_DATA];
    if (![[jsonDict valueForKey:_DATA] isKindOfClass:[NSString class]]) {
        
        
        [[NSUserDefaults standardUserDefaults]setObject:[dataDict valueForKey:_02] forKey:@"WaterLevel"];
  
        if ([[dataDict valueForKey:_04] isEqualToString:@"0"]) {
            
            
            self.pumpOnOff.on = NO;
            [self.pumpOnOff setOnImage:[UIImage imageNamed:@"pump-1-off.png"]];
            
            }
        else{
            self.pumpOnOff.on = YES;
            [self.pumpOnOff setOnImage:[UIImage imageNamed:@"pump-1-on.png"]];
        }

        if ([[dataDict valueForKey:_02] isEqualToString:@"000"]) {
            
//            UIStoryboard *mainStroyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//            
            [self.progressBar setValue:0.0];
            self.dropProgressView.progress = 0.0/10.0f;
            NSString *percentage = @"0 %";
            self.percentageLabel.text = percentage;
            
        }
        
        else if ([[dataDict valueForKey:_02] isEqualToString:@"005"])
        {
            self.dropProgressView.progress = 0.5/10.0f;
            // [self.dropProgressView setProgress:25.0];
            [self.progressBar setValue:5.0];
            NSString *percentage = @"5 %";
            self.percentageLabel.text = percentage;
            
            
        }
        else if ([[dataDict valueForKey:_02] isEqualToString:@"010"])
        {
            self.dropProgressView.progress = 1.0/10.0f;
            // [self.dropProgressView setProgress:25.0];
            [self.progressBar setValue:10.0];
            NSString *percentage = @"10 %";
            self.percentageLabel.text = percentage;
            
            
        }else if ([[dataDict valueForKey:_02] isEqualToString:@"015"])
        {
            self.dropProgressView.progress = 1.5/10.0f;
            // [self.dropProgressView setProgress:25.0];
            [self.progressBar setValue:15.0];
            NSString *percentage = @"15 %";
            self.percentageLabel.text = percentage;
            
            
        }else if ([[dataDict valueForKey:_02] isEqualToString:@"020"])
        {
            self.dropProgressView.progress = 2.0/10.0f;
            // [self.dropProgressView setProgress:25.0];
            [self.progressBar setValue:20.0];
            NSString *percentage = @"20 %";
            self.percentageLabel.text = percentage;
            
            
        }
        
        
        
        else if ([[dataDict valueForKey:_02] isEqualToString:@"025"])
        {
             self.dropProgressView.progress = 2.5/10.0f;
           // [self.dropProgressView setProgress:25.0];
            [self.progressBar setValue:25.0];
            NSString *percentage = @"25 %";
            self.percentageLabel.text = percentage;
           
            
        }
        
        else if ([[dataDict valueForKey:_02] isEqualToString:@"030"])
        {
            self.dropProgressView.progress = 3.0/10.0f;
            // [self.dropProgressView setProgress:25.0];
            [self.progressBar setValue:30.0];
            NSString *percentage = @"30 %";
            self.percentageLabel.text = percentage;
            
            
        }
        
        else if ([[dataDict valueForKey:_02] isEqualToString:@"035"])
        {
            self.dropProgressView.progress = 3.5/10.0f;
            // [self.dropProgressView setProgress:25.0];
            [self.progressBar setValue:35.0];
            NSString *percentage = @"35 %";
            self.percentageLabel.text = percentage;
            
            
        }
        else if ([[dataDict valueForKey:_02] isEqualToString:@"040"])
        {
            self.dropProgressView.progress = 4.0/10.0f;
            // [self.dropProgressView setProgress:25.0];
            [self.progressBar setValue:35.0];
            NSString *percentage = @"35 %";
            self.percentageLabel.text = percentage;
            
            
        }
        else if ([[dataDict valueForKey:_02] isEqualToString:@"045"])
        {
            self.dropProgressView.progress = 4.5/10.0f;
            // [self.dropProgressView setProgress:25.0];
            [self.progressBar setValue:45.0];
            NSString *percentage = @"45 %";
            self.percentageLabel.text = percentage;
            
            
        }
        
        
        else if ([[dataDict valueForKey:_02] isEqualToString:@"050"])
                 {
                      self.dropProgressView.progress = 5.0/10.0f;
                     //[self.dropProgressView setProgress:50.0];
                     [self.progressBar setValue:50.0];
                     NSString *percentage = @"50 %";
                     self.percentageLabel.text = percentage;

                 }
        
        else if ([[dataDict valueForKey:_02] isEqualToString:@"055"])
        {
            self.dropProgressView.progress = 5.5/10.0f;
            // [self.dropProgressView setProgress:25.0];
            [self.progressBar setValue:55.0];
            NSString *percentage = @"55 %";
            self.percentageLabel.text = percentage;
            
            
        }
        else if ([[dataDict valueForKey:_02] isEqualToString:@"060"])
        {
            self.dropProgressView.progress = 6.0/10.0f;
            // [self.dropProgressView setProgress:25.0];
            [self.progressBar setValue:60.0];
            NSString *percentage = @"60 %";
            self.percentageLabel.text = percentage;
            
            
        }
        
        else if ([[dataDict valueForKey:_02] isEqualToString:@"065"])
        {
            self.dropProgressView.progress = 6.5/10.0f;
            // [self.dropProgressView setProgress:25.0];
            [self.progressBar setValue:65.0];
            NSString *percentage = @"65 %";
            self.percentageLabel.text = percentage;
            
            
        }
        
        else if ([[dataDict valueForKey:_02] isEqualToString:@"070"])
        {
            self.dropProgressView.progress = 7.0/10.0f;
            // [self.dropProgressView setProgress:25.0];
            [self.progressBar setValue:70.0];
            NSString *percentage = @"70 %";
            self.percentageLabel.text = percentage;
            
            
        }


        else if ([[dataDict valueForKey:_02] isEqualToString:@"075"])
        {
             self.dropProgressView.progress = 7.5/10.0f;
            //[self.dropProgressView setProgress:75.0];
            [self.progressBar setValue:75.0];
            NSString *percentage = @"75 %";
            self.percentageLabel.text = percentage;
    
        }
        
        else if ([[dataDict valueForKey:_02] isEqualToString:@"080"])
        {
            self.dropProgressView.progress = 8.0/10.0f;
            //[self.dropProgressView setProgress:75.0];
            [self.progressBar setValue:80.0];
            NSString *percentage = @"80 %";
            self.percentageLabel.text = percentage;
            
        }
        
        else if ([[dataDict valueForKey:_02] isEqualToString:@"085"])
        {
            self.dropProgressView.progress = 8.5/10.0f;
            //[self.dropProgressView setProgress:75.0];
            [self.progressBar setValue:85.0];
            NSString *percentage = @"85 %";
            self.percentageLabel.text = percentage;
            
        }

        else if ([[dataDict valueForKey:_02] isEqualToString:@"090"])
        {
            self.dropProgressView.progress = 9.0/10.0f;
            //[self.dropProgressView setProgress:75.0];
            [self.progressBar setValue:90.0];
            NSString *percentage = @"90 %";
            self.percentageLabel.text = percentage;
            
        }
        
        else if ([[dataDict valueForKey:_02] isEqualToString:@"095"])
        {
            self.dropProgressView.progress = 9.5/10.0f;
            //[self.dropProgressView setProgress:75.0];
            [self.progressBar setValue:95.0];
            NSString *percentage = @"95 %";
            self.percentageLabel.text = percentage;
            
        }
        
        else if ([[dataDict valueForKey:_02] isEqualToString:@"100"])
        {
             self.dropProgressView.progress = 10.0/10.0f;
           // [self.dropProgressView setProgress:100.0];
            [self.progressBar setValue:100.0];
            NSString *percentage = @"100 %";
            self.percentageLabel.text = percentage;

        }
        [self.longPresstankVolume  setTitle: self.percentageLabel.text forState:UIControlStateNormal];
   
  
    }

}

//- (void)updateProgressBarView:(NSTimer *)timer
//{
//    static int count =0; count++;
//    
//    if (count <=10)
//    {
//        //self.updateLabel.text = [NSString stringWithFormat:@"%d %%",count*10];
//        self.dropProgressView.progress = (float)count/10.0f;
//    }
//    else
//    {
//       // [self.myt invalidate];
//        //self.myTimer = nil;
//    }
//}
//  


-(void)didRecieveError:(NSError *)error {
   
    
     [SVProgressHUD dismiss];
    [self.view makeToast:NSLocalizedString(@"Socket not connected", nil)];
   //  [[Communicator sharedInstance]setup];
    
}


//pump switch event
- (IBAction)pumpActivated:(id)sender {
    
    if (self.pumpOnOff.on) {
        
        [self callPumpOnApi];
        //[_pumpOnOff setOn:NO animated:YES];
        
        
    }
    else
    {
        //[_pumpOnOff setOn:YES animated:YES];
        [self callPumpOffApi];
    }
}


-(void)callPumpOnApi
{
    OrderedDictionary *dataDict = [[OrderedDictionary alloc]initWithCapacity:4];
    NSString *tankNumber = @"1";
    
    [dataDict insertObject:tankNumber forKey:_01 atIndex:0];
    
    OrderedDictionary *requestDict = [[OrderedDictionary alloc]initWithCapacity:5];
    [requestDict insertObject:_SIDVALUE forKey:_SID atIndex:0];
    //[requestDict insertObject:_BADVALUE forKey:_BAD atIndex:1];
    [requestDict insertObject:_SADVALUE forKey:_SAD atIndex:1];
    [requestDict insertObject:_2000 forKey:_CMD atIndex:2];
    [requestDict insertObject:dataDict forKey:_DATA atIndex:3];
    NSError *writeError;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:requestDict options:kNilOptions error:&writeError];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    [SVProgressHUD showWithStatus:@"Switching ON.."];
    [TcpPacketClass sharedInstance].delegate = self;
    [[TcpPacketClass sharedInstance]writeOut:jsonString second:[requestDict objectForKey:_CMD]];
    timer = [NSTimer scheduledTimerWithTimeInterval:8.0 target:self selector:@selector(pumpON:) userInfo:nil repeats: NO];

}


#pragma mark - pump on status response here

-(void)didRecievePumpSuccessResponse:(NSDictionary *)jsonDict
{
    
    NSMutableArray *pumpSuccessArray = [[NSMutableArray alloc]init];
    [timer invalidate];
    [SVProgressHUD dismiss];
    NSDictionary *dataDict = [jsonDict valueForKey:_DATA];
    NSString *tank = [dataDict objectForKey:@"01"];
    NSString *pumpState = [dataDict objectForKey:@"02"];
    NSInteger pumpStateInt = [pumpState integerValue];
    NSString *dateString = [dataDict objectForKey:_03];
    
    //to save date in DB in timestamp
    NSString *formattedDateString = dateString;
    //10-03-17-11-55-00
    NSRange range = NSMakeRange(8,1);
    NSRange range1 = NSMakeRange(11,1);
    NSRange range2 = NSMakeRange(14, 1);
    
    NSString *newText = [formattedDateString stringByReplacingCharactersInRange:range withString:@" "];
    NSString *newText1 = [newText stringByReplacingCharactersInRange:range1 withString:@":"];
    NSString *finalString = [newText1 stringByReplacingCharactersInRange:range2 withString:@":"];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT"]];
    NSDate *DateFromString = [dateFormatter dateFromString:finalString];
    double timestamp = [DateFromString timeIntervalSince1970];
    NSNumber *timeInMilliseconds = [NSNumber numberWithDouble:(timestamp*1000)];
    NSString *timeinMillStr = [ NSString stringWithFormat:@"%@",timeInMilliseconds];
    datamodel.date=timeinMillStr;
    datamodel.tankNum = tank;
    datamodel.isRead = @"0";
    switch (pumpStateInt) {
        case 0:
            if ([[dataDict valueForKey:_04] isEqualToString:@"1"])
                
            {
                NSString *reason1 = @"Motor trigger from Switch";
                
                //datamodel.date = date;
                datamodel.reason = reason1;
                datamodel.pumpState = @"OFF";
                
                [pumpSuccessArray addObject:datamodel];
                [[WMSStorageManager sharedInstance]savePumpResponseInDB:pumpSuccessArray];
                [self.view makeToast:NSLocalizedString(@"Motor trigger from Switch", nil)];
                
                
                
            }
            
            else if ([[dataDict valueForKey:_04]isEqualToString:@"2"])
            {
                
                NSString *reason1 = @"Motor trigger from App";
                
                
                //datamodel.date = date;
                datamodel.reason = reason1;
                datamodel.pumpState = @"OFF";
                [pumpSuccessArray addObject:datamodel];
                [[WMSStorageManager sharedInstance]savePumpResponseInDB:pumpSuccessArray];
                [self.view makeToast:NSLocalizedString(@"Motor trigger from App", nil)];
                
            }
            
            else if ([[dataDict valueForKey:_04]isEqualToString:@"3"])
            {
                
                NSString *reason1 = @"Motor trigger from Schedule";
                
                
                //datamodel.date = date;
                //datamodel.tank = tank;
                datamodel.reason = reason1;
                datamodel.pumpState = @"OFF";
                [pumpSuccessArray addObject:datamodel];
                [[WMSStorageManager sharedInstance]savePumpResponseInDB:pumpSuccessArray];
                //[self checkForDuplicates];
                
                [self.view makeToast:NSLocalizedString(@"Motor trigger from Schedule", nil)];
            }
            
            else if ([[dataDict valueForKey:_04]isEqualToString:@"4"])
            {
                
                NSString *reason1 = @"Motor trigger from Dry Run";
                
                
                //datamodel.date = date;
                //  datamodel.tank = tank;
                datamodel.reason = reason1;
                datamodel.pumpState = @"OFF";
                [pumpSuccessArray addObject:datamodel];
                [[WMSStorageManager sharedInstance]savePumpResponseInDB:pumpSuccessArray];                //[self checkForDuplicates];
                
                [self.view makeToast:NSLocalizedString(@"Motor trigger from Dry Run", nil)];
                
            }
            
            else if ([[dataDict valueForKey:_04]isEqualToString:@"5"])
            {
                
                // NSString *reason1 = @"Motor turn off due to water level 100%";
                NSString *reason1 = @"Motor trigger from Auto task";
                
                //datamodel.date = date;
                datamodel.reason = reason1;
                datamodel.pumpState = @"OFF";
                self.pumpOnOff.on = NO;
                [pumpSuccessArray addObject:datamodel];
                [[WMSStorageManager sharedInstance]savePumpResponseInDB:pumpSuccessArray];                //[self checkForDuplicates];
                
                [self.view makeToast:NSLocalizedString(@"Motor trigger from Auto task", nil)];
                
            }
            
            else if ([[dataDict valueForKey:_04]isEqualToString:@"6"])
                
                
            {
                
                NSString *reason1 = @"Motor trigger from Power Failure";
                
                
                //datamodel.date = date;
                
                datamodel.reason = reason1;
                datamodel.pumpState = @"OFF";
                [pumpSuccessArray addObject:datamodel];
                [[WMSStorageManager sharedInstance]savePumpResponseInDB:pumpSuccessArray];
                //[self checkForDuplicates];
                
                [self.view makeToast:NSLocalizedString(@"Motor trigger from Power Failure", nil)];
                
                
            }
            [_pumpOnOff setOn:NO animated:YES];
            
            break;
            
            
        case 1 :
            
            if ([[dataDict valueForKey:_04] isEqualToString:@"1"])
                
            {
                NSString *reason1 = @"Motor trigger from Switch";
                
                
                
                //datamodel.date = date;
                
                datamodel.reason = reason1;
                datamodel.pumpState = @"ON";
                [_pumpOnOff setOn:YES animated:YES];
                [pumpSuccessArray addObject:datamodel];
                [[WMSStorageManager sharedInstance]savePumpResponseInDB:pumpSuccessArray];
                //[self checkForDuplicates];
                
                [self.view makeToast:NSLocalizedString(@"Motor trigger from Switch", nil)];
            }
            
            else if ([[dataDict valueForKey:_04]isEqualToString:@"2"])
            {
                
                NSString *reason1 = @"Motor trigger from App";
                
                
                //datamodel.date = date;
                
                datamodel.reason = reason1;
                datamodel.pumpState = @"ON";
                [_pumpOnOff setOn:YES animated:YES];
                [pumpSuccessArray addObject:datamodel];
                [[WMSStorageManager sharedInstance]savePumpResponseInDB:pumpSuccessArray];
                
                
                [self.view makeToast:NSLocalizedString(@"Motor trigger from App", nil)];
                
            }
            
            else if ([[dataDict valueForKey:_04]isEqualToString:@"3"])
            {
                
                NSString *reason1 = @"Motor trigger from Schedule";
                
                
                //datamodel.date = date;
                datamodel.reason = reason1;
                datamodel.pumpState = @"ON";
                [_pumpOnOff setOn:YES animated:YES];
                [pumpSuccessArray addObject:datamodel];
                [[WMSStorageManager sharedInstance]savePumpResponseInDB:pumpSuccessArray];
                //[self checkForDuplicates];
                
                [self.view makeToast:NSLocalizedString(@"Motor trigger from Schedule", nil)];
            }
            
            else if ([[dataDict valueForKey:_04]isEqualToString:@"4"])
            {
                
                NSString *reason1 = @"Motor trigger from Dry Run";
                
                
                //datamodel.date = date;
                datamodel.reason = reason1;
                datamodel.pumpState = @"ON";
                [_pumpOnOff setOn:YES animated:YES];
                [pumpSuccessArray addObject:datamodel];
                [[WMSStorageManager sharedInstance]savePumpResponseInDB:pumpSuccessArray];
                //[self checkForDuplicates];
                
                [self.view makeToast:NSLocalizedString(@"Motor trigger from Dry Run", nil)];
                
            }
            
            
            else if ([[dataDict valueForKey:_04]isEqualToString:@"5"])
            {
                
                //NSString *reason1 = @"Motor turn off due to water level 100%";
                NSString *reason1 = @"Motor trigger from Presence Sensor";
                //datamodel.date = date;
                //datamodel.tank = tank;
                datamodel.reason = reason1;
                datamodel.pumpState = @"ON";
                [_pumpOnOff setOn:YES];
                [pumpSuccessArray addObject:datamodel];
                [[WMSStorageManager sharedInstance]savePumpResponseInDB:pumpSuccessArray];
                //[self checkForDuplicates];
                
                [self.view makeToast:NSLocalizedString(@"Motor trigger from Presence Sensor", nil)];
                
            }
            
            else if ([[dataDict valueForKey:_04]isEqualToString:@"6"])
                
                
            {
                
                NSString *reason1 = @"Motor trigger from Power Failure";
                
                
                //datamodel.date = date;
                //datamodel.tank = tank;
                datamodel.reason = reason1;
                datamodel.pumpState = @"ON";
                [_pumpOnOff setOn:YES animated:YES];
                [pumpSuccessArray addObject:datamodel];
                [[WMSStorageManager sharedInstance]savePumpResponseInDB:pumpSuccessArray];
                //[self checkForDuplicates];
                
                [self.view makeToast:NSLocalizedString(@"Motor trigger from Power Failure", nil)];
                
                
            }
            // [_pumpOnOff setOn:YES animated:YES];
            break;
            
            
        case 17:
            if ([[dataDict valueForKey:_04] isEqualToString:@"1"])
                
            {
                NSString *reason1 = @"Motor trigger from Switch";
                
                
                
                //datamodel.date = date;
                // datamodel.tank = tank;
                datamodel.reason = reason1;
                datamodel.pumpState = @"OFF";
                [pumpSuccessArray addObject:datamodel];
                [[WMSStorageManager sharedInstance]savePumpResponseInDB:pumpSuccessArray];
                //[self checkForDuplicates];
                
                [self.view makeToast:NSLocalizedString(@"Motor trigger from Switch", nil)];
            }
            
            else if ([[dataDict valueForKey:_04]isEqualToString:@"2"])
            {
                
                NSString *reason1 = @"Motor trigger from App";
                
                
                //datamodel.date = date;
                //datamodel.tank = tank;
                datamodel.reason = reason1;
                datamodel.pumpState = @"OFF";
                [pumpSuccessArray addObject:datamodel];
                [[WMSStorageManager sharedInstance]savePumpResponseInDB:pumpSuccessArray];
                //[self checkForDuplicates];
                
                [self.view makeToast:NSLocalizedString(@"Motor trigger from App", nil)];
                
            }
            
            else if ([[dataDict valueForKey:_04]isEqualToString:@"3"])
            {
                
                NSString *reason1 = @"Motor trigger from Schedule";
                
                
                //datamodel.date = date;
                //datamodel.tank = tank;
                datamodel.reason = reason1;
                datamodel.pumpState = @"OFF";
                [pumpSuccessArray addObject:datamodel];
                [[WMSStorageManager sharedInstance]savePumpResponseInDB:pumpSuccessArray];
                //[self checkForDuplicates];
                
                [self.view makeToast:NSLocalizedString(@"Motor trigger from Schedule", nil)];
            }
            
            else if ([[dataDict valueForKey:_04]isEqualToString:@"4"])
            {
                
                NSString *reason1 = @"Motor trigger from Dry Run";
                
                
                //datamodel.date = date;
                //  datamodel.tank = tank;
                datamodel.reason = reason1;
                datamodel.pumpState = @"OFF";
                [pumpSuccessArray addObject:datamodel];
                [[WMSStorageManager sharedInstance]savePumpResponseInDB:pumpSuccessArray];                //[self checkForDuplicates];
                
                [self.view makeToast:NSLocalizedString(@"Motor trigger from Dry Run", nil)];
                
            }
            
            else if ([[dataDict valueForKey:_04]isEqualToString:@"5"])
            {
                
                //NSString *reason1 = @"Motor turn off due to water level 100%";
                NSString *reason1 = @"Motor trigger from Auto task";
                
                //datamodel.date = date;
                //datamodel.tank = tank;
                datamodel.reason = reason1;
                datamodel.pumpState = @"OFF";
                [pumpSuccessArray addObject:datamodel];
                [[WMSStorageManager sharedInstance]savePumpResponseInDB:pumpSuccessArray];
                //[self checkForDuplicates];
                
                [self.view makeToast:NSLocalizedString(@"Motor trigger from Auto task", nil)];
                
            }
            
            else if ([[dataDict valueForKey:_04]isEqualToString:@"6"])
                
                
            {
                
                NSString *reason1 = @"Motor trigger from Power Failure";
                
                
                //datamodel.date = date;
                //datamodel.tank = tank;
                datamodel.reason = reason1;
                datamodel.pumpState = @"OFF";
                [pumpSuccessArray addObject:datamodel];
                [[WMSStorageManager sharedInstance]savePumpResponseInDB:pumpSuccessArray];
                //[self checkForDuplicates];
                
                [self.view makeToast:NSLocalizedString(@"Motor trigger from Power Failure", nil)];
                
                
            }
            [_pumpOnOff setOn:NO animated:YES];
            
            break;
            
            
        case 25:
            //pump state is off
            if ([[dataDict valueForKey:_04] isEqualToString:@"1"])
                
            {
                NSString *reason1 = @"Motor trigger from Switch";
                
                
                
                //datamodel.date = date;
                // datamodel.tank = tank;
                datamodel.reason = reason1;
                datamodel.pumpState = @"OFF";
                [pumpSuccessArray addObject:datamodel];
                [[WMSStorageManager sharedInstance]savePumpResponseInDB:pumpSuccessArray];
                //[self checkForDuplicates];
                
                [self.view makeToast:NSLocalizedString(@"Motor trigger from Switch", nil)];
            }
            
            else if ([[dataDict valueForKey:_04]isEqualToString:@"2"])
            {
                
                NSString *reason1 = @"Motor trigger from App";
                
                
                //datamodel.date = date;
                //datamodel.tank = tank;
                datamodel.reason = reason1;
                datamodel.pumpState = @"OFF";
                [pumpSuccessArray addObject:datamodel];
                [[WMSStorageManager sharedInstance]savePumpResponseInDB:pumpSuccessArray];
                //[self checkForDuplicates];
                
                [self.view makeToast:NSLocalizedString(@"Motor trigger from App", nil)];
                
            }
            
            else if ([[dataDict valueForKey:_04]isEqualToString:@"3"])
            {
                
                NSString *reason1 = @"Motor trigger from Schedule";
                
                
                //datamodel.date = date;
                //datamodel.tank = tank;
                datamodel.reason = reason1;
                datamodel.pumpState = @"OFF";
                [pumpSuccessArray addObject:datamodel];
                [[WMSStorageManager sharedInstance]savePumpResponseInDB:pumpSuccessArray];
                //[self checkForDuplicates];
                
                [self.view makeToast:NSLocalizedString(@"Motor trigger from Schedule", nil)];
            }
            
            else if ([[dataDict valueForKey:_04]isEqualToString:@"4"])
            {
                
                NSString *reason1 = @"Motor trigger from Dry Run";
                
                
                //datamodel.date = date;
                //  datamodel.tank = tank;
                datamodel.reason = reason1;
                datamodel.pumpState = @"OFF";
                [pumpSuccessArray addObject:datamodel];
                [[WMSStorageManager sharedInstance]savePumpResponseInDB:pumpSuccessArray];
                //[self checkForDuplicates];
                
                [self.view makeToast:NSLocalizedString(@"Motor trigger from Dry Run", nil)];
                
            }
            
            else if ([[dataDict valueForKey:_04]isEqualToString:@"5"])
            {
                
                //   NSString *reason1 = @"Motor turn off due to water level 100%";
                NSString *reason1 = @"Motor trigger from Auto task";
                
                
                //datamodel.date = date;
                //datamodel.tank = tank;
                datamodel.reason = reason1;
                datamodel.pumpState = @"OFF";
                [pumpSuccessArray addObject:datamodel];
                [[WMSStorageManager sharedInstance]savePumpResponseInDB:pumpSuccessArray];
                //[self checkForDuplicates];
                
                [self.view makeToast:NSLocalizedString(@"Motor trigger from Auto task", nil)];
                
            }
            
            else if ([[dataDict valueForKey:_04]isEqualToString:@"6"])
                
                
            {
                
                NSString *reason1 = @"Motor trigger from Power Failure";
                
                
                //datamodel.date = date;
                //datamodel.tank = tank;
                datamodel.reason = reason1;
                datamodel.pumpState = @"OFF";
                [pumpSuccessArray addObject:datamodel];
                [[WMSStorageManager sharedInstance]savePumpResponseInDB:pumpSuccessArray];
                
                //[self checkForDuplicates];
                
                [self.view makeToast:NSLocalizedString(@"Motor trigger from Power Failure", nil)];
                
                
            }
            [_pumpOnOff setOn:NO animated:YES];
            
            break;
            
            
        default:
            break;
    }
    
    [self setTopHeaderViewInHomeScreen];
    
}
    

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
  
    
    // Fetch the devices from persistent data store
//    NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
//    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Notification"];
//    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"date" ascending:YES];
//   
//    self.notification = [[managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
//    _sortedFetch = [_notification sortedArrayUsingDescriptors:[NSArray arrayWithObject:sortDescriptor]];
    
   
    
   // [self.tableView reloadData];
}
-(void)showAlertView{
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@"Error"
                                  message:@"Something Went Wrong.Please press Retry "
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"EXIT"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             [alert dismissViewControllerAnimated:YES completion:nil];
                             
                             
                         }];
    
    UIAlertAction* reTry = [UIAlertAction
                            actionWithTitle:@"RETRY"
                            style:UIAlertActionStyleDefault
                            handler:^(UIAlertAction * action)
                            {
                                
                                
                                [alert dismissViewControllerAnimated:YES completion:nil];
                            }];
    [alert addAction:ok];
    [alert addAction:reTry];
    [self presentViewController:alert animated:YES completion:nil];

   // [self.navigationController presentViewController:alert animated:YES completion:nil];
}


-(void)viewdidDisappear:(BOOL)animated{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NET_WORK_CHANGE object:nil];
    [[TouchUtil getInstance]stopTimer];

    //[timer invalidate];
}
-(void)callPumpOffApi
{
    OrderedDictionary *dataDict = [[OrderedDictionary alloc]initWithCapacity:4];
    NSString *tankNumber = @"1";
    [dataDict insertObject:tankNumber forKey:_01 atIndex:0];
    
    OrderedDictionary *requestDict = [[OrderedDictionary alloc]initWithCapacity:5];
    [requestDict insertObject:_SIDVALUE forKey:_SID atIndex:0];
    //[requestDict insertObject:_BADVALUE forKey:_BAD atIndex:1];
    [requestDict insertObject:_SADVALUE forKey:_SAD atIndex:1];
    [requestDict insertObject:_2001 forKey:_CMD atIndex:2];
    [requestDict insertObject:dataDict forKey:_DATA atIndex:3];
    NSError *writeError;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:requestDict options:kNilOptions error:&writeError];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    [SVProgressHUD showWithStatus:@"Switching OFF.."];
    [TcpPacketClass sharedInstance].delegate = self;
      [[TcpPacketClass sharedInstance]writeOut:jsonString second:[requestDict objectForKey:_CMD]];
    timer = [NSTimer scheduledTimerWithTimeInterval:8.0 target:self selector:@selector(pumpOFF:) userInfo:nil repeats: NO];
    
    
    
}


- (void)pumpOFF:(NSTimer *)theTimer {
    
    [self.view makeToast:NSLocalizedString(@"Somthing went to wrong, Please Try Again!!!", nil)];

    if (_pumpOnOff.isOn) {
        _pumpOnOff.on = NO;
    }
    else
    {
        _pumpOnOff.on = YES;
        
    }
    [theTimer invalidate];
    [SVProgressHUD dismiss];
    //    self.popupView.hidden=NO;
    //    self.passwordView.hidden=NO;
}

#pragma mark - pump failure status response here
-(void)didRecievePumpFailureResponse:(NSDictionary *)jsonDict
{
    NSMutableArray *pumpFailResponseArray = [[NSMutableArray alloc]init];
    [timer invalidate];
    [SVProgressHUD dismiss];
    NSDictionary *dataDict = [jsonDict valueForKey:_DATA];
    NSString *tank = [dataDict objectForKey:@"01"];
    NSString *notificationNumber = [dataDict objectForKey:@"02"];
    NSInteger notification=[notificationNumber intValue];
    NSString *notificationField = [dataDict objectForKey:@"03"];
    NSString *dateStr = [dataDict objectForKey:@"04"];
    //to save date in DB in timestamp
    NSString *formattedDateString = dateStr;
    //10-03-17-11-55-00
    NSRange range = NSMakeRange(8,1);
    NSRange range1 = NSMakeRange(11,1);
    NSRange range2 = NSMakeRange(14, 1);
    
    NSString *newText = [formattedDateString stringByReplacingCharactersInRange:range withString:@" "];
    NSString *newText1 = [newText stringByReplacingCharactersInRange:range1 withString:@":"];
    NSString *finalString = [newText1 stringByReplacingCharactersInRange:range2 withString:@":"];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT"]];
    NSDate *DateFromString = [dateFormatter dateFromString:finalString];
    double timestamp = [DateFromString timeIntervalSince1970];
    NSNumber *timeInMilliseconds = [NSNumber numberWithDouble:(timestamp*1000)];
    NSString *timeinMillStr = [ NSString stringWithFormat:@"%@",timeInMilliseconds];
    datamodel.date=timeinMillStr;
    datamodel.isRead = @"0";
    datamodel.tankNum =tank;
    
    switch (notification) {
            
            //notification number is 1
        case 1:
        {
            if ([notificationField isEqualToString:@"1"]) {
                NSString *notification1 = @"Sensor number 1 malfunction";
                datamodel.reason = notification1;
                datamodel.pumpState=@"";
                [pumpFailResponseArray addObject:datamodel];
                [[WMSStorageManager sharedInstance]savePumpResponseInDB:pumpFailResponseArray];
                [self.view makeToast:NSLocalizedString(@"Sensor number 1 malfunction", nil)];
                
            }
            else if ([notificationField isEqualToString:@"2"]) {
                NSString *notification1 = @"Sensor number 2 malfunction";
                datamodel.reason = notification1;
                datamodel.pumpState=@"";
                
                [pumpFailResponseArray addObject:datamodel];
                [[WMSStorageManager sharedInstance]savePumpResponseInDB:pumpFailResponseArray];
                // [self checkForDuplicates];
                
                [self.view makeToast:NSLocalizedString(@"Sensor number 2 malfunction", nil)];
            }
            else if ([notificationField isEqualToString:@"3"]) {
                NSString *notification1 = @"Sensor number 3 malfunction";
                datamodel.reason = notification1;
                datamodel.pumpState=@"";
                
                [pumpFailResponseArray addObject:datamodel];
                [[WMSStorageManager sharedInstance]savePumpResponseInDB:pumpFailResponseArray];
                //  [self checkForDuplicates];
                
                [self.view makeToast:NSLocalizedString(@"Sensor number 3 malfunction", nil)];
                
            }
            
            else if ([notificationField isEqualToString:@"4"]) {
                NSString *notification1 = @"Sensor number 4 malfunction";
                datamodel.reason = notification1;
                datamodel.pumpState=@"";
                
                [pumpFailResponseArray addObject:datamodel];
                [[WMSStorageManager sharedInstance]savePumpResponseInDB:pumpFailResponseArray];
                
                [self.view makeToast:NSLocalizedString(@"Sensor number 3 malfunction", nil)];
                
            }
            
            else if ([notificationField isEqualToString:@"1-2"]) {
                NSString *notification1 = @"Sensor number 1 and 2 malfunction";
                datamodel.reason = notification1;
                datamodel.pumpState=@"";
                
                [pumpFailResponseArray addObject:datamodel];
                [[WMSStorageManager sharedInstance]savePumpResponseInDB:pumpFailResponseArray];
                
                [self.view makeToast:NSLocalizedString(@"Sensor number 1 and 2 malfunction", nil)];
                
            }
            
            else if ([notificationField isEqualToString:@"1-3"]) {
                NSString *notification1 = @"Sensor number 1 and 3 malfunction";
                datamodel.reason = notification1;
                datamodel.pumpState=@"";
                
                [pumpFailResponseArray addObject:datamodel];
                [[WMSStorageManager sharedInstance]savePumpResponseInDB:pumpFailResponseArray];
                [self.view makeToast:NSLocalizedString(@"Sensor number 1 and 3 malfunction", nil)];
                
            }
            
            else if ([notificationField isEqualToString:@"1-4"]) {
                NSString *notification1 = @"Sensor number 1 and 4 malfunction";
                datamodel.reason = notification1;
                datamodel.pumpState=@"";
                
                [pumpFailResponseArray addObject:datamodel];
                [[WMSStorageManager sharedInstance]savePumpResponseInDB:pumpFailResponseArray];
                [self.view makeToast:NSLocalizedString(@"Sensor number 1 and 4 malfunction", nil)];
                
            }
            
            else if ([notificationField isEqualToString:@"1-2-3"]) {
                NSString *notification1 = @"Sensor number 1,2 and 3 malfunction";
                datamodel.reason = notification1;
                datamodel.pumpState=@"";
                
                [pumpFailResponseArray addObject:datamodel];
                [[WMSStorageManager sharedInstance]savePumpResponseInDB:pumpFailResponseArray];
                [self.view makeToast:NSLocalizedString(@"Sensor number 1,2 and 3 malfunction", nil)];
                
            }
            
            else if ([notificationField isEqualToString:@"1-2-3-4"]) {
                NSString *notification1 = @"Sensor number 1,2,3 and 4 malfunction";
                datamodel.reason = notification1;
                datamodel.pumpState=@"";
                
                [pumpFailResponseArray addObject:datamodel];
                [[WMSStorageManager sharedInstance]savePumpResponseInDB:pumpFailResponseArray];
                [self.view makeToast:NSLocalizedString(@"Sensor number 1,2,3 and 4  malfunction", nil)];
                
            }
            
            else if ([notificationField isEqualToString:@"2-4"]) {
                NSString *notification1 = @"Sensor number 2 and 4 malfunction";
                datamodel.reason = notification1;
                datamodel.pumpState=@"";
                
                [pumpFailResponseArray addObject:datamodel];
                [[WMSStorageManager sharedInstance]savePumpResponseInDB:pumpFailResponseArray];
                
                [self.view makeToast:NSLocalizedString(@"Sensor number 3 malfunction", nil)];
                
            }
            
            else if ([notificationField isEqualToString:@"3-4"]) {
                NSString *notification1 = @"Sensor number 3 and 4 malfunction";
                datamodel.reason = notification1;
                datamodel.pumpState=@"";
                
                [pumpFailResponseArray addObject:datamodel];
                [[WMSStorageManager sharedInstance]savePumpResponseInDB:pumpFailResponseArray];
                
                [self.view makeToast:NSLocalizedString(@"Sensor number 3 and 4  malfunction", nil)];
                
            }
            
            else if ([notificationField isEqualToString:@"2-3-4"]) {
                NSString *notification1 = @"Sensor number 2,3 and 4 malfunction";
                datamodel.reason = notification1;
                datamodel.pumpState=@"";
                
                [pumpFailResponseArray addObject:datamodel];
                [[WMSStorageManager sharedInstance]savePumpResponseInDB:pumpFailResponseArray];
                
                [self.view makeToast:NSLocalizedString(@"Sensor number 2,3 and 4  malfunction", nil)];
                
            }
        }
            
            break;
            
        case 2 :
        { NSString *notification2 = @"Sensor hits more than 6 times in a minute";
            datamodel.reason = notification2;
            datamodel.pumpState=@"OFF";
            
            [pumpFailResponseArray addObject:datamodel];
            [[WMSStorageManager sharedInstance]savePumpResponseInDB:pumpFailResponseArray];
            
            [self.view makeToast:NSLocalizedString(@"Sensor hits more than 6 times in a minute", nil)];
        }
            break;
            
            
        case 3 :
        { NSString *notification3 = @"Pump state changes more than 4 times in a minute";
            datamodel.reason = notification3;
            datamodel.pumpState=@"OFF";
            
            [pumpFailResponseArray addObject:datamodel];
            [[WMSStorageManager sharedInstance]savePumpResponseInDB:pumpFailResponseArray];
            
            [self.view makeToast:NSLocalizedString(@"Pump state changes more than 4 times in a minute", nil)];
        }
            break;
            
        case 4 :
        { NSString *notification4 = @"Pump wire disconnected";
            datamodel.reason = notification4;
            datamodel.pumpState=@"OFF";
            
            [pumpFailResponseArray addObject:datamodel];
            [[WMSStorageManager sharedInstance]savePumpResponseInDB:pumpFailResponseArray];
            
            
            [self.view makeToast:NSLocalizedString(@"Pump wire disconnected", nil)];
        }
            break;
        case 5 :
        { NSString *notification5 = @"Pump wire connected";
            datamodel.reason = notification5;
            datamodel.pumpState=@"OFF";
            
            [pumpFailResponseArray addObject:datamodel];
            [[WMSStorageManager sharedInstance]savePumpResponseInDB:pumpFailResponseArray];
            
            [self.view makeToast:NSLocalizedString(@"Pump wire connected", nil)];
            
        }
            break;
            
        case 6 :
        { NSString *notification6 = @"Pump can't be started due to wire  disconnected";
            datamodel.reason = notification6;
            datamodel.pumpState=@"OFF";
            
            [pumpFailResponseArray addObject:datamodel];
            [[WMSStorageManager sharedInstance]savePumpResponseInDB:pumpFailResponseArray];
            
            [self.view makeToast:NSLocalizedString(@"Pump can't be started due to wire  disconnected", nil)];
        }
            break;
        case 7 :
        { NSString *notification7 = @"Pump can't be started due to ac mains off";
            datamodel.reason = notification7;
            datamodel.pumpState=@"OFF";
            
            [pumpFailResponseArray addObject:datamodel];
            [[WMSStorageManager sharedInstance]savePumpResponseInDB:pumpFailResponseArray];
            
            [self.view makeToast:NSLocalizedString(@"Pump can't ne started due to ac mains off", nil)];
            
        }
            break;
        case 8 :
        { NSString *notification8 = @"Automated task Nnumber 1 executed";
            datamodel.reason = notification8;
            datamodel.pumpState=@"OFF";
            [pumpFailResponseArray addObject:datamodel];
            [[WMSStorageManager sharedInstance]savePumpResponseInDB:pumpFailResponseArray];
            
            [self.view makeToast:NSLocalizedString(@"Automated task Nnumber 1 executed", nil)];
            
        }
            break;
        case 9 :
        { NSString *notification9 = @"Automated task Nnumber 2 executed";
            datamodel.reason = notification9;
            datamodel.pumpState=@"OFF";
            
            [pumpFailResponseArray addObject:datamodel];
            [[WMSStorageManager sharedInstance]savePumpResponseInDB:pumpFailResponseArray];
            
            [self.view makeToast:NSLocalizedString(@"Automated task Nnumber 2 executed", nil)];
            
        }
            break;
        case 10 :
        { NSString *notification10 = @"Automated task Nnumber 3 executed";
            datamodel.reason = notification10;
            datamodel.pumpState=@"OFF";
            
            [pumpFailResponseArray addObject:datamodel];
            [[WMSStorageManager sharedInstance]savePumpResponseInDB:pumpFailResponseArray];
            
            [self.view makeToast:NSLocalizedString(@"Automated task Nnumber 3 executed", nil)];
            
        }
            break;
            
            
            
        case 17 :
        {
            //
            //
            //
            NSString *notification11 = @"Schedule can't be run";
            datamodel.reason = notification11;
            datamodel.pumpState=@"OFF";
            
            [pumpFailResponseArray addObject:datamodel];
            [[WMSStorageManager sharedInstance]savePumpResponseInDB:pumpFailResponseArray];
            
            [self.view makeToast:NSLocalizedString(@"Schedule can't be run", nil)];
            //            if ([[dataDict valueForKey:_03] isEqualToString:@"1"])
            //
            //            {
            //                NSString *reason1 = @"Motor trigger from Switch";
            //
            //
            //
            //                //datamodel.date = date;
            //                datamodel.reason = reason1;
            //                datamodel.pumpState = @"OFF";
            //                [pumpFailResponseArray addObject:datamodel];
            //                [[WMSStorageManager sharedInstance]savePumpResponseInDB:pumpFailResponseArray];
            //                [self.view makeToast:NSLocalizedString(@"Motor trigger from Switch", nil)];
            //            }
            //
            //            else if ([[dataDict valueForKey:_03]isEqualToString:@"2"])
            //            {
            //
            //                NSString *reason1 = @"Motor trigger from App";
            //
            //
            //                //datamodel.date = date;
            //                //datamodel.tank = tank;
            //                datamodel.reason = reason1;
            //                datamodel.pumpState = @"OFF";
            //                [pumpFailResponseArray addObject:datamodel];
            //                [[WMSStorageManager sharedInstance]savePumpResponseInDB:pumpFailResponseArray];
            //
            //                [self.view makeToast:NSLocalizedString(@"Motor trigger from App", nil)];
            //
            //            }
            //
            //            else if ([[dataDict valueForKey:_03]isEqualToString:@"3"])
            //            {
            //
            //                NSString *reason1 = @"Motor trigger from Schedule";
            //
            //
            //                //datamodel.date = date;
            //
            //                datamodel.reason = reason1;
            //                datamodel.pumpState = @"OFF";
            //                [pumpFailResponseArray addObject:datamodel];
            //                [[WMSStorageManager sharedInstance]savePumpResponseInDB:pumpFailResponseArray];                [self.view makeToast:NSLocalizedString(@"Motor trigger from Schedule", nil)];
            //            }
            //
            //            else if ([[dataDict valueForKey:_03]isEqualToString:@"4"])
            //            {
            //
            //                NSString *reason1 = @"Motor trigger from Dry Run";
            //
            //
            //                //datamodel.date = date;
            //                //  datamodel.tank = tank;
            //                datamodel.reason = reason1;
            //                datamodel.pumpState = @"OFF";
            //                [pumpFailResponseArray addObject:datamodel];
            //                [[WMSStorageManager sharedInstance]savePumpResponseInDB:pumpFailResponseArray];
            //                [self.view makeToast:NSLocalizedString(@"Motor trigger from Dry Run", nil)];
            //
            //            }
            //
            //            else if ([[dataDict valueForKey:_03]isEqualToString:@"5"])
            //            {
            //
            //               // NSString *reason1 = @"Motor turn off due to water level 100%";
            //
            //                NSString *reason1 = @"Motor trigger from Auto task";
            //
            //                //datamodel.date = date;
            //                //datamodel.tank = tank;
            //                datamodel.reason = reason1;
            //                datamodel.pumpState = @"OFF";
            //                [pumpFailResponseArray addObject:datamodel];
            //                [[WMSStorageManager sharedInstance]savePumpResponseInDB:pumpFailResponseArray];                [self.view makeToast:NSLocalizedString(@"Motor trigger from Auto task", nil)];
            //
            //            }
            //
            //            else if ([[dataDict valueForKey:_03]isEqualToString:@"6"])
            //
            //
            //            {
            //
            //                NSString *reason1 = @"Motor trigger from Power Failure";
            //
            //
            //                //datamodel.date = date;
            //                //datamodel.tank = tank;
            //                datamodel.reason = reason1;
            //                datamodel.pumpState = @"OFF";
            //                [pumpFailResponseArray addObject:datamodel];
            //                [[WMSStorageManager sharedInstance]savePumpResponseInDB:pumpFailResponseArray];
            //
            //                [self.view makeToast:NSLocalizedString(@"Motor trigger from Power Failure", nil)];
            //
            //
            //            }
            //
            //            else if ([[dataDict valueForKey:_03]isEqualToString:@"14"])
            //
            //
            //            {
            //
            //                NSString *reason1 = @"Motor can't be started due to tank Full";
            //
            //
            //                //datamodel.date = date;
            //                //datamodel.tank = tank;
            //                datamodel.reason = reason1;
            //                datamodel.pumpState = @"OFF";
            //                [pumpFailResponseArray addObject:datamodel];
            //                [[WMSStorageManager sharedInstance]savePumpResponseInDB:pumpFailResponseArray];
            //                [self.view makeToast:NSLocalizedString(@"Motor can't be started due to tank Full", nil)];
            //
            //
            //            }
            //            [_pumpOnOff setOn:NO animated:YES];
        }
            
            break;
            
            
        case 18 :
        { NSString *notification18 = @"System on battery";
            datamodel.reason = notification18;
            datamodel.pumpState=@"OFF";
            [pumpFailResponseArray addObject:datamodel];
            [[WMSStorageManager sharedInstance]savePumpResponseInDB:pumpFailResponseArray];
            [self.view makeToast:NSLocalizedString(@"System on battery", nil)];
            
        }
            break;
            
            
            
            
        case 15 :
        { NSString *notification18 = @"Motor can't be stared due to tank full";
            datamodel.reason = notification18;
            datamodel.pumpState=@"OFF";
            
            [pumpFailResponseArray addObject:datamodel];
            [[WMSStorageManager sharedInstance]savePumpResponseInDB:pumpFailResponseArray];
            
            [self.view makeToast:NSLocalizedString(@"Motor can't be stared due to tank full", nil)];
            
        }
            break;
        case 19 :
        { NSString *notification19 = @"System on mains";
            datamodel.reason = notification19;
            datamodel.pumpState=@"OFF";
            
            [pumpFailResponseArray addObject:datamodel];
            [[WMSStorageManager sharedInstance]savePumpResponseInDB:pumpFailResponseArray];
            
            [self.view makeToast:NSLocalizedString(@"System on mains", nil)];
            
        }
            break;
        case 24 :
        {
            
            //pump state is on
            if ([[dataDict valueForKey:_03] isEqualToString:@"1"])
                
            {
                NSString *reason1 = @"Motor trigger from Switch";
                
                
                
                //datamodel.date = date;
                datamodel.reason = reason1;
                datamodel.pumpState = @"ON";
                [pumpFailResponseArray addObject:datamodel];
                [[WMSStorageManager sharedInstance]savePumpResponseInDB:pumpFailResponseArray];
                [self.view makeToast:NSLocalizedString(@"Motor trigger from Switch", nil)];
            }
            
            else if ([[dataDict valueForKey:_03]isEqualToString:@"2"])
            {
                
                NSString *reason1 = @"Motor trigger from App";
                
                
                //datamodel.date = date;
                //datamodel.tank = tank;
                datamodel.reason = reason1;
                datamodel.pumpState = @"ON";
                [pumpFailResponseArray addObject:datamodel];
                [[WMSStorageManager sharedInstance]savePumpResponseInDB:pumpFailResponseArray];
                
                [self.view makeToast:NSLocalizedString(@"Motor trigger from App", nil)];
                
            }
            
            else if ([[dataDict valueForKey:_03]isEqualToString:@"3"])
            {
                
                NSString *reason1 = @"Motor trigger from Schedule";
                
                
                //datamodel.date = date;
                
                datamodel.reason = reason1;
                datamodel.pumpState = @"ON";
                [pumpFailResponseArray addObject:datamodel];
                [[WMSStorageManager sharedInstance]savePumpResponseInDB:pumpFailResponseArray];
                
                [self.view makeToast:NSLocalizedString(@"Motor trigger from Schedule", nil)];
            }
            
            else if ([[dataDict valueForKey:_03]isEqualToString:@"4"])
            {
                
                NSString *reason1 = @"Motor trigger from Dry Run";
                
                
                //datamodel.date = date;
                //  datamodel.tank = tank;
                datamodel.reason = reason1;
                datamodel.pumpState = @"ON";
                [pumpFailResponseArray addObject:datamodel];
                [[WMSStorageManager sharedInstance]savePumpResponseInDB:pumpFailResponseArray];
                
                [self.view makeToast:NSLocalizedString(@"Motor trigger from Dry Run", nil)];
                
            }
            
            else if ([[dataDict valueForKey:_03]isEqualToString:@"5"])
            {
                
                // NSString *reason1 = @"Motor turn off due to water level 100%";
                NSString *reason1 = @"Motor trigger from Auto task";
                
                //datamodel.date = date;
                //datamodel.tank = tank;
                datamodel.reason = reason1;
                datamodel.pumpState = @"ON";
                [pumpFailResponseArray addObject:datamodel];
                [[WMSStorageManager sharedInstance]savePumpResponseInDB:pumpFailResponseArray];
                
                [self.view makeToast:NSLocalizedString(@"Motor trigger from Auto task", nil)];
                
            }
            
            else if ([[dataDict valueForKey:_03]isEqualToString:@"6"])
                
                
            {
                
                NSString *reason1 = @"Motor trigger from Power Failure";
                
                
                //datamodel.date = date;
                //datamodel.tank = tank;
                datamodel.reason = reason1;
                datamodel.pumpState = @"ON";
                [pumpFailResponseArray addObject:datamodel];
                [[WMSStorageManager sharedInstance]savePumpResponseInDB:pumpFailResponseArray];
                
                [self.view makeToast:NSLocalizedString(@"Motor trigger from Power Failure", nil)];
                
                
            }
            
            else if ([[dataDict valueForKey:_03]isEqualToString:@"14"])
                
                
            {
                
                NSString *reason1 = @"Motor can't be started due to tank Full";
                
                
                //datamodel.date = date;
                //datamodel.tank = tank;
                datamodel.reason = reason1;
                datamodel.pumpState = @"OFF";
                [pumpFailResponseArray addObject:datamodel];
                [[WMSStorageManager sharedInstance]savePumpResponseInDB:pumpFailResponseArray];
                
                [self.view makeToast:NSLocalizedString(@"Motor can't be started due to tank Full", nil)];
                
                
            }
            [_pumpOnOff setOn:YES animated:YES];
            
            
            
        }
            break;
        case 25 :
        {
            
            if ([[dataDict valueForKey:_03] isEqualToString:@"1"])
                
            {
                NSString *reason1 = @"Motor trigger from Switch";
                
                
                
                //datamodel.date = date;
                datamodel.reason = reason1;
                datamodel.pumpState = @"OFF";
                [pumpFailResponseArray addObject:datamodel];
                [[WMSStorageManager sharedInstance]savePumpResponseInDB:pumpFailResponseArray];
                
                [self.view makeToast:NSLocalizedString(@"Motor trigger from Switch", nil)];
            }
            
            else if ([[dataDict valueForKey:_03]isEqualToString:@"2"])
            {
                
                NSString *reason1 = @"Motor trigger from App";
                
                
                //datamodel.date = date;
                //datamodel.tank = tank;
                datamodel.reason = reason1;
                datamodel.pumpState = @"OFF";
                [pumpFailResponseArray addObject:datamodel];
                [[WMSStorageManager sharedInstance]savePumpResponseInDB:pumpFailResponseArray];
                
                [self.view makeToast:NSLocalizedString(@"Motor trigger from App", nil)];
                
            }
            
            else if ([[dataDict valueForKey:_03]isEqualToString:@"3"])
            {
                
                NSString *reason1 = @"Motor trigger from Schedule";
                
                
                //datamodel.date = date;
                
                datamodel.reason = reason1;
                datamodel.pumpState = @"OFF";
                [pumpFailResponseArray addObject:datamodel];
                [[WMSStorageManager sharedInstance]savePumpResponseInDB:pumpFailResponseArray];
                
                [self.view makeToast:NSLocalizedString(@"Motor trigger from Schedule", nil)];
            }
            
            else if ([[dataDict valueForKey:_03]isEqualToString:@"4"])
            {
                
                NSString *reason1 = @"Motor trigger from Dry Run";
                
                
                //datamodel.date = date;
                //  datamodel.tank = tank;
                datamodel.reason = reason1;
                datamodel.pumpState = @"OFF";
                [pumpFailResponseArray addObject:datamodel];
                [[WMSStorageManager sharedInstance]savePumpResponseInDB:pumpFailResponseArray];
                
                [self.view makeToast:NSLocalizedString(@"Motor trigger from Dry Run", nil)];
                
            }
            
            else if ([[dataDict valueForKey:_03]isEqualToString:@"5"])
            {
                
                // NSString *reason1 = @"Motor turn off due to water level 100%";
                
                NSString *reason1 = @"Motor trigger from Auto task";
                //datamodel.date = date;
                //datamodel.tank = tank;
                datamodel.reason = reason1;
                datamodel.pumpState = @"OFF";
                [pumpFailResponseArray addObject:datamodel];
                [[WMSStorageManager sharedInstance]savePumpResponseInDB:pumpFailResponseArray];
                
                [self.view makeToast:NSLocalizedString(@"Motor trigger from Auto task", nil)];
                
            }
            
            else if ([[dataDict valueForKey:_03]isEqualToString:@"6"])
                
                
            {
                
                NSString *reason1 = @"Motor trigger from Power Failure";
                
                
                //datamodel.date = date;
                //datamodel.tank = tank;
                datamodel.reason = reason1;
                datamodel.pumpState = @"OFF";
                [pumpFailResponseArray addObject:datamodel];
                [[WMSStorageManager sharedInstance]savePumpResponseInDB:pumpFailResponseArray];
                
                [self.view makeToast:NSLocalizedString(@"Motor trigger from Power Failure", nil)];
                
                
            }
            
            else if ([[dataDict valueForKey:_03]isEqualToString:@"14"])
                
                
            {
                
                NSString *reason1 = @"Motor can't be started due to tank Full";
                
                
                //datamodel.date = date;
                //datamodel.tank = tank;
                datamodel.reason = reason1;
                datamodel.pumpState = @"OFF";
                [pumpFailResponseArray addObject:datamodel];
                [[WMSStorageManager sharedInstance]savePumpResponseInDB:pumpFailResponseArray];
                
                [self.view makeToast:NSLocalizedString(@"Motor can't be started due to tank Full", nil)];
                
                
            }
            
        }
            break;
            
            
            
            
        default:
            break;
    }
    
    [_pumpOnOff setOn:NO animated:YES];
    [self setTopHeaderViewInHomeScreen];
}



-(void)viewWillDisappear:(BOOL)animated {
//    isSelf = TRUE;
//    [timer invalidate];
//    timer = nil;
    [[TouchUtil getInstance]stopTimer];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NET_WORK_CHANGE object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:SOCKET_CONNECTED object:nil];
    [super viewWillDisappear:animated];
    //[self deRegisterFromNotification];
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
//#pragma mark - get current water level
//-(void)callCurrentPumpStatusApi {
//
//
//    OrderedDictionary *requestDict = [[OrderedDictionary alloc]initWithCapacity:3];
//    [requestDict insertObject:_SIDVALUE forKey:_SID atIndex:0];
//    [requestDict insertObject:_SADVALUE forKey:_SAD atIndex:1];
//    [requestDict insertObject:_8519 forKey:_CMD atIndex:2];
//    NSError *writeError;
//    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:requestDict options:kNilOptions error:&writeError];
//    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
//    //[SVProgressHUD showWithStatus:@"Please Wait.."];
//    [TcpPacketClass sharedInstance].delegate = self;
//    [[TcpPacketClass sharedInstance]writeOut:jsonString second:[requestDict objectForKey:_CMD]];
//    pumpTimer = [NSTimer scheduledTimerWithTimeInterval:5.0 target:self selector:@selector(sendPumpPacketAgain:) userInfo:nil repeats: NO];
//}
//
//-(void)sendPumpPacketAgain:(NSTimer *)thetimer
//{
//
//    [thetimer invalidate];
//   // [SVProgressHUD dismiss];
//
//    [self callCurrentPumpStatusApi];
//
//}
//
//
//-(void)getCurrentPumpStatus:(NSDictionary *)jsonDict
//{
//    [pumpTimer invalidate];
//    // [SVProgressHUD dismiss];
//
//
//    if (![[jsonDict valueForKey:_DATA] isKindOfClass:[NSString class]]) {
//        NSDictionary *dataDict = [jsonDict valueForKey:_DATA];
//        if ([[dataDict valueForKey:_04] isEqualToString:@"0"]) {
//
//
//            self.pumpOnOff.on = NO;
//
//        }
//        else{
//            self.pumpOnOff.on = YES;
//        }
//
//
//    }
//    [self callGetTankSettingsApi];
//}
@end


//static private void connectUsingSocket() {
//    connecting = true;
//    //create a socket
//    try {
//        
//        
//        new Thread(new Runnable() {
//            @Override
//            public void run() {
//                
//                try {
//                    clientSocket = new Socket(ConstantUtil.stationModeIP, ConstantUtil.IBuildConstant.PORT);
//                    clientSocket.setKeepAlive(false);
//                    clientSocket.setSoTimeout(ConstantUtil.SOCKET_TIMEOUT);
//                    connectionCreated = clientSocket.isConnected();
//                    if (connectionCreated) {
//                        if (ConstantUtil.stationModeIP == ConstantUtil.IBuildConstant.AP_MODE_IP) {
//                            ConstantUtil.CONNECTED_NETWORK_MODE = ConstantUtil.CONNECTED_NETWORK_AP_MODE;
//                        } else {
//                            ConstantUtil.CONNECTED_NETWORK_MODE = ConstantUtil.CONNECTED_NETWORK_STATION_MODE;
//                        }
//                        dataOutputStream = new DataOutputStream(clientSocket.getOutputStream());
//                        ConnectionManager.get_instance().setIsConnectedWifi(connectionCreated);
//                        retryReqFromHashMap();
//                    } else {
//                        
//                        connectionCreated = false;
//                        ConnectionManager.get_instance().setIsConnectedWifi(connectionCreated);
//                    }
//                    
//                    
//                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(1024);
//                    byte[] buffer = new byte[2048];
//                    int bytesRead;
//                    
//                    try {
//                        while (true) {
//                            byteArrayOutputStream.reset();
//                            
//                            inputStream = clientSocket.getInputStream();
//                            if (inputStream != null) {
//                                if (clientSocket.isConnected() && !clientSocket.isInputShutdown()) {
//                                    
//                                    bytesRead = inputStream.read(buffer);
//                                    if (bytesRead < 0) {
//                                        connectionCreated = false;
//                                        connecting = false;
//                                        dataOutputStream.close();
//                                        inputStream.close();
//                                        clientSocket.close();
//                                        Thread.sleep(3000);
//                                        sBuffer = new StringBuffer(2048);
//                                        isWifiConnected(null);
//                                    } else {
//                                        byteArrayOutputStream.write(buffer, 0, bytesRead);
//                                        
//                                        
//                                        String response = byteArrayOutputStream.toString("UTF-8");
//                                        try {
//                                            // Run loop to get 1 packet at a time. This string may have multiple responses.
//                                            
//                                            String st = response;
//                                            st = st.trim();
//                                            sqLiteDbObj = SQLiteHelper.getInstance().getWritableDatabase();
//                                            Logger.wrtOnFil(TAGS.USBBroadcastReceiver, "Receiving " + "  " + st + " pkt size : " + st.length() + "\n\n", DateFormat.getDateTimeInstance().format(new Date()));
//                                            if (st.length() == 6 && (!st.contains("{") || !st.contains("}"))) {
//                                                updateOnUi(st);
//                                            } else if (st.length() == 8 && (!st.contains("{") || !st.contains("}")) && st.contains("~") && st.contains("^")) {
//                                                updateOnUi(st);
//                                            } else if (st.length() == 0) {
//                                                return;
//                                            } else {
//                                                if (st.contains("{\"sid\":\"9000\"") && !st.contains(",{\"sid\":\"9000\"")) {
//                                                    sBuffer = new StringBuffer(2048);
//                                                }
//                                                if (st.length() <= 200 && JsonUtil.isJSONValid(st)) {
//                                                    String cmd = Util.getJsonDataByField("cmd", st);
//                                                    String data = Util.getJsonDataByField("data", st);
//                                                    if (cmd.equalsIgnoreCase(CommandUtil.cmd_pump_status_response)) {
//                                                        requestHashMap.remove(CommandUtil.cmd_pump_Off);
//                                                        requestHashMap.remove(CommandUtil.cmd_pump_On);
//                                                    } else if (cmd.equalsIgnoreCase(CommandUtil.cmd_pump_status_error_response8510)) {
//                                                        JSONObject reader = null;
//                                                        try {
//                                                            reader = new JSONObject(data);
//                                                            if (reader.getString("02").equalsIgnoreCase("1")) {
//                                                                
//                                                            } else {
//                                                                requestHashMap.remove(CommandUtil.cmd_pump_Off);
//                                                                requestHashMap.remove(CommandUtil.cmd_pump_On);
//                                                            }
//                                                        } catch (JSONException e) {
//                                                            e.printStackTrace();
//                                                        }
//                                                    } else {
//                                                        if (data.contains("7009")) {
//                                                            requestHashMap.remove(7009);
//                                                        } else {
//                                                            requestHashMap.remove(cmd);
//                                                        }
//                                                        
//                                                    }
//                                                    updateOnUi(st);
//                                                    sBuffer = new StringBuffer(2048);
//                                                } else {
//                                                    sBuffer.append(st);
//                                                    
//                                                    if (sBuffer.toString().startsWith("{{") && sBuffer.toString().endsWith("}}")) {
//                                                        String cmd = Util.getJsonDataByField("cmd", sBuffer.toString());
//                                                        String data = Util.getJsonDataByField("data", st);
//                                                        if (cmd.equalsIgnoreCase(CommandUtil.cmd_pump_status_response)) {
//                                                            requestHashMap.remove(CommandUtil.cmd_pump_Off);
//                                                            requestHashMap.remove(CommandUtil.cmd_pump_On);
//                                                        } else if (cmd.equalsIgnoreCase(CommandUtil.cmd_pump_status_error_response8510)) {
//                                                            JSONObject reader = null;
//                                                            try {
//                                                                reader = new JSONObject(data);
//                                                                
//                                                                if (reader.getString("02").equalsIgnoreCase("1")) {
//                                                                } else {
//                                                                    requestHashMap.remove(CommandUtil.cmd_pump_Off);
//                                                                    requestHashMap.remove(CommandUtil.cmd_pump_On);
//                                                                }
//                                                            } catch (JSONException e) {
//                                                                e.printStackTrace();
//                                                            }
//                                                        } else {
//                                                            if (data.contains("7009")) {
//                                                                requestHashMap.remove(7009);
//                                                            } else {
//                                                                requestHashMap.remove(cmd);
//                                                            }
//                                                            
//                                                        }
//                                                        updateOnUi(sBuffer.toString());
//                                                        sBuffer = new StringBuffer(2048);
//                                                    } else if (JsonUtil.isJSONValid(sBuffer.toString())) {
//                                                        boolean checkJson = JsonUtil.isJSONValid("{\"sid\":\"9000\",\"sad\":\"xx\",\"cmd\":\"8511\",\"data\":{\"01\":\"1\",\"02\":\"15\",\"03\":\"0\",\"04\":\"17-04-13-11-56-53\"}}{\"sid\":\"9000\",\"sad\":\"xx\",\"cmd\":\"8511\",\"data\":{\"01\":\"1\",\"02\":\"17\",\"03\":\"14\",\"04\":\"17-04-13-12-00-00\"}}{\"sid\":\"9000\",\"sad\":\"xx\",\"cmd\":\"8511\",\"data\":{\"01\":\"1\",\"02\":\"17\",\"03\":\"14\",\"04\":\"17-04-13-12-04-00\"}}{\"sid\":\"9000\",\"sad\":\"xx\",\"cmd\":\"8511\",\"data\":{\"01\":\"1\",\"02\":\" ");
//                                                        Logger.d("Packets", "Final : .Valid JSON => " + sBuffer + " " + checkJson);
//                                                        String cmd = Util.getJsonDataByField("cmd", sBuffer.toString());
//                                                        if (cmd.equalsIgnoreCase(CommandUtil.cmd_pump_status_response)) {
//                                                            requestHashMap.remove(CommandUtil.cmd_pump_Off);
//                                                            requestHashMap.remove(CommandUtil.cmd_pump_On);
//                                                        } else if (cmd.equalsIgnoreCase(CommandUtil.cmd_pump_status_error_response8510)) {
//                                                            JSONObject reader = null;
//                                                            try {
//                                                                reader = new JSONObject(sBuffer.toString());
//                                                                if (reader.getString("02").equalsIgnoreCase("1")) {
//                                                                } else {
//                                                                    requestHashMap.remove(CommandUtil.cmd_pump_Off);
//                                                                    requestHashMap.remove(CommandUtil.cmd_pump_On);
//                                                                }
//                                                            } catch (JSONException e) {
//                                                                e.printStackTrace();
//                                                            }
//                                                        } else {
//                                                            if (sBuffer.toString().contains("7009")) {
//                                                                requestHashMap.remove(7009);
//                                                            } else {
//                                                                requestHashMap.remove(cmd);
//                                                            }
//                                                            
//                                                        }
//                                                        updateOnUi(sBuffer.toString());
//                                                        sBuffer = new StringBuffer(2048);
//                                                    }
//                                                }
//                                            }
//                                        } catch (Exception e) {
//                                            e.printStackTrace();
//                                        }
//                                    }
//                                    
//                                    
//                                } else {
//                                    connectionCreated = false;
//                                    connecting = false;
//                                    dataOutputStream.close();
//                                    inputStream.close();
//                                    clientSocket.close();
//                                    Thread.sleep(3000);
//                                    sBuffer = new StringBuffer(2048);
//                                    isWifiConnected(null);
//                                }
//                                
//                            } else {
//                                connectionCreated = false;
//                                connecting = false;
//                                dataOutputStream.close();
//                                inputStream.close();
//                                clientSocket.close();
//                                Thread.sleep(3000);
//                                sBuffer = new StringBuffer(2048);
//                                isWifiConnected(null);
//                            }
//                            
//                        }
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                        connectionCreated = false;
//                        connecting = false;
//                        try {
//                            dataOutputStream.close();
//                            inputStream.close();
//                            clientSocket.close();
//                            Thread.sleep(3000);
//                            sBuffer = new StringBuffer(2048);
//                            // // TODO: 1/13/2017 for time being we are passing null here after we wil remove this
//                            isWifiConnected(null);
//                        } catch (Exception e1) {
//                            e.printStackTrace();
//                            
//                        }
//                    }
//                } catch (Exception e) {
//                    Log.i("socketcreateprob", e.toString());
//                    connectionCreated = false;
//                    connecting = false;
//                    try {
//                        dataOutputStream.close();
//                        inputStream.close();
//                        clientSocket.close();
//                        Thread.sleep(3000);
//                        sBuffer = new StringBuffer(2048);
//                        // // TODO: 1/13/2017 for time being we are passing null here after we wil remove this
//                        isWifiConnected(null);
//                    } catch (Exception e1) {
//                        e.printStackTrace();
//                        
//                        
//                    }
//                }
//            }
//        }).start();
//    } catch (Exception e) {
//        e.printStackTrace();
//        connectionCreated = false;
//        connecting = false;
//        try {
//            dataOutputStream.close();
//            inputStream.close();
//            clientSocket.close();
//            Thread.sleep(3000);
//            sBuffer = new StringBuffer(2048);
//            // // TODO: 1/13/2017 for time being we are passing null here after we wil remove this
//            isWifiConnected(null);
//        } catch (Exception e1) {
//            e.printStackTrace();
//            
//            
//        }
//    }
//}
