//
//  Constants.h
//  Homesmart
//
//  Created by Neotech on 25/04/16.
//  Copyright © 2016 Neotech. All rights reserved.
//

#ifndef Constants_h
#define Constants_h

#define PLIST @"plist"
enum {
    BLUE_THEME,
    BLACK_THEME
};
typedef NSInteger ThemeName;

enum {
    CURTAIN_TYPE,
    LGHVAC,
    LUTRON_LIGHT,
    DO_TYPE,
    THIRD_PARTY
};
typedef NSInteger ThemeName;


enum {
    PROFILE_IMMEDIATE,
    PROFILE_SCHEDULE,
    PROFILE_DELAY
};
typedef NSInteger ProfileType;


extern int const HTTP_RESPONSE_SUCCESS;

#define REGEX_EMAIL @"[A-Z0-9a-z._%+-]{3,}+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
#define DIGITAL_INPUT   @"DIGITAL INPUT"
#define DIGITAL_OUTPUT  @"DIGITAL OUTPUT"
#define ANALOG_OUTPUT   @"ANALOG OUTPUT"
#define SOFT_OUTPUT     @"SOFT OUTPUT"
#define LATEST_HEX_FILE_VERSION   @"11.00.24"

#define DO_D_TYPE @"digitalOutput"
#define AO_D_TYPE @"analogeOutput"
#define SO_D_TYPE @"softOutput"
#define SI_D_TYPE @"softIntput"
#define DI_D_TYPE @"digitalInput"
#endif /* Constants_h */
