//
//  CustomView.m
//  Homesmart
//
//  Created by Neotech on 11/11/16.
//  Copyright © 2016 Neotech. All rights reserved.
//

#import "CustomView.h"
#import "ThemeManager.h"
@implementation CustomView
-(void)awakeFromNib {
    self.backgroundColor = [[ThemeManager sharedManger] colorFromKey:BACKGROUND_COLOR];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
