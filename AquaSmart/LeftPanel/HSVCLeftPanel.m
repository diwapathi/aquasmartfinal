//
//  HSVCLeftPanel.m
//  Homesmart
//
//  Created by Neotech on 26/04/16.
//  Copyright © 2016 Neotech. All rights reserved.
//

#import "HSVCLeftPanel.h"
#import "SettingsVC.h"
#import "MenuCell.h"
#import "MVYSideMenuController.h"
#import "MainViewController.h"
#import "ForgatePassword.h"
#import "ThemeManager.h"
#import "BeginScheduleVC.h"
#import "SystemNetworkVC.h"
#import "AboutVC.h"
#import "WMSSettingsController.h"
#import "UIView+Toast.h"
#import "FirmwareUpgradeVC.h"
@interface HSVCLeftPanel ()<PacketReciveProtocol>
@property (weak, nonatomic) IBOutlet UIImageView *profilePic;
@property (weak, nonatomic) IBOutlet UITableView *menuTableView;
@property (weak, nonatomic) IBOutlet UILabel *userNameLbl;
@property (weak, nonatomic) IBOutlet UILabel *userAddressLabel;
@property(nonatomic,strong)NSArray *menutitleArray;
@property(nonatomic,strong)NSArray *menuImageArray;
@property (strong, nonatomic) IBOutlet UIView *mainView;
@property (weak, nonatomic) IBOutlet UIView *seperatorView;
@end

@implementation HSVCLeftPanel
{
    NSTimer *timer;
}
- (void)viewDidLoad {
    
    
    [self.popupView setHidden:YES];
    [super viewDidLoad];
    [self setInitialConfiguration];
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(closetoggled) name:@"closetoggle" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(opentoggled) name:@"opentoggle" object:nil];
//     [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(adduserDetails) name:@"addUserDetail" object:nil];
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:YES];
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}

//-(void)adduserDetails {
//    NSString *userName = [[WMSSettingsController sharedInstance]getAppUserName];
//    NSString *userAddress = [[WMSSettingsController sharedInstance]getAppAddress];
//    UIImage *image = [[WMSSettingsController sharedInstance]getUserProfileImage];
//    self.userNameLbl.text = userName?userName:@"";
//    self.userAddressLabel.text = userAddress?userAddress:@"";
//    self.profilePic.image = image?image:[UIImage imageNamed:@"ApplicationIcon"];
//}

-(void)closetoggled {
    
   // [self setThemeData];
    //[self.menuTableView reloadData];
}

-(void)opentoggled {
 //  [self setThemeData];
 //  [self.menuTableView reloadData];
}

-(void)setInitialConfiguration {
    
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(endEditing)];
    [tapRecognizer setNumberOfTapsRequired:1];
    tapRecognizer.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tapRecognizer];
    self.menutitleArray = @[NSLocalizedString(@"Settings", nil),NSLocalizedString(@"Pumping Schedule", nil),NSLocalizedString(@"Forgot Password", nil),NSLocalizedString(@"Network Settings", nil),NSLocalizedString(@"About", nil)];
    
    self.menuImageArray = @[NSLocalizedString(@"icons8-Settings Filled-50.png", nil),NSLocalizedString(@"icons8-Clock-50.png", nil),
                            NSLocalizedString(@"icons8-Key-50.png", nil),NSLocalizedString(@"icons8-Wi-Fi Filled-50.png", nil),
                            NSLocalizedString(@"icons8-Info-50.png", nil)];
    //[self setThemeData];
}

//-(void)setThemeData {
//    self.mainView.backgroundColor =  [[ThemeManager sharedManger]colorFromKey:LEFT_PANEL_BGCOLOR];
//    self.seperatorView.backgroundColor =  [[ThemeManager sharedManger]colorFromKey:LEFT_PANEL_LABEL_COLOR];
//    self.menuImageArray = @[[[ThemeManager sharedManger]getImageNameForKey:SETTING_UNSELECTED],[[ThemeManager sharedManger]getImageNameForKey:SYSTEM_HEALTH_UNSEL],[[ThemeManager sharedManger]getImageNameForKey:WALKTHROUGH_UNSELECTED],[[ThemeManager sharedManger]getImageNameForKey:ABOUT_UNSELECTED]];
////    self.profilePic.layer.cornerRadius = self.profilePic.frame.size.width/2;
////    self.profilePic.clipsToBounds = YES;
//}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)endEditing {
    [self.view endEditing:YES];
}
#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 5;
}

- (void)tableView:(UITableView *)tableView
  willDisplayCell:(UITableViewCell *)cell
forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [cell setBackgroundColor:[UIColor clearColor]];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Menucell";
    MenuCell *cell = (MenuCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[MenuCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    [cell setSelectionStyle:UITableViewCellSelectionStyleDefault];
    [cell setData:[self.menutitleArray objectAtIndex:indexPath.row] andImage:[self.menuImageArray objectAtIndex:indexPath.row]];
    
    return cell;

}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    UIStoryboard *mainStroyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    if (indexPath.row == 0) {
        [[NSUserDefaults standardUserDefaults] setBool:TRUE forKey:@"SETTINGS"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        //[self.popupView setHidden:NO];
//        SettingsVC *mainVC = [mainStroyBoard instantiateViewControllerWithIdentifier:@"SettingsVC"];
//        mainVC.isFromLeftPanel = YES;
//        settingsVC.isFromLeftPanel = YES;
//       UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:mainVC];
//           [[self sideMenuController] changeContentViewController:navigationController closeMenu:YES];

        [[self sideMenuController] closeMenu];
       

    }
    if (indexPath.row == 1) {
        BeginScheduleVC *beginVC = [mainStroyBoard instantiateViewControllerWithIdentifier:@"BeginScheduleVC"];
        beginVC.isFromLeftPanel = YES;
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:beginVC];
        [[self sideMenuController] changeContentViewController:navigationController closeMenu:YES];
        
    }
    
    if (indexPath.row == 2) {
        ForgatePassword *forgotVC = [mainStroyBoard instantiateViewControllerWithIdentifier:@"ForgatePassword"];
        forgotVC.isFromLeftPanel = YES;
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:forgotVC];
        [[self sideMenuController] changeContentViewController:navigationController closeMenu:YES];
        
    }
    
     if (indexPath.row == 3)
    {
        
        SystemNetworkVC *systemNetwork = [mainStroyBoard instantiateViewControllerWithIdentifier:@"SystemNetworkVC"];
        systemNetwork.isFromLeftPanel = YES;
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:systemNetwork];
        [[self sideMenuController] changeContentViewController:navigationController closeMenu:YES];
    }
    
    
   
    if (indexPath.row == 4) {
        AboutVC *aboutVC = [mainStroyBoard instantiateViewControllerWithIdentifier:@"AboutVC"];
        aboutVC.isFromLeftPanel = YES;
        UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:aboutVC];
        [[self sideMenuController] changeContentViewController:navigationController closeMenu:YES];
    }
}




/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
