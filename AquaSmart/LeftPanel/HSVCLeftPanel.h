//
//  HSVCLeftPanel.h
//  Homesmart
//
//  Created by Neotech on 26/04/16.
//  Copyright © 2016 Neotech. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TcpPacketClass.h"
@interface HSVCLeftPanel : UIViewController
@property (weak, nonatomic) IBOutlet UIView *passwordView;
@property (weak, nonatomic) IBOutlet UITextField *passwordText;
@property (weak, nonatomic) IBOutlet UIView *popupView;
@end
